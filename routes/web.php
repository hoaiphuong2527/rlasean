<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|5
*/
//admin
Route::post('orders/create/add-product','Order\ApiController@addProduct');
Route::post('orders/create/update-amount-product','Order\ApiController@updateAmountProduct');
Route::post('orders/create/update-session','Order\ApiController@updateSession');
Route::get('orders/backup-session','Order\ApiController@backupSession');

Route::group(['prefix' => 'admin','middleware' => ['auth','admin_login']], function () {

    Route::resource('/','Admin\HomeController')->names('admin_dashboard');
    Route::get('search','Admin\HomeController@search')->name('admin_search_product');
    Route::resource('posts','Post\AdminController')->names('admin_post');
    Route::post('posts/update-status/{id}','Post\AdminController@updateStatusPost');
    Route::post('posts/{post_slug}/clone', 'Post\AdminController@clone')->name('admin_post_clone.store');
    Route::get('posts/{id}/update-lang', 'Post\AdminController@editTextLang')->name('admin_post.edit_lang');
    Route::put('posts/{id}/update-lang', 'Post\AdminController@updateLang')->name('admin_post.update_lang');
    Route::resource('slideshow', 'Admin\SlideshowController')->names('admin_slideshow');
    Route::post('slideshows/slideshow-update-status/{id}','Admin\SlideshowController@updateStatusByAjax');
    Route::resource('static-page','Pages\AdminController')->names('admin_page');
    Route::post('static-page/page-update-status/{id}','Pages\AdminController@updateStatusByAjax');
    Route::get('static-page/{id}/update-lang', 'Pages\AdminController@editTextLang')->name('admin_page.edit_lang');
    Route::put('static-page/{id}/update-lang', 'Pages\AdminController@updateLang')->name('admin_page.update_lang');
    Route::get('banners', 'Admin\BannerController@index')->name('admin_banners.index');
    Route::post('banners','Admin\BannerController@update')->name('admin_banners.update');
    Route::resource('partner','Partner\AdminController')->names('admin_partners');
    Route::resource('warranty','Warranty\AdminController')->names('admin_warranties');
    Route::resource('news-letters','NewsLetter\AdminController')->names('admin_news');
    Route::middleware('can:CAE')->group(function () {
        Route::resource('categories', 'Category\AdminController')->names('admin_category');
        Route::post('categories/update-status/{id}','Category\AdminController@updateStatusCategory');
        Route::resource('products', 'Products\Product\AdminController')->names('admin_product');
        Route::post('products/{product_slug}/clone', 'Products\Product\AdminController@clone')->name('admin_product_clone.store');
        Route::post('products/update-status/{id}','Products\Product\AdminController@updateStatusProduct')->name('admin_update_status');
        Route::post('products/sort_feature/{id}','Products\Product\AdminController@sortFeature')->name('admin_product.sort_feature');
        Route::resource('products/{product_slug}/variants', 'Products\Variant\AdminController')->names('admin_product_variant');
        Route::post('products/is-feature/{id}','Products\Product\AdminController@isFeature')->name('admin_products.featured');
        Route::post('product-variants/position/{id}','Products\Variant\AdminController@sortPosition')->name('admin_product_variant.sort_position');
        Route::get('products/{id}/update-lang', 'Products\Product\AdminController@editTextLang')->name('admin_product.edit_lang');
        Route::put('products/{id}/update-lang', 'Products\Product\AdminController@updateLang')->name('admin_product.update_lang');
    	Route::resource('informations','Information\AdminController')->names('admin_information');
	    Route::post('variants/update-status/{id}','Products\Variant\AdminController@updateStatusVariant');
        Route::resource('contacts', 'Contacts\AdminController')->names('admin_contacts');
        Route::post('delete-multi','Contacts\AdminController@deleteMutile')->name('admin_contacts.deleteMulti');
	    Route::resource('options','Admin\OptionController')->names('admin_option');
        Route::resource('codes','Code\AdminController')->names('admin_code');
        Route::resource('menu', 'DynamicMenu\AdminController')->names('admin_menu');
        Route::post('codes/update-status/{id}','Code\AdminController@updateStatus');
        Route::resource('productcodes','Code\PromotionProductController')->names('admin_productcode');
        Route::resource('orders','Order\AdminController')->names('admin_order');
        Route::resource('reviews', 'Review\AdminController')->names('admin_reviews');
        Route::post('review-update-status/{id}','Review\AdminController@updateStatusByAjax');
        Route::resource('histories','Products\Warehouse\AdminController')->names('admin_warehouse');
        Route::resource('warehouse','Products\History\AdminController')->names('admin_history');        
        Route::get('/lang-file/{file_name}/{locale}','Admin\LangController@index')->name('admin_lang_index');
        Route::post('/lang-file/{file_name}/{locale}', 'Admin\LangController@update')->name('save_lang');

        Route::get('/media', 'Admin\MediaController@index')->name('admin_media_index');

    });
    Route::middleware('can:controller_admin')->group(function () {
        Route::resource('orders','Order\AdminController')->names('admin_order');
        Route::resource('options','Admin\OptionController')->names('admin_option');
        Route::post('change-status-payment','Admin\OptionController@statusPayment');
        // Route::get('excel_export','Order\AdminController@export')->name('admin_order.export');
    });
    Route::middleware(['can:controller'])->group(function () {
        Route::resource('users', 'Admin\UserController')->names('admin_user');
        Route::post('users/update-flag-true/{id}','Admin\UserController@updateFlag')->name('admin_user.update_flag_true');
    });
});

Auth::routes();
//guest
Route::group(['middleware' => 'localization'], function () {

    /********************************TEST LANG*****************************/
    Route::get('locale','HomeController@setLocale')->name('set_lang');
    $locale = Cookie::get("lang");
    if(!$locale) $locale = 'vi';
    app()->setLocale($locale);
    // dd(($locale));
    /********************************END*****************************/
    Route::post('/set-intended-url', 'Auth\LoginController@setIntendedUrl');
    Route::get('activate_account/{id}/{token}','Auth\ActivateAccountController@activate')->name('activate_account');
    Route::get('auth/{provider}', 'Auth\SocialiteController@redirectToGoogle');
    Route::get('auth/{provider}/callback', 'Auth\SocialiteController@handleGoogleCallback');
    Route::post('add-product-into-cart','Cart\ApiController@addProduct');
    Route::post('add-product-into-cart-in-single-view','Cart\ApiController@addProductInSingleView');
    Route::post('add-product-into-cart/increment','Cart\ApiController@increment');
    Route::post('add-product-into-cart/decrease','Cart\ApiController@decrease');
    Route::post('remove-product','Cart\ApiController@remove');
    Route::post('destroy','Cart\ApiController@destroy');
    Route::get('get-cart','Cart\ApiController@getCart');
    Route::post('update-amount','Cart\ApiController@updateAmount');
    //update route khong su dun resource
    Route::resource(trans('route_text.routes.order'),'Order\GuestController')->names('guest_order.'.$locale);
    Route::get(trans('route_text.routes.order_successfully').'/{order_number}','Order\GuestController@thanksBooking')->name('guest_order.success');
    Route::get(trans('route_text.routes.posts'),'Post\GuestController@index')->name('guest_posts.index.'.$locale);
    Route::get(trans('route_text.routes.get_post_by_category').'/{id}/{slug}','Post\GuestController@category')->name('guest_posts.category.'.$locale);
    Route::get(trans('route_text.routes.posts').'/{id}/{slug}','Post\GuestController@show')->name('guest_posts.show.'.$locale);
    Route::get(trans('route_text.routes.category').'/{id}/{slug}','Products\Product\GuestController@getProductByCategory')->name('guest_products.category.'.$locale);
    Route::get(trans('route_text.routes.product_detail').'/{id}/{slug}','Products\Product\GuestController@show')->name('guest_products.show.'.$locale);
    Route::get('/','Category\GuestController@index')->name('guest_home.index.'.$locale);
    Route::get(trans('route_text.routes.categories'),'Category\GuestController@getAllCategoryOnline')->name('guest_all_categories.index.'.$locale);
    Route::get(trans('route_text.routes.contact'),'Contacts\GuestController@index')->name('guest.contact.'.$locale);
    Route::get(trans('route_text.routes.corporate_gift'),'Contacts\GuestController@corporateGift')->name('guest.corporate_gift.'.$locale);
    Route::get(trans('route_text.routes.warranty'),'Warranty\GuestController@index')->name('guest_warranties.index.'.$locale);
    Route::post(trans('route_text.routes.warranty'),'Warranty\GuestController@store')->name('guest_warranties.store.'.$locale);
    Route::get(trans('route_text.routes.product_promotion'),'HomeController@getPromotionProduct')->name('product_promotion.index.'.$locale);
    Route::get(trans('route_text.routes.new_product'),'HomeController@getNewProduct')->name('new_product.index.'.$locale);

    /********************************WORKING*****************************/
    //khong su dung resource trong guest
    Route::get(trans('route_text.routes.search'),'HomeController@search')->name('guest_search.index.'.$locale);
    Route::get(trans('route_text.routes.partners'), 'Partner\GuestController@index')->name('guest_partners.index.'.$locale);
    Route::post('products-review/{id}/{type}','Review\UserController@review')->name('guest_products.review');
    /********************************END*****************************/

    Route::post('categories/{id}', 'Products\Product\GuestController@getKeyProduct')->name('guest_products');
    Route::post('news-letters','NewsLetter\GuestController@store')->name('guest_news.store');
    Route::post('/contact','Contacts\GuestController@store')->name('guest.post_contact');
    //manageorders
    // Route::get('/manageorders','ManageOrders\ManageOrdersController@index')->name('guest.manageorders');
    // Route::get('/manageorders/view',function(){
    //     return view('manageorders\guest\ordernumber');
    // })->name('guest.ordernumber');
    Route::get('user/{token}/change-password/{active}', 'ProfileController@changePassword')->name('user_profiles.check');
    Route::group(['prefix' => 'user/{token}','middleware' => ['auth','can:can-see,token']], function () {
        Route::get('locale','HomeController@setLocale')->name('set_lang');
        $locale = Cookie::get("lang");
        if(!$locale) $locale = 'vi';
        app()->setLocale($locale);
        Route::resource('/your-profile', 'ProfileController')->names('user_profiles');
        Route::resource(trans('route_text.routes.manage_orders'), 'Order\UserController')->names('user_order');
        //resource các route của nó lần lượt là 'user_order.index' , 'user_order.create' ...... 
        //nếu em đặt 1 route mới 'user_order.index' như thế này thì nó bị trùng tên rồi. nên nó sẽ bỏ qua cái function index trong Order\UserController
        //em đã định n''ghĩ resource rồi thì không cần viết 
        // Route::get('','Order\UserController@index') hay Route::post('','Order\UserController@store') Right!
        // Route::resource(trans('route_text.routes.manage_orders').'/{order_number_para}','Order\UserController');
        
    });
    // luon luon nam cuoi cung
    Route::get('/{slug}','StaticPageController@index')->name('guest.view_static_page');
    
});
