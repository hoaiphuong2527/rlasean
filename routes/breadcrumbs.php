<?php
use App\Models\Post;
use App\Models\Products\Product;
use App\Models\Products\ProductVariant;
use App\Models\Categories\ProductCategory;
use App\Models\Categories\PostCategory;
use App\Models\Option;
use App\Models\Slideshow;
use App\Models\Contact;
use App\Models\Categories\Category;

// Home
Breadcrumbs::for('guest_home.index.'.app()->getLocale(), function($trail)
{
    $trail->push(trans("route_text.breadcrumbs.home"), route('guest_home.index.'.app()->getLocale()));
});

Breadcrumbs::for('guest.view_static_page', function($trail,$slug)
{
    $post = Post::getItemBySlug($slug);
    $trail->parent('guest_home.index.'.app()->getLocale());
    $trail->push($post->getAttributeLang('name'));
});

Breadcrumbs::for('guest_all_categories.index.'.app()->getLocale(), function($trail)
{
    $category = Category::find(1);
    $trail->parent('guest_home.index.'.app()->getLocale());
    $trail->push($category->getAttributeLang('name'),route('guest_all_categories.index.'.app()->getLocale()));
});

Breadcrumbs::for('guest_posts.index.'.app()->getLocale(), function($trail)
{
    $trail->parent('guest_home.index.'.app()->getLocale());
    $trail->push(trans('page_text.post.page_title'),route('guest_posts.index.'.app()->getLocale()));
});

Breadcrumbs::for('guest_products.category.'.app()->getLocale(), function($trail,$slug)
{
    if(isset($slug['slug']))
        $category = Category::getItemBySlug($slug['slug']);
    else $category = Category::getItemBySlug($slug);
    if ($category->parent_id != 0) {
        $arr = [
            'id' => $category->parent_id,
            'slug' => $category->parent->getAttributeLang('slug')
        ];
        $trail->parent('guest_products.category.'.app()->getLocale(),$arr);
    } else {
        $trail->parent('guest_home.index.'.app()->getLocale());
    }
    $trail->push($category->getAttributeLang('name'),route('guest_products.category.'.app()->getLocale(),[$category->id,$category->getAttributeLang('slug')]));
});

Breadcrumbs::for('guest_products.show.'.app()->getLocale(), function($trail,$slug)
{
    if(isset($slug['slug']))
        $product = Product::getItemBySlug($slug['slug']);
    else $product = Product::getItemBySlug($slug);
    $trail->parent('guest_products.category.'.app()->getLocale(),$product->category->slug);
    $trail->push($product->getAttributeLang('name'));
});

Breadcrumbs::for('guest_warranties.index.'.app()->getLocale(), function($trail)
{
    $trail->parent('guest_home.index.'.app()->getLocale());
    $trail->push(trans('page_text.warrant.page_title'));
});

Breadcrumbs::for('guest.contact.'.app()->getLocale(), function($trail)
{
    $trail->parent('guest_home.index.'.app()->getLocale());
    $trail->push(trans('page_text.contact.page_title'));
});

Breadcrumbs::for('guest_partners.index.'.app()->getLocale(), function($trail)
{
    $trail->parent('guest_home.index.'.app()->getLocale());
    $trail->push(trans('page_text.partner.page_title'));
});
// manageorders
 Breadcrumbs::for('user_order'.app()->getLocale(), function ($trail) {
    $trail->parent('guest_home.index.'.app()->getLocale());
  $trail->push(trans('page_text.manageorders.page_title'));
});
Breadcrumbs::for('guest_posts.category.'.app()->getLocale(), function($trail,$slug)
{
    if(isset($slug['slug']))
        $category = Category::getItemBySlug($slug['slug']);
    else $category = Category::find($slug);
    if($category->parent_id == 2)
        $trail->parent('guest_posts.index.'.app()->getLocale());
    else {
        $trail->parent('guest_posts.category.'.app()->getLocale(),[$category->parent->id,$category->parent->getAttributeLang('slug')]);
    }
    $trail->push($category->getAttributeLang('name'),route('guest_posts.category.'.app()->getLocale(),[$category->id,$category->getAttributeLang('slug')]));
});

Breadcrumbs::for('guest_posts.show.'.app()->getLocale(), function($trail,$slug)
{
    if(isset($slug['slug']))
        $post = Post::getItemBySlug($slug['slug']);
    else $post = Post::getItemBySlug($slug);
    if($post->category_id == PostCategory::firstCateChild()->id){
        $trail->parent('guest_posts.index.'.app()->getLocale());
    }else
        $trail->parent('guest_posts.category.'.app()->getLocale(),$post->post_category->id,$post->post_category->getAttributeLang('slug'));
    $trail->push($post->getAttributeLang('name'));
});

//Back end

Breadcrumbs::for('admin_dashboard.index', function($trail)
{
    $trail->push('Tổng quan', route('admin_dashboard.index'));
});

Breadcrumbs::for('admin_user.index', function($trail)
{
    $trail->parent('admin_dashboard.index');
    $trail->push('QL Người dùng', route('admin_user.index'));
});

Breadcrumbs::for('admin_user.create', function($trail)
{
    $trail->parent('admin_user.index');
    $trail->push('Chỉnh sửa thông tin người dùng', route('admin_user.create'));
});

Breadcrumbs::for('admin_user.edit', function($trail,$id)
{
    $trail->parent('admin_user.index');
    $trail->push('Chỉnh sửa thông tin người dùng', route('admin_user.edit',$id));
});

Breadcrumbs::for('admin_category.index', function($trail)
{
    $trail->parent('admin_dashboard.index');
    $trail->push('QL Danh mục', url()->previous());
});

Breadcrumbs::for('admin_category.edit', function($trail,$id)
{
    $trail->parent('admin_category.index');
    $trail->push('Chỉnh sửa thông tin', route('admin_category.edit',$id));
});

Breadcrumbs::for('admin_product.index', function($trail)
{
    $trail->parent('admin_dashboard.index');
    $trail->push('QL Sản phẩm', route('admin_product.index'));
});

Breadcrumbs::for('admin_search_product', function($trail)
{
    $trail->parent('admin_dashboard.index');
    $trail->push('Kết quả tìm kiếm', route('admin_search_product'));
});

Breadcrumbs::for('admin_product.edit', function($trail,$id)
{
    $trail->parent('admin_product.index');
    $trail->push('Thông tin ' . Product::find($id)->name, route('admin_product.edit',$id));
});

Breadcrumbs::for('admin_product_variant.edit', function($trail,$product_slug,$id)
{
    $product = Product::whereSlug($product_slug)->firstOrFail();
    $variant = ProductVariant::findOrFail($id);
    $trail->parent('admin_product.edit',$product->id);
    $trail->push('Chỉnh sửa thông tin biến thể');
});

Breadcrumbs::for('admin_page.index', function($trail)
{
    $trail->parent('admin_dashboard.index');
    $trail->push('QL Trang tĩnh', route('admin_page.index'));
});

Breadcrumbs::for('admin_page.edit', function($trail,$id)
{
    $trail->parent('admin_page.index');
    $trail->push('Chỉnh sửa thông tin', route('admin_page.edit',$id));
});


Breadcrumbs::for('admin_order.index', function($trail)
{
    $trail->parent('admin_dashboard.index');
    $trail->push('QL Đơn hàng', route('admin_order.index'));
});

Breadcrumbs::for('admin_order.edit', function($trail,$id)
{
    $trail->parent('admin_order.index');
    $trail->push('Chỉnh sửa thông tin', route('admin_order.edit',$id));
});

Breadcrumbs::for('admin_order.create', function($trail)
{
    $trail->parent('admin_order.index');
    $trail->push('Chỉnh sửa thông tin', route('admin_order.create'));
});

Breadcrumbs::for('admin_post.index', function ($trail) {
    $trail->parent('admin_dashboard.index');
    $trail->push('QL Bài viết', route('admin_post.index'));
});

Breadcrumbs::for('admin_post.edit', function ($trail,$id) {
    $trail->parent('admin_post.index');
    $trail->push('Chỉnh sửa thông tin '. Post::find($id)->name , route('admin_post.edit',$id));
});

Breadcrumbs::for('admin_option.index', function ($trail) {
    $trail->parent('admin_dashboard.index');
    $trail->push('QL Option', route('admin_option.index'));
});

Breadcrumbs::for('admin_option.edit', function ($trail,$id) {
    $trail->parent('admin_option.index');
    $trail->push('Chỉnh sửa thông tin', route('admin_option.edit', $id));
});
Breadcrumbs::for('admin_slideshow.index', function ($trail) {
    $trail->parent('admin_dashboard.index');
    $trail->push('QL Sildeshow', route('admin_slideshow.index'));
});

Breadcrumbs::for('admin_slideshow.create', function ($trail) {
    $trail->parent('admin_slideshow.index');
    $trail->push('Thêm mới hình ảnh', route('admin_slideshow.create'));
});

Breadcrumbs::for('admin_slideshow.edit', function ($trail,$id) {
    $trail->parent('admin_slideshow.index');
    $trail->push('Chỉnh sửa thông tin', route('admin_slideshow.edit',$id));
});

Breadcrumbs::for('admin_contacts.index', function ($trail) {
    $trail->parent('admin_dashboard.index');
    $trail->push('Liên hệ', route('admin_contacts.index'));
});

Breadcrumbs::for('admin_contacts.edit', function ($trail,$id) {
    $trail->parent('admin_contacts.index');
    $trail->push('Phản hồi đến '.Contact::find($id)->name, route('admin_contacts.edit',$id));
});

Breadcrumbs::for('admin_banners.index', function ($trail) {
    $trail->parent('admin_dashboard.index');
    $trail->push('QL Banner');
});

Breadcrumbs::for('admin_code.index', function ($trail) {
    $trail->parent('admin_dashboard.index');
    $trail->push('QL Code', route('admin_code.index'));
});

Breadcrumbs::for('admin_code.edit', function ($trail,$id) {
    $trail->parent('admin_code.index');
    $trail->push('Chỉnh sửa thông tin', route('admin_code.edit', $id));
});

Breadcrumbs::for('admin_productcode.index', function ($trail) {
    $trail->parent('admin_dashboard.index');
    $trail->push('QL Sản Phảm khuyến mãi', route('admin_productcode.index'));
});

Breadcrumbs::for('admin_productcode.edit', function ($trail, $id) {
    $trail->parent('admin_productcode.index');
    $trail->push('Chỉnh sửa thông tin', route('admin_productcode.edit', $id));
});

Breadcrumbs::for('admin_lang_index', function ($trail, $id) {
    $trail->parent('admin_dashboard.index');
    $trail->push('Cập nhật ngôn ngữ');
});

Breadcrumbs::register('admin_history.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin_dashboard.index');
    $breadcrumbs->push('Kho hàng',route('admin_history.index'));
});

Breadcrumbs::register('admin_warehouse.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin_history.index');
    $breadcrumbs->push('Lịch sử nhập hàng', route('admin_warehouse.index'));
});

Breadcrumbs::register('admin_warehouse.edit', function ($breadcrumbs,$id) {
    $breadcrumbs->parent('admin_history.index');
    $breadcrumbs->push('Chỉnh sửa thông tin');
});

Breadcrumbs::for('admin_partners.index', function ($trail) {
    $trail->parent('admin_dashboard.index');
    $trail->push('Danh sách đối tác', route('admin_partners.index'));
});

Breadcrumbs::for('admin_partners.edit', function ($trail,$id) {
    $trail->parent('admin_partners.index');
    $trail->push('Chỉnh sửa thông tin');
});

Breadcrumbs::for('admin_warranties.index', function ($trail) {
    $trail->parent('admin_dashboard.index');
    $trail->push('Liên hệ Bảo Hành', route('admin_warranties.index'));
});

Breadcrumbs::for('admin_news.index', function ($trail) {
    $trail->parent('admin_dashboard.index');
    $trail->push('DS đăng ký nhận tin tức', route('admin_news.index'));
});

Breadcrumbs::for('admin_reviews.index', function ($trail) {
    $trail->parent('admin_dashboard.index');
    $trail->push('Review sản phẩm', route('admin_reviews.index'));
});
Breadcrumbs::for('admin_product.edit_lang', function ($trail, $id) {
    $trail->parent('admin_product.edit',$id);
    $trail->push('Cập nhật ngôn ngữ');
});
Breadcrumbs::for('admin_post.edit_lang', function ($trail, $id) {
    $trail->parent('admin_post.edit',$id);
    $trail->push('Cập nhật ngôn ngữ');
});
Breadcrumbs::for('admin_page.edit_lang', function ($trail, $id) {
    $trail->parent('admin_page.edit',$id);
    $trail->push('Cập nhật ngôn ngữ');
});

Breadcrumbs::for('admin_media_index', function ($trail) {
    $trail->parent('admin_dashboard.index');
    $trail->push('Thư viện ảnh');
});


Breadcrumbs::register('admin_information.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin_dashboard.index');
    $breadcrumbs->push('QL Thông số sản phẩm', route('admin_information.index'));
});

Breadcrumbs::register('admin_information.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin_information.index');
    $breadcrumbs->push('Chỉnh sửa thông tin', route('admin_information.edit', $id));
});

Breadcrumbs::register('admin_menu.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin_option.index');
    $breadcrumbs->push('QL Menu website', route('admin_menu.index'));
});

