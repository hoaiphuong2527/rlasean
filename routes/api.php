<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(array('middleware' => ['auth:api']), function() {
    Route::post('categories/{category_id}/informations','Information\ApiController@store');
    Route::post('categories/{category_id}/informations-save','Information\ApiController@save');
    Route::post('categories/{category_id}/informations/{id}','Information\ApiController@destroy');
    Route::get('media', 'Admin\MediaController@getAll');
    Route::post('upload-image', 'Api\ImageUploadController@store');
    Route::put('media/{id}', 'Admin\MediaController@update');
    Route::post('media/{id}', 'Admin\MediaController@destroy');
    Route::post('categories/{category_id}/informations','Information\ApiController@store');
    Route::post('categories/{category_id}/informations-delete','Information\ApiController@destroyGroup');
    Route::put('categories/{category_id}/informations/{id}','Information\ApiController@update');
    Route::post('codes/validate','Code\ApiController@validateCode');
    
    Route::post('menu/sorting','DynamicMenu\ApiController@sorting')->name('admin_menu.sorting');
    Route::post('slideshows/sorting','Admin\SlideshowController@sorting')->name('admin_slideshow.sorting');
    // Route::post('codes/search-products','Code\ApiController@searchProductByNameInvalid');
    Route::post('products/check-amount','Products\Product\ApiController@checkAmountProduct');
    Route::get('order-chart','Admin\HomeController@getValueChart');
    Route::post('options/save/district-id/{id}','Admin\OptionController@saveDistrictID');
});
// Media API
// Route::get('media', 'Admin\MediaController@getAll');
// Route::put('media/{id}', 'Admin\MediaController@update');
// Route::delete('media/{id}', 'Admin\MediaController@destroy');
Route::post('del-photo-variant/{id}','Products\Variant\ApiController@deletePhoto');
Route::post('upload-images/{id}','Products\Variant\ApiController@uploadImage')->name('admin_product_variant.upload_image');
Route::get('single-product/{id}','Products\Product\ApiController@show');
Route::post('codes/checkInvalid', 'Code\ApiController@checkCode');
Route::get('get-variant/{id}','Products\Product\ApiController@getVariant');
Route::get('check-auth','Auth\CheckAuthController@checkAuth');
Route::post('check-email','Code\ApiController@checkEmail');
Route::post('check-amount-variant/{order_id}','Order\ApiController@checkAmountVariant');
Route::get('search-product','Products\Variant\ApiController@search');

Route::post('categories/products/filter','Category\ApiController@filterProducts');
Route::get('categories/filter','Category\ApiController@getProductByCategoryId');
Route::get('categories/{id}/related','Category\ApiController@getCategoriesRelated');
Route::get('categories/{id}/informations','Category\ApiController@getInformations');
Route::get('categories/{category_id}/filter-products','Category\ApiController@filterProduct');
Route::post('variants/search','Products\Variant\ApiController@search');
Route::post('codes/search-products','Code\ApiController@searchProductByNameInvalid');
// Route::post('products/check-amount','Products\Product\ApiController@checkAmountProduct');
Route::post('codes/search-promotions','Code\ApiController@searchPromotion');
Route::post('calculator-ghtk-shipping-cost','Order\ApiController@calculatorGhtkShippingCost');
// tao info trong cate
// Route::post('categories/{category_id}/informations','Information\ApiController@store');
// Route::put('categories/{category_id}/informations/{id}','Information\ApiController@update');
// Route::delete('categories/{category_id}/informations/{id}','Information\ApiController@destroy');
// Route::post('codes/validate','Code\ApiController@validateCode');

// Route::post('menu/sorting','DynamicMenu\ApiController@sorting')->name('admin_menu.sorting');
// Route::post('slideshows/sorting','Admin\SlideshowController@sorting')->name('admin_slideshow.sorting');
