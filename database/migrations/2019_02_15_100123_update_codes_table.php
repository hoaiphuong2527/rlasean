<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('codes', function (Blueprint $table) {
            $table->datetime('begin_day')->nullable()->change();
            $table->datetime('end_day')->nullable()->change();
            $table->string('status')->default('D');
            $table->string('title')->nullable();
            $table->string('apply')->nullable();
            $table->integer('is_over')->default(0);
            $table->bigInteger('value_above')->nullable();
            $table->integer('amount_ordered')->default(0);
        });
        Schema::table('product_code', function (Blueprint $table) {
            $table->dropForeign(['product_id']);            
            $table->dropColumn('product_id');
            $table->integer('objecttable_id')->nullable();
            $table->string('objecttable_type')->nullable();
            $table->integer('amount_default')->nullable();
            $table->integer('amount_ordered')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('codes', function (Blueprint $table) {
            $table->date('begin_day')->nullable()->change();
            $table->date('end_day')->nullable()->change();
            $table->dropColumn('status');
            $table->dropColumn('title');
            $table->dropColumn('apply');
            $table->dropColumn('is_over');
            $table->dropColumn('value_above');
            $table->dropColumn('amount_ordered');
        });

        Schema::table('product_code', function (Blueprint $table) {
            $table->unsignedInteger('product_id')->nullable();
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->dropColumn('object_id');
            $table->dropColumn('objecttable_type');
            $table->dropColumn('amount_default');
            $table->dropColumn('amount_ordered');
        });
    }
}
