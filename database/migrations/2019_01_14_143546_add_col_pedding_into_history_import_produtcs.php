<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColPeddingIntoHistoryImportProdutcs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('history_import_products', function (Blueprint $table) {
            $table->integer('pendding')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('history_import_products', function (Blueprint $table) {
            $table->dropColumn('pendding');
        });

    }
}
