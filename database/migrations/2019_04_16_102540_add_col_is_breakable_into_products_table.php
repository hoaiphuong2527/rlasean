<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColIsBreakableIntoProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->integer('is_breakable')->default(1);
        });
        Schema::table('options', function (Blueprint $table) {
            $table->integer('percent_breakable')->default(0);
            $table->string('province_stock')->nullable();
            $table->string('district_stock')->nullable();
            $table->string('address_stock')->nullable();
        });
        Schema::table('orders', function (Blueprint $table) {
            $table->integer('breakable_cost')->nullable();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('is_breakable');
        });
        Schema::table('options', function (Blueprint $table) {
            $table->dropColumn('percent_breakable');
            $table->dropColumn('province');
            $table->dropColumn('district');
            $table->dropColumn('address_stock');
        });
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('breakable_cost');
        });
    }
}
