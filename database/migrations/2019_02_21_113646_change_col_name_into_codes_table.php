<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColNameIntoCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('codes', function (Blueprint $table) {
            $table->renameColumn('promotion_type', 'reduction_type');
            $table->renameColumn('apply_type', 'promotion_type');
            $table->renameColumn('apply', ' apply_type');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('codes', function (Blueprint $table) {
            $table->renameColumn('reduction_type', 'promotion_type');
            $table->renameColumn('promotion_type', 'apply_type');
            $table->renameColumn('apply', ' apply_type');
        });
    }
}
