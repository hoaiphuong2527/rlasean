<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('codes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('promotion')->nullable();
            $table->integer('promotion_price')->nullable();
            $table->string('promotion_type')->nullable();
            $table->string('apply_type')->nullable();
            $table->integer('amount')->nullable();
            $table->date('begin_day')->nullable();
            $table->date('end_day')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('codes');
    }
}
