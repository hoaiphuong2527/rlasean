<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('options', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('main_logo');
            $table->string('footer_logo')->nullable();
            $table->string('address',255);
            $table->string('email');
            $table->string('phone');
            $table->text('iframe_gg');
            $table->string('facebook_acc')->nullable();
            $table->string('intagram_acc')->nullable();
            $table->string('youtube_acc')->nullable();
            $table->string('type_order')->default('NP'); //NP:none payment, HP: has payment
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('options');
    }
}
