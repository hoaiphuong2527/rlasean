<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contacts', function (Blueprint $table) {
            $table->dropColumn('name');
            $table->string('f_name')->nullable();
            $table->string('l_name')->nullable();
            $table->string('company',255)->nullable();
            $table->string('website',255)->nullable();
            $table->string('country',255)->nullable();
            $table->string('type')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contacts', function (Blueprint $table) {
            $table->string('name');
            $table->dropColumn('f_name');
            $table->dropColumn('l_name');
            $table->dropColumn('company');
            $table->dropColumn('website');
            $table->dropColumn('country');
            $table->dropColumn('type');
        });
    }
}
