<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Models\User::create([
            'name' => 'admin',
            'email' => 'admin@iluva.net',
            'email_verified_at' => "2018-08-27 07:26:37",
            'password' => bcrypt('iluva_admin'),
            'is_admin' => 1,
            'middleware_token' => md5(Carbon::now() . rand(1000, 9999)),
            'flag' => 1,
            'role' => 'controller'
        ]);
    }
}
