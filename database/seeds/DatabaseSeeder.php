<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(CategorySeeder::class);
        $this->call(StaticPageSeeder::class);
        $this->call(OptionSeeder::class);
        $this->call(BannerSeeder::class);
        $this->call(AddCategoryBannerSeeder::class);
        $this->call(TranslatePageSeeder::class);
        $this->call(DynamicMenuSeeder::class);
    }
}
