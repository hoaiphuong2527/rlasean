<?php

use App\Models\Option;
use Illuminate\Database\Seeder;
use App\Helpers\CreateTranslation;

class OptionTranslateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $option = Option::first();
        foreach(Option::$translate_column as $col){
            CreateTranslation::createPolymorphism($col,$option->id,config('ecommerce.locale_table_types.option'));
        }
    }
}
