<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            [
                'name'  => 'Sản Phẩm',
                'slug'  => 'san-pham',
                'status' => 1,
                'parent_id' => 0,
                'context_type' => config('ecommerce.context_type.product'),
            ],
            [
                'name'  => 'Tin tức',
                'slug'  => 'tin-tuc',
                'status' => 1,
                'parent_id' => 0,
                'context_type' => config('ecommerce.context_type.post'),
            ],
            [
                'name'  => 'Trang tĩnh',
                'slug'  => 'trang-tinh',
                'status' => 1,
                'parent_id' => 0,
                'context_type' => config('ecommerce.context_type.page'),
            ],
            [
                'name'  => 'Thông tin',
                'slug'  => 'thong-tin',
                'status' => 1,
                'parent_id' => 3,
                'context_type' => config('ecommerce.context_type.page'),
            ],
            [
                'name'  => 'Chính sách',
                'slug'  => 'chinh-sach',
                'status' => 1,
                'parent_id' => 3,
                'context_type' => config('ecommerce.context_type.page'),
            ],
        ]);
    }
}
