<?php

use App\Models\DynamicMenu;
use Illuminate\Database\Seeder;

class DynamicMenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('dynamic_menus')->insert([
            [
                'display_name'  => 'Về chúng tôi',
                'sort'          => 1
            ],
            [
                'display_name'  => 'Sản phẩm',
                'sort'          => 2
            ],
            [
                'display_name'  => 'Bảo Hành',
                'sort'          => 3
            ],
            [
                'display_name'  => 'Bài viết',
                'sort'          => 4
            ],
            [
                'display_name'  => 'Liên hệ',
                'sort'          => 5
            ],
            [
                'display_name'  => 'Đối tác',
                'sort'          => 6
            ]
        ]);
        foreach(DynamicMenu::all() as $item){
            foreach(config('ecommerce.locale_displays') as $key => $lang){
                DB::table('menu_translations')->insert([
                    [
                        'column_name'       => 'url',
                        'locale'            => $key
                    ],[
                        'column_name'       => 'display_name',
                        'locale'            => $key
                    ]
                ]);
            }
        }
        
    }
}
