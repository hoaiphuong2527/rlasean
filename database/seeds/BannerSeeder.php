<?php

use Illuminate\Database\Seeder;

class BannerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('banners')->insert([
            [
                'key'  => 'contact',
                'name' => 'Liên hệ'
            ],
            [
                'key'   => 'static_page',
                'name'  => 'Trang tĩnh'
            ]
        ]);
    }
}
