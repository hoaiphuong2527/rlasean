<?php

use Illuminate\Database\Seeder;

class AddCategoryBannerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('banners')->insert([
            [
                'key'  => 'category',
                'name' => 'Danh mục'
            ]
        ]);
    }
}
