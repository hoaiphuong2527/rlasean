<?php

use Illuminate\Database\Seeder;

class OptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('options')->insert([
            [
                'name' => 'Iluva',
                'email' => 'agency@iluva.net',
                'phone' => '0931.87.87.99',
                'address' => '66/10 Đường Phạm Ngọc Thạch,  Phường 6, Quận 3, Thành phố Hồ Chí Minh',
                'main_logo' => 'image',
                'iframe_gg' => 'iframe_gg',
                'sologan'   => 'ILUVA"'
            ]
        ]);
    }
}
