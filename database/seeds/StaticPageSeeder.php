<?php

use Illuminate\Database\Seeder;

class StaticPageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->insert([
            [
                'name' => 'Giới thiệu',
                'slug' => 'gioi-thieu',
                'status' => 1,
                'user_id' => 1,
                'category_id' => 3,
            ],
            [
                'name' => 'Liên hệ',
                'slug' => 'lien-he',
                'status' => 1,
                'user_id' => 1,
                'category_id' => 3,
            ],
            [
                'name' => 'Chính sách bán hàng',
                'slug' => 'chinh-sach-ban-hang',
                'status' => 1,
                'user_id' => 1,
                'category_id' => 5,
            ],
            [
                'name' => 'Chính sách bảo hành',
                'slug' => 'chinh-sach-bao-hanh',
                'status' => 1,
                'user_id' => 1,
                'category_id' => 5,
            ],
            [
                'name' => 'Chính sách đổi trả',
                'slug' => 'chinh-sach-doi-tra',
                'status' => 1,
                'user_id' => 1,
                'category_id' => 5,
            ],
            [
                'name' => 'Chính sách bảo mật',
                'slug' => 'chinh-sach-bao-mat',
                'status' => 1,
                'user_id' => 1,
                'category_id' => 5,
            ],
            [
                'name' => 'Tìm kiếm',
                'slug' => 'tim-kiem',
                'status' => 1,
                'user_id' => 1,
                'category_id' => 4,
            ],
            [
                'name' => 'Tài khoản thanh toán',
                'slug' => 'tai-khoan-thanh-toan',
                'status' => 1,
                'user_id' => 1,
                'category_id' => 4,
            ],
        ]);
    }
}
