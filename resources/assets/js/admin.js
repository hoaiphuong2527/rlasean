window.axios = require('axios');

window.axios.defaults.headers.common = {
    'X-Requested-With': 'XMLHttpRequest',
};
let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}
require('./admin/index');

function previewFile() {
    var preview = document.querySelector('img#preview');
    var file = document.getElementById('ilv-upload-img').files[0];
    var reader = new FileReader();

    reader.addEventListener("load", function () {
        preview.src = reader.result;
    }, false);

    if (file) {
        document.getElementById("preview").style.display = "block";
        reader.readAsDataURL(file);
    }
}

$("#ilv-upload-img").change(function () {
    previewFile();
})

$(".ilv-chkbox").click(function () {
    var hideImage = $(this).parent();
    var id = $(this).attr('photo-id');
    const URL = '/api/del-photo-variant/' + id;
    axios.post(URL)
        .then(function (response) {
            hideImage.parent().remove();
        })
        .catch(function (error) {
            console.log(error)
        });
});

$(document).ready(function () {
    $(".chkStatusSlideshow").click(function () {
        var id = $(this).attr("data-id");
        const URL = 'slideshows/slideshow-update-status/' + id;

        axios.post(URL)
            .then(function (response) {
                alert("Cập nhật thành công!");
            })
            .catch(function (error) {
                // alert("Lỗi!");  
            });

    });

    $("#chkPayment").click(function () {
        const URL = 'change-status-payment';

        axios.post(URL)
            .then(function (response) {
                alert("Cập nhật thành công!");
            })
            .catch(function (error) {
                // alert("Lỗi!");  
            });

    });
    $(".chkCategory").click(function () {
        var id = $(this).attr("data-id");
        const URL = 'categories/update-status/' + id;

        axios.post(URL)
            .then(function (response) {
                alert("Cập nhật thành công!");
                location.reload();
            })
            .catch(function (error) {
                // alert("Lỗi!");  
            });

    });
    $(".chkProduct").click(function () {
        var id = $(this).attr("data-id");
        const URL = 'products/update-status/' + id;

        axios.post(URL)
            .then(function (response) {
                alert("Cập nhật thành công!");
                location.reload();
            })
            .catch(function (error) {
                // alert("Lỗi!");  
            });

    });
    $(".chkVariant").click(function () {
        var id = $(this).attr("data-id");
        const URL = '/admin/variants/update-status/' + id;

        axios.post(URL)
            .then(function (response) {
                alert("Cập nhật thành công!");
            })
            .catch(function (error) {
                // alert("Lỗi!");  
            });

    });
    $(".chkPost").click(function () {
        var id = $(this).attr("data-id");
        const URL = 'posts/update-status/' + id;

        axios.post(URL)
            .then(function (response) {
                alert("Cập nhật thành công!");
            })
            .catch(function (error) {
                // alert("Lỗi!");  
            });

    });
    $(".chkStatusPromotion").click(function () {
        var id = $(this).attr("data-id");
        const URL = 'codes/update-status/' + id;

        axios.post(URL)
            .then(function (response) {
                console.log(response.data)
                alert("Cập nhật thành công!");
            })
            .catch(function (error) {
                // alert("Lỗi!");  
            });

    });
    $("#role_1").click(function () {
        if ($(this).is(':checked') == true) {
            document.getElementById('role_admin').style.display = "block";
        }
    });

    $("#role_0").click(function () {
        if ($(this).is(':checked') == true) {
            document.getElementById('role_admin').style.display = "none";
        }
    });

    if ($("#role_1").is(':checked')) {
        document.getElementById('role_admin').style.display = "block";
    }

    $(".ilv-upload-banner").change(function (e) {
        var id = $(this).attr("banner-id");
        var file = e.target.files[0];

        $(`#preview_${id}`).attr('src', URL.createObjectURL(file));
    })

    $(function () {
        $('input[name="daterange"]').daterangepicker({
            autoUpdateInput: false,
            opens: 'left'
        }, function (start, end, label) {
            $('input[name="daterange"]').val(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
            $("#start_date").val(start.format('YYYY-MM-DD'))
            $("#end_date").val(end.format('YYYY-MM-DD'))
            // console.log( document.getElementById('end_date').val())
            console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
        });
    });

});
$(document).ready(function () {
    tinymce.init({
        editor_selector: "mceEditor",
        height: 500,
        selector: '.myPost',
        images_upload_handler: function (blobInfo, success, failure) {
            var xhr, formData;
            xhr = new XMLHttpRequest();
            xhr.withCredentials = false;
            xhr.open('POST', '/api/upload-image');
            xhr.onload = function () {
                var json;

                if (xhr.status != 200) {
                    failure('HTTP Error: ' + xhr.status);
                    return;
                }
                json = JSON.parse(xhr.responseText);
                if (!json || typeof json.link != 'string') {
                    failure('Invalid JSON: ' + xhr.responseText);
                    return;
                }
                success(json.link);
            };
            formData = new FormData();
            formData.append('file', blobInfo.blob());
            xhr.send(formData);
        },
        plugins: [

            'advlist autolink lists link image charmap print preview hr anchor pagebreak',

            'searchreplace wordcount visualblocks visualchars code fullscreen',

            'insertdatetime media nonbreaking save table contextmenu directionality',

        ],
        toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | responsivefilemanager',
        toolbar2: 'print preview media | forecolor backcolor emoticons',
        toolbar: "sizeselect | bold italic | fontselect |  fontsizeselect",
        image_advtab: true,
        fontsize_formats: "8pt 10pt 12pt 13pt 14pt 18pt 24pt 36pt"
    });
    // $('.myPost').summernote({
    //     height: 500,
    //     minHeight: null,
    //     maxHeight: null,
    //     tabsize: 2,
    //     toolbar: [
    //         ['style', ['style']],
    //         ['font', ['bold', 'italic', 'underline', 'superscript', 'subscript', 'strikethrough', 'clear']],
    //         ['fontname', ['fontname']],
    //         ['fontsize', ['fontsize']], // Still buggy
    //         ['color', ['color']],
    //         ['para', ['ul', 'ol', 'paragraph']],
    //         ['height', ['height']],
    //         ['table', ['table']],
    //         ['insert', ['link', 'picture', 'video', 'hr']],
    //         ['view', ['fullscreen', 'codeview']],
    //         ['help', ['help']]
    //     ],
    //     fontSizes: ['8', '9', '10', '11', '12', '13', '14', '16', '18', '20', '22', '24', '26', '28', '36', '48', '72'],
    //     fontNames: [
    //         'Serif', 'Sans', 'Arial', 'Arial Black', 'Courier',
    //         'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande',
    //         'Lucida Sans', 'Tahoma', 'Times', 'Times New Roman', 'Verdana'
    //     ],
    //     callbacks: {
    //         onImageUpload: function (files, editor, $editable) {
    //             sendFile(files[0], editor, $editable);
    //         }
    //     }
    // });

    // function sendFile(file, editor, welEditable) {
    //     data = new FormData();
    //     data.append("file", file);
    //     $.ajax({
    //         data: data,
    //         type: "POST",
    //         url: "/api/upload-image",
    //         cache: false,
    //         contentType: false,
    //         processData: false,
    //         success: function (res) {
    //             insertFile(res.link)
    //         }
    //     });
    // }

    // function insertFile(url) {
    //     var image = $('<img>').attr('src', url);
    //     $('.myPost').summernote("insertNode", image[0]);
    // }
});

$('.m-menu__link').each(function () {
    var currentLocation = window.location;
    
    if ($(this).attr('href') == currentLocation) {
        $(this).parents('li').addClass('m-menu__item--active m-menu__item--open');
    }

})

$(document).ready(function () {
    var el = $("input[type=radio][name=reduction_type]");
    if (el.val() == 'percent' && el.attr('checked') == 'checked') {
        $("input[type=number][name=promotion_price]").attr('max', 100)
    } else {
        $("input[type=number][name=promotion_price]").attr('max', '')
    }
    el.on('click', function () {
        if ($(this).val() == 'percent') {
            $("input[type=number][name=promotion_price]").attr('max', 100)
        } else {
            $("input[type=number][name=promotion_price]").attr('max', '')
        }
    })

    var elPro = $("input[type=radio][name=apply_type]");
    if (elPro.val() == 1 && elPro.attr('checked') == 'checked') {
        $("#percent-choice").removeClass('d-none');
    } else {
        $("#percent-choice").addClass('d-none');
    }
    $("#apply_1").click(function () {
        if ($(this).is(':checked') == true) {
            $("#percent-choice").removeClass('d-none');
        }
    });
    $("#apply_0").click(function () {
        if ($(this).is(':checked') == true) {
            $("#percent-choice").addClass('d-none');
        }
    });

    if ($("#apply_1").is(':checked')) {
        $("#percent-choice").removeClass('d-none');
    }
    $(".save-order").click(function () {
        var id = $(this).attr("data-id");
        const URL = '/api/check-amount-variant/' + id;
        var order_edit_url = $(this).attr("order-edit-url");
        axios.post(URL)
            .then(function (response) {
                if (response.data.result == 0) {
                    alert('Số lượng trong kho không đủ để giao hàng!');
                    window.location.href = order_edit_url;
                }
            })
            .catch(function (error) {
                // alert("Lỗi!");  
            });

    });

    $(".change-amount-variant").change(function () {
        var currentPivotAmount = parseInt($(".variant-amount").text())
        var newAmount = parseInt($(".change-amount-variant").val())
        var total = parseInt(currentPivotAmount + newAmount)
        $(".variant-amount").text(total)
    });

    $('form input').on('keypress', function (e) {
        return e.which !== 13;
    });
    $('.btn_submit').click(function () {
        $("#formEdit").submit();
    });
    $('.input-submit-form').click(function () {
        var name = $(this).attr('name')
        $('#inputSubmitType').attr('name', name)
        $("#formEdit").submit();
    });
    $('.submit-menu').click(function () {
        $("#formMenu"+$(this).attr('row-id')).submit();
    });
    $('#exportOrder').click(function () {
        var name = $(this).attr('name')
        $('#inputSubmitExport').attr('name', name)
        $("#orderForm").submit();
    });
    var currentLocation = window.location.pathname;
    if(currentLocation == '/admin'){
        const url = '/api/order-chart';
        let dataChart = []
        axios.get(url).then(function (res) {
            dataChart = res.data
            var config = {
            type: 'line',
            data: {
                labels: ['Tháng 1', 'Tháng 2','Tháng 3','Tháng 4','Tháng 5','Tháng 6','Tháng 7','Tháng 8','Tháng 9','Tháng 10','Tháng 11','Tháng 12'],
                datasets: [{
                        label: "Doanh thu theo tháng",
                        data: dataChart,
                        fill: false,
                        backgroundColor: "#ea5126",
                        borderColor: "#ea5126",
                        borderCapStyle: 'butt',
                        // borderDash: [5, 5],
                    }]
            },
            options: {
                responsive: true,
                legend: {
                    position: 'bottom',
                },
                hover: {
                    mode: 'label'
                },
                scales: {
                    xAxes: [{
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Tháng'
                            }
                        }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'VNĐ'
                        },
                        ticks: {
                            beginAtZero: true,
                            min: 1
                        }
                    }]
                },
                title: {
                    display: true,
                    text: 'Doanh thu năm ' + new Date().getFullYear()
                }
            }
            };
        var ctx = document.getElementById("canvas").getContext("2d");
        new Chart(ctx, config);
    })
    }
});

$(".chkStatusReview").click(function () {
    var id = $(this).attr("data-id");
    const URL = 'review-update-status/' + id;

    axios.post(URL)
        .then(function (response) {
            alert("Cập nhật thành công!");
        })
        .catch(function (error) {
            // alert("Lỗi!");  
        });
});
require('./colorpicker');

$('#colorpickerField').ColorPicker({
        onSubmit: function (hsb, hex, rgb, el) {
            $(el).val(hex);
        },
        onChange: function (hsb, hex, rgb) {
            $('#colorpickerField').val(hex)
        },
        onBeforeShow: function () {}
    })
    .bind('keyup', function () {
        $(this).ColorPickerSetColor(this.value);
    });
$('.ilv-sort-order').sortable({
    connectWith: '.ilv-sort-order',
    stop: function (event, ui) {
        var array_id = [];
        var arr_pos = [];
        var pos = 1;
        var children = $(this).children().each(function (index, element) {
            array_id.push(element.id)
            arr_pos.push(pos)
            pos += 1
        });

        var data = {
            array_id: array_id,
            arr_pos: arr_pos
        }
        const url = '/api/menu/sorting';
        axios.post(url, data).then(function (res) {
            console.log(res)
        })
    }
});

$('.ilv-sort-slideshow').sortable({
    connectWith: '.ilv-sort-slideshow',
    stop: function (event, ui) {
        var array_id = [];
        var arr_pos = [];
        var pos = 1;
        var children = $(this).children().each(function (index, element) {
            array_id.push(element.id)
            arr_pos.push(pos)
            pos += 1
        });

        var data = {
            array_id: array_id,
            arr_pos: arr_pos
        }
        const url = '/api/slideshows/sorting';
        axios.post(url, data).then(function (res) {
        })
    }
});

$(document).ready(function () {
    if($('#statusOrder').val() == 'De'){
        $("#typePayment").addClass('d-none');
        $("#paymentStatus").addClass('d-none');
    }
    $('#statusOrder').on('change', function() {
        if($('#statusOrder').val() == 'De'){
            $("#typePayment").addClass('d-none');
            $("#paymentStatus").addClass('d-none');    
        }else{
            $("#typePayment").removeClass('d-none');
            $("#paymentStatus").removeClass('d-none');    
        }
    });

    if($('#typePaymentOption').val() != 'On'){
        var selectobject = document.getElementById("paymentStatusOption");
        if(selectobject){
            for (var i=0; i < selectobject.length; i++){
                if (selectobject.options[i].value == 'paid' )
                selectobject.remove(i);
            }
        }
    }
    $('#typePaymentOption').on('change', function() {
        var selectobject = document.getElementById("paymentStatusOption");
        if(selectobject){
            if($('#typePaymentOption').val() != 'On'){
                for (var i=0; i<selectobject.length; i++){
                    if (selectobject.options[i].value == 'paid' )
                    selectobject.remove(i);
                }
            }else{
                var option = document.createElement("option");
                option.text = "Đã thanh toán";
                option.value = "paid";
                option.id = "paid";
                selectobject.add(option);
            }
        }
    });
    $('#province_nomal').on('change', function() {
        var address = $('#stress').val()+","+$('#ward_nomal').val()+","+$('#district_nomal').val()+","+$(this).val()
        $('#addressBE').val(address)
    })
    $('#district_nomal').on('change', function() {
        var address = $('#stress').val()+","+$('#ward_nomal').val()+","+$(this).val()+","+$('#province_nomal').val()
        $('#addressBE').val(address)
    })
    $('#ward_nomal').on('change', function() {
        var address = $('#stress').val()+","+$(this).val()+","+$('#district_nomal').val()+","+$('#province_nomal').val()
        $('#addressBE').val(address)
    })
    $('#stress').on('change', function() {
        var address = $(this).val()+","+$('#ward_nomal').val()+","+$('#district_nomal').val()+","+$('#province_nomal').val()
        $('#addressBE').val(address)
    })
    $('#province_stock').on('change', function() {
        var addressStock = $('#stress_stock').val()+","+$('#ward_stock').val()+","+$('#district_stock').val()+","+$(this).val()
        $('#addressStock').val(addressStock)
    })
    $('#district_stock').on('change', function() {
        var addressStock = $('#stress_stock').val()+","+$('#ward_stock').val()+","+$(this).val()+","+$('#province_stock').val()
        $('#addressStock').val(addressStock)
    })
    $('#ward_stock').on('change', function() {
        var addressStock = $('#stress_stock').val()+","+$(this).val()+","+$('#district_stock').val()+","+$('#province_stock').val()
        $('#addressStock').val(addressStock)
    })
    $('#stress_stock').on('change', function() {
        var addressStock = $(this).val()+","+$('#ward_stock').val()+","+$('#district_stock').val()+","+$('#province_stock').val()
        $('#addressStock').val(addressStock)
    })
    $('#saveDistrictID').on('click',function(){
        var element = $('#district_stock').find('option:selected'); 
        var id = element.attr("district-id"); 
        const URL = '/api/options/save/district-id/' + id;
        var data = {
            district_stock: $('#district_stock').val(),
            province_stock: $('#province_stock').val(),
            address_stock: $(stress_stock).val()+","+$('#ward_stock').val()+","+$('#district_stock').val()+","+$('#province_stock').val()
        }
        axios.post(URL,data)
    })
    
})
import 'bootstrap-input-spinner';

$("input[type='number']").inputSpinner()