import React, { Component } from "react";
import ReactDOM from "react-dom";

export default class Calculator extends Component {
    static subtotal(array, prop1, prop2) {
        let subtotal = 0;
        array.forEach((item) => {
            subtotal += item[prop1] * item[prop2]
        })
        return subtotal
    }
}