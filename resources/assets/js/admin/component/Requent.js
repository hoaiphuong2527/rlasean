import React, { Component } from "react";
import ReactDOM from "react-dom";

export default class Requent extends Component {
    static create(target_item, array) {
        var index = array.findIndex(item => item.id === target_item.id)
        if (index === -1){
            array.push(target_item);
        }
        return array;
    }
    static addOrRemove(target_item, array) {
        var index = array.findIndex(item => item.id === target_item.id)
        if (index === -1){
            array.push(target_item);
        } else {
            array.splice( index, 1)
        }
        return array;
    }
    static update(property, value, target_item, array) {
        let newArray = array.map((array_item) => {
            if (array_item !== target_item) return array_item;
            let new_array_item = {...array_item}
            new_array_item[property] = value
            return new_array_item
        });
        return newArray;
    }
    static updateObjectInArray(new_object, object, array) {
        let newArray = array.map((array_item) => {
            if (array_item !== object) return array_item;
            let new_array_item = {...array_item}
            new_array_item = new_object
            return new_array_item
        });
        return newArray;
    }
    static objectToArray(object) {
        var array = [];
        for (var key in object) {
            array.push(object[key])
        }
        return array
    }
    static add(target_item, array) {
        array.push(target_item);
        return array;
    }
    static remove(target_item, array) {
        var index = array.findIndex(item => item === target_item)
        array.splice( index, 1)
        return array;
    }
}