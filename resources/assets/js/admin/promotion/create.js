import React, { Component } from "react";
import ReactDOM from "react-dom";
import Requent from "./../component/Requent";
import Calculator from "./../component/Calculator";

import DatePicker from "react-datepicker";
 
import "react-datepicker/dist/react-datepicker.css";

export default class CreatePromotion extends Component {
    constructor() {
        super();
        this.state = {
            promotion_type: "coupon",
            all_promotion_type: [
                {
                    value: "coupon", // val = 0
                    display: "Mã giảm giá (coupon)"
                },
                {
                    value: "product", // val = 1
                    display: "Chương trình khuyến mãi"
                }
            ],
            reduction_type: "percent",
            all_reduction_type: [
                {
                    value: "percent",
                    display: "% Giảm",
                    icon: "%"
                },
                {
                    value: "absolute",
                    display: "VND",
                    icon: "đ"
                }
            ],
            apply_type: "all_order",
            promotion: {
                coupon: {
                    text: {
                        name: "Mã khuyến mãi",
                        desc:
                            "Đây là đoạn mã được khách hàng nhập vào để giảm giá"
                    },
                    apply: [
                        {
                            value: "all_order",
                            display: "Tất cả đơn hàng"
                        },
                        {
                            value: "value_above",
                            display: "Đơn hàng có giá trị từ"
                        }
                    ]
                },
                product: {
                    text: {
                        name: "Chương trình khuyến mãi",
                        desc: ""
                    },
                    apply: [
                        {
                            value: "product",
                            display: "Sản phảm"
                        },
                        {
                            value: "variant",
                            display: "Biến thế"
                        }
                    ]
                }
            },
            // Search product state
            products: [],
            searched_product: [],
            search_status: "ie-off",
            startDate: new Date(),
            endDate: new Date(),
            limit_endDate: false,
            validate_text: "",
            amount_order: 'd-none',
            value_above: 'd-none',
            type_search:"",
            max:0,
            arr_object_id: [],
            arr_amount:[],
            required:true,
            amount_submit:[]
        };
        this.handleChangeStart = this.handleChangeStart.bind(this);
        this.handleChangeEnd = this.handleChangeEnd.bind(this);
    }
    componentWillMount() {
        this.setState({
            amount_order: "",
        })
    }

    handleChangeStart(date) {
        if (date < this.state.endDate) {
            this.setState({
                startDate: date
            })
        }
    }
    handleChangeEnd(date) {
        if (date > this.state.startDate) {

            this.setState({
                endDate: date
            })
        }
    }
    getText(type, property) {
        return this.state.promotion[type].text[property];
    }
    getOption(type) {
        return this.state.promotion[type].apply;
    }

    handleNormalChange(e) {
        if(e.target.value == 'coupon'){
            this.setState({
                [e.target.name]: e.target.value,
                amount_order: "",
                required:true
            });
        }else if(e.target.value == 'value_above'){
            this.setState({
                [e.target.name]: e.target.value,
                value_above: "",
                required:true
            });
        }else if(e.target.value == 'product'){
            this.handleGetSearchStatus(e.target.value)
            this.setState({
                [e.target.name]: e.target.value,
                amount_order: "d-none",
                value_above: 'd-none',
                type_search:'product',
                required:false,
                arr_object_id:[],
                arr_amount:[],
                products:[]
            });
            this.searchFocusDisplay('product')
        }else if(e.target.value == 'variant'){
            this.setState({
                [e.target.name]: e.target.value,
                amount_order: "d-none",
                value_above: 'd-none',
                type_search:'variant',
                required:false,
                arr_object_id:[],
                arr_amount:[],
                products:[]
            })
            this.searchFocusDisplay('variant')
        }else{
            this.setState({
                reduction_type: e.target.value
            })
        }
    }

    searchFocusDisplay(type){
        let endpoint = "/api/codes/search-products";
        let data = {
            apply_type:type
        };
        axios.post(endpoint, data).then(res => {
            if (res.data.result == 1) {
                this.setState({
                    search_status: "ie-on",
                    searched_product: res.data.products
                });
            }
        });
    }

    getIcon(reduction_type) {
        let the_type = this.state.all_reduction_type.find(
            item => item.value == reduction_type
        );
        return the_type.icon;
    }
    handleGetSearchStatus(apply_type) {
        if (apply_type == 'product') {
            this.setState({
                search_status: "ie-on"
            })
        }else {
            this.setState({
                search_status: "ie-off"
            })
        }        
    }

    handleSearchFocus(e) {
        let endpoint = "/api/codes/search-products";
        let data = {
            search: e.target.value,
            apply_type:this.state.type_search
        };
        axios.post(endpoint, data).then(res => {
            if (res.data.result == 1) {
                this.setState({
                    search_status: "ie-on",
                    searched_product: res.data.products
                });
            }
        });
    }

    handleSearchProduct(e) {
        let endpoint = "/api/codes/search-products";
        let data = {
            search: e.target.value,
            apply_type :this.state.type_search
        };
        axios.post(endpoint, data).then(res => {
            if (res.data.result == 1) {
                this.setState({
                    searched_product: res.data.products
                });
            }
        });
    }
    handleProductClick(e, item) {
        let newProduct = item;
        let productArray = this.state.products;
        var find = productArray.find(
            i => i.id == item.id
        );
        if(find){
            var new_amount = parseInt(find.order_amount) + 1;
            let newArr = Requent.update('order_amount',new_amount, find, productArray)
            let newAmountSubmit = Requent.update(
                'value',
                new_amount,
                this.state.amount_submit.find(
                    i => i.id == item.id
                ),this.state.amount_submit)
            this.setState({
                // search_status: "ie-off",
                products: newArr,
                amount_submit:newAmountSubmit
            });
        }else{
            item.order_amount = 1;
            productArray.push(newProduct);
            let newAmountSubmit = {
                'id': item.id,
                'value': 1
            }
            let newArrAmountSubmit = Requent.add(newAmountSubmit,this.state.amount_submit)
            this.setState({
                // search_status: "ie-off",
                products: productArray,
                amount_submit:newArrAmountSubmit
            });
        }
    }
    handleSearchClose(e) {
        this.setState({
            search_status: "ie-off"
        });
    }
    handleEndLimit(e) {
        this.setState({
            limit_endDate: e.target.checked
        })
    }
    handleChangePromotion(e){
        let endpoint = "/api/codes/validate";
        let data = {
            promotion: e.target.value,
            promotion_type: this.state.promotion_type
        };
        if(e.target.value == ""){
            this.setState({
                validate_text: "Vui lòng điền nội dung vào ô trên"
            });
        }else{
            axios.post(endpoint, data).then(res => {
                this.setState({
                    validate_text: res.data.message
                });
            });
        }
    }
    
    handleChangeAmount(e,item){
        let endpoint = "/api/products/check-amount";
        let data = {
            apply_type: this.state.type_search,
            id: item.id,
            amount: e.target.value
        };
        var find = this.state.products.find(
            i => i.id == item.id
        );
        if(find){
            let newArr = Requent.update('order_amount',data.amount, find, this.state.products)
            let newAmountSubmit = Requent.update(
                'value',
                data.amount,
                this.state.amount_submit.find(
                    i => i.id == item.id
                ),this.state.amount_submit)
            this.setState({
                products: newArr,
                amount_submit:newAmountSubmit
            });
        }
        // this.setState({
        //     arr_object_id: [...this.state.arr_object_id,item.id],
        //     arr_amount: [...this.state.arr_amount,data.amount],
        // });
    }

    render() {
        console.log('amount',this.state.amount_submit)
        const all_promotion_type = this.state.all_promotion_type;
        const all_reduction_type = this.state.all_reduction_type;
        const code_name_text = this.getText(this.state.promotion_type, "name");
        const code_name_desc = this.getText(this.state.promotion_type, "desc");
        const reduction_icon = this.getIcon(this.state.reduction_type);
        const apply_option = this.getOption(this.state.promotion_type);
        const validate_text = this.state.validate_text;

        // Search product
        const selected_product = this.state.products;
        const searched_product = this.state.searched_product;
        const search_status = this.state.search_status;
        const promotion_search_status = this.state.search_status
        // Date range
        const limit_endDate = this.state.limit_endDate;
        const type_search = this.state.type_search;
        const max_input = this.state.max;
        return (
            <div className="contianer-fluid">
            <input className="d-none" name="arr_object" value={JSON.stringify(this.state.amount_submit)}/>
                <div className="row">
                    <div className="col-md-6">
                        <div className="form-group m-form__group">
                            <label htmlFor="col-form-label">
                                <b>Loại khuyến mãi</b>
                            </label>
                            <select
                                className="form-control"
                                name="promotion_type" // apply_type
                                onChange={e => this.handleNormalChange(e)}
                            >
                                {all_promotion_type.map((item, index) => {
                                    return (
                                        <option key={index} value={item.value}>
                                            {item.display}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="form-group m-form__group">
                            <label htmlFor="col-form-label">
                                <b>{code_name_text}</b>
                            </label>
                            <input
                                name="promotion"
                                type="text"
                                className="form-control m-input"
                                onChange={e => this.handleChangePromotion(e)}
                            />
                            <p className="mt-1">{validate_text}</p>
                            <p className="mt-1">{code_name_desc}</p>
                        </div>
                    </div>
                    <div className="col-md-6 ie-verical-border-left">
                            <label htmlFor="col-form-label">
                                <b>Bắt đầu khuyến mãi</b>
                            </label>
                        <div className="form-group m-form__group">
                            <DatePicker
                                name='begin_day'
                                selected={this.state.startDate}
                                todayButton={"Hôm nay"}
                                selectsStart
                                showTimeSelect
                                dateFormat="dd/MM/yyyy h:mm aa"
                                startDate={this.state.startDate}
                                endDate={this.state.endDate}
                                onChange={this.handleChangeStart}
                            />
                        </div>
                            <label htmlFor="col-form-label">
                                <b>Hết hạn khuyến mãi</b>
                            </label>
                        <div className="form-group m-form__group">
                            <DatePicker
                                name='end_day'                            
                                selected={this.state.endDate}
                                selectsEnd
                                showTimeSelect
                                dateFormat="dd/MM/yyyy h:mm aa"
                                disabled={limit_endDate}
                                startDate={this.state.startDate}
                                endDate={this.state.endDate}
                                onChange={this.handleChangeEnd}
                            />
                        </div>
                        <label className="m-checkbox m-checkbox--primary">
							<input type="checkbox" name='is_over' checked={limit_endDate} onChange={(e) => this.handleEndLimit(e)}/> Không có ngày hết hạn
							    <span></span>
						</label>
                    </div>
                </div>

                <div className="row mt-5">
                    <div className="col">
                        <div className="form-group m-form__group">
                            <label htmlFor="col-form-label">
                                <b>Cách thức giảm</b>
                            </label>
                            <div className="row align-items-center">
                                <div className="col-2">
                                    <select
                                        className="form-control"
                                        name="reduction_type" 
                                        onChange={e =>
                                            this.handleNormalChange(e)
                                        }
                                    >
                                        {all_reduction_type.map(
                                            (item, index) => {
                                                return (
                                                    <option
                                                        key={index}
                                                        value={item.value}
                                                    >
                                                        {item.display}
                                                    </option>
                                                );
                                            }
                                        )}
                                    </select>
                                </div>
                                <div className="col-1">
                                    <b>Giảm</b>
                                </div>
                                <div className="col-2">
                                    <div className="input-group">
                                        <input
                                            name="promotion_price"
                                            type="text"
                                            className="form-control m-input"
                                        />
                                        <div className="input-group-append">
                                            <span className="input-group-text">
                                                {reduction_icon}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-1">
                                    <b>Áp dụng cho</b>
                                </div>
                                <div className="col-3">
                                    <select
                                        className="form-control"
                                        name="apply_type"
                                        onChange={e =>
                                            this.handleNormalChange(e)
                                        }
                                        //them input gia tri don hang tu.. co name = value_above
                                    >
                                        {apply_option.map((item, index) => {
                                            return (
                                                <option
                                                    key={index}
                                                    value={item.value}
                                                >
                                                    {item.display}
                                                </option>
                                            );
                                        })}
                                    </select>
                                </div>
                            </div>
                            <br></br>
                            <div className="row mt-2">
                                <div className={"col-3 " + this.state.amount_order}>
                                    <b>Số lượng mã giảm giá</b>
                                    <input
                                        name="amount"
                                        type="number"
                                        className="form-control m-input"
                                        placeholder="Số lượng"
                                        min="0"
                                        // required={this.state.required}
                                        />
                                </div>
                                <div className={"col-3 input-group " + this.state.value_above}>
                                    <b>Áp dụng cho đơn hàng có giá trị</b>
                                    <input
                                        name="value_above"
                                        type="text"
                                        className="form-control m-input custom-input"
                                        placeholder="Giá trị từ"
                                        min="0"
                                        // required={this.state.required}
                                    />
                                    <div className="input-group-append">
                                        <span className="input-group-text">
                                            đ
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div className={promotion_search_status}>
                                <div className="row mt-4">
                                    <div className="col-9">
                                        {selected_product.map((item, index) => {
                                            return (
                                                <div
                                                    key={index}
                                                    className="row my-1 align-items-center"
                                                >
                                                    <div className="col-5">
                                                        <b>
                                                            { type_search == 'product' ? item.name :item.name  + "-"+ item.title}
                                                        </b>
                                                    </div>
                                                    <div className="col-2 text-right">
                                                        Số lần
                                                    </div>
                                                    <div className="col-2">
                                                        <input
                                                            className="form-control"
                                                            type="number"
                                                            min="0"
                                                            max={item.max}
                                                            value={item.order_amount}
                                                            onChange={e => this.handleChangeAmount(e,item)}
                                                            id={'amount'+item.id}
                                                        />
                                                    </div>
                                                    <div className="col-3">
                                                        Để trống nếu không giới
                                                        hạn
                                                    </div>
                                                </div>
                                            );
                                        })}
                                    </div>
                                </div>
                                <div className="row mt-2">
                                    <div className="col-6">
                                        <div className="input-group m-input-group">
                                            <input
                                                autoComplete="off"
                                                name="search"
                                                type="text"
                                                className="form-control m-input"
                                                placeholder="Thêm sản phẩm"
                                                onChange={e =>
                                                    this.handleSearchProduct(e)
                                                }
                                                onFocus={e =>
                                                    this.handleSearchFocus(e)
                                                }
                                            />
                                            <div className="input-group-append">
                                                <span
                                                    className="input-group-text"
                                                    id="basic-addon1"
                                                >
                                                    <i className="fa fa-plus" />
                                                </span>
                                            </div>
                                        </div>

                                        <div
                                            className={
                                                "m-portlet " + search_status
                                            }
                                        >
                                            <div className="container-fluid ie-search-var-wrapper">
                                                {searched_product.map(
                                                    (item, index) => {
                                                        return (
                                                            <div
                                                                key={index}
                                                                className="row ie-search-var-item py-3"
                                                                onClick={e =>
                                                                    this.handleProductClick(
                                                                        e,
                                                                        item
                                                                    )
                                                                }
                                                            >
                                                                <div className="col-2">
                                                                    <img
                                                                        src={
                                                                            item.image
                                                                        }
                                                                        className="img-fluid"
                                                                    />
                                                                </div>
                                                                <div className="col-6">
                                                                    <div className="mb-1">
                                                                        {  item.name }
                                                                    </div>
                                                                    <div>
                                                                        { type_search == 'product' ? "" : item.title}
                                                                    </div>
                                                                </div>
                                                                <div className="col-4">
                                                                    { item.price_round}
                                                                </div>
                                                            </div>
                                                        );
                                                    }
                                                )}
                                                <div
                                                    className="row ie-search-close py-1 text-center"
                                                    onClick={e =>
                                                        this.handleSearchClose(
                                                            e
                                                        )
                                                    }
                                                >
                                                    <div className="col">
                                                        <i className="fa fa-angle-up fa-2x" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

if (document.getElementById("admin-react-create-promotion")) {
    ReactDOM.render(
        <CreatePromotion />,
        document.getElementById("admin-react-create-promotion")
    );
}
