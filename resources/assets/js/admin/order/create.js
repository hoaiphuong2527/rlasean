import React, { Component } from "react";
import ReactDOM from "react-dom";
import { ProductRow } from "./component";
import Requent from "./../component/Requent";
import Calculator from "./../component/Calculator";
import { objectToArray } from '../../helper/ObjectToArray'
import { map, sortBy, findIndex } from 'lodash';
import NumberFormat from 'react-number-format';

import cities from '../../city/cities.json';

const toArray = (Obj) => map(Obj, (o, key) => ({ ...o, key }));


export default class CreateOrder extends Component {
    constructor() {
        super();
        
        const citiesArray = toArray(cities);

        const citiesArraySort = sortBy(citiesArray, function (city) {
            return city.slug;
        })
        this.state = {
            products: [],
            searched_product: [],
            search_status: 'ie-off',
            promotion: 0,
            sub_total: 0,
            total: 0,
            amount_accept: [],
            promotionOrder:'',
            discount:0,
            labelRequiredEmail: "",
            styleInput:[],
            checkSubmitForm:true,
            cities: citiesArraySort,
            selectedCity: citiesArraySort[0],
            selectedWardId: 0,
            DistrictID:'',
            shipFee:'0đ',
            breakableCost: 0,
            ghnShippingCost:0,
            ghtkShippingCost:0
        };
        this.onChangeCity = this.onChangeCity.bind(this);
        this.onChangeWrad = this.onChangeWrad.bind(this);
    }
    componentWillMount() {
        let endpoint = "/orders/backup-session";
        axios.get(endpoint).then(res => {})
    }
    componentDidUpdate() {

       
    }
    handleSearchFocus(e) {
        let endpoint = "/api/variants/search";
        let data = {
            search: e.target.value
        };
        axios.post(endpoint, data).then(res => {
            if (res.data.result == 1) {
                this.setState({
                    search_status: 'ie-on',
                    searched_product: res.data.variants
                });
            }
        });
    }

    handleSearchProduct(e) {
        let endpoint = "/api/variants/search";
        let data = {
            search: e.target.value
        };
        axios.post(endpoint, data).then(res => {
            if (res.data.result == 1) {
                this.setState({
                    searched_product: res.data.variants
                });
            }
        });
    }
    calculatorShipFee(dist){
        let serviceUrl = `${'https://console.ghn.vn/api/v1/apiv3/FindAvailableServices'}`
        const formId = option.district_id
        var totalWeight = 0; 
        this.state.products.map((item) => {
            totalWeight = totalWeight + item.weight
        })
        let data = {
            "token": option.ghn_token,
            "Weight": totalWeight,
            "Length": 0,
            "Width": 0,
            "Height": 0,   
            "FromDistrictID": parseInt(formId),
            "ToDistrictID": dist.DistrictID
        }
        fetch(serviceUrl, {
        method: 'POST',
        headers: new Headers({
                    'Content-Type': 'application/x-www-form-urlencoded', // <-- Specifying the Content-Type
            }),
        body: JSON.stringify(data) // <-- Post parameters
        })
        .then((response) => response.text())
        .then((responseText) => {
            let responseInfo = JSON.parse(responseText).data[0]
            this.setState({
                shipFee: responseInfo.ServiceFee,
                ghnShippingCost: responseInfo.ServiceFee,
                DistrictID: dist.DistrictID
            })
        })
        .catch((error) => {
            console.error(error);
        });
    }
    handleProductClick(e, item) {
        let endpoint = "/orders/create/add-product";
        const { selectedCity } = this.state;
        const districts = selectedCity.quan_huyen;
        const districtArray = toArray(districts);
        const value = districtArray[0].name_with_type;
        const dist = this.getDistrictID(districtArray,value)  
        this.calculatorShipFee(dist)
        this.setState({
            DistrictID: dist.DistrictID
        })
        let data = {
            'id' :item.id
        };
        axios.post(endpoint, data).then(res => {
            let newProduct = item
            var findProductArr = []
            let productArray = this.state.products
            productArray.map((i, index) => {
                if(i.id == item.id)
                findProductArr.push(i)
            })
            if(res.data.has_code == 1 && res.data.amount_accept >= 1){
                newProduct.price_round = item.discount
                if(findProductArr.length){
                    // var lastItem = findProductArr[0];
                    // var new_order_amount = Requent.update('order_amount', parseInt(lastItem.order_amount) + 1,lastItem,productArray)
                    // newProduct = {...lastItem, order_amount : new_order_amount[0].order_amount}
                    // productArray = Requent.updateObjectInArray(newProduct,lastItem,productArray)
                    // this.updateSeesion(res.data.code_id,res.data.amount_accept,'sub',lastItem.id)
                }else{
                    newProduct.order_amount = 1
                    productArray.push(newProduct)
                    this.updateSeesion(res.data.code_id,res.data.amount_accept,'sub',newProduct.id)
                }
            }else{
                if(findProductArr.length >= 1){
                    // var lastItem = findProductArr.pop()
                    // var new_order_amount = Requent.update('order_amount', parseInt(lastItem.order_amount) + 1,lastItem,productArray)
                    // newProduct = {...lastItem, order_amount : new_order_amount[0].order_amount}
                    // productArray = Requent.updateObjectInArray(newProduct,lastItem,productArray)
                }else{
                    newProduct.order_amount = 1
                    productArray.push(newProduct)
                }
            }
            let breakableCost = this.state.breakableCost;
            productArray.map((item) => {
                if(item.breakable){
                    breakableCost = breakableCost + (item.price_round * option.percent_breakable / 100) * item.order_amount 
                }
            })
            this.setState({
                breakableCost:breakableCost,
                search_status: 'ie-off',
                products: productArray
            }) 
        })

    }

    updateSeesion(code_id,amount_accept,operator,item_id){
        let endpoint = "/orders/create/update-session";
        let data = {
            'code_id' :code_id,
            'amount_accept' :amount_accept, //2
            'operator':operator,
            'variant_id': item_id
        };
        axios.post(endpoint, data).then(res => {
            var amount = {
                code_id: code_id,
                amount_accept: res.data.session,
                variant_id: item_id
            }
            let amountArr = this.state.amount_accept
            var find = ''
            amountArr.find(i => { if( i.code_id == code_id)
                find = i
            });
            let newAmountArr = []
            if(!find)
                newAmountArr = Requent.add(amount,amountArr)
            else {
                var new_amount = Requent.update('amount_accept', res.data.session,find,amountArr)
                var newItem = find
                newItem = {...find, amount_accept : new_amount[0].amount_accept}

                newAmountArr = Requent.updateObjectInArray(newItem,find,amountArr)
            }
            this.setState({
                amount_accept: newAmountArr
            })
        })
    }

    handleSearchClose(e) {
        this.setState({
            search_status: 'ie-off'
        })
    }
    handleAmountChange(e, item) {
        let endpoint = "/orders/create/update-amount-product";
        let productArray = this.state.products
        let data = {
            'id' :item.id,
            'amount' : e.target.value,
            'productArray' : productArray.length
        };
        var newAmount = e.target.value;
        let findProductArr = []
        productArray.map((i, index) => {
            if(i.id == item.id)
            findProductArr.push(i)
        })
        axios.post(endpoint, data).then(res => {
            if(newAmount == 0){
                findProductArr.map((i, index) => {
                    if(i.id == item.id)
                    this.handleRemoveItem(e,i)
                })
            }else{
                if(res.data.has_promotion){
                    if(res.data.more == 1){
                        //update
                        if(res.data.session == 1){
                            var newProduct = findProductArr.pop();
                            var new_amount = res.data.normal
                            var new_order_amount = Requent.update('order_amount',new_amount,newProduct,productArray)
                            var findItemRightId = this.getRightItem(new_order_amount,newProduct)
                            newProduct = {...newProduct, order_amount : parseInt(findItemRightId.order_amount)}
                            productArray = Requent.updateObjectInArray(newProduct,newProduct,productArray)
                        }else{
                            var newProduct = findProductArr[0];
                            var new_order_amount = Requent.update('order_amount',res.data.has_code,newProduct,productArray)
                            var findItemRightId = this.getRightItem(new_order_amount,newProduct)
                            newProduct = {...newProduct, order_amount : parseInt(findItemRightId.order_amount)}
                            productArray = Requent.updateObjectInArray(newProduct,findProductArr[0],productArray)
                            var newItem = item
                            newItem.order_amount = res.data.normal
                            newItem.price_round = Math.round(item.price)
                            Requent.add(newItem,productArray)
                        }
                    }else{
                        var newProduct = findProductArr[0];
                        var new_order_amount = Requent.update('order_amount',newAmount,newProduct,productArray)
                        var findItemRightId = this.getRightItem(new_order_amount,newProduct)
                        newProduct = {...newProduct, order_amount : parseInt(findItemRightId.order_amount)}
                        productArray = Requent.updateObjectInArray(newProduct,findProductArr[0],productArray)
                    }
                }else{
                    var newProduct = findProductArr[0];
                    var new_order_amount = Requent.update('order_amount',newAmount,newProduct,productArray)
                    var findItemRightId = this.getRightItem(new_order_amount,newProduct)
                    newProduct = {...newProduct, order_amount : parseInt(findItemRightId.order_amount)}
                    productArray = Requent.updateObjectInArray(newProduct,findProductArr[0],productArray)
                }
                let breakableCost = 0;
                productArray.map((item) => {
                if(item.breakable){
                    breakableCost = breakableCost + (item.price_round * option.percent_breakable / 100) * item.order_amount 
                }
                })
                this.setState({ 
                    breakableCost:breakableCost,
                    products: productArray
                 });
            }
            
        });
    }

    getRightItem(new_order_amount,newProduct){
        var findItemRightId = new_order_amount.pop();
        new_order_amount.map((item, index) => {
            if(item.id == newProduct.id)
            findItemRightId = item
        });
        return findItemRightId;
    }

    handleRemoveItem(e,item){
        let new_products = Requent.remove(item, this.state.products)
        if(item.product.product_codes[0].code_id){
            this.updateSeesion(item.product.product_codes[0].code_id,item.order_amount,'add',item.id)
        }else if(item.product_codes[0].code_id){
            this.updateSeesion(item.product_codes[0].code_id,item.order_amount,'add',item.id)
        }
        let breakableCost = 0;
        new_products.map((item) => {
        if(item.breakable){
            breakableCost = breakableCost + (item.price_round * option.percent_breakable / 100) * item.order_amount 
        }
        })
        this.setState({
            breakableCost:breakableCost,
            products: new_products
        })
    }

    handlePromotionOrder(e){
        let endpoint = "/api/codes/checkInvalid";
        var value = e.target.value
        let data = {
            'receiveCode' :value
        };
        if(value == null){
            this.setState({
                labelRequiredEmail: ""
            });
        }
        axios.post(endpoint, data).then(res => {
            if(res.data.resultCode == 1){
                this.setState({
                    promotionOrder: res.data.code
                });
                if(value == ''){
                    this.setState({
                        labelRequiredEmail: "",
                        checkSubmitForm:true,
                        styleInput:{
                            border:"1px solid #ebedf2"
                        },
                    });
                }else{
                    if(document.getElementById('customerEmail').value  == "" ){
                        this.setState({
                            labelRequiredEmail: "Vui lòng nhập email để kiểm tra"
                        })
                    }
                }
            }else{
                this.setState({
                    styleInput:{
                        border:"1px solid red"
                    },
                    labelRequiredEmail: "Mã khuyến mãi không tồn tại",
                    checkSubmitForm:false
                });
                document.getElementById('noti').innerText='';
            }
        });
    }
    handleCheckEmail(e){
        let endpoint = "/api/check-email";
        let data = {
            'checkCode': this.state.promotionOrder.promotion,
            'checkEmail': e.target.value
        };
        if(this.state.promotionOrder){
            axios.post(endpoint, data).then(res => {
                this.setState({
                    labelRequiredEmail: ""
                })
                document.getElementById('noti').innerText=''+res.data.message;
                document.getElementById('code').innerText=''+res.data.code.promotion;
                if(res.data.resultCode == 1){
                    var code = res.data.code
                    var newTotal = 0;
                    var sub_total = Calculator.subtotal(this.state.products, 'price_round', 'order_amount');
                    if(code.reduction_type == 'absolute'){
                        newTotal = sub_total - code.promotion_price;
                    }else if(code.reduction_type == 'percent'){
                        newTotal =  sub_total - (sub_total*(code.promotion_price/100));
                    }
                    this.setState({
                        discount:newTotal + this.state.shipFee,
                    })
                }else{
                    this.setState({
                        checkSubmitForm:false
                    })
                }
                
            });
        }
    }
    submitForm(){
        if(this.state.checkSubmitForm)
            return 'submit';
        return 'button'
    }

    onChangeCity(e) {
        const { cities } = this.state;
        const value = e.target.value
        const index = findIndex(cities, function (city) {
            return city.name_with_type === value;
        })
        this.setState({
            selectedCity: cities[index]
        })
        if(document.getElementById('stress').value){
            this.setShippingCost(
                document.getElementById('district').value,
                document.getElementById('province').value,
                document.getElementById('ward').value + document.getElementById('stress').value)
        }

    }

    onChangeWrad(e) {
        const { selectedCity } = this.state;
        const districts = selectedCity.quan_huyen;
        const districtArray = toArray(districts);
        const value = e.target.value;
        const dist = this.getDistrictID(districtArray,value)
        const index = findIndex(districtArray, function (district) {
            return district.name_with_type === value;
        })
        let serviceUrl = `${'https://console.ghn.vn/api/v1/apiv3/FindAvailableServices'}`
        const formId = option.district_id
        var totalWeight = 0; 
        this.state.products.map((item) => {
            totalWeight = totalWeight + item.weight
        })
        let data = {
            "token": option.ghn_token,
            "Weight": totalWeight,
            "Length": 0,
            "Width": 0,
            "Height": 0,   
            "FromDistrictID": parseInt(formId),
            "ToDistrictID": dist.DistrictID
        }
        fetch(serviceUrl, {
        method: 'POST',
        headers: new Headers({
                    'Content-Type': 'application/x-www-form-urlencoded', // <-- Specifying the Content-Type
            }),
        body: JSON.stringify(data) // <-- Post parameters
        })
        .then((response) => response.text())
        .then((responseText) => {
            let responseInfo = JSON.parse(responseText).data[0]
            let totalPromotion = this.state.totalBill  + parseInt(responseInfo.ServiceFee);

            this.setState({ 
                shipFee:responseInfo.ServiceFee,
                selectedWardId: index,
                ghnShippingCost:responseInfo.ServiceFee,
                totalPromotion:totalPromotion
            })
        })
        .catch((error) => {
            console.error(error);
        });
        if(document.getElementById('stress').value){
            this.setShippingCost(
                document.getElementById('district').value,
                document.getElementById('province').value,
                document.getElementById('ward').value + document.getElementById('stress').value)
        }
    }

    getDistrictID(districtArray,value){
        var district = districtArray.pop();
        districtArray.map((item, index) => {
            if(item.name_with_type == value)
            district = item
        });
        return district
    }

    handleOnchangeAddress(e){
        let value = e.target.value
        var district = document.getElementById('district').value
        var province = document.getElementById('province').value
        var address = document.getElementById('ward').value + value
        this.setShippingCost(province,district,address)
    }

    setShippingCost(province,district,address){
        this.calculatorGhtkShippingCost(province,district,address)
        var shipFee = this.state.shipFee
        if(this.state.ghtkShippingCost > this.state.ghnShippingCost){
            shipFee = this.state.ghtkShippingCost
        }else {
            shipFee = this.state.ghnShippingCost
        }
        this.setState({
            shipFee: shipFee
        })
    }

    calculatorGhtkShippingCost(province,district,address){
        var totalWeight = 0; 
        this.state.products.map((item) => {
            totalWeight = totalWeight + item.weight
        })
        let data = {
            province : province,
            district : district,
            address : address,
            // province: "Hà nội",
            // district : "Quận Cầu Giấy",
            // address : "P.503 tòa nhà Auu Việt, số 1 Lê Đức Thọ",
            weight : totalWeight,
            value : Calculator.subtotal(this.state.products, 'price_round', 'order_amount'),
        }
        let endpoint = '/api/calculator-ghtk-shipping-cost';
        var ghtkShippingCost = 0;
        axios.post(endpoint,data).then(res => {
            var fee = res.data.pop().fee
            ghtkShippingCost = fee.fee + fee.insurance_fee
            this.setState({
                ghtkShippingCost: ghtkShippingCost
            })

        })
    }

    render() {
        const selected_product = this.state.products;
        const searched_product = this.state.searched_product;
        const search_status = this.state.search_status;
        const sub_total = (Calculator.subtotal(selected_product, 'price_round', 'order_amount')).toLocaleString('vi-VN', { style: 'currency', currency: 'VND' });
        const sub_total_display = (Calculator.subtotal(selected_product, 'price_round', 'order_amount') + this.state.breakableCost + this.state.shipFee).toLocaleString('vi-VN', { style: 'currency', currency: 'VND' });
        const discount = (this.state.discount).toLocaleString('vi-VN', { style: 'currency', currency: 'VND' });
        const total = (this.state.discount).toLocaleString('vi-VN', { style: 'currency', currency: 'VND' });
        const shipFee = (this.state.shipFee).toLocaleString('vi-VN', { style: 'currency', currency: 'VND' });
        const breakableCost = (this.state.breakableCost).toLocaleString('vi-VN', { style: 'currency', currency: 'VND' });
        const total_submit = Calculator.subtotal(selected_product, 'price_round', 'order_amount');
        const total_after_submit = Calculator.subtotal(selected_product, 'price_round', 'order_amount') + this.state.shipFee;
        const total_display = (this.state.discount + this.state.shipFee).toLocaleString('vi-VN', { style: 'currency', currency: 'VND' });
        const sub_total_discount = (this.state.discount) + (this.state.shipFee);
        const { cities, selectedCity, selectedWardId } = this.state;
        const districts = selectedCity.quan_huyen;
        const districtArray = toArray(districts);
        const districtArraySort = sortBy(districtArray, function (item) {
            return item.slug;
        })
        const wards = districtArray[selectedWardId].xa_phuong;
        const wardsArray = toArray(wards);
        return (
            <div className="row">
                <div className="col-8">
                    <div className="m-portlet m-portlet--mobile">
                        <div className="m-portlet__head">
                            <div className="m-portlet__head-caption">
                                <div className="m-portlet__head-title">
                                    <h3 className="m-portlet__head-text">
                                        Tạo đơn hàng mới
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div className="m-portlet__body">
                            <table
                                className="table table-striped- table-bordered table-hover table-checkable"
                                id="m_table_1"
                            >
                                <thead className="m-datatable__head">
                                    <tr
                                        className="m-datatable__row"
                                        style={{ left: "0px" }}
                                    >
                                        <th className=" m-datatable__cell ">
                                            {" "}
                                            <span style={{ width: "80px" }}>
                                                #
                                            </span>
                                        </th>
                                        <th className=" m-datatable__cell ">
                                            {" "}
                                            <span style={{ width: "150px" }}>
                                                Sản phẩm
                                            </span>
                                        </th>
                                        <th className=" m-datatable__cell ">
                                            {" "}
                                            <span style={{ width: "80px" }}>
                                                Số lượng
                                            </span>
                                        </th>
                                        <th className=" m-datatable__cell ">
                                            {" "}
                                            <span style={{ width: "100px" }}>
                                                Đơn giá
                                            </span>
                                        </th>
                                        <th className=" m-datatable__cell ">
                                            {" "}
                                            <span style={{ width: "100px" }}>
                                                Thành tiền
                                            </span>
                                        </th>
                                        <th className=" m-datatable__cell ">
                                            {" "}
                                            <span style={{ width: "100px" }}>
                                                Hành động
                                            </span>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody className="m-datatable__body">
                                    {selected_product.map((item, index) => {
                                        return (
                                            <ProductRow
                                                key={index}
                                                index={index + 1}
                                                image={item.image}
                                                name={item.product.name}
                                                title={item.title}
                                                amount={item.order_amount}
                                                onAmountChange={(e) => this.handleAmountChange(e, item)}
                                                price={item.price_round}
                                                total={item.order_amount * item.price_round}
                                                onRemoveItem = {(e) => this.handleRemoveItem(e,item)}
                                            />
                                        );
                                    })}
                                </tbody>
                            </table>
                            <div className="row">
                                <div className="col-12">
                                    <div className="input-group m-input-group">
                                        <input
                                            autoComplete="off"
                                            name="search"
                                            type="text"
                                            className="form-control m-input"
                                            placeholder="Thêm sản phẩm"
                                            onChange={e =>
                                                this.handleSearchProduct(e)
                                            }
                                            onFocus={(e) => this.handleSearchFocus(e)}
                                            
                                        />
                                        <div className="input-group-append"><span className="input-group-text" id="basic-addon1"><i className="fa fa-plus"></i></span></div>
                                    </div>
                                    
                                    <div className={"m-portlet " + search_status}>
                                        <div className="container-fluid ie-search-var-wrapper">
                                            {searched_product.map((item, index) => {
                                                return(
                                                    <div key={index} className="row ie-search-var-item py-3" onClick={(e) => this.handleProductClick(e, item)}>
                                                        <div className="col-2">
                                                            <img src={item.image} className="img-fluid" />
                                                        </div>
                                                        <div className="col-6">
                                                            <div className="mb-1">
                                                                {item.product.name}
                                                            </div>
                                                            <div>{item.title}</div>
                                                        </div>
                                                        <div className="col-4">
                                                            {item.price_round}
                                                        </div>
                                                    </div>
                                                )
                                            })}
                                            <div className="row ie-search-close py-1 text-center"
                                                onClick={(e) => this.handleSearchClose(e)}
                                            >
                                                <div className="col">
                                                    <i className="fa fa-angle-up fa-2x"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-6">
                                    <div className="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                                        <b className="col-form-label col-lg-3 col-sm-12">
                                            Khuyến mãi
                                        </b>
                                        <input
                                            name=""
                                            type="text"
                                            min="1"
                                            className="form-control m-input"
                                            onChange= {(e) => this.handlePromotionOrder(e)}
                                            name="promotion"
                                            style={this.state.styleInput}
                                        />
                                        <label id="" className="" style={{ color: 'red' }}>{this.state.labelRequiredEmail}</label>
                                    </div>
                                </div>
                            </div>
                            <div className="row text-right">
                                <div className="col-6" />
                                <div className="col-6">
                                    <table className="table">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <b>Tổng tiền</b>
                                                </td>
                                                <td>{sub_total}</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>Khuyến mãi </b>
                                                    <br />
                                                    <p> <label id="code"></label></p>
                                                </td>
                                                <td>{ discount }</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>Vận chuyển</b>
                                                </td>
                                                <td>{shipFee}</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>Chi phí hàng dễ vỡ</b>
                                                </td>
                                                <td>{breakableCost}</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>Thanh toán</b>
                                                </td>
                                                <td>{this.state.discount ? total_display: sub_total_display}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <input id="checkSubmitForm" className="d-none" value={this.state.checkSubmitForm}/>
                            <div className="m-portlet__foot m-portlet__foot--fit">
                                <div className="m-form__actions text-center">
                                    <input type={this.submitForm()} className="btn btn-success" name="publish" value="Lưu"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-4">
                    <div className="m-portlet m-portlet--mobile">
                        <div className="m-portlet__head">
                            <div className="m-portlet__head-caption">
                                <div className="m-portlet__head-title">
                                    <h3 className="m-portlet__head-text">
                                        Khách hàng
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div className="m-portlet__body">
                            <div className="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                                <b>
                                    Họ và tên
                                </b>
                                <input
                                    name="customer_name"
                                    type="text"
                                    className="form-control m-input"
                                    required
                                />
                            </div>
                            <div className="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                                <b>
                                    Email
                                </b>
                                <input
                                    id="customerEmail"
                                    name="customer_email"
                                    type="text"
                                    className="form-control m-input"
                                    onChange={(e) => this.handleCheckEmail(e)}
                                    required
                                />
                                <label id="noti" className="text-success" style={{ color: "red" }}></label>
                            </div>
                            <div className="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                                <b >
                                    Số điện thoại
                                </b>
                                <input
                                    name="customer_phone"
                                    type="text"
                                    className="form-control m-input"
                                    required
                                />
                            </div>
                            {/* <div className="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                                <b className="col-form-label col-lg-3 col-sm-12">
                                    Địa chỉ giao hàng
                                </b>
                                <textarea
                                    name="address"
                                    className="form-control m-input"
                                    cols=""
                                    rows="2"
                                    required
                                />
                            </div> */}
                            <div className=" form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                                <b htmlFor="firstName">Tỉnh/Thành Phố</b>
                                <select
                                    className="form-control"
                                    name="province"
                                    id="province"
                                    onChange={this.onChangeCity}
                                >
                                    {cities.map(function (city) {
                                        return <option key={city.key} value={city.name_with_type}>{city.name_with_type}</option>
                                    })}
                                </select>
                                <br></br>
                                <b htmlFor="firstName">Quận/Huyện</b>
                                <select
                                    className="form-control"
                                    name="district"
                                    id="district"
                                    onChange={this.onChangeWrad}
                                >
                                    {districtArraySort.map(function (district) {
                                        return <option key={district.key} value={district.name_with_type}>{district.name_with_type}</option>
                                    })}
                                </select>
                                <br></br>
                                <b htmlFor="firstName">Phường/Xã</b>
                                <select className="form-control" name="ward">
                                    {wardsArray.map(function (ward) {
                                        return <option key={ward.key} id="ward" value={ward.name_with_type}>{ward.name_with_type}</option>
                                    })}
                                </select>
                                <br></br>
                                <b htmlFor="address">Địa chỉ</b>
                                <textarea
                                    name="stress"
                                    className="form-control m-input"
                                    cols=""
                                    rows="2"
                                    id="stress"
                                    required
                                    onChange={(e) => this.handleOnchangeAddress(e)}
                                />
                            </div>
                            <input name="total" value={total_submit} className="d-none" />
                            <input name="shipping_fee" value={this.state.shipFee} type="hidden" />
                            <input name="total_after" value={this.state.discount ? sub_total_discount: total_after_submit} className="d-none" />
                            <input type='text' name="selected_product" value={JSON.stringify(selected_product)} className="d-none" />
                            <input type='text' name="breakable_cost" value={this.state.breakableCost} className="d-none" />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

if (document.getElementById("admin-react-create-order")) {
    ReactDOM.render(
        <CreateOrder />,
        document.getElementById("admin-react-create-order")
    );
}
