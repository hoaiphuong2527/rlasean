import React, { Component } from 'react';
import ReactDOM from 'react-dom';

export function ProductRow(props) {
    return (
        <tr className="m-datatable__row" style={{ left: "0px" }}>
            <td className=" m-datatable__cell ">
                {" "}
                <span style={{ width: "80px" }}>
                    {props.index}
                </span>
            </td>
            <td className=" m-datatable__cell ">
                {" "}
                <span style={{ width: "150px" }}> 
                    {props.name} <br/>
                    {props.title} 
                </span>
            </td>
            <td className=" m-datatable__cell ">
                {" "}
                <span style={{ width: "80px" }}> 
                    <input className="form-control" type="number" 
                    value={props.amount}
                    onChange={props.onAmountChange}
                    /> 
                </span>
            </td>
            <td className=" m-datatable__cell ">
                {" "}
                <span style={{ width: "100px" }}>{props.price.toLocaleString('vi-VN', { style: 'currency', currency: 'VND' })}</span>
            </td>
            <td className=" m-datatable__cell ">
                {" "}
                <span style={{ width: "100px" }}>{props.total.toLocaleString('vi-VN', { style: 'currency', currency: 'VND' })}</span>
            </td>
            <td className=" m-datatable__cell ">
                {" "}
                <span style={{ width: "100px" }}>
                <button className="btn btn-sm btn-danger" onClick={(e) => props.onRemoveItem(e) }>
                        <i className="fa fa-trash-alt"></i>
                    </button>
                </span>
            </td>
        </tr>
    );
}
