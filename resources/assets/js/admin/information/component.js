import React, { Component } from "react";
import ReactDOM from "react-dom";
import Requent from "./../component/Requent";
import Calculator from "./../component/Calculator";

export class ValueGroup extends Component {
    constructor() {
        super();
        this.state = {
            new_value: ""
        };
    }
    handleNormalChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        });
    }
    handleAddValue(e, new_value, item) {
        this.props.onAddValue(e, new_value, item);
        this.setState({
            new_value: ""
        });
    }

    handleRemoveValue(e,item){
        this.props.onRemoveValue(e, item);
    }

    render() {
        const params = this.props.item.params;
        const name = this.props.item.name;
        const theItem = this.props.item;
        const newValue = this.state.new_value;
        return (
            <div>
                <div>
                    {params.map((item, index) => {
                        return (
                            <ValueItem
                                key={index}
                                value={item.value}
                                id={item.id}
                                onItemChange={e =>
                                    this.props.onItemChange(e, item)
                                }
                                onItemUpdate ={(e,value) => this.props.onItemUpdate(e,item)}
                                onItemDelete={(e, value) => this.props.onItemDelete(e, value, item)}
                                onItemSaveUpdate={(e,value,theItem) => this.props.onItemSaveUpdate(e, value, theItem)}
                                class_default={this.props.class_default}
                                class_d_none={this.props.class_d_none}
                            />
                        );
                    })}
                </div>
                <div className="input-group m-input-group mt-2 mb-4">
                    <input
                        className="form-control m-input"
                        type="text"
                        autoComplete="off"
                        name="new_value"
                        value={newValue}
                        placeholder={"Thêm giá trị cho " + name}
                        onChange={e => this.handleNormalChange(e)}
                    />
                    <div
                        className="input-group-append ilv-input-append-btn-primary"
                        onClick={e => this.handleAddValue(e, newValue, theItem)}
                    >
                        <span className="input-group-text" id="basic-addon1">
                            <i className="fa fa-plus" />
                        </span>
                    </div>
                    <div
                        className="input-group-append ilv-input-append-btn-transparence"
                        onClick={e => this.handleRemoveValue(e, theItem)}
                    >
                        <span className="input-group-text">
                        <i className="fa fa-trash-alt" />
                        </span>
                    </div>
                </div>
            </div>
        );
    }
}

export class ValueItem extends Component {
    constructor() {
        super();
    }

    render() {
        let value = this.props.value;
        let id = this.props.id;
        return (
            <div className="input-group m-input-group mb-2">
                <label className="col-8">{value}</label>
                <input
                    autoComplete="off"
                    name="value"
                    type="text"
                    className={"form-control m-input custom-d-none"}
                    defaultValue={value}
                    id={id}
                    onChange={e => this.props.onItemChange(e)}
                />
                <div
                    className={"input-group-append ilv-input-append-btn-transparence "}
                >
                    <span className="input-group-text inner-d-none">
                        <i className="fa fa-check" onClick={e => this.props.onItemSaveUpdate(e, value, id)}></i>
                        <i className="fa fa-pencil-alt" onClick={e => this.props.onItemUpdate(e, value,id)}></i>
                    </span>
                </div>
                <div
                    className="input-group-append ilv-input-append-btn-transparence"
                    onClick={e => this.props.onItemDelete(e, value)}
                >
                    <span className="input-group-text">
                        <i className="fa fa-trash-alt" />
                    </span>
                </div>
            </div>
        );
    }
}
