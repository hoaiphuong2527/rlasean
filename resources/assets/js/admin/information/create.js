import React, { Component } from "react";
import ReactDOM from "react-dom";
import Requent from "./../component/Requent";
import CreateSlug from "./../component/CreateSlug";
import Calculator from "./../component/Calculator";

import { ValueItem, ValueGroup } from "./component";

export default class CreateInfor extends Component {
    constructor() {
        super();
        this.state = {
            allInfo: [
                {
                    key: "ram",
                    name: "RAM",
                    params: [
                        {
                            id: 1,
                            value: "2GB"
                        },
                        {
                            id: 2,
                            value: "4GB"
                        }
                    ]
                },
                {
                    key: "cpu",
                    name: "CPU",
                    params: [
                        {
                            id: 3,
                            value: "Core i3"
                        },
                        {
                            id: 4,
                            value: "Core i5"
                        }
                    ]
                }
            ],
            new_key: "",
            information_array: [],
            class_default: "",
            class_d_none: "d-none",
            new_value:""
        };
    }
    componentWillMount() {
        this.setState({
            allInfo: Requent.objectToArray(allInfo),
            information_array: information_array
        });
    }
    handleNormalChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        });
    }
    handleAddKey(e) {
        e.preventDefault();
        if(this.state.new_key != ''){
            let newKey = {
                key: "",
                name: this.state.new_key,
                params: []
            };
            let newAllInfo = Requent.add(newKey, this.state.allInfo)
            this.setState({
                allInfo: newAllInfo,
                new_key: ''
            })
        }
        
    }
    handleAddValue(e, new_value, item) {
        let newValue = {
            value: new_value,
        }
        let endpoint = "/api/categories/" + categoryId + "/informations";
        let data = {
            value: new_value,
            key: CreateSlug.create(item.name),
            name: item.name
        };
        axios.post(endpoint, data).then(res => {
            if (res.data.result == 1) {
                let array = this.state.allInfo
                let newValue = {
                    id: res.data.new_id,
                    value: new_value,
                }
                let newArray = array.map((array_item) => {
                    if (array_item !== item) return array_item;
                    let new_array_item = {...array_item}
                    new_array_item.params.push(newValue)
                    return new_array_item
                });
                
                this.setState({
                    allInfo: newArray,
                    information_array: [...this.state.information_array, res.data.new_id]
                });
            }
            this.sendArrayTosave()
        });
    }

    handleRemoveValue(e,item,group){
        let allInfoArray = this.state.allInfo
        allInfoArray.map((i, index) => {
            if(i.name == item.name){
                let data = {
                    key: item.key
                }
                let endpoint = "/api/categories/" + categoryId + "/informations-delete";
                axios.post(endpoint, data)
                var index = allInfoArray.findIndex(item => item === group)
                allInfoArray.splice( index, 1)
                this.setState({
                    allInfo: allInfoArray
                })
            }
        })
    }

    sendArrayTosave(){
        let endpoint = "/api/categories/" + categoryId + "/informations-save";
        let data = {
            information_array:this.state.information_array
        };
        axios.post(endpoint, data)
    }

    handleItemUpdate(e,value_item,item){


        if(e.target.className == 'fa fa-pencil-alt'){
        e.target.previousElementSibling.style.display="block"
        e.target.style.display="none"
        e.target.parentElement.offsetParent.firstChild.style.display="none"
        e.target.parentElement.offsetParent.firstChild.nextElementSibling.style.display="block"

        }else{
            e.target.className = 'fa fa-pencil-alt'
            e.target.offsetParent.firstChild.setAttribute('distext-trueabled',true)

        }
    }

    handleItemSaveUpdate(e,value_item,id){
        let endpoint = "/api/categories/" + categoryId + "/informations/"+id;
        let data = {
            value: this.state.new_value,
        };
        axios.put(endpoint, data).then(res => {
            if(res.data.result == 1){
                this.setState({
                    class_default: "",
                    class_d_none: "d-none"
                })
            }
        })
                e.target.nextElementSibling.style.display="block"
                e.target.style.display="none"
                e.target.parentElement.offsetParent.firstChild.style.display="block"
                e.target.parentElement.offsetParent.firstChild.nextElementSibling.style.display="none"
        
    }
    
    handleItemChange(e, value_item, item) {
        let new_item_params = Requent.update('value', e.target.value, value_item, item.params)
        let new_item = {...item, params : new_item_params}
        let newAllInfo = Requent.updateObjectInArray(new_item, item, this.state.allInfo)

        this.setState({
            allInfo: newAllInfo,
            new_value:e.target.value
        })
    }
    handleItemDelete(e, value, item, group) {
        let endpoint = "/api/categories/" + categoryId + "/informations/" + item.id;
        axios.post(endpoint).then(res => {
            if (res.data.result == 1) {
                let new_group_params = Requent.remove(item, group.params)
                let new_group = {...group, params : new_group_params}
                let newAllInfo = Requent.updateObjectInArray(new_group, group, this.state.allInfo)
                this.setState({
                    allInfo: newAllInfo
                })
            }
        });    
    }
    render() {
        let newKey = this.state.new_key;
        let allInfo = this.state.allInfo;
        return (
            <div className="form-group m-form__group col-lg-6 col-md-6 col-sm-6">
            <input className="d-none" name="information_array" value={this.state.information_array}/>
                <div className="form-group m-form__group mt-4">
                    <label>Thông số sản phẩm</label>
                    {/* <p class="ilv-description ilv-font-size-smaller">Các thuộc tính của sản phẩm trong danh mục.</p> */}
                    {allInfo.map((item, index) => {
                        return (
                            <div className="row" key={index}>
                                <div className="col-3">
                                    <b>{item.name}</b>
                                </div>
                                <div className="col-9">
                                    <ValueGroup 
                                    item={item}
                                    onAddValue={(e, new_value, item) => this.handleAddValue(e, new_value, item)}
                                    onRemoveValue={(e,the_item,item) => this.handleRemoveValue(e,the_item,item)}
                                    onItemChange={(e, value_item) => this.handleItemChange(e, value_item, item)}
                                    onItemDelete={(e, value, the_item) => this.handleItemDelete(e, value, the_item, item)}
                                    onItemUpdate ={(e, value, the_item)  => this.handleItemUpdate(e,value, the_item, item)}
                                    onItemSaveUpdate={(e,value,item) => this.handleItemSaveUpdate(e, value, item)}
                                    class_default={this.state.class_default}
                                    class_d_none={this.state.class_d_none}
                                    />
                                </div>
                            </div>
                        );
                    })}

                    <div className="input-group m-input-group mt-4">
                        <input
                            autoComplete="off"
                            name="new_key"
                            type="text"
                            className="form-control m-input"
                            placeholder="Thêm thông số"
                            value={newKey}
                            onChange={e => this.handleNormalChange(e)}
                        />
                        <div
                            className="input-group-append ilv-input-append-btn-primary"
                            onClick={e => this.handleAddKey(e)}
                        >
                            <span
                                className="input-group-text"
                                id="basic-addon1"
                            >
                                <i className="fa fa-plus" />
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

if (document.getElementById("admin-react-information")) {
    ReactDOM.render(
        <CreateInfor />,
        document.getElementById("admin-react-information")
    );
}
