import React, { Component } from "react";
import ReactDOM from "react-dom";
import Requent from "./../component/Requent";

export default class IndexMedia extends Component {
    constructor() {
        super();
        this.state = {
            view: "index",
            mediaId: null,
            allFiles: [],
            uploadedFiles: [],
            single: {},
            draggingClass: '',
            single_alt: "",
            single_desc: "",
            isRemove:false

        };
    }
    componentWillMount() {
        let endPoint = '/api/media'
        axios.get(endPoint).then(res => {
            this.setState({
                allFiles: res.data
            })
        })
    }
    openUpload() {
        this.setState({
            view: "upload"
        });
    }
    closeUpload() {
        this.setState({
            view: "index"
        });
    }
    handleDrop(e) {
        e.preventDefault();
        let files = e.dataTransfer.items;

        if (files) {
            for (var i = 0; i < files.length; i++) {
                // If dropped items aren't files, reject them
                if (files[i].kind === "file") {
                    let file = files[i].getAsFile();
                    let data = new FormData();
                    data.append("file", file);
                    let endPoint = "/api/upload-image";
                    axios
                        .post(endPoint, data, {
                            headers: {
                                "Content-Type": "multipart/form-data"
                            }
                        })
                        .then(res => {
                            let all_files = this.state.allFiles
                            let uploaded_files = this.state.uploadedFiles
                            uploaded_files.push(res.data)
                            all_files.unshift(res.data)
                            this.setState({
                                allFiles: all_files
                            })
                            // uploaded_files.push( res.data );
                        });
                }
            }
        }

        if (this.state.uploadedFiles.length == 1) {
            this.setState({
                draggingClass: '',
                view: 'single',
            })
        } else {
            this.setState({
                view: 'index',
                draggingClass: ''
            })
        }
    }
    handleDragOver(e) {
        e.preventDefault();
        this.setState({
            draggingClass: 'ilv-dragging'
        })
    }
    handleDragLeave(e) {
        e.preventDefault()
        this.setState({
            draggingClass: ''
        })
    }
    handleSingleImage(item) {
        if(item.alt == null) var alt = ""; else var alt = item.alt
        if(item.description == null) var description = ""; else var description = item.description
        this.setState({
            single_alt: alt,
            single_desc: description,
            view: 'single',
            single: item
        })
    }
    handleUpload(e){
        e.preventDefault();
        let files = e.target.files;
        for (var i = 0; i < files.length; i++) {
            let file = files[i]
            let data = new FormData();
            data.append("file", file);
            let endPoint = "/api/upload-image";
            axios.post(endPoint, data, {
                headers: {
                    "Content-Type": "multipart/form-data"
                }
            })
            .then(res => {
                let all_files = this.state.allFiles
                let uploaded_files = this.state.uploadedFiles
                uploaded_files.push(res.data)
                all_files.unshift(res.data)
                this.setState({
                    allFiles: all_files
                })
                // uploaded_files.push( res.data );
            });
        }
        
    }
    handleChangeAtt(e,type){
        if(type == "alt"){
            this.setState({
                single_alt: e.target.value
            })
        }else{
            this.setState({
                single_desc: e.target.value
            })
        }
    }

    handleSaveUpdate(single){
        let endPoint = '/api/media/'+ single.id
        let data = {
            alt: this.state.single_alt,
            description: this.state.single_desc
        };
        axios.put(endPoint,data).then(res => {
            if(res.data.result == 1){
                this.setState({
                    single_alt: res.data.item.alt,
                    single_desc: res.data.item.description
                })
            }
            let newAllFiles= Requent.updateObjectInArray(single, res.data.item, this.state.allFiles)
            this.setState({
                allFiles: newAllFiles
            })
        })
    }
    handleRemoveItem(item){
        let endPoint = '/api/media/'+ item.id
        axios.post(endPoint).then(res => {
            if(res.data.result == 1){
                this.setState({
                    single_alt: "",
                    single_desc: "",
                    isRemove: true,
                    view:'index'
                })
            }
            let newAllFiles = Requent.remove(item, this.state.allFiles)
            this.setState({
                allFiles: newAllFiles
            })
        })
    }
    render() {
        const view = this.state.view;
        switch (view) {
            case "index":
                const all_files = this.state.allFiles;
                return (
                    <div className="m-portlet m-portlet--mobile">
                        <div className="m-portlet__head">
                            <div className="m-portlet__head-caption">
                                <div className="m-portlet__head-title">
                                    <h3 className="m-portlet__head-text">
                                        Thư viện ảnh
                                    </h3>
                                </div>
                            </div>
                            <div className="m-portlet__head-tools">
                                <button
                                    onClick={() => this.openUpload()}
                                    className="btn btn-primary m-btn m-btn--custom m-btn--pill m-btn--air"
                                >
                                    Tải ảnh mới
                                </button>
                            </div>
                        </div>
                        <div className="m-portlet__body">
                            <div className="container-fluid">
                                <div className="row">
                                    {all_files.map((item, index) => {
                                        return (
                                            <div key={index} className="ilv-media-img-thumb m-2" >
                                                <div onClick={() => this.handleSingleImage(item)} className="ilv-img-block">
                                                <img 
                                                    className="img-fluid ilv-position-relative ilv-top-50 ilv-transform-translate h-100 w-100 ilv-object-fit-cover"
                                                    src={item.link}
                                                    alt=""
                                                />
                                                </div>
                                            </div>
                                        )
                                    })}
                                </div>
                            </div>
                        </div>
                    </div>
                );
                break;

            case "upload":
                const draggingClass = this.state.draggingClass
                return (
                    <div className="m-portlet m-portlet--mobile">
                        <div className="m-portlet__head">
                            <div className="m-portlet__head-caption">
                                <div className="m-portlet__head-title">
                                    <h3 className="m-portlet__head-text">
                                        Tải ảnh mới
                                    </h3>
                                </div>
                            </div>
                            <div className="m-portlet__head-tools">
                                <button
                                    onClick={() => this.closeUpload()}
                                    className="btn btn-outline-metal m-btn m-btn--icon m-btn--icon-only"
                                >
                                    <i className="fa fa-times" />
                                </button>
                            </div>
                        </div>
                        <div className="m-portlet__body">
                            <div
                                className={"ilv-draggable " + draggingClass}
                                onDrop={e => this.handleDrop(e)}
                                onDragOver={e => this.handleDragOver(e)}
                                onDragLeave={e => this.handleDragLeave(e)}
                            >
                                <div className="row justify-content-center text-center">
                                    <div className="ilv-media-upload-icon mt-5">
                                        <i className="fa fa-upload fa-5x"></i>
                                    </div>
                                    <div className="col-6 ilv-media-browse my-auto">

                                        <h3 className="m-portlet__head-text mt-5">
                                            Chọn hoặc kéo thả ảnh vào đây
                                    </h3>
                                        <div className="custom-file mt-2">
                                        <input type="file" className="custom-file-input" multiple id="customFile" onChange={e => this.handleUpload(e)} />
                                        <label className="custom-file-label" htmlFor="customFile">Choose file</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                );
                break;
            case 'single':
                const single = this.state.single
                return (
                    <div className="m-portlet m-portlet--mobile">
                        <div className="m-portlet__head">
                            <div className="m-portlet__head-caption">
                                <div className="m-portlet__head-title">
                                    <h3 className="m-portlet__head-text">
                                        Thông tin chi tiết
                                    </h3>
                                </div>
                            </div>
                            <div className="m-portlet__head-tools">
                                <button
                                    onClick={() => this.closeUpload()}
                                    className="btn btn-outline-metal m-btn m-btn--icon m-btn--icon-only"
                                >
                                    <i className="fa fa-times" />
                                </button>
                            </div>
                        </div>
                        <div className="m-portlet__body">
                            <div className="container-fluid">
                                <div className="row">
                                    <div className="col-8">
                                        <img src={single.link} className="img-fluid" alt="" />
                                    </div>
                                    <div className="col-4 ie-verical-border-left">
                                        <div>
                                            <b>Tên file: </b> <br /> {single.file_name}
                                        </div>
                                        <div>
                                            <b>Dung lượng:</b><br />
                                            {single.size}
                                        </div>
                                        <div>
                                            <b>Kích thước:</b><br />
                                            {single.resolution}
                                        </div>
                                        <div>
                                            <span style={{'color':'red','fontStyle': 'italic','cursor': 'pointer'}} onClick={() => this.handleRemoveItem(single)}>
                                                Xóa hình ảnh
                                            </span>
                                        </div>
                                        <div className="form-group m-form__group mt-2">
                                            <label htmlFor="col-form-label">
                                                <b>Alt Text</b>
                                            </label>
                                            <input
                                                    name="promotion"
                                                    type="text"
                                                    className="form-control m-input"
                                                    value={this.state.single_alt}
                                                    onChange={e =>
                                                        this.handleChangeAtt(e,'alt')
                                                    }
                                                />
                                            <p className="mt-1">Thẻ alt tối ưu SEO hình ảnh</p>
                                        </div>
                                        <div className="form-group m-form__group">
                                            <label htmlFor="col-form-label">
                                                <b>Description</b>
                                            </label>
                                            <textarea name="description" className="form-control m-input" onChange={e =>
                                                        this.handleChangeAtt(e,'description')
                                                    } value={this.state.single_desc}></textarea>                                        
                                        </div>
                                        <button
                                            
                                            className="btn btn-primary m-btn m-btn--custom"  onClick={e => this.handleSaveUpdate(single)}
                                        >
                                            Lưu
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                )
                break
        }
    }
}

if (document.getElementById("admin-react-media-app")) {
    ReactDOM.render(
        <IndexMedia />,
        document.getElementById("admin-react-media-app")
    );
}
