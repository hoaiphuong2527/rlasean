import React, { Component } from "react";
import ReactDOM from "react-dom";
import Requent from "./../component/Requent";

export class MediaZone extends Component {
    constructor() {
        super()
        this.state = {
            isMultiple: false,
            selected: [],
            single_alt: "",
            single_desc: "",
            isRemove:false
        }
    }
    componentWillMount() {
        this.setState({
            selected: this.props.preview,
            isMultiple: this.props.isMultiple
        })
    }
    handleSingleImage(e, item) {
        if (this.state.isMultiple) {
            let newSelected = Requent.addOrRemove(item, this.state.selected)
            this.setState({
                selected: newSelected
            })
        } else {
            if(item.alt == null) var alt = ""; else var alt = item.alt
            if(item.description == null) var description = ""; else var description = item.description
            let newSelected = Requent.addOrRemove(item, this.state.selected)
            let newSelectedArr = []
            if(newSelected.length > 1){
                newSelectedArr = this.state.selected.slice(1)
            }else  newSelectedArr = newSelected
            this.setState({
                single_alt: alt,
                single_desc: description,
                selected: newSelectedArr
            })
            this.props.onSingleClick(e, item)
        }
        
    }
    handleSingleChange(e, item, selected) {
        e.preventDefault()
        this.props.onSingleChange(e, item, selected)
    }
    handleMultipleClick(e) {
        e.preventDefault()
        if (e.target.checked == true) {
            this.props.onMultipleSelect(false)
        }
        this.setState({
            isMultiple: e.target.checked
        })
    }
    handleChangeAtt(e,type){
        if(type == "alt"){
            this.setState({
                single_alt: e.target.value
            })
        }else{
            this.setState({
                single_desc: e.target.value
            })
        }
    }

    handleSaveUpdate(single){
        let endPoint = '/api/media/'+ single.id
        let data = {
            alt: this.state.single_alt,
            description: this.state.single_desc
        };
        axios.put(endPoint,data).then(res => {
            if(res.data.result == 1){
                this.setState({
                    single_alt: res.data.item.alt,
                    single_desc: res.data.item.description
                })
            }
            this.props.onSaveUpdate(single,res.data.item);
        })
    }

    handleRemoveItem(item){
        let endPoint = '/api/media/'+ item.id
        axios.post(endPoint).then(res => {
            if(res.data.result == 1){
                this.setState({
                    single_alt: "",
                    single_desc: "",
                    isRemove: true
                })
                Requent.remove(item,this.state.selected)
                this.props.onRemoveItem(item);
            }else{
                console.log('res.data.result',res.data.result)
            }
        })
    }

    render() {
        const all_files = this.props.allFiles;
        const single = this.props.single
        const isMultiple = this.state.isMultiple
        const isMultipleSupport = this.props.isMultiple
        const selected = this.state.selected
        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-9">
                        <div className="row my-2">
                            {isMultipleSupport ?
                                (
                                    <div className="col">
                                        <label className="m-checkbox m-checkbox--primary">
                                            <input type="checkbox" checked={isMultiple} onChange={(e) => this.handleMultipleClick(e)} /> Chọn nhiều hình ảnh
                                        <span></span>
                                        </label>
                                    </div>
                                ) : (null)}
                        </div>

                        {all_files.map((item, index) => {
                            if (isMultiple) {
                                let isSelectedIndex = selected.findIndex(selected_item => selected_item.id === item.id)
                                if (isSelectedIndex === -1) {
                                    return (
                                        <div key={index} className="ilv-media-img-thumb m-2">
                                            <div onClick={(e) => this.handleSingleImage(e,item)} className="ilv-img-block">
                                            <img
                                                className="img-fluid ilv-position-relative ilv-top-50 ilv-transform-translate h-100 w-100 ilv-object-fit-cover"
                                                src={item.link}
                                                alt=""
                                            />
                                            </div>
                                        </div>
                                    )
                                } else {
                                    return (
                                        <div key={index} className="ilv-media-img-thumb m-2 ilv-media-img-active">
                                            <div onClick={(e) => this.handleSingleImage(e,item)} className="ilv-img-block">
                                            <img
                                                className="img-fluid ilv-position-relative ilv-top-50 ilv-transform-translate h-100 w-100 ilv-object-fit-cover"
                                                src={item.link}
                                                alt=""
                                            />
                                            </div>
                                        </div>
                                    )
                                }
                            } else if (single && item.id == single.id) {
                                return (
                                    <div key={index} className="ilv-media-img-thumb m-2 ilv-media-img-active">
                                        <div onClick={(e) => this.handleSingleImage(e,item)} className="ilv-img-block">
                                        <img
                                            className="img-fluid ilv-position-relative ilv-top-50 ilv-transform-translate h-100 w-100 ilv-object-fit-cover"
                                            src={item.link}
                                            alt=""
                                        />
                                        </div>
                                    </div>
                                )
                            } else {
                                return (
                                    <div key={index} className="ilv-media-img-thumb m-2">
                                        <div onClick={(e) => this.handleSingleImage(e,item)} className="ilv-img-block">
                                        <img
                                            className="img-fluid ilv-position-relative ilv-top-50 ilv-transform-translate h-100 w-100 ilv-object-fit-cover"
                                            src={item.link}
                                            alt=""
                                        />
                                        </div>
                                    </div>
                                )
                            }
                        })}
                    </div>
                    <div className="col-3 ie-verical-border-left ilv-fill py-4">
                        <div className="row">
                            {single ?
                                (
                                    <div className="w-100">
                                        <div className="col-12 text-center">
                                            <img
                                                className="img-fluid ilv-max-height-300 ilv-width-auto"
                                                src={single.link}
                                                alt=""
                                            />
                                        </div>
                                        <div className="col-12 pt-3">    
                                            <div>
                                                <b>Tên file: </b> <br /> {single.file_name}
                                            </div>
                                            <div>
                                                <b>Dung lượng:</b><br />
                                                {single.size}
                                            </div>
                                            <div>
                                                <b>Kích thước:</b><br />
                                                {single.resolution}
                                            </div>
                                            <div className="mt-2">
                                                <label htmlFor="col-form-label">
                                                    <b>Alt Text</b>
                                                </label>
                                                <input
                                                    name="promotion"
                                                    type="text"
                                                    className="form-control m-input"
                                                    value={this.state.single_alt}
                                                    onChange={e =>
                                                        this.handleChangeAtt(e,'alt')
                                                    }
                                                />
                                                <p className="mt-1">Thẻ alt tối ưu SEO hình ảnh</p>
                                            </div>
                                            <div className="">
                                                <label htmlFor="col-form-label">
                                                    <b>Description</b>
                                                </label>
                                                <textarea name="description" className="form-control m-input" onChange={e =>
                                                        this.handleChangeAtt(e,'description')
                                                    } value={this.state.single_desc}></textarea>
                                                <div>
                                                    <label style={{'fontStyle': 'italic','cursor': 'pointer'}} onClick={e => this.handleSaveUpdate(single)}>
                                                        Lưu </label>
                                                        <label className="ilv-margin-media">/</label>
                                                    <span style={{'color':'red','fontStyle': 'italic','cursor': 'pointer'}} onClick={() => this.handleRemoveItem(single)}>
                                                        Xóa hình ảnh
                                                    </span>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                )
                                :
                                isMultiple ?
                                    (
                                        selected.map((item, index) => {
                                            return (
                                                <div key={index} className="ilv-col-6 p-2">
                                                    <img
                                                        className="img-fluid ilv-position-relative ilv-top-50 ilv-transform-translate p-2 ilv-border-1-px-solid h-100 w-100 ilv-object-fit-cover ilv-max-height"
                                                        src={item.link}
                                                        alt=""
                                                    />
                                                </div>
                                            )
                                        })

                                    )
                                    : (null)
                            }

                            <div className="col-12 text-center pt-3">
                            <button
                                onClick={(e) => this.handleSingleChange(e, single, selected)}
                                className="btn btn-primary m-btn m-btn--custom"
                                >
                                Chọn
                                    </button>
                            </div>


                        </div>

                    </div>
                </div>
            </div>
        )
    }
}