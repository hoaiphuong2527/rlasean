import React, { Component } from "react";
import ReactDOM from "react-dom";

export class UploadZone extends Component {
    constructor() {
        super()
        this.state = {
            draggingClass: ''
        }
    }
    handleDrop(e) {
        e.preventDefault();
        let files = e.dataTransfer.items;

        if (files) {
            for (var i = 0; i < files.length; i++) {
                // If dropped items aren't files, reject them
                if (files[i].kind === "file") {
                    let file = files[i].getAsFile();
                    let data = new FormData();
                    data.append("file", file);
                    let endPoint = "/api/upload-image";
                    axios
                        .post(endPoint, data, {
                            headers: {
                                "Content-Type": "multipart/form-data"
                            }
                        })
                        .then(res => {
                            this.props.onAfterUpload(res.data)
                            // let all_files = this.state.allFiles
                            // let uploaded_files = this.state.uploadedFiles
                            // uploaded_files.push(res.data)
                            // all_files.unshift(res.data)
                            // this.setState({
                            //     allFiles: all_files
                            // })
                            // uploaded_files.push( res.data );
                        });
                }
            }
        }
            this.setState({
                draggingClass: ''
            })
    }
    handleDragOver(e) {
        e.preventDefault();
        this.setState({
            draggingClass: 'ilv-dragging'
        })
    }
    handleDragLeave(e) {
        e.preventDefault()
        this.setState({
            draggingClass: ''
        })
    }
    handleUpload(e){
        e.preventDefault();
        let files = e.target.files;
        for (var i = 0; i < files.length; i++) {
            let file = files[i]
            let data = new FormData();
            data.append("file", file);
            let endPoint = "/api/upload-image";
            axios.post(endPoint, data, {
                headers: {
                    "Content-Type": "multipart/form-data"
                }
            })
            .then(res => {
                this.props.onAfterUpload(res.data)
            });
        }
        
    }

    render() {
        const draggingClass = this.state.draggingClass
        return(
            <div
            className={"ilv-draggable " + draggingClass}
            onDrop={e => this.handleDrop(e)}
            onDragOver={e => this.handleDragOver(e)}
            onDragLeave={e => this.handleDragLeave(e)}
        >
            <div className="row justify-content-center text-center">
                <div className="ilv-media-upload-icon mt-5">
                        <i className="fa fa-upload fa-5x"></i>
                </div>
                <div className="col-6 ilv-media-browse my-auto">
                   
                <h3 className="m-portlet__head-text mt-5">
                Chọn hoặc kéo thả ảnh vào đây
                </h3>
                <div className="custom-file mt-2">
                    <input type="file" className="custom-file-input" multiple id="customFile" onChange={e => this.handleUpload(e)} />
                    <label className="custom-file-label">Choose file</label>
                </div>
                </div>
            </div>
        </div>
        )
    }
}