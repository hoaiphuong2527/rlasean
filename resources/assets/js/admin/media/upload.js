import React, { Component } from "react";
import ReactDOM from "react-dom";

import { UploadZone } from './dragger';
import { MediaZone } from './media';
import Requent from "../component/Requent";

export default class UploadMedia extends Component {
    constructor() {
        super()
        this.state = {
            previewLink: '',
            modalClass: 'ie-off',
            activeTab: 'media',
            single: false,
            selected: [],
            allFiles: [],

        }
    }
    componentWillMount() {
        let endPoint = '/api/media'
        axios.get(endPoint).then(res => {
            this.setState({
                allFiles: res.data
            })
        })
        this.setState({
            selected: this.props.previewImage,
            single: this.props.single
        })
    }
    openUpload(e) {
        e.preventDefault()
        this.setState({
            modalClass: ''
        })
    }
    closeUpload(e) {
        e.preventDefault()
        this.setState({
            modalClass: 'ie-off'
        })
    }
    handleTabClick(e, type) {
        e.preventDefault()
        this.setState({
            activeTab: type
        })
    }
    getActive(type) {
        if (this.state.activeTab == type) {
            return 'active'
        }
        return ''
    }
    handleSingleClick(e, item) {
        e.preventDefault()
        this.setState({
            single: item,
            selected:this.state.selected.slice(1)
        })
    }
    handleMultipleSelect(item) {
        this.setState({
            single: item
        })
    }
    handleSingleChange(e, item, selected) {
        if (this.props.type == 'multiple') {
            this.setState({
                selected: selected,
                modalClass: 'ie-off'
            })
        } else {
            this.setState({
                selected: item,
                previewLink: item.link,
                modalClass: 'ie-off'
            })
        }
    }

    handleSaveUpdate(single,item){
        let newAllFiles= Requent.updateObjectInArray(item, single, this.state.allFiles)
        this.setState({
            allFiles: newAllFiles
        })

    }

    handleRemoveItem(item){
        let newAllFiles = Requent.remove(item, this.state.allFiles)
        let newSelected = this.state.selected;
        if((this.state.selected).length){
            newSelected = Requent.remove(item, this.state.selected)
        }
        this.setState({
            allFiles: newAllFiles,
            single:false,
            selected:newSelected
        })

    }


    handleAfterUpload(item) {
        let all_files = this.state.allFiles
        all_files.unshift(item)
        let newArrSelected = this.state.selected
        newArrSelected.push(item)
        this.setState({
            allFiles: all_files,
            activeTab: 'media',
            // single: item,
            selected:newArrSelected,
            isMultiple:true
        })
    }
    render() {
        const previewLink = this.state.previewLink
        const inputName = this.props.inputName
        const hidden = this.props.hidden
        const allFiles = this.state.allFiles
        const single = this.state.single
        const selected = this.state.selected
        const modalClass = this.state.modalClass
        const uploadTabClass = this.getActive('upload')
        const uploadPaneClass = this.getActive('upload')
        const mediaTabClass = this.getActive('media')
        const mediaPaneClass = this.getActive('media')
        const type = this.props.type
        var isMultiple = false
        if (type == 'multiple') {
            isMultiple = true
        }else isMultiple = false
        return (
            <div>
                {(() => {
                    if (isMultiple) {
                        return(
                            selected.map((item, index) => {
                                return (
                                    <div className="ilv-media-img-thumb m-2 ">
                                        <img src={item.link} className="img-fluid" alt="" />
                                        <input type="hidden" name={inputName + '[]'} value={item.id} />
                                    </div>
                                )
                            } 
                        )
                    )
                    } else if (single) {
                    return (
                        <div>
                            <img src={single.link} className="img-fluid ilv-media-img-preview fixed-height" alt="" onClick={(e) => this.openUpload(e)} />
                            <input type="hidden" name={inputName } value={single.id} />
                        </div>
                    )
                    } else {
                    return (
                        <div></div>
                    )
                    }
                })()}
                <div className="mt-2">
                    <button className="btn btn-secondary" onClick={(e) => this.openUpload(e)}> <i className="fa fa-image"></i> Chọn hình ảnh </button>
                </div>
                <div className={"ilv-modal " + modalClass} >
                    <div className="ilv-modal-overlay"></div>
                    <div className="ilv-modal-content">
                        <div className="m-portlet m-portlet--mobile">
                            <div className="m-portlet__head">
                                <div className="m-portlet__head-caption">
                                    <div className="m-portlet__head-title">
                                        <h3 className="m-portlet__head-text">
                                            Thêm hình ảnh
                                        </h3>
                                    </div>
                                </div>
                                <div className="m-portlet__head-tools">
                                    <button
                                        onClick={(e) => this.closeUpload(e)}
                                        className="btn btn-outline-metal m-btn m-btn--icon m-btn--icon-only"
                                    >
                                        <i className="fa fa-times" />
                                    </button>
                                </div>
                            </div>
                            <div className="m-portlet__body">
                                <ul className="nav nav-tabs" role="tablist">
                                    <li className="nav-item">
                                        <a className={"nav-link " + uploadTabClass} onClick={(e) => this.handleTabClick(e, 'upload')} >Tải ảnh lên</a>
                                    </li>
                                    <li className="nav-item">
                                        <a className={"nav-link " + mediaTabClass} onClick={(e) => this.handleTabClick(e, 'media')}>Thư viện ảnh</a>
                                    </li>

                                </ul>
                                <div className="tab-content">
                                    <div className={"tab-pane " + uploadPaneClass} >
                                        <UploadZone
                                            onAfterUpload={(item) => this.handleAfterUpload(item)}
                                        />
                                    </div>
                                    <div className={"tab-pane" + mediaPaneClass} >
                                        <MediaZone
                                            allFiles={allFiles}
                                            single={single}
                                            preview={selected}
                                            onSingleClick={(e, item) => this.handleSingleClick(e, item)}
                                            onSingleChange={(e, item, selected) => this.handleSingleChange(e, item, selected)}
                                            isMultiple={isMultiple}
                                            onMultipleSelect={(item) => this.handleMultipleSelect(item)}
                                            onSaveUpdate={(single,item) => this.handleSaveUpdate(single,item)}
                                            onRemoveItem={single => this.handleRemoveItem(single)}
                                        />
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


if (document.getElementById("react-admin-upload-btn")) {
    var ele = document.getElementById("react-admin-upload-btn")
    ReactDOM.render(
        <UploadMedia
            preview={ele.getAttribute('data-img')}
            inputName={ele.getAttribute('data-input-name')}
            type={ele.getAttribute('data-type')}
            previewImage = {previewImage1}
        />, ele);
}
if (document.getElementById("react-admin-upload-icon-btn")) {
    var ele = document.getElementById("react-admin-upload-icon-btn")
    ReactDOM.render(
        <UploadMedia
            preview={ele.getAttribute('data-img')}
            inputName={ele.getAttribute('data-input-name')}
            type={ele.getAttribute('data-type')}
            previewImage = {previewImage}
            single = {single}
        />, ele);
}
if (document.getElementById("react-admin-upload-img-btn")) {
    var ele = document.getElementById("react-admin-upload-img-btn")
    ReactDOM.render(
        <UploadMedia
            preview={ele.getAttribute('data-img')}
            inputName={ele.getAttribute('data-input-name')}
            type={ele.getAttribute('data-type')}
            previewImage = {previewImage1}
            single = {single1}
        />, ele);
}
if (document.getElementById("react-admin-upload-logo-footer-btn")) {
    var ele = document.getElementById("react-admin-upload-logo-footer-btn")
    ReactDOM.render(
        <UploadMedia
            preview={ele.getAttribute('data-img')}
            inputName={ele.getAttribute('data-input-name')}
            type={ele.getAttribute('data-type')}
            previewImage = {previewImage2}
            single = {singlef}
        />, ele);
}
if (document.getElementById("react-admin-upload-btn-0")) {
    for(var i = 0; i <= 2; i++){
        if(i == 0) {
            var p = previewImage0;
            var s = single0;
        }
        else if(i == 1) {
            var p = previewImage1;
            var s = single1;
        }
        else {
            var p = previewImage2;
            var s = single2;
        }
        if (document.getElementById("react-admin-upload-btn-" + i)) {
            var ele = document.getElementById("react-admin-upload-btn-" + i)
            ReactDOM.render(
                <UploadMedia
                    preview={ele.getAttribute('data-img')}
                    inputName={ele.getAttribute('data-input-name')}
                    type={ele.getAttribute('data-type')}
                    previewImage = {p}
                    single = {s}
                />, ele);
        }
    }
}