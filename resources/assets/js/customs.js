import 'jquery-validation';
import './helper/SimpleMoneyFormat';

$("#signup").click(function () {
    $("#first").fadeOut("fast", function () {
        $("#second").fadeIn("fast");
    });
});

$("#signin").click(function () {
    $("#second").fadeOut("fast", function () {
        $("#first").fadeIn("fast");
    });
});

$(function () {
    $("form[name='login']").validate({
        rules: {

            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
            }
        },
        messages: {
            email: "Please enter a valid email address",

            password: {
                required: "Please enter password",

            }

        },
        submitHandler: function (form) {
            form.submit();
        }
    });
});



$(function () {

    $("form[name='registration']").validate({
        rules: {
            firstname: "required",
            lastname: "required",
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 5
            }
        },

        messages: {
            firstname: "Please enter your firstname",
            lastname: "Please enter your lastname",
            password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long"
            },
            email: "Please enter a valid email address"
        },

        submitHandler: function (form) {
            form.submit();
        }
    });
});
var totalSteps = $(".steps li").length;

$(".submit").on("click", function () {
    return false;
});

$(".steps li:nth-of-type(1)").addClass("active");
$(".myContainer .form-container:nth-of-type(1)").addClass("active");

$(".form-container").on("click", ".next", function () {
    $(".steps li").eq($(this).parents(".form-container").index() + 1).addClass("active");
    $(this).parents(".form-container").removeClass("active").next().addClass("active flipInX");
});

$(".form-container").on("click", ".back", function () {
    $(".steps li").eq($(this).parents(".form-container").index() - totalSteps).removeClass("active");
    $(this).parents(".form-container").removeClass("active flipInX").prev().addClass("active flipInY");
});


/*=========================================================
*     If you won't to make steps clickable, Please comment below code 
=================================================================*/
$(".steps li").on("click", function () {
    var stepVal = $(this).find("span").text();
    $(this).prevAll().addClass("active");
    $(this).addClass("active");
    $(this).nextAll().removeClass("active");
    $(".myContainer .form-container").removeClass("active flipInX");
    $(".myContainer .form-container:nth-of-type(" + stepVal + ")").addClass("active flipInX");
});
$(document).ready(function () {


    var readURL = function (input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.avatar').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }


    $(".file-upload").on('change', function () {
        readURL(this);
    });

    $(".sort-link-name").on('click', function () {
        var valueSort = $(this).attr('value');
        switch (valueSort) {
            case "1":
                {
                    $('#sort-name').val('asc');
                    $('#form_sort').submit();
                    break;
                }
            case "2":
                {
                    $('#sort-name').val('desc');
                    $('#form_sort').submit();
                    break;
                }
        }
    })

    $(".sort-link-price").on('click', function () {
        var valueSort = $(this).attr('value');
        switch (valueSort) {
            case "3":
                {
                    $('#sort-price').val('asc');
                    $('#form_sort').submit();
                    break;
                }
            case "4":
                {
                    $('#sort-price').val('desc');
                    $('#form_sort').submit();
                    break;
                }
        }
    })
});
//made by vipul mirajkar thevipulm.appspot.com
var TxtType = function (el, toRotate, period) {
    this.toRotate = toRotate;
    this.el = el;
    this.loopNum = 0;
    this.period = parseInt(period, 10) || 2000;
    this.txt = '';
    this.tick();
    this.isDeleting = false;
};

TxtType.prototype.tick = function () {
    var i = this.loopNum % this.toRotate.length;
    var fullTxt = this.toRotate[i];

    if (this.isDeleting) {
        this.txt = fullTxt.substring(0, this.txt.length - 1);
    } else {
        this.txt = fullTxt.substring(0, this.txt.length + 1);
    }

    this.el.innerHTML = '<span class="wrap">' + this.txt + '</span>';

    var that = this;
    var delta = 200 - Math.random() * 100;

    if (this.isDeleting) {
        delta /= 2;
    }

    if (!this.isDeleting && this.txt === fullTxt) {
        delta = this.period;
        this.isDeleting = true;
    } else if (this.isDeleting && this.txt === '') {
        this.isDeleting = false;
        this.loopNum++;
        delta = 500;
    }

    setTimeout(function () {
        that.tick();
    }, delta);
};

window.onload = function () {
    var elements = document.getElementsByClassName('typewrite');
    for (var i = 0; i < elements.length; i++) {
        var toRotate = elements[i].getAttribute('data-type');
        var period = elements[i].getAttribute('data-period');
        if (toRotate) {
            new TxtType(elements[i], JSON.parse(toRotate), period);
        }
    }
    // INJECT CSS
    var css = document.createElement("style");
    css.type = "text/css";
    css.innerHTML = ".typewrite > .wrap { border-right: 0.08em solid #fff}";
    document.body.appendChild(css);
};

$(document).ready(function () {
    $('.collapse')
        .on('shown.bs.collapse', function () {
            $(this)
                .parent()
                .find(".fa-plus")
                .removeClass("fa-plus")
                .addClass("fa-minus");
        })
        .on('hidden.bs.collapse', function () {
            $(this)
                .parent()
                .find(".fa-minus")
                .removeClass("fa-minus")
                .addClass("fa-plus");
        });
});
// some scripts

// jquery ready start
$(document).ready(function () {
    // jQuery code




    /* ///////////////////////////////////////

    THESE FOLLOWING SCRIPTS ONLY FOR BASIC USAGE, 
    For sliders, interactions and other

    */ ///////////////////////////////////////


    //////////////////////// Prevent closing from click inside dropdown
    $(document).on('click', '.dropdown-menu', function (e) {
        e.stopPropagation();
    });

    ///////////////// fixed menu on scroll for desctop
    if ($(window).width() > 768) {

        $(window).scroll(function () {
            if ($(this).scrollTop() > 125) {
                $('.navbar-landing').addClass("fixed-top");
            } else {
                $('.navbar-landing').removeClass("fixed-top");
            }
        });
    } // end if

    //////////////////////// Fancybox. /plugins/fancybox/
    if ($("[data-fancybox]").length > 0) { // check if element exists
        $("[data-fancybox]").fancybox();
    } // end if

    //////////////////////// Bootstrap tooltip
    if ($('[data-toggle="tooltip"]').length > 0) { // check if element exists
        $('[data-toggle="tooltip"]').tooltip()
    } // end if

    /////////////////////// Closes the Responsive Menu on Menu Item Click
    $('.navbar-collapse ul li a.page-scroll').click(function () {
        $('.navbar-toggler:visible').click();
    });

    //////////////////////// Menu scroll to section for landing
    $('a.page-scroll').click(function (event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top - 50
        }, 1000);
        event.preventDefault();
    });

    /////////////////  items slider. /plugins/slickslider/
    if ($('.slick-slider').length > 0) { // check if element exists
        $('.slick-slider').slick();
    } // end if

});
// jquery end

function flyToElement(flyer, flyingTo) {
    var $func = $(this);
    var divider = 6;
    var flyerClone = $(flyer).clone();
    $(flyerClone).css({
        position: 'absolute',
        borderRadius: '50%',
        top: $(flyer).offset().top + "px",
        left: $(flyer).offset().left + "px",
        opacity: 1,
        'z-index': 1000
    });
    $('body').append($(flyerClone));
    var gotoX = $(flyingTo).offset().left + ($(flyingTo).width() / 2) - ($(flyer).width() / divider) / 2;
    var gotoY = $(flyingTo).offset().top + ($(flyingTo).height() / 2) - ($(flyer).height() / divider) / 2;
    $(flyerClone).animate({
            opacity: 0.4,
            left: gotoX,
            top: gotoY,
            width: $(flyer).width() / divider,
            height: $(flyer).height() / divider
        }, 700,
        function () {
            $(flyingTo).fadeOut('fast', function () {
                $(flyingTo).fadeIn('fast', function () {
                    $(flyerClone).fadeOut('fast', function () {
                        $(flyerClone).remove();
                    });
                });
            });
        });
}
$(document).ready(function () {
    $('#display-iframe').append($('#display-iframe').attr('data'));
    $('#fly-to-cart').on('click', function () {
        console.log('aaaa')
        $('html, body').animate({
            'scrollTop': $("#react-btn").position().top
        });
        var itemImg = $(this).parents('.no-gutters').find('.img-big-wrap').find('img').eq(0);
        flyToElement($(itemImg), $('#react-btn'));
    });

    /* 1. Visualizing things on Hover - See next part for action on click */
    $('#stars li').on('mouseover', function () {
        var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on
        // Now highlight all the stars that's not after the current hovered star
        $(this).parent().children('li.star').each(function (e) {
            if (e < onStar) {
                $(this).addClass('hover');
            } else {
                $(this).removeClass('hover');
            }
        });
    }).on('mouseout', function () {
        $(this).parent().children('li.star').each(function (e) {
            $(this).removeClass('hover');
        });
    });
    /* 2. Action to perform on click */
    $('#stars li').on('click', function () {
        var onStar = parseInt($(this).data('value'), 10); // The star currently selected
        var stars = $(this).parent().children('li.star');
        console.log(stars);
        for (var i = 0; i < stars.length; i++) {
            $(stars[i]).removeClass('selected');
        }
        for (var i = 0; i < onStar; i++) {
            $(stars[i]).addClass('selected');
        }
        // JUST RESPONSE (Not needed)
        var ratingValue = parseInt($('#stars li.selected').last().data('value'), 10);
        $('#rating-star').val(ratingValue);
    });
    $('#review-form').hide();
    $('#show-review').on('click',function(){
    $('#review-form').slideToggle('slow');
})

    $('.form-comment').hide();
    $('.reply-comment').on('click',function(){
    var parentRow = $(this).parents('.row').first();
    console.log(parentRow.children('form').slideToggle('slow'))

})

    
});
require('./helper/clamp')
