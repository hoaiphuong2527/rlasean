import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import UserDetails from './UserDetails';
import PersonalDetails from './PersonalDetails';
import Confirmation from './Confirmation';
import Success from './Success';
import axios from 'axios';
import langs from '../../../lang/lang.json';
import NumberFormat from 'react-number-format';
import NumericInput from 'react-numeric-input';
import { map, sortBy, findIndex } from 'lodash';
import cities from '../../..//city/cities.json';
import { objectToArray } from '../../../helper/ObjectToArray';

const toArray = (Obj) => map(Obj, (o, key) => ({ ...o, key }));

class MainForm extends Component {
    constructor() {
        super();
        const citiesArray = toArray(cities);
        const citiesArraySort = sortBy(citiesArray, function (city) {
            return city.slug;
        })
        this.state = {
            step: 1,
            myCart:[],
            status_payment:'Off',
            type_order: '',
            promotion_price:'0đ',
            totalPromotion:'',
            promotionMessage:'',
            code:[],
            checkCode:'',
            checkEmail:'',
            langs: langs,
            cities: citiesArraySort,
            selectedCity: citiesArraySort[0],
            selectedDistrict:'',
            selectedWardId: 0,
            DistrictID:'',
            shipFee:'0đ',
            commune:'',
            address:'',
            ward:'',
            city:'',
            district:'',
            name:'',
            email:'',
            phone:'',
            note:'',
            totalBill:'',
            ghtkShippingCost:0,
            ghnShippingCost:0,
            breakableCost:0,
            totalAfter:0,
            payment_type:''
        }
    }
    componentWillMount() { 
        const { selectedCity } = this.state;
        const districts = selectedCity.quan_huyen;
        const districtArray = toArray(districts);
        const value = districtArray[0].name_with_type;
        const dist = this.getDistrictID(districtArray,value)
        var ward = toArray(districtArray[0].xa_phuong)
        this.setState({
            city: selectedCity.name_with_type,
            district:districtArray[0].name_with_type,
            ward:ward[0].name_with_type,
            DistrictID: dist.DistrictID,
            selectedDistrict:dist
        })
        let productCart = '/get-cart'
        axios.get(productCart).then(res => {
            let cartArray = objectToArray(res.data.cart);
            this.setState({
                myCart:cartArray,
                cartItem:res.data.total_item,
            })
        }).then(()=>{
            let totalBill = 0;
            let breakableCost = 0;
            this.state.myCart.map((item) => {
                totalBill = totalBill + parseInt(item.subtotal);
                if(item.options.breakable){
                    breakableCost = breakableCost + (item.price * option.percent_breakable / 100) * item.qty 
                }
            })
            this.setState({
                totalBill,
                breakableCost:breakableCost,
                totalPromotion:totalBill
            })
        })
    }

    componentDidMount() {
    }


    onChangeCity=(e)=> {
        const { cities } = this.state;
        const value = e.target.value
        const index = findIndex(cities, function (city) {
            return city.name_with_type === value;
        })
        
        this.setState({
            selectedCity: cities[index],
            city:value
        })
    }

    onChangeWard =(e)=> {
        const { selectedCity } = this.state;
        const districts = selectedCity.quan_huyen;
        const districtArray = toArray(districts);
        const value = e.target.value;
        const dist = this.getDistrictID(districtArray,value)
        const index = findIndex(districtArray, function (district) {
            return district.name_with_type === value;
        })
        this.calculatorGhnShipFee(dist)
        this.setShipCost()

        this.setState({ 
            selectedDistrict:dist,
            selectedWardId: index,
            district:value
        })
    }

    getDistrictID = (districtArray,value) =>{
        var district = districtArray.pop();
        districtArray.map((item, index) => {
            if(item.name_with_type == value)
            district = item
        });
        return district
    }

    calculatorGhnShipFee(dist){
        let serviceUrl = `${'https://console.ghn.vn/api/v1/apiv3/FindAvailableServices'}`
        const formId = option.district_id
        let totalWeight = 0
        this.state.myCart.map((item) => {
            totalWeight = totalWeight + item.weight
        })
        let data = {
            "token": "5ca5c8dd94c06b4f230c372e",
            "Weight": totalWeight,
            "Length": 0,
            "Width": 0,
            "Height": 0,   
            "FromDistrictID": parseInt(formId),
            "ToDistrictID": dist.DistrictID
        }
        fetch(serviceUrl, {
        method: 'POST',
        headers: new Headers({
                    'Content-Type': 'application/x-www-form-urlencoded', // <-- Specifying the Content-Type
            }),
        body: JSON.stringify(data) // <-- Post parameters
        })
        .then((response) => response.text())
        .then((responseText) => {
            let responseInfo = JSON.parse(responseText).data[0]
            this.setState({
                ghnShippingCost: responseInfo.ServiceFee,
                DistrictID: dist.DistrictID,
            })
        })
        .catch((error) => {
            console.error(error);
        });
    }

    handleClick=(e)=>{
        e.preventDefault();
        let totalAfter = e.target.getAttribute('total-after')
        this.setState({
            totalAfter:totalAfter
        })
    }

    handleCheckCoupon = (e)=>{
            var data = {
                checkCode:e.target.value,
                checkEmail:''
            };
            var endpoint ='/api/check-email'
            axios.post(endpoint,data).then(res => {
            let code = res.data.code;
            let totalPromotion = 0;
                this.setState({
                    code,
                    message:res.data.message
                })
                    var message = this.state.message;
                    document.getElementById('noti').innerText=''+message;
            if(res.data.resultCode == 1 && code.reduction_type =='absolute')
            {
                document.getElementById('noti').classList.remove('text-danger')

                totalPromotion = this.state.totalBill - code.promotion_price;
                this.setState({
                    promotion_price:code.promotion_price + 'đ',
                    totalPromotion,
                })
            } else if(res.data.resultCode == 1 && code.reduction_type =='percent')            
            {
                document.getElementById('noti').classList.remove('text-danger')

                totalPromotion = this.state.totalBill - (this.state.totalBill*(code.promotion_price /100));
                this.setState({
                    promotion_price:code.promotion_price + '%',
                    totalPromotion
                })
            } else {
                document.getElementById('noti').classList.add('text-danger')

                totalPromotion = this.state.totalBill;
                this.setState({
                    promotion_price:'0đ',
                    totalPromotion
                })
            }
        })
    }

    handleClickAmountInc =(e) => {
        e.preventDefault()
        var productId = e.target.getAttribute('product-id');
        var rowId = e.target.getAttribute('row-id');
        var data = {
            'product_id': productId,
            'rowId': rowId,
        }

        var productEndpoint = `/api/get-variant/${productId}`
        axios.get(productEndpoint).then(res => {
            this.state.myCart.map((item) => {
                if(productId == item.id)
                {
                    if(item.qty < res.data.intoventory)
                    {
                        axios.post('/add-product-into-cart/increment', data).then(res => {
                            var cartArray = objectToArray(res.data.cart)
                            var breakableCost = 0;
                            cartArray.map((item) => {
                                if(item.options.breakable){
                                    breakableCost = breakableCost + (item.price * option.percent_breakable / 100) * item.qty 
                                }
                            })
                            this.setState({
                                breakableCost,
                                myCart:cartArray,
                                cartItem:res.data.total_item,
                            })
                        }).then(()=>{
                            ReactDOM.render(<ModalCart shoppingCart = {this.state.myCart} itemOnIncrease={this.handleClickAmountInc} itemOnDecrease={this.handleClickAmountDec} itemOnDelete={this.handleDeleteItem} countItem={this.state.cartItem} />, 
                                document.getElementById('shopping-cart'));
                            ReactDOM.render(
                                <span className="cart-products-count">{this.state.cartItem}</span>, 
                                document.getElementById('react-btn'));
                        })                    }
                }
            })
        })
    }

    handleClickAmountDec =(e)=> {
        e.preventDefault()
        var productId = e.target.getAttribute('product-id');
        var rowId = e.target.getAttribute('row-id');
        let data = {
            'product_id': productId,
            'rowId': rowId,
        }
        axios.post('/add-product-into-cart/decrease', data).then(res => {
            let cartArray = objectToArray(res.data.cart)
            var breakableCost = 0;
            cartArray.map((item) => {
                if(item.options.breakable){
                    breakableCost = breakableCost + (item.price * option.percent_breakable / 100) * item.qty 
                }
            })
            this.setState({
                breakableCost,
                myCart:cartArray,
                cartItem:res.data.total_item,
            })
        }).then(()=>{
            ReactDOM.render(<ModalCart shoppingCart = {this.state.myCart} itemOnIncrease={this.handleClickAmountInc} itemOnDecrease={this.handleClickAmountDec} itemOnDelete={this.handleDeleteItem} countItem={this.state.cartItem} />, 
            document.getElementById('shopping-cart'));
            ReactDOM.render(
                <span className="cart-products-count">{this.state.cartItem}</span>, 
                document.getElementById('react-btn'));
        })
    }

    handleChange =(e,productId,rowId,value) =>{
        let data = {
            'product_id': productId,
            'rowId': rowId,
        }
        var endPoint = '';
        if(value > e)
        {
            endPoint = '/add-product-into-cart/decrease'
        }
        else{
            endPoint = '/add-product-into-cart/increment'
        }
        axios.post(endPoint,data).then(res => {
            let cartArray = objectToArray(res.data.cart)
            var breakableCost = 0;
            cartArray.map((item) => {
                if(item.options.breakable){
                    breakableCost = breakableCost + (item.price * option.percent_breakable / 100) * item.qty 
                }
            })
            this.setState({
                breakableCost,
                myCart:cartArray,
                cartItem:res.data.total_item,
            })
        }).then(()=>{
            let totalBill = 0;
            var breakableCost = 0;
            this.state.myCart.map((item) => {
                totalBill = totalBill + parseInt(item.subtotal);
                if(item.options.breakable){
                    breakableCost = breakableCost + (item.price * option.percent_breakable / 100) * item.qty 
                }
            })
            this.setState({
                breakableCost,
                totalBill,
                totalPromotion:totalBill + breakableCost
            })
            ReactDOM.render(
                <span className="cart-products-count">{this.state.cartItem}</span>, 
                document.getElementById('react-btn'));
        })
    }

    handleDeleteItem = (e) =>{
        let data = {
            'product_id': e.target.getAttribute('product-id'),
            'rowId': e.target.getAttribute('row-id')
        }
        axios.post('/remove-product',data).then(res => {
            let cartArray = objectToArray(res.data.cart)
            this.setState({
                myCart:cartArray,
                cartItem:res.data.total_item,
            })
        }).then(()=>{
            ReactDOM.render(<ModalCart shoppingCart = {this.state.myCart} itemOnIncrease={this.handleClickAmountInc} itemOnDecrease={this.handleClickAmountDec} itemOnDelete={this.handleDeleteItem} countItem={this.state.cartItem} />, 
            document.getElementById('shopping-cart'));
            ReactDOM.render(
                <span className="cart-products-count">{this.state.cartItem}</span>, 
                document.getElementById('react-btn'));
        })
    }

    handleChangeInfo = input => event => {
        this.setState({ [input] : event.target.value })
    }

    nextStep = () => {
        const { step } = this.state
        if(step == 2){
            const { city, district, ward ,address} = this.state;
            this.calculatorGhtkShippingCost(city,district,address+','+ward)
            this.calculatorGhnShipFee(city)
            this.setShipCost()
        }
        this.setState({
            step : step + 1
        })
    }

    prevStep = () => {
        const { step } = this.state
        this.setState({
            step : step - 1
        })
    }

    setShipCost(){
        let shipFee = this.state.shipFee
        if(this.state.ghnShippingCost < this.state.ghtkShippingCost)
            shipFee = this.state.ghtkShippingCost
        else shipFee = this.state.ghnShippingCost

        this.setState({
            shipFee: shipFee
        })
    }

    calculatorGhtkShippingCost(province,district,address){
        var totalWeight = 0; 
        this.state.myCart.map((item) => {
            totalWeight = totalWeight + item.weight
        })
        let data = {
            province : province,
            district : district,
            address : address,
            // province: "Hà nội",
            // district : "Quận Cầu Giấy",
            // address : "P.503 tòa nhà Auu Việt, số 1 Lê Đức Thọ",
            weight : totalWeight,
            value : this.state.totalBill,
        }
        let endpoint = '/api/calculator-ghtk-shipping-cost';
        var ghtkShippingCost = 0;
        axios.post(endpoint,data).then(res => {
            var fee = res.data.pop().fee
            ghtkShippingCost = fee.fee + fee.insurance_fee
            this.setState({
                ghtkShippingCost: ghtkShippingCost
            })


        })
    }

    render(){
        var csrf = document.getElementById('token_form').children[0].getAttribute('value');

        const {step} = this.state;
        const { cities, selectedCity, selectedWardId } = this.state;
        const districts = selectedCity.quan_huyen;
        const districtArray = toArray(districts);
        const districtArraySort = sortBy(districtArray, function (item) {
            return item.slug;
        })
        const wards = districtArray[selectedWardId].xa_phuong;
        const wardsArray = toArray(wards);

        const { myCart, status_payment, type_order, promotion_price, totalPromotion, promotionMessage,code, checkCode
            , checkEmail, langs, DistrictID, shipFee,selectedWard,selectedCityString,name,
            email,
            phone,
            note, totalBill, selectedDistrict,ghtkShippingCost,ghnShippingCost,commune,city,ward,address,district,breakableCost,totalAfter } = this.state;
        const values = { myCart, status_payment, type_order, promotion_price, totalPromotion, promotionMessage,code, checkCode
            , checkEmail, langs, cities, selectedCity, selectedWardId, DistrictID, shipFee,districtArraySort,wardsArray,selectedWard,selectedCityString, name,email,phone,totalBill,selectedDistrict,
            note,ghtkShippingCost,ghnShippingCost,commune,city,ward,address,district,breakableCost,totalAfter,csrf};
        switch(step) {
        case 1:
            return <UserDetails 
                    nextStep={this.nextStep} 
                    handleChange = {this.handleChange}
                    handleCheckCoupon = {this.handleCheckCoupon}
                    handleDeleteItem = {this.handleDeleteItem}
                    values={values}
                    />
        case 2:
            return <PersonalDetails 
                    nextStep={this.nextStep}
                    prevStep={this.prevStep}
                    values={values}
                    onChangeWard={this.onChangeWard}
                    onChangeCity={this.onChangeCity}
                    handleChangeInfo={this.handleChangeInfo}
                    calculatorGhnShipCost={this.calculatorGhnShipFee}
                    calculatorGhtkShipCost={this.calculatorGhtkShipCost}
                    setShipCost={this.setShipCost}
                    csrf={csrf}
                    />
        case 3:
            return <Confirmation 
                    nextStep={this.nextStep}
                    prevStep={this.prevStep}
                    values={values}
                    handleClick={this.handleClick}
                    csrf={csrf}
                    />
        case 4:
            return <Success />
        }
    }
}

export default MainForm;