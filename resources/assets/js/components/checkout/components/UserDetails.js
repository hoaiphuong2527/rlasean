import React, { Component } from 'react';
import { Form, Button } from 'semantic-ui-react';
import NumberFormat from 'react-number-format';
import NumericInput from 'react-numeric-input';
class UserDetails extends Component{

    saveAndContinue = (e) => {
        e.preventDefault()
        this.props.nextStep()
    }

    render(){
        const { values } = this.props;
        const myCart =values.myCart;
        var total = 0;
        return(
            <div className="container my-5 ilv-container-benefit pt-4 pb-4 ">
            <div className="row">
                <div className="col-md-8">
                    <h4 className="d-flex justify-content-between align-items-center mb-3">
                        <span className="text-muted">{values.langs[locale]['your_cart']}</span>
                    </h4>
                    <div className="shopping-cart">
                        <div className="column-labels">
                        <label className="product-image">{values.langs[locale]['image']}</label>
                        <label className="product-details">{values.langs[locale]['product']}</label>
                        <label className="product-price">{values.langs[locale]['price']}</label>
                        <label className="product-quantity">{values.langs[locale]['amount']}</label>
                        <label className="product-removal">{values.langs[locale]['remove']}</label>
                        <label className="product-line-price">{values.langs[locale]['summary']}</label>
                        </div>
                        {
                            myCart.map((item) => {
                            total = total + parseInt(item.subtotal);
                            return(
                                <div className="product" key={item.rowId}>
                                <div className="product-image">
                                    <img src={item.options.image}/>
                                </div>
                                <div className="product-details">
                                    <div className="product-title">{item.name}</div>
                                    <p className="product-description"></p>
                                </div>
                                <div className="product-price"><NumberFormat value={item.price} displayType={'text'} thousandSeparator={true}  /></div>
                                <div className="product-quantity">
                                <NumericInput 
                                            min={ 0 } 
                                            size={ 6} 
                                            mobile = {true}
                                            name="qty" 
                                            value= {item.qty}
                                            onChange = { (e,productId,rowId,value) => this.props.handleChange(e,item.id,item.rowId,item.qty) }
                                            />
                                </div>
                                <div className="product-removal">
                                    <button className="remove-product" product-id={item.id} row-id={item.rowId} onClick={(e) => this.props.handleDeleteItem(e)}>
                                    <i className="fa fa-trash"></i>
                                    </button>
                                </div>
                                    <div className="product-line-price">
                                    <NumberFormat value={item.subtotal} displayType={'text'} thousandSeparator={true}  />
                                    </div>
                                </div>
                                )
                            })
                        }
                    </div>
                </div>
                <div className="col-md-4 order-md-2 mb-4">
                    <h4 className="d-flex justify-content-between align-items-center mb-3">
                        <span className="text-muted">{values.langs[locale]['total_order']}</span>
                        <span className="badge badge-secondary badge-pill">{values.cartItem}</span>
                    </h4>
                    <ul className="list-group mb-3">
                        <li className="list-group-item d-flex justify-content-between">
                            <span>{values.langs[locale]['total']} (VND)</span>
                            <small><NumberFormat value={values.totalBill} displayType={'text'} thousandSeparator={true}  /> </small>
                        </li>
                        <li className="list-group-item d-flex justify-content-between">
                            <span>{values.langs[locale]['disscount']}:</span>
                            <small>{values.promotion_price}</small>
                        </li>
                        {/* <li className="list-group-item d-flex justify-content-between">
                            <span>{values.langs[locale]['ship']}</span>
                            <strong><NumberFormat value={values.shipFee} displayType={'text'} thousandSeparator={true}  />đ</strong>
                        </li>
                        {  
                            values.breakableCost !== 0  && 
                            (<li className="list-group-item d-flex justify-content-between">
                            <span>{values.langs[locale]['breakable_cost']}</span>
                            <strong><NumberFormat value={values.breakableCost} displayType={'text'} thousandSeparator={true}  />đ</strong>
                            </li>)
                        } */}
                        <li className="list-group-item d-flex justify-content-between">
                            <span>{values.langs[locale]['after_disscount']}</span>
                            <strong><NumberFormat value={values.totalPromotion} displayType={'text'} thousandSeparator={true}  />đ</strong>
                        </li>
                    </ul>
                    <div className="card p-2">
                        <div className="input-group">
                            <input type="text" className="form-control" id="coupon"  placeholder={values.langs[locale]['coupon']}  onChange={(e) => this.props.handleCheckCoupon(e)}/>
                            <div className="input-group-append">
                            </div>
                        </div>
                    </div>
                    <div className=" d-flex justify-content-between">
                        <label id="noti" className="text-success"></label>
                    </div>
                </div>
            </div>
            <div className="ilv-center-block width-mc">
                <Button className="btn btn-outline-primary button-animate" onClick={this.saveAndContinue}>{values.langs[locale]['next']}</Button>
            </div>
            </div>
        )
    }
}

export default UserDetails;