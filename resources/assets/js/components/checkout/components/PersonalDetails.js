import React, { Component } from 'react';
import { Form, Button } from 'semantic-ui-react';
import { throws } from 'assert';

class PersonalDetails extends Component{
    saveAndContinue = (e) => {
        e.preventDefault();
        this.props.nextStep();
    }

    back  = (e) => {
        e.preventDefault();
        this.props.prevStep();
    }

    render(){
        const { values } = this.props
        const { cities, selectedCity, selectedWardId,districtArraySort,wardsArray } = values;
        return(
            <div className="container my-5 ilv-container-benefit pt-4 pb-4 ">
            <div className="row">
                    <div className="col-md-12 order-md-1">
                        <h4 className="mb-3">{values.langs[locale]['address']}</h4>
                        <form id="form-info-order" action={values.langs[locale]['get_route']} method="post" className="needs-validation" noValidate="">
                        <div className="row">
                                <input type="hidden" name="_token" value={this.props.csrf} />
                                <input type="hidden" name="total" value={values.totalBill} />
                                <input type="hidden" name="total_after" value={values.totalAfter} />
                                <input type="hidden" name="codeName" value={values.code.promotion} />
                                <input type="hidden" name="shipping_fee" value={values.shipFee} />
                                <input type="hidden" name="breakable_cost" value={values.breakableCost}/>
                            <div className=" mb-3 col-md-6">
                                <label htmlFor="firstName">{values.langs[locale]['full_name']}</label>
                                <input type="text" className="form-control" id="firstName" placeholder={values.langs[locale]['full_name']}  required="" name="customer_name" onChange={this.props.handleChangeInfo('name')}/>
                                <div className="invalid-feedback">
                                {values.langs[locale]['errors']['name']}
                                </div>
                                <label htmlFor="email">Email</label>
                                <input type="email" className="form-control" id="email" placeholder="Email" name="customer_email" onChange={this.props.handleChangeInfo('email')}/>
                                <div className="invalid-feedback">
                                {values.langs[locale]['errors']['email']}
                                </div>
                                <label htmlFor="phone">{values.langs[locale]['phone']}</label>
                                <input type="number" className="form-control" id="phone" placeholder={values.langs[locale]['phone']} name="customer_phone" onChange={this.props.handleChangeInfo('phone')}/>
                                <div className="invalid-feedback">
                                {values.langs[locale]['errors']['phone']}
                                </div>
                            </div>
                            <div className=" mb-3 col-md-6">
                                <label htmlFor="firstName">{values.langs[locale]['province']}</label>
                                <select
                                    className="form-control"
                                    name="province"
                                    id="province"
                                    onChange={(e) => this.props.onChangeCity(e)}
                                >
                                    {cities.map(function (city) {
                                        return <option key={city.key} value={city.name_with_type}>{city.name_with_type}</option>
                                    })}
                                </select>
                                <label htmlFor="firstName">{values.langs[locale]['district']}</label>
                                <select
                                    className="form-control"
                                    name="district"
                                    id="district"
                                    onChange={(e) => this.props.onChangeWard(e)}
                                >
                                    {districtArraySort.map(function (district) {
                                        return <option key={district.key} value={district.name_with_type}>{district.name_with_type}</option>
                                    })}
                                </select>
                                <label htmlFor="firstName">{values.langs[locale]['ward']}</label>
                                <select className="form-control" name="ward" onChange={this.props.handleChangeInfo('ward')}>
                                    {wardsArray.map(function (ward) {
                                        return <option key={ward.key} id="ward" value={ward.name_with_type}>{ward.name_with_type}</option>
                                    })}
                                </select>
                                <label htmlFor="address">{values.langs[locale]['address']}</label>
                                <input type="text" className="form-control" id="stress" placeholder={values.langs[locale]['address']} required="" name="stress" onChange={this.props.handleChangeInfo('address')}/>
                                <div className="invalid-feedback">
                                    {values.langs[locale]['errors']['address']}
                                </div>
                            </div>
                            <div className="mb-3 col-md-12">
                            <label htmlFor="address">{values.langs[locale]['note']}</label>
                                    <textarea className="form-control" placeholder={values.langs[locale]['note']} name="note" onChange={this.props.handleChangeInfo('note')}></textarea>
                                    </div>                           
                                    <div className="mb-3 col-md-12">
                                <select   className="form-control"  placeholder="Phương thức thanh toán" name="payment_type" value={values.status_payment} onChange={(e) => this.props.handleChangeInfo('status_payment')}>
                                        <option value="Off">
                                        {values.langs[locale]['payment_off']}
                                        </option>
                                        {  
                                            values.type_order === 'HP' && 
                                            (<option value="On" id="online">{values.langs[locale]['payment_on']}</option>)
                                        }
                                    </select>
                                    </div>
                                </div>
                                <hr className="mb-4"/>
                        </form>
                    </div>
                </div>
                <div className="ilv-center-block width-mc">
                <Button className="btn btn-outline-danger button-animate mx-2" onClick={this.back}>{values.langs[locale]['back']}</Button>
                <Button className="btn btn-outline-success button-animate" onClick={this.saveAndContinue}>{values.langs[locale]['next']}</Button>
                </div>
            </div>
        )
    }
}

export default PersonalDetails;