import React, { Component } from 'react';
import { Button, List } from 'semantic-ui-react';
import NumberFormat from 'react-number-format';

class Confirmation extends Component{
    saveAndContinue = (e) => {
        e.preventDefault();
        this.props.nextStep();
    }

    back  = (e) => {
        e.preventDefault();
        this.props.prevStep();
    }

    setShippingCost(ghtkShippingCost,ghnShippingCost){
        let shipFee = 0
        if(ghnShippingCost < ghtkShippingCost)
            shipFee = ghtkShippingCost
        else shipFee = ghnShippingCost
        return shipFee
    }


    render(){
        const { values } = this.props
        const { address,commune,ward,city,district, name,email,phone,totalBill,selectedDistrict,note,totalPromotion,ghtkShippingCost,ghnShippingCost,status_payment} = values;
        const shippingFree = this.setShippingCost(ghtkShippingCost,ghnShippingCost)
        const totalAfter = parseInt(shippingFree) + parseInt(totalPromotion) + parseInt(values.breakableCost)
        return(
            <div className="container my-5 ilv-container-benefit pt-4 pb-4 ">
            <div className="row">
                    <div className="col-md-8 box">
                        <div className="">
                            <dl>
                            <dt><h3>{values.langs[locale]['your_info']}</h3>
                            <hr/>
                            </dt>
                            </dl>
                            <dl>
                            <dt>{values.langs[locale]['full_name']}: </dt>
                            <dd>{name}</dd>
                            </dl>
                            <dl>
                            <dt>{values.langs[locale]['address1']}: </dt>
                            <dd>
                            {`${address},${ward},${district},${city}`}
                            </dd>
                            </dl>
                            <dl>
                            <dt>{values.langs[locale]['phone']}:</dt>
                            <dd>{phone}</dd>
                            </dl>
                            <dt>{values.langs[locale]['note']}:</dt>
                            <dd>{note}</dd>
                            
                        </div> 
                    </div>
                    <div className="col-md-4 box">
                        <div className="">
                            <dl>
                            <dt><h3>{values.langs[locale]['order_info']}:</h3>
                            <hr/>
                            </dt>
                            </dl>
                            <dl>
                            <dt>{values.langs[locale]['total']}: <NumberFormat value={totalBill} displayType={'text'} thousandSeparator={true}/>đ</dt>
                            <dd></dd>
                            </dl>
                            <dl>
                            <dt>{values.langs[locale]['ship']}: <NumberFormat value={shippingFree} displayType={'text'} thousandSeparator={true}/>đ </dt>
                            <dd></dd>
                            </dl>
                            {  
                                values.breakableCost !== 0  && 
                                (
                                    <dl>
                                    <dt>{values.langs[locale]['breakable_cost']}: <NumberFormat value={values.breakableCost} displayType={'text'} thousandSeparator={true}/>đ</dt>
                                    <dd></dd>
                                    </dl>
                                )
                            }
                            <dl>
                            <dt>{values.langs[locale]['total_order']}: <NumberFormat value={totalAfter} displayType={'text'} thousandSeparator={true}/>đ</dt>
                            <dd></dd>
                            </dl>
                        </div> 
                    </div>
                </div>
                <div className="my-3 row">
                <form id="form-info-order" action={values.langs[locale]['get_route']} method="post" className="needs-validation col-12" noValidate="">
                    <input type="hidden" name="_token" value={this.props.csrf} />
                    <input type="hidden" name="total" value={values.totalBill} />
                    <input type="hidden" name="total_after" value={totalAfter} />
                    <input type="hidden" name="codeName" value={values.code.promotion} />
                    <input type="hidden" name="shipping_fee" value={values.shipFee} />
                    <input type="hidden" name="breakable_cost" value={values.breakableCost}/>
                    <input type="hidden" name="customer_name" value={name} />
                    <input type="hidden" name="customer_email" value={email} />
                    <input type="hidden" name="customer_phone" value={phone} />
                    <input type="hidden" name="province" value={city} />
                    <input type="hidden" name="district" value={district} />
                    <input type="hidden" name="ward" value={ward}/>
                    <input type="hidden" name="payment_type" value={status_payment} />
                    <input type="hidden" name="address" value={address} />
                    <div className="ilv-center-block width-mc">
                    <Button className="btn btn-outline-danger button-animate mx-2 " onClick={this.back}>{values.langs[locale]['back']}</Button>
                    <input type="submit" value={values.langs[locale]['submit_order']} className="btn btn-outline-primary button-animate"/>
                    </div>
                </form>
                </div>
            </div>
        )
    }
}

export default Confirmation;