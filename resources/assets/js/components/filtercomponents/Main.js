import React from 'react'
import { Switch, Route } from 'react-router-dom'

import { objectToArray } from '../../helper/ObjectToArray'

class Main  extends React.Component {
    constructor(props) {
        super(props);
        this.state={
            cateInformations:[
            ],
            dataProduct:[
            ],
            dataFilter:{
                arr_categories:[],
                arr_prices:[],
                arr_informations:[],
                arr_categories_master:[]
            },
            filterLabel:[
            ],
            currentLink:''
            
        }
        this.handleChecked = this.handleChecked.bind(this);
        this.handleCheckPrice = this.handleCheckPrice.bind(this);
        this.handleLableChange = this.handleLableChange.bind(this);
    }
    componentWillReceiveProps(nextProps){
        let data = {
            dataFilter:{
                arr_categories:[nextProps.match.params.id],
                arr_prices:[],
                arr_informations:[],
                arr_categories_master:[]
            },
        }
        let productEndpoint = `/api/categories/products/filter`
        axios.post(productEndpoint,data).then(res => {
            this.setState({
                dataProduct:res.data.products,
                currentLink:nextProps.match.params.id,
            })
        });
        let cateInforEndpoint = `/api/categories/${nextProps.match.params.id}/informations`;
        axios.get(cateInforEndpoint).then(res => {
            var cateInformations = objectToArray(res.data.informations)
            function getUnique(arr, comp) {
                const unique = arr
                    .map(e => e[comp])
                .map((e, i, final) => final.indexOf(e) === i && i)
                .filter(e => arr[e]).map(e => arr[e]);
                return unique;
            };
            cateInformations = getUnique(cateInformations,'key');
            this.setState({
                cateInformations,
                dataFilter:data.dataFilter
            })
        });
        var currentName =  document.getElementById(`${nextProps.match.params.id}`)
            console.log(this.state)
            console.log($('.breadcrumb-item.active'))
    }
    componentDidMount(){
        console.log()
        this.setState({
            currentLink:this.props.computedMatch.params.id
        }),function(){
            var currentName =  document.getElementById(`${this.state.currentLink}`)
            console.log(this.state)
            console.log($('.breadcrumb-item.active'))
        }
    }
    handleLableChange (e) {
        e.target.children[0].firstChild.innerText
        var checkedArray = this.state.dataFilter.arr_informations;
        var labelArray = this.state.filterLabel;
        var lable = {
            text:e.target.children[0].firstChild.innerText,
            id:e.target.value
        }
            function arrayRemove(arr, value) {
                return arr.filter(function(ele){
                    return ele != value;
                });
            }
            function arrayRemoveObject(arr, value) {
                return arr.filter(function(ele){
                    return ele.id != value.id;
                });
            }
            checkedArray = arrayRemove(checkedArray, lable.id);
            labelArray = arrayRemoveObject(labelArray, lable);
        let uniqueArr =[...(new Set(checkedArray))];
        let uniqueLableArr =[...(new Set(labelArray))];
        this.setState({
            dataFilter:{
                    ...this.state.dataFilter,
                    arr_informations:uniqueArr
            } ,
            filterLabel:uniqueLableArr
        },function () {
            var data = {
                dataFilter:this.state.dataFilter
            }
            let productEndpoint = `/api/categories/products/filter`
            axios.post(productEndpoint,data).then(res => {
                this.setState({
                    dataProduct:res.data.products,
                })
            });
        });
    }
    handleCheckPrice (e) {
        let minPrice = document.getElementById('min_price').value ? document.getElementById('min_price').value: '1';
        let maxPrice = document.getElementById('max_price').value ? document.getElementById('max_price').value: '1';

        console.log(minPrice);
        let dataPrice =[minPrice,maxPrice];

        this.setState({
            dataFilter:{
                    ...this.state.dataFilter,
                    arr_prices:dataPrice
            } 
        },function () {
            var data = {
                dataFilter:this.state.dataFilter
            }
            console.log(data);
            let productEndpoint = `/api/categories/products/filter`
            axios.post(productEndpoint,data).then(res => {
                this.setState({
                    dataProduct:res.data.products,
                })
            });
        });
    }

    handleChecked (e) {
        var checkedArray = this.state.dataFilter.arr_informations;
        var labelArray = this.state.filterLabel;
        var lable = {
            text:e.target.nextElementSibling.innerText,
            id:e.target.value
        }
        if(e.target.checked){
            labelArray.push(lable)
            checkedArray.push(e.target.value)
        }
        else {
            function arrayRemove(arr, value) {
                return arr.filter(function(ele){
                    return ele != value;
                });
            }
            function arrayRemoveObject(arr, value) {
                return arr.filter(function(ele){
                    return ele.id != value.id;
                });
            }
            checkedArray = arrayRemove(checkedArray, e.target.value);
            labelArray = arrayRemoveObject(labelArray, lable);
        }
        let uniqueArr =[...(new Set(checkedArray))];
        let uniqueLableArr =[...(new Set(labelArray))];
        this.setState({
            dataFilter:{
                    ...this.state.dataFilter,
                    arr_informations:uniqueArr
            } ,
            filterLabel:uniqueLableArr
        },function () {
            var data = {
                dataFilter:this.state.dataFilter
            }
            let productEndpoint = `/api/categories/products/filter`
            axios.post(productEndpoint,data).then(res => {
                this.setState({
                    dataProduct:res.data.products,
                })
            });
        });
    }

    render() {
        const informations = this.state.cateInformations;
        const dataProduct = this.state.dataProduct;
        const arrPriceFilter = objectToArray(priceFilter);
        const filterLabel = this.state.filterLabel;
        return(
            <div className="row">
            <div className="col-md-3">
            {this.props.sideBar}
            {
                informations.map((info,index)=>{
                    return(
                        <div className="card" key={index}>
                            <article className="card-group-item">
                                <header className="card-header">
                                    <h6 className="title">{info.name}</h6>
                                </header>
                                <div className="card-body">
                                {
                                    info.params.map((param,index)=>{
                                        return(
                                            <label className="form-check" key={index}>
                                            <input className="form-check-input" type="checkbox" 
                                            value={param.id} onChange={ (e) => this.handleChecked(e) }/>
                                            <span className="form-check-label">
                                                {param.value}
                                            </span>
                                            </label> 
                                        )
                                    })
                                }
                                </div> 
                            </article> 
                        </div> 
                    )
                })
            }
            <div className="card">
                <article className="card-group-item">
                    <header className="card-header">
                        <h6 className="title">Giá</h6>
                    </header>
                    <div className="card-body">
                        <div className="form-row">
                            <div className="form-group col-md-6">
                            <label>Min</label>
                            <input type="number" className="form-control" id="min_price" placeholder=""/>
                            </div>
                            <div className="form-group col-md-6 text-right">
                            <label>Max</label>
                            <input type="number" className="form-control" id="max_price" placeholder=""/>
                            </div>
                            <div className="form-group col-md-12">
                            <a className="btn-outline-primary btn"  placeholder="" onClick={ (e) => this.handleCheckPrice(e) }>Gửi</a>
                            </div>
                        </div>
                    </div> 
                </article> 
            </div> 
            </div>
            <div className="col-md-9">
                <div className="d-flex">
                {
                    filterLabel.map((label,index)=>{
                        return (
                            <div className="lable-btn">
                                <button value={label.id} onClick={ (e) => this.handleLableChange(e) } className="social-container twitter">
                                    <div className="social-cube">
                                    <div className="front">
                                        {label.text}
                                    </div>
                                    <div className="bottom">
                                        <i className="fa fa-trash"></i>
                                    </div>
                                </div>
                                </button>                        
                            </div>
                        )
                    })
                }
                </div>
                <div className="row">
                    {
                        dataProduct.map((item,index)=>{
                            let colorToShow = 
                            <div className="row">
                            <div className="ilv-center-block">
                                {
                                    item.product_variant_onlines.map((variant,index)=>{
                                    var elStype = {backgroundColor:variant.color}
                                    return <span key={index} className="ilv-color-tag mx-1" style={elStype}>&nbsp;</span>
                                })
                            }
                            </div>
                            </div>
                            let discountVal = '';
                            let priceToShow = 
                            <div className="price-wrap h5">
                                <span className="price-new ">{item.default_price}</span> 
                                <del className="price-old text-white">&nbsp;</del>
                            </div> 
                            if(item.discount_value){
                                discountVal = 
                                <span className="badge-offer">
                                    <b>{item.discount_value} </b>
                                </span>
                                priceToShow = 
                                <div className="price-wrap h5 ">
                                    <span className="price-new ">{item.discount_price_display}</span> 
                                    <del className="price-old ">{item.default_price}</del>
                                </div>
                            }
                            return(
                                <div  key={index} className="col-md-4 px-1 pb-1">
                                    <a href={item.url}>
                                        <figure className="card card-product">
                                            {discountVal}
                                            <div className="img-wrap">
                                                <img src={item.images[0]}/>
                                            </div>
                                            <figcaption className="info-wrap">
                                                    <h4 className="title">{item.name}</h4>
                                            </figcaption>
                                            <div className="bottom-wrap">
                                                {priceToShow}
                                                {colorToShow}
                                            </div>
                                        </figure>
                                    </a>
                                </div> 
                            )
                        })
                    }
                </div>
            </div>
        </div>
        );
    }
}
export default Main

