import React from 'react'
import Main from './Main'
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";
import RouteProps from 'react-route-props'

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            cateLinks:[],
            cateInformations:[],
            currentLink:''
        };
    }

    componentWillMount(){
        let cateRelatedEndpoint = `/api/categories/${cateId}/related`
        axios.get(cateRelatedEndpoint).then(res => {
            this.setState({
                cateLinks:res.data.categories,
            })
        });
    }

    handleSetCurrent(e){
        this.setState({
            currentLink:e.target.innerText,
        })
    }

    render() {
    const categories = this.state.cateLinks;
    var parentsCate='';
    var childrendCate='';
    var currentLink=this.state.currentLink;

        if (!categories) {
            return <div>Cate not found!</div>
        }
        parentsCate =
            categories.map((cate,index) =>{
                if (cate.children_online && Array.isArray(cate.children_online)) {
                    childrendCate =
                    cate.children_online.map((child,index)=>{
                        return(
                            <Link key={index} className="list-group-item" id={child.id} to={`/danh-muc/${child.id}/${child.slug}`} onClick={(e)=>{this.handleSetCurrent(e)}}>{child.name}</Link>
                        )
                })
                }
                return(
                <div className="card" key={index}>
                    <article className="card-group-item">
                        <header className="card-header">
                        <h6 className="title">
                            {cate.name}
                        </h6>
                        </header>
                        <div className="filter-content">
                            <div className="list-group list-group-flush">
                                {childrendCate}
                            </div> 
                        </div>
                    </article> 
                </div>
                )
            })
            return (
            <div className="col-md-12">
                <Switch>
                        <RouteProps path={`/danh-muc/:id/:slug`}  component={Main} sideBar={parentsCate} currentName={currentLink}/>
                </Switch>
            </div>
            );
        }
    
    
}
export default App
