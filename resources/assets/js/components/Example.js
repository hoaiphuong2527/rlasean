import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import ModalCart from './ShoppingCart';

import { objectToArray } from '../helper/ObjectToArray'
import { checkQuantity } from '../helper/CheckQuantity'


export default class ShoppingComponent extends Component {
    constructor() {
        super()
        this.state = {
            productInfo:'',
            productVariants:[],
            cartItem:0,
            myCart:[],
            id:0,
            price:'',
            description:'',
            amount:'',
            name :'',
            imageName :'',
            size:'',
            galleries:'',
            qty:0,
            listItemId:''
        }
        
        this.handleDeleteItem = this.handleDeleteItem.bind(this);
        this.handleClickAmountInc = this.handleClickAmountInc.bind(this);
        this.handleClickAmountDec = this.handleClickAmountDec.bind(this);
        this.handleAddToCart = this.handleAddToCart.bind(this);
        
    }
    
    componentWillMount() {
            let productCart = '/get-cart'
            axios.get(productCart).then(res => {
                let cartArray = objectToArray(res.data.cart);
                this.setState({
                    myCart:cartArray,
                    cartItem:res.data.total_item,
                })
            })
        .then(()=>{
            ReactDOM.render(<ModalCart shoppingCart = {this.state.myCart} itemOnIncrease={this.handleClickAmountInc} itemOnDecrease={this.handleClickAmountDec} itemOnDelete={this.handleDeleteItem} countItem={this.state.cartItem} />, 
            document.getElementById('shopping-cart'));
            ReactDOM.render(
                <span className="cart-products-count">{this.state.cartItem}</span>, 
            document.getElementById('react-btn'));
            }) ;
            // var currentLocation = window.location.pathname;
            // if(currentLocation.match('danh-muc')!==null){
            //     var listProductId = arr_product_id;
            //     if(listProductId){
            //         listProductId.map((item, index) => {
            //         ReactDOM.render(
            //         <a href="#" className="btn btn-ys ilv-circle-20 ys-text-color ilv-italic" id={item} onClick={(e)  => this.handleAddToCart(e)}>Thêm vào giỏ hàng</a>, 
            //         document.getElementById(`btn-add-to-card-${item}`));
            //         })
            //     }
            // }
    }
    componentDidMount() {

    }

    handleAddToCart(e) {
        e.preventDefault();
        var countStock = 0;
        let productEndpoint = `/api/single-product/${e.target.id}`
        axios.get(productEndpoint).then(res => {        
            countStock = (res.data.product.product_variant_onlines[0].amount) - (res.data.product.product_variant_onlines[0].inventory)
            if(countStock > 0 ){
                this.setState({
                    qty:1,
                })
            }
            this.setState({
                productInfo:res.data.product,
                productVariants:res.data.product.product_variant_onlines,
                price:res.data.product.product_variant_onlines[0].beauty_price,
                description:res.data.product.short_description,
                amount:countStock,
                name :res.data.product.name,
                size:res.data.product.product_variant_onlines[0].size,
                id:res.data.product.product_variant_onlines[0].id,
                imageName:res.data.product.product_variant_onlines[0].galleries[0].image,
            })
        }).then(()=>{
            var data = {
                'id' :this.state.id,
                'name' :this.state.name,
                'description' :this.state.description,
                'price' :this.state.price,
                'size':this.state.size,
                'qty' :this.state.qty,
            }
            this.state.myCart.map((item) => {
                if(this.state.id == item.id)
                {
                    var tempStock = (this.state.amount - item.qty)
                    if(item.qty >= (this.state.amount) || this.state.qty > tempStock)
                    {
                        data = {
                            'id' :this.state.id,
                            'name' :this.state.name,
                            'description' :this.state.description,
                            'price' :this.state.price,
                            'size':this.state.size,
                            'qty' :0,
                        }
                    }
                }
            })
        var addToCart ='/add-product-into-cart'
        axios.post(addToCart,data).then(res => {
            var cartArray = objectToArray(res.data.cart)
            this.setState({
                myCart:cartArray,
                cartItem:res.data.total_item,
            })
        }).then(()=>{
            ReactDOM.render(<ModalCart shoppingCart = {this.state.myCart} itemOnIncrease={this.handleClickAmountInc} itemOnDecrease={this.handleClickAmountDec} itemOnDelete={this.handleDeleteItem} countItem={this.state.cartItem} />, 
                document.getElementById('shopping-cart'));
            ReactDOM.render(
                <span className="cart-products-count">{this.state.cartItem}</span>, 
                document.getElementById('react-btn'));
        }).then(this.setState({
            qty:0
        })
        )
    })
    }

    handleClickAmountInc(e) {
        e.preventDefault()
        var productId = e.target.getAttribute('product-id');
        var rowId = e.target.getAttribute('row-id');
        var data = {
            'product_id': productId,
            'rowId': rowId,
        }
        console.log('aa')

        var productEndpoint = `/api/get-variant/${productId}`
        axios.get(productEndpoint).then(res => {
            this.state.myCart.map((item) => {
                if(productId == item.id)
                {
                    if(item.qty < res.data.intoventory)
                    {
                        axios.post('/add-product-into-cart/increment', data).then(res => {
                            var cartArray = objectToArray(res.data.cart)
                            this.setState({
                                myCart:cartArray,
                                cartItem:res.data.total_item,
                            })
                        }).then(()=>{
                            ReactDOM.render(<ModalCart shoppingCart = {this.state.myCart} itemOnIncrease={this.handleClickAmountInc} itemOnDecrease={this.handleClickAmountDec} itemOnDelete={this.handleDeleteItem} countItem={this.state.cartItem} />, 
                                document.getElementById('shopping-cart'));
                            ReactDOM.render(
                                <span className="cart-products-count">{this.state.cartItem}</span>, 
                                document.getElementById('react-btn'));
                        })                    }
                }
            })
        })
    }

    handleClickAmountDec(e) {
        e.preventDefault()
        var productId = e.target.getAttribute('product-id');
        var rowId = e.target.getAttribute('row-id');
        let data = {
            'product_id': productId,
            'rowId': rowId,
        }
        axios.post('/add-product-into-cart/decrease', data).then(res => {
            let cartArray = objectToArray(res.data.cart)
            this.setState({
                myCart:cartArray,
                cartItem:res.data.total_item,
            })
        }).then(()=>{
            ReactDOM.render(<ModalCart shoppingCart = {this.state.myCart} itemOnIncrease={this.handleClickAmountInc} itemOnDecrease={this.handleClickAmountDec} itemOnDelete={this.handleDeleteItem} countItem={this.state.cartItem} />, 
            document.getElementById('shopping-cart'));
            ReactDOM.render(
                <span className="cart-products-count">{this.state.cartItem}</span>, 
                document.getElementById('react-btn'));
        })
    }

    handleDeleteItem(e) {
        let data = {
            'product_id': e.target.getAttribute('product-id'),
            'rowId': e.target.getAttribute('row-id')
        }
        axios.post('/remove-product',data).then(res => {
            let cartArray = objectToArray(res.data.cart)
            this.setState({
                myCart:cartArray,
                cartItem:res.data.total_item,
            })
        }).then(()=>{
            ReactDOM.render(<ModalCart shoppingCart = {this.state.myCart} itemOnIncrease={this.handleClickAmountInc} itemOnDecrease={this.handleClickAmountDec} itemOnDelete={this.handleDeleteItem} countItem={this.state.cartItem} />, 
            document.getElementById('shopping-cart'));
            ReactDOM.render(
                <span className="cart-products-count">{this.state.cartItem}</span>, 
                document.getElementById('react-btn'));
        })
    }
    render() {
        return (
            <div>
            </div>
        );
    }
}


if (document.getElementById('shopping-render')) {
    ReactDOM.render(<ShoppingComponent />, document.getElementById('shopping-render'));
}