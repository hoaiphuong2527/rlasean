import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import langs from '../lang/lang.json';
import NumberFormat from 'react-number-format';
import NumericInput from 'react-numeric-input';
import { map, sortBy, findIndex } from 'lodash';
import cities from '../city/cities.json';
const toArray = (Obj) => map(Obj, (o, key) => ({ ...o, key }));


import { objectToArray } from '../helper/ObjectToArray'
import { checkQuantity } from '../helper/CheckQuantity'


export default class CheckOutComponent extends Component {
    constructor(props) {
        super(props);
        const citiesArray = toArray(cities);

        const citiesArraySort = sortBy(citiesArray, function (city) {
            return city.slug;
        })
        this.state = {
            cartItem:0,
            myCart:[],
            status_payment:'Off',
            type_order: '',
            promotion_price:'0đ',
            totalPromotion:'',
            promotionMessage:'',
            code:[],
            checkCode:'',
            checkEmail:'',
            langs: langs,
            cities: citiesArraySort,
            selectedCity: citiesArraySort[0],
            selectedWardId: 0,
            DistrictID:'',
            shipFee:'0đ',
            ghnShippingCost:0,
            ghtkShippingCost:0
        }
        
        this.onChangeCity = this.onChangeCity.bind(this);
        this.onChangeWrad = this.onChangeWrad.bind(this);
        this.handleDeleteItem = this.handleDeleteItem.bind(this);
        this.handleClickAmountInc = this.handleClickAmountInc.bind(this);
        this.handleClickAmountDec = this.handleClickAmountDec.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.handleCheckCoupon = this.handleCheckCoupon.bind(this);
    }
    componentWillMount() { 
        const { selectedCity } = this.state;
        const districts = selectedCity.quan_huyen;
        const districtArray = toArray(districts);
        const value = districtArray[0].name_with_type;
        const dist = this.getDistrictID(districtArray,value)  
        this.calculatorShipFee(dist)
        this.setState({
            DistrictID: dist.DistrictID
        })
        let productCart = '/get-cart'
        axios.get(productCart).then(res => {
            let cartArray = objectToArray(res.data.cart);
            this.setState({
                myCart:cartArray,
                cartItem:res.data.total_item,
            })
        }).then(()=>{
            let totalBill = 0;
            let breakableCost = 0;
            this.state.myCart.map((item) => {
                totalBill = totalBill + parseInt(item.subtotal);
                if(item.options.breakable){
                    breakableCost = breakableCost + (item.price * option.percent_breakable / 100) * item.qty 
                }
            })
            this.setState({
                totalBill,
                breakableCost,
                totalPromotion:totalBill + parseInt(this.state.shipFee) + breakableCost
            })
        })
    }

    componentDidMount() {
        
    }

    onChangeCity(e) {
        const { cities } = this.state;
        const value = e.target.value
        const index = findIndex(cities, function (city) {
            return city.name_with_type === value;
        })
        if(document.getElementById('stress').value){
            var district = document.getElementById('district').value
            var address = document.getElementById('ward').value + document.getElementById('address').value
            this.calculatorGhtkShippingCost(value,district,address)
        }
        this.setState({
            selectedCity: cities[index]
        })
    }

    onChangeWrad(e) {
        const { selectedCity } = this.state;
        const districts = selectedCity.quan_huyen;
        const districtArray = toArray(districts);
        const value = e.target.value;
        const dist = this.getDistrictID(districtArray,value)
        // this.calculatorShipFee(dist)
        const index = findIndex(districtArray, function (district) {
            return district.name_with_type === value;
        })
        let serviceUrl = `${'https://console.ghn.vn/api/v1/apiv3/FindAvailableServices'}`
        const formId = option.district_id
        let data = {
            "token": "5ca5c8dd94c06b4f230c372e",
            "Weight": 0,
            "Length": 0,
            "Width": 0,
            "Height": 0,   
            "FromDistrictID": parseInt(formId),
            "ToDistrictID": dist.DistrictID
        }
        fetch(serviceUrl, {
        method: 'POST',
        headers: new Headers({
                    'Content-Type': 'application/x-www-form-urlencoded', // <-- Specifying the Content-Type
            }),
        body: JSON.stringify(data) // <-- Post parameters
        })
        .then((response) => response.text())
        .then((responseText) => {
            let responseInfo = JSON.parse(responseText).data[0]
            let totalPromotion = this.state.totalBill  + parseInt(responseInfo.ServiceFee) + this.state.breakableCost;
            
            if(document.getElementById('stress').value){
                var district = document.getElementById('district').value
                var address = document.getElementById('ward').value + document.getElementById('address').value
                this.calculatorGhtkShippingCost(value,district,address)
            }
            var shipFee = this.state.shipFee
            if(this.state.ghtkShippingCost > responseInfo.ServiceFee){
                shipFee = this.state.ghtkShippingCost

            }else {
                shipFee = responseInfo.ServiceFee
            }
            this.setState({ 
                ghnShippingCost: responseInfo.ServiceFee,
                selectedWardId: index,
                totalPromotion:totalPromotion,
                ghnShippingCost:parseInt(responseInfo.ServiceFee),
                shipFee: shipFee
            })
        })
        .catch((error) => {
            console.error(error);
        });
    }

    getDistrictID(districtArray,value){
        var district = districtArray.pop();
        districtArray.map((item, index) => {
            if(item.name_with_type == value)
            district = item
        });
        return district
    }

    calculatorShipFee(dist){
        let serviceUrl = `${'https://console.ghn.vn/api/v1/apiv3/FindAvailableServices'}`
        const formId = option.district_id
        let data = {
            "token": "5ca5c8dd94c06b4f230c372e",
            "Weight": 0,
            "Length": 0,
            "Width": 0,
            "Height": 0,   
            "FromDistrictID": parseInt(formId),
            "ToDistrictID": dist.DistrictID
        }
        fetch(serviceUrl, {
        method: 'POST',
        headers: new Headers({
                    'Content-Type': 'application/x-www-form-urlencoded', // <-- Specifying the Content-Type
            }),
        body: JSON.stringify(data) // <-- Post parameters
        })
        .then((response) => response.text())
        .then((responseText) => {
            let responseInfo = JSON.parse(responseText).data[0]
            this.setState({
                shipFee: responseInfo.ServiceFee,
                ghnShippingCost: responseInfo.ServiceFee,
                DistrictID: dist.DistrictID,
                ghnShippingCost:parseInt(responseInfo.ServiceFee)
            })
        })
        .catch((error) => {
            console.error(error);
        });
    }

    calculatorGhtkShippingCost(province,district,address){
        let data = {
            province : province,
            district : district,
            address : address,
            // province: "Hà nội",
            // district : "Quận Cầu Giấy",
            // address : "P.503 tòa nhà Auu Việt, số 1 Lê Đức Thọ",
            weight : 1000,
            value : this.state.totalPromotion,
        }
        let endpoint = '/api/calculator-ghtk-shipping-cost';
        var ghtkShippingCost = 0;
        axios.post(endpoint,data).then(res => {
            var fee = res.data.pop().fee
            ghtkShippingCost = fee.fee + fee.insurance_fee
            this.setState({
                ghtkShippingCost: ghtkShippingCost
            })

        })
    }

    handleClick(e){
        e.preventDefault();
        document.getElementById('form-info').submit();
    }

    handleCheckCoupon(e){
        document.getElementById('coupon').value;
            var data = {
                checkCode:document.getElementById('coupon').value,
                checkEmail:document.getElementById('email').value
            };
            var endpoint ='/api/check-email'
            axios.post(endpoint,data).then(res => {
            let code = res.data.code;
            let totalPromotion = 0;
                this.setState({
                    code,
                    message:res.data.message
                })
                    var message = this.state.message;
                    document.getElementById('noti').innerText=''+message;
            if(res.data.resultCode == 1 && code.reduction_type =='absolute')
            {
                document.getElementById('noti').classList.remove('text-danger')

                totalPromotion = this.state.totalBill - code.promotion_price + this.state.breakableCost;
                this.setState({
                    promotion_price:code.promotion_price + 'đ',
                    totalPromotion,
                })
            } else if(res.data.resultCode == 1 && code.reduction_type =='percent')            
            {
                document.getElementById('noti').classList.remove('text-danger')

                totalPromotion = this.state.totalBill - (this.state.totalBill*(code.promotion_price /100)) + this.state.breakableCost;
                this.setState({
                    promotion_price:code.promotion_price + '%',
                    totalPromotion
                })
            } else {
                document.getElementById('noti').classList.add('text-danger')

                totalPromotion = this.state.totalBill + this.state.breakableCost;
                this.setState({
                    promotion_price:'0đ',
                    totalPromotion
                })
            }
        })
    }

    handleClickAmountInc(e) {
        e.preventDefault()
        var productId = e.target.getAttribute('product-id');
        var rowId = e.target.getAttribute('row-id');
        var data = {
            'product_id': productId,
            'rowId': rowId,
        }
        var productEndpoint = `/api/get-variant/${productId}`
        axios.get(productEndpoint).then(res => {
            this.state.myCart.map((item) => {
                if(productId == item.id)
                {
                    if(item.qty < res.data.intoventory)
                    {
                        axios.post('/add-product-into-cart/increment', data).then(res => {
                            var cartArray = objectToArray(res.data.cart)
                            var breakableCost = 0;
                            cartArray.map((item) => {
                                if(item.options.breakable){
                                    breakableCost = breakableCost + (item.price * option.percent_breakable / 100) * item.qty 
                                }
                            })
                            this.setState({
                                breakableCost,
                                myCart:cartArray,
                                cartItem:res.data.total_item,
                            })
                        }).then(()=>{
                            ReactDOM.render(<ModalCart shoppingCart = {this.state.myCart} itemOnIncrease={this.handleClickAmountInc} itemOnDecrease={this.handleClickAmountDec} itemOnDelete={this.handleDeleteItem} countItem={this.state.cartItem} />, 
                                document.getElementById('shopping-cart'));
                            ReactDOM.render(
                                <span className="cart-products-count">{this.state.cartItem}</span>, 
                                document.getElementById('react-btn'));
                        })                    }
                }
            })
        })
    }

    handleClickAmountDec(e) {
        e.preventDefault()
        var productId = e.target.getAttribute('product-id');
        var rowId = e.target.getAttribute('row-id');
        let data = {
            'product_id': productId,
            'rowId': rowId,
        }
        axios.post('/add-product-into-cart/decrease', data).then(res => {
            let cartArray = objectToArray(res.data.cart)
            var breakableCost = 0;
            cartArray.map((item) => {
                if(item.options.breakable){
                    breakableCost = breakableCost + (item.price * option.percent_breakable / 100) * item.qty 
                }
            })
            this.setState({
                breakableCost,
                myCart:cartArray,
                cartItem:res.data.total_item,
            })
        }).then(()=>{
            ReactDOM.render(<ModalCart shoppingCart = {this.state.myCart} itemOnIncrease={this.handleClickAmountInc} itemOnDecrease={this.handleClickAmountDec} itemOnDelete={this.handleDeleteItem} countItem={this.state.cartItem} />, 
            document.getElementById('shopping-cart'));
            ReactDOM.render(
                <span className="cart-products-count">{this.state.cartItem}</span>, 
                document.getElementById('react-btn'));
        })
    }

    handleChange(e,productId,rowId,value){
        let data = {
            'product_id': productId,
            'rowId': rowId,
        }
        var endPoint = '';
        if(value > e)
        {
            endPoint = '/add-product-into-cart/decrease'
        }
        else{
            endPoint = '/add-product-into-cart/increment'
        }
        axios.post(endPoint,data).then(res => {
            let cartArray = objectToArray(res.data.cart)
            var breakableCost = 0;
            cartArray.map((item) => {
                if(item.options.breakable){
                    breakableCost = breakableCost + (item.price * option.percent_breakable / 100) * item.qty 
                }
            })
            this.setState({
                breakableCost,
                myCart:cartArray,
                cartItem:res.data.total_item,
            })
        }).then(()=>{
            let totalBill = 0;
            var breakableCost = 0;
            this.state.myCart.map((item) => {
                totalBill = totalBill + parseInt(item.subtotal);
                if(item.options.breakable){
                    breakableCost = breakableCost + (item.price * option.percent_breakable / 100) * item.qty 
                }
            })
            this.setState({
                breakableCost,
                totalBill,
                totalPromotion:totalBill + breakableCost
            })
            ReactDOM.render(
                <span className="cart-products-count">{this.state.cartItem}</span>, 
                document.getElementById('react-btn'));
        })
    }

    handleDeleteItem(e) {
        let data = {
            'product_id': e.target.getAttribute('product-id'),
            'rowId': e.target.getAttribute('row-id')
        }
        axios.post('/remove-product',data).then(res => {
            let cartArray = objectToArray(res.data.cart)
            this.setState({
                myCart:cartArray,
                cartItem:res.data.total_item,
            })
        }).then(()=>{
            ReactDOM.render(<ModalCart shoppingCart = {this.state.myCart} itemOnIncrease={this.handleClickAmountInc} itemOnDecrease={this.handleClickAmountDec} itemOnDelete={this.handleDeleteItem} countItem={this.state.cartItem} />, 
            document.getElementById('shopping-cart'));
            ReactDOM.render(
                <span className="cart-products-count">{this.state.cartItem}</span>, 
                document.getElementById('react-btn'));
        })
    }

    handleOnChangeAddress(e){
        let value = e.target.value
        var district = document.getElementById('district').value
        var province = document.getElementById('province').value
        var address = document.getElementById('ward').value + value
        this.calculatorGhtkShippingCost(province,district,address)
        var shipFee = this.state.shipFee
        console.log('ghtkShippingCost',this.state.ghtkShippingCost)

        if(this.state.ghtkShippingCost > this.state.ghnShippingCost){
            shipFee = this.state.ghtkShippingCost
        }else {
            shipFee = this.state.ghnShippingCost
        }
        this.setState({
            shipFee: shipFee
        })
    }

    render() {
        const myCart =this.state.myCart;
        var total = 0;
        var csrf = document.getElementById('token_form').children[0].getAttribute('value');
        const { cities, selectedCity, selectedWardId } = this.state;
        const districts = selectedCity.quan_huyen;
        const districtArray = toArray(districts);
        const districtArraySort = sortBy(districtArray, function (item) {
            return item.slug;
        })
        const wards = districtArray[selectedWardId].xa_phuong;
        const wardsArray = toArray(wards);
        return (
        <div className="container">
            <div className="row">
                <div className="col-md-8">
                    <h4 className="d-flex justify-content-between align-items-center mb-3">
                        <span className="text-muted">{this.state.langs[locale]['your_cart']}</span>
                    </h4>
                    <div className="shopping-cart">
                        <div className="column-labels">
                        <label className="product-image">{this.state.langs[locale]['image']}</label>
                        <label className="product-details">{this.state.langs[locale]['product']}</label>
                        <label className="product-price">{this.state.langs[locale]['price']}</label>
                        <label className="product-quantity">{this.state.langs[locale]['amount']}</label>
                        <label className="product-removal">{this.state.langs[locale]['remove']}</label>
                        <label className="product-line-price">{this.state.langs[locale]['summary']}</label>
                        </div>
                        {
                            myCart.map((item) => {
                            total = total + parseInt(item.subtotal);
                            return(
                                <div className="product" key={item.rowId}>
                                <div className="product-image">
                                    <img src={item.options.image}/>
                                </div>
                                <div className="product-details">
                                    <div className="product-title">{item.name}</div>
                                    <p className="product-description"></p>
                                </div>
                                <div className="product-price"><NumberFormat value={item.price} displayType={'text'} thousandSeparator={true}  /></div>
                                <div className="product-quantity">
                                <NumericInput 
                                            min={ 0 } 
                                            size={ 6} 
                                            mobile = {true}
                                            name="qty" 
                                            value= {item.qty}
                                            onChange = { (e,productId,rowId,value) => this.handleChange(e,item.id,item.rowId,item.qty) }
                                            />
                                </div>
                                <div className="product-removal">
                                    <button className="remove-product" product-id={item.id} row-id={item.rowId} onClick={(e) => this.handleDeleteItem(e)}>
                                    <i className="fa fa-trash"></i>
                                    </button>
                                </div>
                                    <div className="product-line-price">
                                    <NumberFormat value={item.subtotal} displayType={'text'} thousandSeparator={true}  />
                                    </div>
                                </div>
                                )
                            })
                        }
                    </div>
                </div>
                <div className="col-md-4 order-md-2 mb-4">
                    <h4 className="d-flex justify-content-between align-items-center mb-3">
                        <span className="text-muted">{this.state.langs[locale]['total_order']}</span>
                        <span className="badge badge-secondary badge-pill">{this.state.cartItem}</span>
                    </h4>
                    <ul className="list-group mb-3">
                        <li className="list-group-item d-flex justify-content-between">
                            <span>{this.state.langs[locale]['total']} (VND)</span>
                            <small><NumberFormat value={this.state.totalBill} displayType={'text'} thousandSeparator={true}  /> </small>
                        </li>
                        <li className="list-group-item d-flex justify-content-between">
                            <span>{this.state.langs[locale]['disscount']}:</span>
                            <small>{this.state.promotion_price}</small>
                        </li>
                        <li className="list-group-item d-flex justify-content-between">
                            <span>{this.state.langs[locale]['ship']}</span>
                            <strong><NumberFormat value={this.state.shipFee} displayType={'text'} thousandSeparator={true}  />đ</strong>
                        </li>
                        {  
                            this.state.breakableCost !== 0  && 
                            (<li className="list-group-item d-flex justify-content-between">
                            <span>{this.state.langs[locale]['breakable_cost']}</span>
                            <strong><NumberFormat value={this.state.breakableCost} displayType={'text'} thousandSeparator={true}  />đ</strong>
                            </li>)
                        }
                        <li className="list-group-item d-flex justify-content-between">
                            <span>{this.state.langs[locale]['after_disscount']}</span>
                            <strong><NumberFormat value={this.state.totalPromotion} displayType={'text'} thousandSeparator={true}  />đ</strong>
                        </li>
                    </ul>
                    <div className="card p-2">
                        <div className="input-group">
                            <input type="text" className="form-control" id="coupon"  placeholder={this.state.langs[locale]['coupon']}  onChange={(e) => this.handleCheckCoupon(e)}/>
                            <div className="input-group-append">
                            </div>
                        </div>
                    </div>
                    <div className=" d-flex justify-content-between">
                        <label id="noti" className="text-success"></label>
                    </div>
                </div>
            </div>
                <div className="row">
                    <div className="col-md-12 order-md-1">
                        <h4 className="mb-3">{this.state.langs[locale]['address']}</h4>
                        <form id="form-info" action={this.state.langs[locale]['get_route']} method="post" className="needs-validation" noValidate="">
                        <div className="row">
                                <input type="hidden" name="_token" value={csrf} />
                                <input type="hidden" name="total" value={this.state.totalBill} />
                                <input type="hidden" name="total_after" value={this.state.totalPromotion} />
                                <input type="hidden" name="codeName" value={this.state.code.promotion} />
                                <input type="hidden" name="shipping_fee" value={this.state.shipFee} />
                                <input type="hidden" name="breakable_cost" value={this.state.breakableCost} />
                            <div className=" mb-3 col-md-6">
                                <label htmlFor="firstName">{this.state.langs[locale]['full_name']}</label>
                                <input type="text" className="form-control" id="firstName" placeholder={this.state.langs[locale]['full_name']}  required="" name="customer_name"/>
                                <div className="invalid-feedback">
                                {this.state.langs[locale]['errors']['name']}
                                </div>
                                <label htmlFor="email">Email</label>
                                <input type="email" className="form-control" id="email" placeholder="Email" name="customer_email" onChange={(e) => this.handleCheckCoupon(e)}/>
                                <div className="invalid-feedback">
                                {this.state.langs[locale]['errors']['email']}
                                </div>
                                <label htmlFor="phone">{this.state.langs[locale]['phone']}</label>
                                <input type="number" className="form-control" id="phone" placeholder={this.state.langs[locale]['phone']} name="customer_phone"/>
                                <div className="invalid-feedback">
                                {this.state.langs[locale]['errors']['phone']}
                                </div>
                            </div>
                            <div className=" mb-3 col-md-6">
                                <label htmlFor="firstName">{this.state.langs[locale]['province']}</label>
                                <select
                                    className="form-control"
                                    name="province"
                                    id="province"
                                    onChange={this.onChangeCity}
                                >
                                    {cities.map(function (city) {
                                        return <option key={city.key} value={city.name_with_type}>{city.name_with_type}</option>
                                    })}
                                </select>
                                <label htmlFor="firstName">{this.state.langs[locale]['district']}</label>
                                <select
                                    className="form-control"
                                    name="district"
                                    id="district"
                                    onChange={this.onChangeWrad}
                                >
                                    {districtArraySort.map(function (district) {
                                        return <option key={district.key} value={district.name_with_type}>{district.name_with_type}</option>
                                    })}
                                </select>
                                <label htmlFor="firstName">{this.state.langs[locale]['ward']}</label>
                                <select className="form-control" name="ward">
                                    {wardsArray.map(function (ward) {
                                        return <option key={ward.key} value={ward.name_with_type} id="ward">{ward.name_with_type}</option>
                                    })}
                                </select>
                                <label htmlFor="address">{this.state.langs[locale]['address']}</label>
                                <input type="text" className="form-control" id="stress" placeholder={this.state.langs[locale]['address']} required="" name="stress" onChange={(e) =>this.handleOnChangeAddress(e)}/>
                                <div className="invalid-feedback">
                                {this.state.langs[locale]['errors']['address']}
                                </div>
                            </div>
                            <div className="mb-3 col-md-12">
                            <label htmlFor="address">{this.state.langs[locale]['note']}</label>
                                    <textarea className="form-control" placeholder={this.state.langs[locale]['note']} name="note"></textarea>
                                    </div>                           
                                    <div className="mb-3 col-md-12">
                                <select   className="form-control"  placeholder="Phương thức thanh toán" name="payment_type" value={this.state.status_payment} onChange={(e) => this.handleChange(e)}>
                                        <option value="Off">
                                        {this.state.langs[locale]['payment_off']}
                                        </option>
                                        {  
                                            this.state.type_order === 'HP' && 
                                            (<option value="On" id="online">{this.state.langs[locale]['payment_on']}</option>)
                                        }
                                    </select>
                                    </div>
                                <div className="mb-3 col-md-12">
                                <input type="submit" value={this.state.langs[locale]['submit']} className=" form-control btn btn-primary btn-lg btn-block text-white" onClick={(e) => this.handleClick(e)}/>
                                </div>
                                </div>
                                <hr className="mb-4"/>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

if (document.getElementById('react-checkout')) {
    ReactDOM.render(<CheckOutComponent />, document.getElementById('react-checkout'));
}