import React from 'react';
import ReactDOM from 'react-dom';
import langs from '../lang/lang.json';
import { map, sortBy, findIndex } from 'lodash';

import cities from '../city/cities.json';

const toArray = (Obj) => map(Obj, (o, key) => ({ ...o, key }));

class City extends React.Component {
    constructor(props) {
        super(props);

        const citiesArray = toArray(cities);

        const citiesArraySort = sortBy(citiesArray, function (city) {
            return city.slug;
        })

        this.state = {
            cities: citiesArraySort,
            selectedCity: citiesArraySort[0],
            selectedWardId: 0,
            langs: langs,
        }

        this.onChangeCity = this.onChangeCity.bind(this);
        this.onChangeWrad = this.onChangeWrad.bind(this);
    }

    onChangeCity(e) {
        const { cities } = this.state;
        const value = e.target.value

        const index = findIndex(cities, function (city) {
            return city.name_with_type === value;
        })

        this.setState({
            selectedCity: cities[index]
        })

    }

    onChangeWrad(e) {
        const { selectedCity } = this.state;
        const districts = selectedCity.quan_huyen;
        const districtArray = toArray(districts);
        const value = e.target.value;
        const index = findIndex(districtArray, function (district) {
            return district.name_with_type === value;
        })

        this.setState({ selectedWardId: index })
    }

    render() {
        const { cities, selectedCity, selectedWardId } = this.state;
        const districts = selectedCity.quan_huyen;
        const districtArray = toArray(districts);
        const districtArraySort = sortBy(districtArray, function (item) {
            return item.slug;
        })
        const wards = districtArray[selectedWardId].xa_phuong;
        const wardsArray = toArray(wards);
        const type = this.props.type;
        return (
            <div>
                <div className="form-group m-form__group">
                    <label htmlFor="firstName">Tỉnh/Thành phố</label>
                    <select
                        className="form-control"
                        name="province"
                        onChange={this.onChangeCity}
                        id={'province_'+type}
                    >
                        {cities.map(function (city) {
                            return <option key={city.key} value={city.name_with_type}>{city.name_with_type}</option>
                        })}
                    </select>
                </div>
                <div className="form-group m-form__group">
                    <label htmlFor="firstName">Quận/Huyện</label>
                    <select
                        className="form-control"
                        name="district"
                        onChange={this.onChangeWrad}
                        id={'district_'+type}
                    >
                        {districtArraySort.map(function (district) {
                            return <option key={district.key} value={district.name_with_typeDist} district-id={district.DistrictID}>{district.name_with_type}</option>
                        })}
                    </select>
                </div>
                <div className="form-group m-form__group">
                    <label htmlFor="firstName">Phường/Xã</label>
                    <select className="form-control" name="ward"  id={'ward_'+type}>
                            {wardsArray.map(function (ward) {
                                return <option key={ward.key} value={ward.name_with_type}>{ward.name_with_type}</option>
                            })}
                        </select>
                </div>
            </div >
        );
    }
}

if (document.getElementById('city')) {
    var ele = document.getElementById("city")
    ReactDOM.render(<City 
        type={ele.getAttribute('type')}/>
    ,ele);
}
if (document.getElementById('city1')) {
    var ele = document.getElementById("city1")
    ReactDOM.render(<City 
        type={ele.getAttribute('type')}/>
    ,ele);
}