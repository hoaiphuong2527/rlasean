import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import langs from '../lang/lang.json';
import NumberFormat from 'react-number-format';


import { objectToArray } from '../helper/ObjectToArray'


export default class ModalCart extends Component {
    constructor(props) {
        super(props);
        this.state = {
            totalCart:0,
            langs: langs,
        }            

    }
    
    componentWillMount() {
    }

    componentDidMount() {

    }
    handleOnChange(e){

    }
    render() {
        
        const myCart = this.props.shoppingCart;
        var total = 0;
        return (
            <div className="modal fade" id="exampleModalCenter" tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
            aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered ilv-mw-1000" role="document">
                    <div className="modal-content ilv-mh-420-mb">
                        <div className="card ">
                            <div className="card-header ys-yellow-bg  ilv-bold ilv-fs-25">
                                <i className="fa fa-shopping-cart" aria-hidden="true">&ensp;</i>{this.state.langs[locale]['cart_info']}
                            </div>
                            <div className="card-body">
                                {
                                    myCart.map((item) => {
                                        total = total + parseInt(item.subtotal);
                                        let style = {   backgroundColor:item.options.size,
                                                        paddingRight: 25,
                                                        margin: 13,
                                                        borderRadius: 5,
                                                        border:'solid 1px #a81d20'
                                            }
                                        return( 
                                        <div className="row" key={item.rowId}>
                                            <div className="col-12  col-md-12 text-md-left col-lg-12">
                                            <h4 className="product-name"><strong>{item.name}</strong></h4>
                                            </div>
                                            <div className="col-md-12  col-lg-12 text-md-left row">
                                                <div className="col-md-12 col-lg-2 ilv-fs-18 ivl-bd-mb">
                                                    <span className="">{this.state.langs[locale]['color']}:</span><strong style={style} className="ilv-pd-8"></strong>
                                                </div>
                                                <div className="col-md-12 col-lg-3 ilv-fs-18 ivl-bd-mb">
                                                    <span >{this.state.langs[locale]['price']}:</span><strong className="ys-or-text ilv-bold ilv-fs-28 money"><NumberFormat value={item.price} displayType={'text'} thousandSeparator={true}  /></strong><span className="currency">đ</span>
                                                </div>
                                                <div className="col-md-12 col-lg-3 ilv-fs-18 ivl-bd-mb">
                                                    <span >{this.state.langs[locale]['amount']}:</span><strong className="ys-or-text ilv-bold ilv-fs-28">     
                                                    <div className="quantity">
                                                        <input type="button" defaultValue="+" className="plus ys-or-text ilv-bold ilv-fs-28" product-id={item.id} row-id={item.rowId} onClick={(e) => this.props.itemOnIncrease(e)}/>
                                                        <input type="number" step="1" max="99" min="1"  title="Qty" className="qty ys-or-text ilv-bold ilv-fs-28"
                                                            value={item.qty} onChange={this.handleOnChange} />
                                                        <input type="button" defaultValue="-" className="minus ys-or-text ilv-bold ilv-fs-28" product-id={item.id} row-id={item.rowId}  onClick={(e) => this.props.itemOnDecrease(e)}/>
                                                    </div>
                                                    </strong>
                                                </div>
                                                <div className="col-md-12 col-lg-3 ilv-fs-18 ivl-bd-mb">
                                                    <span >{this.state.langs[locale]['summary']}: </span><strong className="ys-or-text ilv-bold ilv-fs-28 money">  <NumberFormat value={item.subtotal} displayType={'text'} thousandSeparator={true}  /></strong><span className="currency">đ</span>
                                                </div>
                                                <div className="col-md-12 col-lg-1 ivl-center-mb">
                                                    <button type="button" className="btn btn-outline-danger btn-xs" product-id={item.id} row-id={item.rowId} onClick={(e) => this.props.itemOnDelete(e)}>
                                                        <i className="fa fa-trash" product-id={item.id} row-id={item.rowId} aria-hidden="true"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        )
                                        {
                                            item.options.session > 0 && 
                                            (<p>Chỉ còn { item.session } sản phẩm được khuyến mãi </p>)
                                        }
                                    })
                                }
                            </div>
                            <div className="card-footer">      
                                <div className="row">
                                    <div className="coupon col-lg-12 col-md-12 no-padding-left pull-left">
                                            <div className="col-12">
                                            {this.state.langs[locale]['total']}: <strong className="ys-or-text ilv-bold ilv-fs-28 money"><NumberFormat value={total} displayType={'text'} thousandSeparator={true}  /></strong><span className="currency">đ</span>
                                            </div>
                                        </div>
                                    <div className="coupon col-lg-12 col-md-12 text-center">
                                        <a href={this.state.langs[locale]['get_route']} className="btn btn-primary btn-lg ">{this.state.langs[locale]['checkout']}</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );

    }
}
