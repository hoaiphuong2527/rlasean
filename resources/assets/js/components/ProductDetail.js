import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import NumberFormat from 'react-number-format';
import NumericInput from 'react-numeric-input';
import Magnifier from "react-magnifier";



import { objectToArray } from '../helper/ObjectToArray'
import { checkQuantity } from '../helper/CheckQuantity'
import langs from '../lang/lang.json'


export default class ProductDetailComponent extends Component {
    constructor() {
        super()
        this.state = {
            productInfo:'',
            productVariants:[],
            cartItem:0,
            myCart:[],
            id:0,
            price:'',
            priceBeforeDiscount:'',
            description:'',
            amount:'',
            name :'',
            imageName :'',
            size:'',
            galleries:[],
            qty:0,
            langs: langs,
            hoverStyle:{
                backgroundImage: '',
                backgroundPosition: '0% 0%'
            },
            discountValue: ""
        }
        this.handleChangeImage = this.handleChangeImage.bind(this);
        this.handleChangeProduct = this.handleChangeProduct.bind(this);
        this.handleAddToCart = this.handleAddToCart.bind(this);
        this.handleDeleteItem = this.handleDeleteItem.bind(this);
        this.handleClickAmountInc = this.handleClickAmountInc.bind(this);
        this.handleClickAmountDec = this.handleClickAmountDec.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }
    
    componentWillMount() {
        let productEndpoint = `/api/single-product/${item.id}`
        axios.get(productEndpoint).then(res => {

                let countStock = (res.data.product.product_variant_onlines[0].amount) - (res.data.product.product_variant_onlines[0].inventory)
                if(countStock > 0 ){
                    this.setState({
                        qty:1,
                    })
                }
                this.setState({
                productInfo:res.data.product,
                productVariants:res.data.product.product_variant_onlines,
                price:res.data.product.product_variant_onlines[0].discount_price,
                priceBeforeDiscount:res.data.product.product_variant_onlines[0].beauty_price,
                discountValue:res.data.product.product_variant_onlines[0].discount_value,
                description:res.data.product.short_description,
                amount:countStock,
                name :res.data.product.name,
                size:res.data.product.product_variant_onlines[0].color,
                id:res.data.product.product_variant_onlines[0].id,
                imageName:res.data.product.product_variant_onlines[0].media[0].link,
                galleries:res.data.product.product_variant_onlines[0].media,
            })
        });
        let productCart = '/get-cart'
        axios.get(productCart).then(res => {
            let cartArray = objectToArray(res.data.cart);
            this.setState({
                myCart:cartArray,
                cartItem:res.data.total_item,
            })
        })
    }

    componentDidMount() {

    }

    handleChangeProduct(e){
        e.preventDefault()
        var el = e.target.parentElement.children;
        for(var i=0; i<el.length; i++)
        {
            el[i].classList.remove('active');
        }
        var targetVal = e.target.getAttribute('value')
        var targetId = parseInt(e.target.getAttribute('id'))
        this.state.productVariants.map((variant, index) => {
            if(variant.color == targetVal){
                e.target.classList.add('active');
                this.setState({
                    amount:variant.amount,
                    size:targetVal,
                    imageName:variant.media[0].link,
                    id:targetId,
                    galleries:variant.media,
                    price:variant.discount_price
                })
            }
        })
    }

    handleChangeImage(e){
        e.preventDefault()
        var targetVal = e.target.getAttribute('value')
            this.setState({
                imageName:targetVal,
            })
    }
    handleChange(e){
        let checkStock = checkQuantity(this.state.amount ,e)
        this.setState({ qty:checkStock});
    }

    handleAddToCart(e) {
        e.preventDefault()
        var data = {
            'id' :this.state.id,
            'name' :this.state.name,
            'description' :this.state.description,
            'price' :this.state.price,
            'size':this.state.size,
            'qty' :this.state.qty,
        }
        this.state.myCart.map((item) => {
            if(this.state.id == item.id)
            {
                var tempStock = (this.state.amount - item.qty)
                if(item.qty >= (this.state.amount) || this.state.qty > tempStock)
                {
                    data = {
                        'id' :this.state.id,
                        'name' :this.state.name,
                        'description' :this.state.description,
                        'price' :this.state.price,
                        'size':this.state.size,
                        'qty' :0,
                    }
                }
            }
        })
        var addToCart ='/add-product-into-cart'
        axios.post(addToCart,data).then(res => {
            var cartArray = objectToArray(res.data.cart)
            this.setState({
                myCart:cartArray,
                cartItem:res.data.total_item,
            })
        }).then(()=>{
            ReactDOM.render(
                <span className="cart-products-count">{this.state.cartItem}</span>, 
            document.getElementById('react-btn'));
        })
    }

    handleClickAmountInc(e) {
        e.preventDefault()
        var productId = e.target.getAttribute('product-id');
        var rowId = e.target.getAttribute('row-id');
        let data = { 
            'product_id': productId,
            'rowId': rowId,
        }
        var productEndpoint = `/api/get-variant/${productId}`
        axios.get(productEndpoint).then(res => {
            this.state.myCart.map((item) => {
                if(rowId == item.rowId)
                {
                    if(item.qty < res.data.intoventory)
                    {
                        axios.post('/add-product-into-cart/increment', data).then(res => {
                            var cartArray = objectToArray(res.data.cart)
                            this.setState({
                                myCart:cartArray,
                                cartItem:res.data.total_item,
                            })
                        }).then(()=>{
                            ReactDOM.render(
                                <span className="cart-products-count">{this.state.cartItem}</span>, 
                                document.getElementById('react-btn'));
                        })
                    }
                }
            })
        })
    }

    handleClickAmountDec(e) {
        e.preventDefault()
        var productId = e.target.getAttribute('product-id');
        let data = {
            'product_id': productId,
        }
        axios.post('/add-product-into-cart/decrease', data).then(res => {
            let cartArray = objectToArray(res.data.cart)
            this.setState({
                myCart:cartArray,
                cartItem:res.data.total_item,
            })
        }).then(()=>{
            ReactDOM.render(
                <span className="cart-products-count">{this.state.cartItem}</span>, 
                document.getElementById('react-btn'));
        })
    }

    handleDeleteItem(e) {
        let data = {
            'product_id': e.target.getAttribute('product-id'),
            'rowId': e.target.getAttribute('row-id')
        }
        axios.post('/remove-product',data).then(res => {
            let cartArray = objectToArray(res.data.cart)
            this.setState({
                myCart:cartArray,
                cartItem:res.data.total_item,
            })
        }).then(()=>{
            ReactDOM.render(
                <span className="cart-products-count">{this.state.cartItem}</span>, 
            document.getElementById('react-btn'));
        })
    }
    
    handleDestroyCart(e) {
        e.preventDefault()
        axios.post('/destroy').then(res =>{
            if (res.data.status == 'deleted') {
                this.setState({
                    myCart: []
                })
            }
        }).then(()=>{
            let totalOrder = 0;
            this.state.myCart.map((item,index) => {
                var totalPrice = (item.price * item.qty);
                totalOrder += totalPrice;
            })
            this.setState({totalOrder})
        })
    }
    handleZoomImages(e) {
        const { left, top, width, height } = e.target.getBoundingClientRect()
        const x = (e.pageX - left) / width * 100
        const y = (e.pageY - top) / height * 100
        this.setState({ backgroundPosition: `${x}% ${y}%` })
    
    }
    
    render() {
        const galaleries = this.state.galleries;
        const product = this.state.productInfo;
        const variants = this.state.productVariants;
        var currentLocation = window.location.origin;
        var discountValue = '';
        var imageUrl =`${currentLocation}/upload/images/default-image.jpg`;
        var baseImageUrl =`${currentLocation}/upload/images/`;
        if(this.state.discountValue != 0)
        {

            discountValue = <div className="mb-3">
                                <var className="price h5">
                                    {this.state.langs[locale]['default_price']}:
                                    <del className="num text-secondary">
                                    <NumberFormat value={this.state.priceBeforeDiscount} displayType={'text'} thousandSeparator={true}  />đ
                                    <span className="iw-discount-value ml-2">-{this.state.discountValue} </span>
                                    </del>
                                </var>
                            </div>;
        }
        if(this.state.imageName != ''){
            imageUrl = `${currentLocation}/upload/images/${this.state.imageName}`
        }
        return (
            <div>
                    <section id="product_detail mx-2">
                        <div id="code_prod_detail">
                            <div className="card">
                                <div className="row no-gutters">
                                    <aside className="col-sm-5 border-right p-3">
                                        <article className="gallery-wrap">
                                            <div className="img-big-wrap">
                                                <div> 
                                                <div className=''>
                                                    <Magnifier style={this.state} className="img-fluid" src={this.state.imageName}/>
                                                    </div>
                                                </div>
                                            </div> 
                                            <div className="img-small-wrap">
                                            {
                                            galaleries.map((item, index) => {
                                                return (
                                                    <a className="" 
                                                        href="#" 
                                                        onClick={this.handleChangeImage}
                                                        key={index}>
                                                        <div className="item-gallery">
                                                            <img  value={item.link} src={item.link}/>
                                                        </div>
                                                    </a>
                                                )
                                            })
                                        }
                                            </div> 
                                        </article> 
                                    </aside>
                                    <aside className="col-sm-7">
                                        <article className="p-5">
                                            <h3 className="title mb-3">{this.state.name}</h3>
                                            <div className="mb-3">
                                                <var className="price h3">
                                                    <span className="num">
                                                        <NumberFormat value={this.state.price} displayType={'text'} thousandSeparator={true}/>đ
                                                    </span>
                                                </var>
                                            </div>
                                            {discountValue}
                                            <hr/>
                                            <dl>
                                                <dt>{this.state.langs[locale]['description']}</dt>
                                                <dd>
                                                    <p className="ilv-mb-text-justify">{this.state.description}</p>
                                                </dd>
                                            </dl>
                                            <dl className="row ilv-detail-responsive">
                                                {/* <dt className="col-sm-3">Model#</dt>
                                                <dd className="col-sm-9">12345611</dd> */}
                                                <dt className="col-sm-3">{this.state.langs[locale]['color']}</dt>
                                                <dd className="col-sm-9">
                                                {
                                                    variants.map((item, index) => {
                                                        let elStyle = {backgroundColor:item.color}
                                                        return <a className="btn btn-sm btn-outline-primary set-color" id={item.id} style={elStyle} value={item.color} href="#" onClick={this.handleChangeProduct} key={index}>    </a>
                                                    })
                                                }
                                                </dd>
                                                <dt className="col-md-3 col-sm-6">{this.state.langs[locale]['stock']}</dt>
                                                <dd className="col-md-9 col-sm-6">{this.state.amount}</dd>
                                            </dl>
                                            {/* <div className="rating-wrap">
                                                <ul className="rating-stars">
                                                    <li style={{width:"80%"}} className="stars-active">
                                                        <i className="fa fa-star"></i> <i className="fa fa-star"></i>
                                                        <i className="fa fa-star"></i> <i className="fa fa-star"></i>
                                                        <i className="fa fa-star"></i>
                                                    </li>
                                                    <li>
                                                        <i className="fa fa-star"></i> <i className="fa fa-star"></i>
                                                        <i className="fa fa-star"></i> <i className="fa fa-star"></i>
                                                        <i className="fa fa-star"></i>
                                                    </li>
                                                </ul>
                                                <div className="label-rating">132 reviews</div>
                                                <div className="label-rating">154 orders </div>
                                            </div> */}
                                            <hr/>
                                            <div className="row">
                                                <div className="col-sm-5">
                                                    <dl className="dlist-inline">
                                                        <dt>Số lượng: </dt>
                                                        <dd>
                                                        <NumericInput 
                                                            min={ 0 } 
                                                            size={ 8 } 
                                                            mobile = {true}
                                                            name="qty" 
                                                            value= { this.state.qty }
                                                            onChange = { (e) => this.handleChange(e) }
                                                            />
                                                        </dd>
                                                    </dl> 
                                                </div>
                                            </div>
                                            <a href="#" className="btn  btn-outline-danger" id="fly-to-cart" onClick={this.handleAddToCart}> 
                                                 <i className="fa fa-shopping-cart"></i> 
                                                <span className="mx-2">{this.state.langs[locale]['add_to_cart']}</span>
                                            </a>
                                        </article> 
                                    </aside>
                                </div> 
                            </div> 
                        </div> 
                    </section>
            </div>
        );
    }
}


if (document.getElementById('react-detail')) {
    ReactDOM.render(<ProductDetailComponent />, document.getElementById('react-detail'));
}