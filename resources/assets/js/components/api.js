import axios from 'axios';

var MemberAPI = {
    categories: [
        { 
        number: 1, 
        nameProduct: "Assus", 
        infomations: [
            {name:'CPU',
                params:['Core i5','Core i7']},
            {name:'RAM',
                params:['8GB','16GB']},
        ]
        ,slug:"Assus" },

        { number: 2, nameProduct: "Dave Defender", infomations: "CEO",slug:"dave-defender" },
        { number: 3, nameProduct: "Sam Sweeper", infomations: "CTO",slug:"sam-sweeper" },
        { number: 4, nameProduct: "Matt Midfielder", infomations: "CIO",slug:"matt-midfielder" },
        { number: 5, nameProduct: "William Winger", infomations: "Member",slug:"william-winger" },
        { number: 6, nameProduct: "Fillipe Forward", infomations: "Member",slug:"fillipe-forward" }
    ],
    data:[],
    all: function() { 
        let productEndpoint = `https://reqres.in/api/users?page=2`
        axios.get(productEndpoint).then(res => {
            this.data = res.data
        });
        return this.categories
    },
    get: function(id) {
        const isMember = p => p.number === id
        return this.categories.find(isMember)
    },
    getBySlug: function(slugFound) { 
        const isMember = a => a.slug === slugFound
        return this.categories.find(isMember)
    },
    getData: function() { 
        return this.data
    }
}

export default MemberAPI
