import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import { BrowserRouter } from 'react-router-dom'
import App from './filtercomponents/App';

export default class FilterComponent extends Component {
    constructor() {
        super();
    }
    
    componentWillMount() {
        
    }
    componentDidMount() {
    }

    render() {
        return (
        <BrowserRouter>
            <App  />
        </BrowserRouter>
        )
    }
}


if (document.getElementById('react-filter')) {
    ReactDOM.render(<FilterComponent />, document.getElementById('react-filter'));
}