<div data-toggle="tooltip" data-placement="top" style="display: inline-block;" title="Xem thông tin đối tượng">
<button type="button" class="btn btn-sm btn-info" data-toggle="modal" data-target="{{ '#show' . $row->id }}">
    <i class="fa fa-eye"></i>
</button>
</div>

<div class="modal fade" id="{{ 'show' . $row->id }}" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content  -->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Thông tin thêm</h4>
                <label data-dismiss="modal" style="cursor: pointer;"><i class="fa fa-times"></i></label>
            </div>
            <div class="modal-body text-left">
                <p>Họ tên: <b>{{ $row->name }}</b></p>
                <p>Email: <b>{{ $row->email }}</b></p>
                <p>SĐT: <b>{{ $row->phone }}</b></p>
                <p>SL sản phẩm: <b>{{ $row->amount_product }}</b></p>
                <p>Ngày mua : <b>{{ $row->purchase_date }}</b></p>
                <p>Địa điểm: <b>{{ $row->location }}</b></p>
                <p>Invite: <b>{{ $row->getInvite() }}</b></p>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-brand active" data-dismiss="modal">Thoát</button>
            </div>
        </div>
    </div>
</div>
