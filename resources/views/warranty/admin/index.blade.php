@section('title') Bảo hành @endsection @extends('layouts.admin') @section('content')
@if(Session::has('status'))
<div class="alert alert-success">
    {{ Session::get('status') }}
</div>
@endif
<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    Danh sách liên hệ bảo hành
                </h3>
            </div>
        </div>
    </div>
    <div class="m-portlet__body">
        <!--begin: Datatable -->
        <div class="m_datatable m-datatable m-datatable--default m-datatable--loaded" style="">
            <table class="m-datatable__table" style="display: block; min-height: 300px; overflow-x: auto;">
                    <thead class="m-datatable__head">
                            <tr class="m-datatable__row" style="left: 0px;">
                        <th class="m-datatable__cell--center m-datatable__cell "> <span style="width: 100px  ;">#</span></th>
                        <th class="m-datatable__cell--center m-datatable__cell "> <span style="width: 100px  ;">Người gửi</span></th>
                        <th class="m-datatable__cell--center m-datatable__cell "> <span style="width: 100px  ;">Email</span></th>
                        <th class="m-datatable__cell--center m-datatable__cell "> <span style="width: 100px  ;">Điện
                                thoại</span></th>
                        <th class="m-datatable__cell--center m-datatable__cell "> <span style="width: 100px  ;">Hành động</span></th>
                    </tr>
                </thead>
                        <tbody style="" class="m-datatable__body">
                    <?php $index = 1; ?>
                    @foreach($warranties as $row)
                            <tr class="m-datatable__row" style="left: 0px;">
                        <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 100px  ;">{{ $index }}</span></td>
                        <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 100px ;">
                                {{ $row->name }}
                            </span></td>
                        <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 100px ;">
                                {{ $row->email }}
                            </span></td>
                        <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 100px ;">
                                {{ $row->phone }}
                            </span></td>
                        <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 100px ;">
                                <div class=" ilv-btn-group">
                                    @include('warranty.admin.show')
                                    @include('warranty.admin.btn_delete')
                                </div>
                            </span></td>
                    </tr>
                    <?php $index++ ?>
                    @endforeach
                </tbody>
            </table>
            {{ $warranties->links() }}
        </div>
    </div>
</div>

@endsection