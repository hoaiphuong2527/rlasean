@extends('layouts.main')
@section('title')
{{ __('page_text.pages.warrant') }}
@endsection
@section('meta')
<meta name="description" content="{{ $option->name }}">
<meta name="keywords" content="{{ $option->name }}">
<meta name="author" content="{{ $option->name }}">
<meta property="og:url"           content="{{ route('guest_warranties.index.'.app()->getLocale()) }}" />
<meta property="og:type"          content="article" />
<meta property="og:title"         content="{{ __('page_text.pages.warrant') }}" />
<meta property="og:description"   content="{{ __('page_text.pages.warrant') }}" />
<meta property="og:image"         content="{{ $option->linkImage('main_logo') }}" />
@endsection
@section('content')

@include('guest.component.banner_title', ['title' => __('page_text.warrant.page_title'), 'imageLink' => $banner->linkImage()])
<div class="container">
    {!! Breadcrumbs::render('guest_warranties.index.'.app()->getLocale())!!}
</div>
<div class="container ilv-mt-60 ilv-mb-60">
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
                @include('guest.component.title_small', ['title' => __('page_text.warrant.title')])
                @include('guest.component.title_small', ['title' => __('page_text.warrant.register')])
                    <form action="{{ route('guest_warranties.store.'.app()->getLocale()) }}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col-md-6 col-sm-12 form-group">
                            <label for="name">{{ __('component_text.labels.firstname') }}</label>
                                <input type="text" name="f_name" class="form-control" id="name" required>
                                @if($errors->has('f_name'))
                                <small style=" color: red !important;" class="form-text text-muted">{{
                                    $errors->first('name') }}</small>
                                @endif
                            </div>
                            <div class="col-md-6 col-sm-12 form-group">
                                    <label for="name">{{ __('component_text.labels.lastname') }}</label>
                                    <input type="text" name="l_name" class="form-control" id="name" required>
                                    @if($errors->has('l_name'))
                                    <small style=" color: red !important;" class="form-text text-muted">{{
                                        $errors->first('name') }}</small>
                                    @endif
                                </div>
                            <div class="col-md-6 col-sm-12 form-group">
                                <label for="email">{{ __('component_text.labels.email') }}</label>
                                <input type="email" name="email" class="form-control" id="email" required>
                                @if($errors->has('email'))
                                <small style=" color: red !important;" class="form-text text-muted">{{
                                    $errors->first('email') }}</small>
                                @endif </div>
                            <div class="col-md-6 col-sm-12 form-group">
                                <label for="email">{{ __('component_text.labels.phone') }}</label>
                                <input type="number" name="phone" class="form-control" id="phone"  required>
                                @if($errors->has('phone'))
                                <small style=" color: red !important;" class="form-text text-muted">{{
                                    $errors->first('phone') }}</small>
                                @endif
                            </div>
                            <div class="col-md-6 col-sm-12 form-group">
                                <label for="email">{{ __('component_text.labels.amount') }}</label>
                                <select class="form-control" name='amount_product'>
                                    @for($i = 1; $i <= 10; $i++)
                                    <option value="{{ $i }}">{{ $i }}</option>
                                    @endfor
                                </select>
                                @if($errors->has('amount_product'))
                                <small style=" color: red !important;" class="form-text text-muted">{{
                                    $errors->first('amount_product') }}</small>
                                @endif
                            </div>
                            <div class="col-md-6 col-sm-12 form-group">
                                <label for="email">{{ __('component_text.labels.purchase_date') }}</label>
                                <input type="date" name="purchase_date" class="form-control" id="purchase_date" aria-describedby="emailHelp"
                                    placeholder="" required>
                                @if($errors->has('purchase_date'))
                                <small style=" color: red !important;" class="form-text text-muted">{{
                                    $errors->first('purchase_date') }}</small>
                                @endif
                            </div>
                            <div class="col-md-6 col-sm-12 form-group">
                                <label for="email">{{ __('component_text.labels.location') }}</label>
                                <select class="form-control" name='location'>
                                    @foreach(config('ecommerce.warranty_location') as $key => $location)
                                    <option value="{{ $key }}">{{ $location }}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('location'))
                                <small style=" color: red !important;" class="form-text text-muted">{{
                                    $errors->first('location') }}</small>
                                @endif
                            </div>
                            <div class="col-md-12 col-sm-12 form-group">
                                <label for="email">{{ __('component_text.labels.invite') }}</label>
                                <div class="row">
                                    <div class="col">

                                        <input type="radio" name="is_invite" value="1"  id="is_invite_1" aria-describedby="emailHelp"
                                        placeholder="" required> {{ __('component_text.labels.yes') }}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">

                                        <input type="radio" name="is_invite" value="0"  id="is_invite_2" aria-describedby="emailHelp"
                                        placeholder="" required> {{ __('component_text.labels.no') }}
                                        @if($errors->has('is_invite'))
                                        <small style=" color: red !important;" class="form-text text-muted">{{
                                            $errors->first('is_invite') }}</small>
                                    @endif
                                </div>
                                </div>
                            </div>
                        </div>

                        <div class="mx-auto">
                            <button type="submit" class="btn btn-outline-primary">{{ __('component_text.buttons.send') }}</button></div>
                    </form>
        </div>
    </div>
</div>

@endsection
