<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>{{ $option->name }} - @yield('title')</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    @yield('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Favicons -->
    <link href="{{ $option->favicon }}" rel="icon">
    <link href="{{ $option->favicon }}" rel="apple-touch-icon">
    <meta name="description" content="{{ $option->name }}">
    <meta name="keywords" content="{{ $option->name }}">
    <meta name="author" content="{{ $option->name }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Bootstrap CSS File -->
    <!-- Font awesome-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">


    <!-- Libraries CSS Files -->
    <link href="{{ asset('lib/nivo-slider/css/nivo-slider.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('lib/owlcarousel/assets/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('lib/owlcarousel/assets/owl.theme.default.min.css') }}">
    <link href="{{ asset('lib/animate/animate.min.css') }}" rel="stylesheet">
    <link href="{{ asset('lib/venobox/venobox.css') }}" rel="stylesheet">


    <!-- Nivo Slider Theme -->
    <link href="{{ asset('css/nivo-slider-theme.css') }}" rel="stylesheet">

    <!-- Main Stylesheet File -->

    <!-- Responsive Stylesheet File -->
    <link href="{{ asset('css/responsive.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- =======================================================
    Theme Name: eBusiness
    Theme URL: https://bootstrapmade.com/ebusiness-bootstrap-corporate-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
    ======================================================= -->
</head>

<body data-spy="scroll" data-target="#navbar-example">
    <div id="preloader"></div>
    <script>
        var locale = "{!! app()->getLocale() !!}"

    </script>
    <!-- Start Header Area -->
    @include('shared.public_header')
    <!-- header end -->
    @yield('content')

    @include('components.shopping_cart_modal')

    <!-- Start Footer bottom Area -->
    @include('shared.public_footer')
    <!-- footer end -->

    <!-- JavaScript Libraries -->
    <script src="{{ asset('lib/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('lib/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('lib/owlcarousel/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('lib/venobox/venobox.min.js') }}"></script>
    <script src="{{ asset('lib/knob/jquery.knob.js') }}"></script>
    <script src="{{ asset('lib/wow/wow.min.js') }}"></script>
    <script src="{{ asset('lib/parallax/parallax.js') }}"></script>
    <script src="{{ asset('lib/easing/easing.min.js') }}"></script>
    <script src="{{ asset('lib/nivo-slider/js/jquery.nivo.slider.js') }}" type="text/javascript"></script>
    <script src="{{ asset('lib/appear/jquery.appear.js') }}"></script>
    <script src="{{ asset('lib/isotope/isotope.pkgd.min.js') }}"></script>
    <!-- Uncomment below if you want to use dynamic Google Maps -->
    <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD8HeI8o-c1NppZA-92oYlXakhDPYR7XMY"></script> -->
    <script src="{{ asset('lib/clamp/clamp.js') }}"></script>

    <!-- Contact Form JavaScript File -->
    <script src="{{ asset('contactform/contactform.js') }}"></script>
    <script>
        // jQuery code
        /////////////////  items carousel. /plugins/owlcarousel/
        if ($('.owl-init').length > 0) { // check if element exists

            $(".owl-init").each(function () {
                var itemsDesktop = $(this).attr('data-items-desk');
                var itemsMobile = $(this).attr('data-items-mobile');
                var itemsTab = $(this).attr('data-items-tab');
                var dataLoop = $(this).attr('data-loop') ? $(this).attr('data-loop'):true;
                var myOwl = $(this);
                var data_auto_play = myOwl.data('auto-play');
                var data_nav = $(this).attr('data-nav');
                var data_dots = $(this).attr('dots');
                var data_margin = 0;
                var data_custom_nav = myOwl.data('custom-nav');
                var id_owl = myOwl.attr('id');
                myOwl.owlCarousel({
                    loop: eval(dataLoop),
                    margin: data_margin,
                    nav: eval(data_nav),
                    dots: eval(data_dots),
                    autoplay: eval(data_auto_play),
                    navText: ["<i class='fa fa-chevron-left'></i>",
                        "<i class='fa fa-chevron-right'></i>"
                    ],
                    //items: 4,
                    responsive: {
                        0: {
                            items: itemsMobile
                        },
                        600: {
                            items: itemsTab
                        },
                        1000: {
                            items: itemsDesktop
                        }
                    }
                });

                // for custom navigation. See example page: example-sliders.html
                $('.' + data_custom_nav + '.owl-custom-next').click(function () {
                    $('#' + id_owl).trigger('next.owl.carousel');
                });

                $('.' + data_custom_nav + '.owl-custom-prev').click(function () {
                    $('#' + id_owl).trigger('prev.owl.carousel');
                });

            }); // each end.//
        } // end if

    </script>
    <script src="{{ asset('js/main.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>

</body>


</html>
