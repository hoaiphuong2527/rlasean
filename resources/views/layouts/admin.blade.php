<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 4
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<html lang="en">
<!-- begin::Head -->

<head>
    <meta charset="utf-8" />
    <title>@yield('title') - {{ $option->name }}</title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{ $option->favicon }}" rel="icon">
    <link href="{{ $option->favicon }}" rel="apple-touch-icon">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/daterangepicker.css') }}" />

    <!--begin::Web font -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: {
                "families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
            },
            active: function () {
                sessionStorage.fonts = true;
            }
        });

    </script>
    <!-- <link href="{{ asset('froala/css/froala_editor.pkgd.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('froala/css/froala_style.min.css') }}" rel="stylesheet" type="text/css" /> -->
    <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
    <link href="{{ asset('css/ie-admin.css') }}" rel="stylesheet">
    <link href="{{ asset('css/color-picker.css') }}" rel="stylesheet">

    <link href="{{ asset('css/multi-select.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap-multiselect.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU"
        crossorigin="anonymous">
    <link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico" />
    <link rel="stylesheet" href="{{ asset('css/dropzone.css') }}" />
    {{-- <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" /> --}}
</head>
<!-- end::Head -->
<!-- begin::Body -->

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
    <!-- begin:: Page -->
    <div class="m-grid m-grid--hor m-grid--root m-page">
        <!-- BEGIN: Header -->
        @include('shared.admin_header')
        <!-- END: Header -->
        <!-- begin::Body -->
        @include('shared.admin_sidebar')
        <!-- end:: Body -->
        <!-- begin::Footer -->
        @include('shared.admin_footer')
        <!-- end::Footer -->
    </div>
    <script src="{{ asset('js/jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset('js/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/daterangepicker.min.js') }}"></script>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"></script>

    <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-bs4.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-bs4.js"></script> -->

    <!-- <script type="text/javascript" src="{{ asset('froala/js/froala_editor.pkgd.min.js') }}"></script> -->
    <script src="{{ asset('js/jquery.multi-select.js') }}"></script>
    <script src="{{ asset('js/dropzone.js') }}"></script>
    <script src="{{ asset('js/scripts.bundle.js') }}" defer></script>
    {{-- <script src="{{ asset('js/select2.min.js') }}"></script>--}}
    <script type="text/javascript" src="{{  asset('js/select2.js') }}"></script>
    <script src="{{ asset('js/admin.js') }}" type="text/javascript"></script>

    <script>
        $(document).ready(function () {
            $('#product_id').select2();
            $('#information_id').select2();
            $('.js-example-basic-multiple').select2();
        });
    </script>
    @yield('scrpit')
    <script src="{{ asset('js/vendors.bundle.js') }}" defer></script>
    <script src="{{ asset('js/tinymce/tinymce.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.js" charset="utf-8"></script>
</body>

</html>
