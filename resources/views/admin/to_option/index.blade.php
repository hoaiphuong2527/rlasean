@section('title') Cài đặt @endsection @extends('layouts.admin') @section('content')
<div class="m-portlet m-portlet--mobile">
    <div class="row">
        <div class="col-md-12">
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    Thêm admin
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <form class="m-form m-form--fit m-form--label-align-right" method="post"
                            action="{{ route('admin_option.store') }}">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <select class="form-control" name="admins">
                                    @if(count($user_is_admins) == 0)
                                    <option value="" disabled selected>None</option>
                                    @endif @foreach($user_is_admins as $admin)
                                    <option value="{{ $admin['id'] }}">{{$admin['name']}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <input type="submit" class="btn btn-info" value="Thêm" />
                        </form>
                    </div>
                    <br>
                    <div class="m-portlet m-portlet--tab">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <span class="m-portlet__head-icon m--hide">
                                        <i class="la la-gear"></i>
                                    </span>
                                    <h3 class="m-portlet__head-text">
                                        Danh sách Admin nhận mail
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__body">
                            <!--begin: Datatable -->
                            <div class="m_datatable m-datatable m-datatable--default m-datatable--loaded" style="">
                                <table class="m-datatable__table"
                                    style="display: block; min-height: 300px; overflow-x: auto;">
                                    <thead class="m-datatable__head">
                                        <tr class="m-datatable__row" style="left: 0px;">
                                            <th class="m-datatable__cell--center m-datatable__cell "> <span
                                                    style="width: 20px ;">#</span></th>
                                            <th class="m-datatable__cell--center m-datatable__cell "> <span
                                                    style="width: 100px ;">Tên
                                                    admin</span></th>
                                            <th class="m-datatable__cell--center m-datatable__cell "> <span
                                                    style="width: 180px ;">Email</span></th>
                                            <th class="m-datatable__cell--center m-datatable__cell "> <span
                                                    style="width: 100px ;">Hành
                                                    động</span></th>
                                        </tr>
                                    </thead>
                                    <tbody style="" class="m-datatable__body">
                                        <?php $index = 1; ?>
                                        @foreach($admins as $item)
                                        <tr class="m-datatable__row" style="left: 0px;">
                                            <td class="m-datatable__cell--center m-datatable__cell"> <span
                                                    style="width: 20px ;">{{
                                                    $index }}</span></td>
                                            <td class="m-datatable__cell--center m-datatable__cell"> <span
                                                    style="width: 100px ;">
                                                    {{ $item->user->name }}
                                                </span></td>
                                            <td class="m-datatable__cell--center m-datatable__cell"> <span
                                                    style="width: 180px ;">
                                                    {{ $item->user->email }}
                                                </span></td>
                                            <td class="m-datatable__cell--center m-datatable__cell"> <span
                                                    style="width: 100px ;">
                                                    <div class=" ilv-btn-group">
                                                        <div data-toggle="tooltip" data-placement="top"
                                                            style="display: inline-block;" title="Xóa đối tượng">
                                                            <form method="post"
                                                                action="{!! route('admin_option.destroy', ['id' => $item->id]) !!}">
                                                                <input type="hidden" name="_method" value="DELETE" /> {!!
                                                                csrf_field()
                                                                !!}
                                                                <button type="submit" class="btn btn-danger">
                                                                    <i class="fa fa-trash-alt"></i>
                                                                </button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </span></td>
                                        </tr>
                                        <?php $index++ ;?>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        <div class="col-md-12">
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Thông tin
                            </h3>
                        </div>
                    </div>
                </div>
                @if (session('status'))
                <div class="alert alert-info">{{session('status')}}</div>
                @endif
                <form class="m-form m-form--fit m-form--label-align-right" method="POST"
                    action="{{ route('admin_option.update',['id' => $option->id]) }}" enctype="multipart/form-data">
                    <input type="hidden" name="_method" value="PUT" /> {{ csrf_field() }}
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-tools">
                            <ul class="nav nav-tabs m-tabs m-tabs-line  reload-page m-tabs-line--right m-tabs-line-danger"
                                role="tablist">
                                <li class="nav-item m-tabs__item">
                                    <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_portlet_tab_1_1"
                                        role="tab" aria-selected="false">
                                        Thông tin cơ bản
                                    </a>
                                </li>
                                <li class="nav-item m-tabs__item">
                                    <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_portlet_tab_1_2"
                                        role="tab" aria-selected="false">
                                        Đa ngôn ngữ
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="m_portlet_tab_1_1">
                                <div class="form-group m-form__group row">
                                    <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                                        <div class="form-group m-form__group">
                                            <label for="">Tên công ty</label>
                                            <input type="text" class="form-control m-input m-input--square" id=""
                                                placeholder="Tên công ty" name="name"
                                                value="{{ old('name',$option->name) }}">
                                            @if ($errors->has('name'))
                                            <span class="invalid-feedback" style="display:block;">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                        </div>

                                        <div class="form-group m-form__group">
                                            <label for="">Email công ty</label>
                                            <input type="text" class="form-control m-input m-input--square" id=""
                                                placeholder="Email công ty" name="email"
                                                value="{{ old('email',$option->email) }}">
                                            @if ($errors->has('email'))
                                            <span class="invalid-feedback" style="display:block;">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-group m-form__group">
                                            <label for="">Số điện thoại</label>
                                            <input type="text" class="form-control m-input m-input--square" id=""
                                                placeholder="Số điện thoại" name="phone"
                                                value="{{ old('phone',$option->phone) }}">
                                            @if ($errors->has('phone'))
                                            <span class="invalid-feedback" style="display:block;">
                                                <strong>{{ $errors->first('phone') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-group m-form__group">
                                            <label for="">Địa chỉ</label>
                                            <textarea rows='3' class="form-control m-input m-input--square"
                                                placeholder="Địa chỉ" name="address" id="addressBE"
                                                >{{ old('address',$option->address) }}</textarea>
                                            @if ($errors->has('address'))
                                            <span class="invalid-feedback" style="display:block;">
                                                <strong>{{ $errors->first('address') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        @include('admin.component.change_address')
                                        <div class="form-group m-form__group">
                                            <label for="">Ifram google</label>
                                            <textarea class="form-control m-input m-input--square" rows="3"
                                                placeholder="Ifram google"
                                                name="iframe_gg">{{ old('iframe_gg',$option->iframe_gg) }}</textarea>
                                            @if ($errors->has('ifram_google'))
                                            <span class="invalid-feedback" style="display:block;">
                                                <strong>{{ $errors->first('ifram_google') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-group m-form__group">
                                            <label for="">Sologan</label>
                                            <textarea class="form-control m-input m-input--square" rows="3"
                                                placeholder="Sologan"
                                                name="sologan">{{ old('sologan',$option->sologan) }}</textarea>
                                            @if ($errors->has('sologan'))
                                            <span class="invalid-feedback" style="display:block;">
                                                <strong>{{ $errors->first('sologan') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class=" d-flex">
                                            <div class=" form-group m-form__group col-lg-6 col-md-12 col-sm-12">
                                                    <label>Favicon</label>
                                                <script>
                                                    var previewImage = []
                                                    var single = {!!collect($option -> getImg('favicon')) !!}
                                                </script>
                                                <div id="react-admin-upload-icon-btn" data-img=""
                                                    data-input-name="favicon" data-type="single">
                                                    </div>
                                            </div>
                                            <div class=" form-group m-form__group col-lg-6 col-md-12 col-sm-12">
                                                <label>Logo</label>
                                                <script>
                                                    var previewImage1 = []
                                                    var single1 = {!!collect($option -> getImg('main_logo')) !!}
                                                </script>
                                                <div id="react-admin-upload-img-btn" data-img=""
                                                    data-input-name="main_logo" data-type="single"></div>
                                            </div>
                                            <br>
                                            <!--
                                            <label>Logo footer</label>
                                            <div class=" col-lg-12 col-md-12 col-sm-12">
                                                <script>
                                                    var previewImage2 = []
                                                    var singlef = {!!collect($option -> getImg('footer_logo')) !!}
                                                </script>
                                                <div id="react-admin-upload-logo-footer-btn" data-img=""
                                                    data-input-name="footer_logo" data-type="single"></div>
                                            </div>
                                            -->
                                            <hr>
                                        </div>

                                            <h5>Social link:</h5>
                                            <div class="form-group m-form__group">
                                                <label for="">Facebook</label>
                                                <input type="text" class="form-control m-input m-input--square" id=""
                                                    placeholder="Facebok link" name="facebook_acc"
                                                    value="{{ old('facebook_acc',$option->facebook_acc) }}">
                                                @if ($errors->has('facebook_link'))
                                                <span class="invalid-feedback" style="display:block;">
                                                    <strong>{{ $errors->first('facebook_acc') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                            <div class="form-group m-form__group">
                                                <label for="">Youtube</label>
                                                <input type="text" class="form-control m-input m-input--square" id=""
                                                    placeholder="Youtube link" name="youtube_acc"
                                                    value="{{ old('youtube_acc',$option->youtube_acc) }}">
                                                @if ($errors->has('youtube_acc'))
                                                <span class="invalid-feedback" style="display:block;">
                                                    <strong>{{ $errors->first('youtube_acc') }}</strong>
                                                </span>
                                                @endif
                                            </div>

                                            <div class="form-group m-form__group">
                                                <label for="">Instagram</label>
                                                <input type="text" class="form-control m-input m-input--square" id=""
                                                    placeholder="Instagram link" name="intagram_acc"
                                                    value="{{ old('intagram_acc',$option->intagram_acc) }}">
                                                @if ($errors->has('intagram_acc'))
                                                <span class="invalid-feedback" style="display:block;">
                                                    <strong>{{ $errors->first('intagram_acc') }}</strong>
                                                </span>
                                                @endif
                                            </div>

                                        <hr>
                                        <!-- <div class="form-group m-form__group">
                                            <label for="">Phương thức thanh toán</label>
                                            <div class="checkbox">
                                                <label class="m-switch m-switch--outline m-switch--primary">
                                                    <input id="chkPayment" type="checkbox" @if($option->type_order == 'HP') checked
                                                    @endif>
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div> -->
                                        <div class="form-group m-form__group">
                                            <label for="">Phần trăm tỷ lệ tính giá trị cho hàng dễ vỡ</label>
                                            <div class="input-group">
                                                <input name="percent_breakable" type="text" class="form-control m-input" value="{{ $option->percent_breakable }}" />
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        %
                                                    </span>
                                                </div>
                                            </div>
                                            <br>
                                                <label for="">Địa chỉ kho hàng</label>
                                                <textarea rows='3' class="form-control m-input m-input--square"
                                                    placeholder="Địa chỉ" name="address_stock" id="addressStock"
                                                    >{{ old('address_stock',$option->address_stock) }}</textarea>
                                                @if ($errors->has('address_stock'))
                                                <span class="invalid-feedback" style="display:block;">
                                                    <strong>{{ $errors->first('address_stock') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                            @include('admin.component.change_address_stock')
                                            <div class="form-group m-form__group">
                                                <label for="">Token Giao hàng nhanh</label>
                                                <input type="text" class="form-control m-input m-input--square" id=""
                                                    placeholder="Token Giao hàng nhanh" name="ghn_token"
                                                    value="{{ old('ghn_token',$option->ghn_token) }}">
                                                @if ($errors->has('ghn_token'))
                                                <span class="invalid-feedback" style="display:block;">
                                                    <strong>{{ $errors->first('ghn_token') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                            <div class="form-group m-form__group">
                                                <label for="">Token Giao hàng tiết kiệm</label>
                                                <input type="text" class="form-control m-input m-input--square" id=""
                                                    placeholder="Token Giao hàng tiết kiệm" name="ghtk_token"
                                                    value="{{ old('ghtk_token',$option->ghtk_token) }}">
                                                @if ($errors->has('ghtk_token'))
                                                <span class="invalid-feedback" style="display:block;">
                                                    <strong>{{ $errors->first('ghtk_token') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="m_portlet_tab_1_2">
                                <div class="m-portlet m-portlet--tabs">
                                    <div class="m-portlet__head">
                                        <div class="m-portlet__head-tools">
                                            <ul class="nav nav-tabs m-tabs m-tabs-line  reload-page m-tabs-line--right m-tabs-line-danger"
                                                role="tablist">
                                                @foreach(config('ecommerce.locale_displays') as $key => $lang)
                                                <li class="nav-item m-tabs__item">
                                                    <a class="nav-link m-tabs__link" data-toggle="tab"
                                                        href="#m_portlet_tab_lang_{{$key}}" role="tab"
                                                        aria-selected="false">
                                                        {{ $lang }}
                                                    </a>
                                                </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="m-portlet__body">
                                        <div class="tab-content">
                                            @foreach(config('ecommerce.locales') as $key => $lang)
                                            <div class="tab-pane" id="m_portlet_tab_lang_{{ $lang }}">
                                                @include('components.lang',['item' => $option, 'lable' => 'Tên công
                                                ty','col_name' => 'name','type' => 'input'])
                                                @include('components.lang',['item' => $option, 'lable' => 'Địa
                                                chỉ','col_name' => 'address','type' => 'textarea'])
                                                @include('components.lang',['item' => $option, 'lable' =>
                                                'Sologan','col_name' => 'sologan','type' => 'textarea'])
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions text-center">
                            <input type="submit" class="btn btn-primary" value="Lưu">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
