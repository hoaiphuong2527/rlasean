@extends('layouts.admin')
@section('title')
{{ __('Tổng quan') }}
@endsection
@section('content')
<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    {{ __('Tổng quan') }}
                </h3>
            </div>
        </div>
    </div>
    <div class="m-portlet ">
        <div class="m-portlet__body  m-portlet__body--no-padding">
            <div class="row m-row--no-padding m-row--col-separator-xl">
                <div class="col-md-12 col-lg-6 col-xl-3">
                    <!--begin::Total Profit-->
                    <div class="m-widget24">
                        <div class="m-widget24__item m--align-center">
                            <h3 class="m-widget24__title">
                                {{ __('Tổng sản phẩm') }}
                            </h3>
                        </div>
                        <div class="m--space-10"></div>
                        <h4
                            class="m-widget24__stats m--font-brand custom-widget24 m--align-center m--icon-font-size-lg5">
                            {{ $total_products }}
                        </h4>
                    </div>
                    <!--end::Total Profit-->
                </div>
                <!-- <div class="col-md-12 col-lg-6 col-xl-3">
                        <div class="m-widget24">
                            <div class="m-widget24__item m--align-center">
                                <h3 class="m-widget24__title">
                                    {{ __('Người dùng đồng hành') }}
                                </h3>
                            </div>
                            <div class="m--space-10"></div>
                            <h4 class="m-widget24__stats m--font-info custom-widget24 m--align-center m--icon-font-size-lg5">
                                {{ $users }}
                            </h4>
                        </div>
                    </div> -->
                <div class="col-md-12 col-lg-6 col-xl-3">
                    <!--begin::New Orders-->
                    <div class="m-widget24">
                        <div class="m-widget24__item m--align-center">
                            <h3 class="m-widget24__title">
                                {{ __('Đơn hàng mới') }}
                            </h3>
                        </div>
                        <div class="m--space-10"></div>
                        <h4
                            class="m-widget24__stats m--font-danger custom-widget24 m--align-center m--icon-font-size-lg5">
                            {{ $total_order_new }}
                        </h4>
                    </div>
                    <!--end::New Orders-->
                </div>
                <div class="col-md-12 col-lg-6 col-xl-3">
                    <!--begin::New Orders-->
                    <div class="m-widget24">
                        <div class="m-widget24__item m--align-center">
                            <h3 class="m-widget24__title">
                                {{ __('Đơn hàng đang xử lí') }}
                            </h3>
                        </div>
                        <div class="m--space-10"></div>
                        <h4
                            class="m-widget24__stats m--font-danger custom-widget24 m--align-center m--icon-font-size-lg5">
                            {{ $waiting_order }}
                        </h4>
                    </div>
                    <!--end::New Orders-->
                </div>
                <div class="col-md-12 col-lg-6 col-xl-3">
                    <!--begin::New Users-->
                    <div class="m-widget24">
                        <div class="m-widget24__item m--align-center">
                            <h3 class="m-widget24__title">
                                {{ __('Tổng đơn hàng') }}
                            </h3>
                        </div>
                        <div class="m--space-10"></div>
                        <h4
                            class="m-widget24__stats m--font-success custom-widget24 m--align-center m--icon-font-size-lg5">
                            {{ $total_orders }}
                        </h4>
                    </div>
                    <!--end::New Users-->
                </div>
            </div>
        </div>
    </div>
</div>
<div class="m-portlet m-portlet--mobile">
    <canvas id="canvas"></canvas>
    <!-- <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12 text-center" style="padding-top:15px">
            <h4>Doanh thu năm <?php echo date("Y"); ?></h4>
        </div>
        {!! $chart->container() !!}
        {!! $chart->script() !!} -->
</div>
<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    Danh sách đơn hàng
                </h3>
            </div>
        </div>
    </div>
    <div class="m-portlet__body">
        <div class="m_datatable m-datatable m-datatable--default m-datatable--loaded" style="">
            <table class="m-datatable__table" style="display: block; min-height: 300px; overflow-x: auto;">
                <thead class="m-datatable__head">
                    <tr class="m-datatable__row" style="left: 0px;">
                        <th class=" m-datatable__cell "> <span style="width: 160px ;">
                                Mã Đơn
                            </span></th>
                        <th class="m-datatable__cell--center m-datatable__cell "> <span style="width: 150px ;">
                                TTKH
                            </span></th>
                        <th class=" m-datatable__cell "> <span style="width: 150px ;">
                                Ngày đặt
                            </span></th>
                        <th class=" m-datatable__cell "> <span style="width: 100px ;">
                                Tổng tiền
                            </span></th>
                        <th class=" m-datatable__cell "> <span style="width: 80px ;">
                                Trạng thái
                            </span></th>
                        <th class=" m-datatable__cell "> <span style="width: 80px ;">
                                Thanh toán
                            </span></th>
                        <th class=" m-datatable__cell "> <span style="width: 90px ;">
                                Hành động
                            </span></th>
                    </tr>
                </thead>
                <tbody style="" class="m-datatable__body">
                    @foreach($latest_orders as $item)
                    <tr class="m-datatable__row" style="left: 0px;">
                        <td class=" m-datatable__cell"> <span style="width: 160px ;">
                                {!! $item->order_number_format !!}
                            </span></td>
                        <td class="m-datatable__cell--left m-datatable__cell"> <span style="width: 150px ;">
                                {{ $item->customerInfo() }}
                            </span></td>
                        <td class=" m-datatable__cell"> <span style="width: 150px ;">
                                {{ $item->create_date }}
                            </span></td>
                        <td class=" m-datatable__cell"> <span style="width: 100px ;">
                                {{ $item->getPrice() }}
                            </span></td>
                        <td class=" m-datatable__cell"> <span style="width: 80px ;">
                                {{ $item->getStatus() }}
                            </span></td>
                        <td class=" m-datatable__cell"> <span style="width: 80px ;">
                                {{ $item->getPaymentType() }}
                            </span></td>
                        <td class=" m-datatable__cell"> <span style="width: 90px ;">
                                <div class="ilv-btn-group">
                                    <div data-toggle="tooltip" data-placement="top" style="display: inline-block;"
                                        title="Chỉnh sửa đơn hàng">
                                        <a class="btn btn-sm btn-warning" href="{{ $item->urlAdminEdit() }}"
                                            style="text-decoration: none;">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                    </div>
                                    @include('order.component.btn_delete')
                                </div>
                            </span></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <a href="{{ route('admin_order.index') }}" class="ilv-link-primary col text-right">
                Xem thêm
            </a>
        </div>
    </div>
</div>

@endsection
@section('scrpit')
<script>
    $(document).ready(function () {
        function number_format(number, decimals, dec_point, thousands_sep) {
        // *     example: number_format(1234.56, 2, ',', ' ');
        // *     return: '1 234,56'
            number = (number + '').replace(',', '').replace(' ', '');
            var n = !isFinite(+number) ? 0 : +number,
                    prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
                    sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
                    dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
                    s = '',
                    toFixedFix = function (n, prec) {
                        var k = Math.pow(10, prec);
                        return '' + Math.round(n * k) / k;
                    };
            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '').length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1).join('0');
            }
            return s.join(dec);
        }
        const url = '/api/order-chart';
        let dataChart = []
        axios.get(url).then(function (res) {
            dataChart = res.data
            var config = {
                type: 'line',
                data: {
                    labels: ['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4', 'Tháng 5', 'Tháng 6',
                        'Tháng 7', 'Tháng 8', 'Tháng 9', 'Tháng 10', 'Tháng 11', 'Tháng 12'
                    ],
                    datasets: [{
                        label: "Doanh thu theo tháng",
                        data: dataChart,
                        fill: false,
                        backgroundColor: "#ea5126",
                        borderColor: "#ea5126",
                        borderCapStyle: 'butt',
                        // borderDash: [5, 5],
                    }]
                },
                options: {
                    responsive: true,
                    legend: {
                        position: 'bottom',
                    },
                    hover: {
                        mode: 'label'
                    },
                    scales: {
                        xAxes: [{
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Tháng'
                            }
                        }],
                        yAxes: [{
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'VNĐ'
                            },
                            ticks: {
                                beginAtZero: true,
                                min: 1,
                                beginAtZero:true,
                                callback: function(value, index, values) {
                                    return number_format(value);
                                }
                            }
                        }]
                    },
                    tooltips: {
                        callbacks: {
                            label: function(tooltipItem, chart){
                                var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
                                return datasetLabel +" " + number_format(tooltipItem.yLabel) + " VNĐ";
                            }
                        }
                    },
                    title: {
                        display: true,
                        text: 'Doanh thu năm ' + new Date().getFullYear()
                    }
                }
            };
            var ctx = document.getElementById("canvas").getContext("2d");
            new Chart(ctx, config);
        })

    });

</script>
@endsection
