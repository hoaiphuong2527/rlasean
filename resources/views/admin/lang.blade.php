@extends('layouts.admin')
@section('title')
Đa ngôn ngữ
@endsection
@section('content')
@if (session('success'))
<div class="alert alert-info">{{session('success')}}</div>
@endif
<div class="m-portlet">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    Đa ngôn ngữ
                </h3>
            </div>
        </div>
    </div>
    <form class="m-form m-form--fit m-form--label-align-right" action="{{ route('save_lang',['file_name' => $file_name,'locale' => $locale]) }}" method="POST">
        @csrf
        <div class="m-portlet__body">
            <div class="m-portlet__head">
                <div class="m-portlet__head-tools">
                    <ul class="nav nav-tabs m-tabs m-tabs-line  reload-page m-tabs-line--right m-tabs-line-danger" role="tablist">
                        @foreach($locales as $key => $lang)
                        <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link" href="{{ route('admin_lang_index',['file_name' => $file_name,'locale' => $key]) }}">
                                {{ $lang }}
                            </a>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="form-group m-form__group">
                <textarea class="form-control m-input" name="lang_file" id="lang" cols="120" rows="80">{{ $file }}</textarea>
                <div class="m-portlet__foot m-portlet__foot--fit text-center">
                    <div class="m-form__actions">
                        <input type="submit" class="btn btn-primary" value="Lưu">
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection
