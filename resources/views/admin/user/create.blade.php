@section('title')
Cập nhật thông tin
@endsection
@extends('layouts.admin') 
@section('content')
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<div class="m-portlet">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    Cập nhật thông tin
                </h3>
            </div>
        </div>
    </div>
    <form class="m-form m-form--fit m-form--label-align-right" method="POST" action="{{ route('admin_user.store') }}">
        {{ csrf_field() }}
        <div class="m-portlet__body">
            <div class="form-group m-form__group row">
                <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                    <div class="form-group m-form__group">
                        <label for="col-form-label col-lg-3 col-sm-12">Email</label>
                        <input name="email" type="email" class="form-control m-input" value="{{ old('email') }}">
                        @if ($errors->has('email'))
                            <span class="invalid-feedback" style="display:block;">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span> 
                        @endif
                    </div>
                </div>
                <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                    <div class="form-group m-form__group">
                        <label for="col-form-label col-lg-3 col-sm-12">Tên</label>
                        <input name="name" type="text" class="form-control m-input" value="{{ old('name') }}">
                        @if ($errors->has('name'))
                            <span class="invalid-feedback" style="display:block;">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span> 
                        @endif
                    </div>
                </div>
                <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                    <div class="form-group m-form__group">
                        <label for="col-form-label col-lg-3 col-sm-12">Mật khẩu</label>
                        <input name="password" type="password" class="form-control m-input">
                        @if ($errors->has('password'))
                            <span class="invalid-feedback" style="display:block;">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span> 
                        @endif
                    </div>
                </div>
                <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                    <div class="form-group m-form__group">
                        <label for="col-form-label col-lg-3 col-sm-12">Xác thực mật khẩu</label>
                        <input name="password_confirmation" type="password" class="form-control m-input">
                    </div>
                </div>
                <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                    <div class="form-group m-form__group">
                        <label for="col-form-label col-lg-3 col-sm-12">Loại người dùng</label><br>
                        @foreach(config('ecommerce.user_role_display') as $key => $role)
                        <label class="m-radio m-radio--bold m-radio--state-brand mr-4">
                            <input type="radio" id="role_{{ $key }}" name="role" value="{{ $key }}" @if($key == 0) checked @endif> {{ $role }}
                            <span></span>
                        </label>
                        @endforeach
                    </div>
                </div>
                <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12" id="role_admin">
                    <div class="form-group m-form__group">
                        <label for="col-form-label col-lg-3 col-sm-12">Chức vụ</label><br>
                        @foreach(config('ecommerce.admin_role_display') as $key => $role)
                        <label class="m-radio m-radio--bold m-radio--state-brand">
                            <input type="radio" name="role_admin" value="{{ $key }}" @if($key === config('ecommerce.admin_role.contributor')) checked @endif> {{ $role }}
                            <span></span>
                        </label>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="m-portlet__foot m-portlet__foot--fit">
                <div class="m-form__actions">
                    <input type="submit" class="btn btn-primary" value="Lưu">
                </div>
            </div>
        </div>
    </form>
</div>
@endsection