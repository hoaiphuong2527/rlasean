@section('title') Danh sách người dùng @endsection @extends('layouts.admin') @section('content')

<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    Danh sách người dùng
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <a href="{{ route('admin_user.create') }}" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">Thêm
                        mới</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <form method="get" class="row">
            <div class="m-form__group m-form__group--inline col-md-3">
                <div class="m-form__label">
                    <label>Tìm kiếm</label>
                </div>
                <div class="m-form__control">
                    <input class="form-control" placeholder="Name,email" name="search" value="{{ $search }}">
                </div>
            </div>
            @include('admin.user.component.filter_role')
            @include('admin.user.component.filter_role_admin')
            @include('admin.user.component.filter_flag')
            <div class="m-form__group m-form__group--inline col-md-3">
                <div class="m-form__label" style="margin-top:7px">
                    <label></label>
                </div>
                <div class="m-form__control">
                    <button class="btn btn-success">Tìm kiếm</button>
                </div>
            </div>
        </form>
        <hr>
        <!--begin: Datatable -->
        <div class="m_datatable m-datatable m-datatable--default m-datatable--loaded" style="">
            <table class="m-datatable__table" style="display: block; min-height: 300px; overflow-x: auto;">
                <thead class="m-datatable__head">
                    <tr class="m-datatable__row" style="left: 0px;">
                        <th class="m-datatable__cell--center m-datatable__cell ">
                            <span style="width: 50px;">#</span>
                        </span></th>
                        <th class="m-datatable__cell--center m-datatable__cell  ">
                            <span style="width: 100px;">Tên</span></th>
                        <th class="m-datatable__cell--center m-datatable__cell  ">
                            <span style="width: 150px;">Email</span></th>
                        <th class="m-datatable__cell--center m-datatable__cell  ">
                            <span style="width: 100px;">Điện thoại</span></th>
                        <th class="m-datatable__cell--center m-datatable__cell  ">
                            <span style="width: 100px;">Loại người dùng</span></th>
                        <th class="m-datatable__cell--center m-datatable__cell  ">
                            <span style="width: 100px;">Chức vụ</span></th>
                        <th class="m-datatable__cell--center m-datatable__cell  ">
                            <span style="width: 100px;">Trạng thái</span></th>
                        <th class="m-datatable__cell--center m-datatable__cell  ">
                            <span style="width: 150px;">Hành động</span></th>
                    </tr>
                </thead>
                <tbody style="" class="m-datatable__body">
                    <?php $index = 1; ?>
                    @foreach($users as $user)
                    <tr class="m-datatable__row" style="left: 0px;">
                        <td class="m-datatable__cell--center m-datatable__cell">
                            <span style="width: 50px;">{{ $index}}</span></td>
                        <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 100px;">
                                {{ $user['name'] }}
                        </span></td>
                        <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 150px;">
                                {{ $user->email }}
                        </span></td>
                        <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 100px;">
                                {{ $user->phone }}
                        </span></td>
                        <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 100px;">
                                {{ $user->getRole() }}
                        </span></td>
                        <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 100px;">
                                {{ $user->getRoleAdmin() }}
                        </span></td>
                        <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 100px;">
                                {{ $user->getFlag() }}
                        </span></td>
                        <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 150px; margin-left: 30px">
                                <div class="ilv-btn-group">
                                    <div data-toggle="tooltip" data-placement="top" style="display: inline-block;" title="Chỉnh sửa thông tin người dùng">
                                <a class="btn btn-warning" href="{{ $user->urlAdminEdit() }}">
                                    <i class="fa fa-edit"></i>
                                </a>
                                    </div>
                                    @if($user->flag == 0)
                                        @include('admin.user.component.update_flag_true')
                                    @else
                                        @include('admin.user.component.update_flag')
                                    @endif
                                </div>
                        </span></td>
                    </tr>
                    <?php $index++ ?>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection