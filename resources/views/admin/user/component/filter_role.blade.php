<div class="m-form__group m-form__group--inline col-md-2">
    <div class="m-form__label">
        <label>Loại người dùng </label>
    </div>
    <div class="m-form__control">
        <select class="custom-select form-control" name="filterRole">
            <option value="All" @if ($filterRole=="All" ) selected @endif>All</option>
            <option value="1" @if ($filterRole=="1" ) selected @endif>Quản trị</option>
            <option value="0" @if ($filterRole=="0" ) selected @endif>Người dùng thường</option>                                
        </select>
    </div>
</div>