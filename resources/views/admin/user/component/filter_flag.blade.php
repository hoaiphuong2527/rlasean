<div class="m-form__group m-form__group--inline col-md-2">
    <div class="m-form__label">
        <label>Trạng thái</label>
    </div>
    <div class="m-form__control">
        <select class="custom-select form-control" name="filterFlag">
            <option value="All" @if ($filterFlag=="All" ) selected @endif>All</option>
            <option value="1" @if ($filterFlag=="1" ) selected @endif>Đang hoạt động</option>
            <option value="0" @if ($filterFlag=="0" ) selected @endif>Khóa</option>                                
        </select>
    </div>
</div>