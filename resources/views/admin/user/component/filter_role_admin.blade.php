<div class="m-form__group m-form__group--inline col-md-2">
    <div class="m-form__label">
        <label>Chức vụ </label>
    </div>
    <div class="m-form__control">
        <select class="custom-select form-control" name="filterRoleAdmin">
            <option value="All" @if ($filterRole ==="All" ) selected @endif>All</option>
            <option value="contributor" @if ($filterRole ==="contributor" ) selected @endif>Contributor</option>
            <option value="editor" @if ($filterRole ==="editor" ) selected @endif>Editor</option>    
            <option value="admin" @if ($filterRole ==="admin" ) selected @endif>Admin</option>
            <option value="controller" @if ($filterRole ==="controller" ) selected @endif>Controller</option>                                                            
        </select>
    </div>
</div>