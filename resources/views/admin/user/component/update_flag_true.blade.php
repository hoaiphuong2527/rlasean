<div data-toggle="tooltip" data-placement="top" style="display: inline-block;" title="Mở khóa tài khoản">
    <button type="button" class="btn btn-info" data-toggle="modal" data-target="{{ '#updae_flag' . $user->id }}">
        <i class="fa fa-upload"></i>
    </button>
</div>
<form method="post" action="{!! route('admin_user.update_flag_true', ['id' => $user->id]) !!}">
    {!! csrf_field() !!}
    <div class="modal fade" id="{{ 'updae_flag' . $user->id }}" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content  -->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Cảnh báo</h4>
                    <label data-dismiss="modal" style="cursor: pointer;"><i class="fa fa-times"></i></label>
                </div>
                <div class="modal-body">
                    <p>Tài khoản của {{ $user->name }} sẽ được kích hoạt lại. Bạn có chắc là muốn như vậy không? </p>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-outline-danger active">Đồng ý</button>
                    <button type="button" class="btn btn-outline-brand active" data-dismiss="modal">Không đồng ý</button>
                </div>
            </div>
        </div>
    </div>
</form>