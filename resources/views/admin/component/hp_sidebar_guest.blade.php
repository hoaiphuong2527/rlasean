@if(Auth::check() && Auth::user()->is_admin)
<div class="header-top ilv-hp-header">
    <div class="wrapper">
        <div class="container">
            <div class="row box-header no-margin ApRow  has-bg bg-fullwidth-container" style="">
                <div class="col-sm-2 col-xs-12 col-sp-12 no-padding left-header  ApColumn ">
                    <a class="ilv-hp-color" href="/admin">
                        <i class="fa fa-home" aria-hidden="true"></i> Trang chủ
                    </a>
                </div>
                <div class="col-md-4 col-sm-4 col-8 col-sp-4-8 center-header ApColumn ">
                    <div class="leo-top-menu navbar-toggleable-md megamenu-off-canvas">
                        <ul class="nav megamenu horizontal">
                            <li class="nav-item dropdown">
                                <a class="nav-link ilv-hp-color" href="javascript:"><i class="fa fa-plus" aria-hidden="true"></i> Thêm mới</a>
                                <div class="dropdown-menu">
                                    <a class="nav-link" href="{{ route('admin_product.index') }}">Sản phẩm</a>
                                    <a class="nav-link" href="{{ route('admin_post.index') }}">Bài viết</a>
                                    <a class="nav-link" href="{{ route('admin_page.index') }}">Trang tĩnh</a>
                                </div>
                            </li>
                            @if(isset($item))
                            <li class="nav-item ">
                                <a class="nav-link ilv-hp-color" href="{{ $item->urlAdminEdit() }}"><i class="fa fa-pencil" aria-hidden="true"></i> Chỉnh sửa</a>
                            </li>
                            @endif

                        </ul>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-4 col-sp-7-2 center-header ApColumn ">
                    <ul class="nav navbar-nav-ivl megamenu horizontal ilv-hn-float-right">
                        <li class="nav-item dropdown">
                            <a class="nav-link ilv-hp-color" href="javascript:">Hi, {{ Auth::user()->name }}</a>
                            <div class="dropdown-menu">
                                <a class="nav-link" href="{{ route('user_profiles.index',['token' => Auth::user()->middleware_token]) }}">Tài khoản của tôi</a>
                                <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Đăng xuất</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endif