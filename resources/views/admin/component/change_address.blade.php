<div class="form-group m-form__group">
<button type="button" class="btn btn-sm btn-info" data-toggle="modal" data-target="#address">
    Đổi địa chỉ
</button>
    <div class="modal fade" id="address" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content  -->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Đổi thông tin địa chỉ</h4>
                    <label data-dismiss="modal" style="cursor: pointer;"><i class="fa fa-times"></i></label>
                </div>
                <div class="modal-body">
                    <div id="city" type="nomal"></div>
                    <div class="form-group m-form__group">
                        <label for="">Địa chỉ</label>
                        <input type="text" class="form-control m-input m-input--square" id="stress" placeholder="Địa chỉ"
                            name="stress" value="{{ old('stress') }}">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="" class="btn btn-outline-danger active" data-dismiss="modal">Xong</button>
                    <button type="button" class="btn btn-outline-brand active" data-dismiss="modal">Thoát</button>
                </div>
            </div>
        </div>
    </div>
</div>