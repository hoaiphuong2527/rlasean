<div class="form-group m-form__group">
<button type="button" class="btn btn-sm btn-info" data-toggle="modal" data-target="#address_stock">
    Đổi địa chỉ
</button>
    <div class="modal fade" id="address_stock" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content  -->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Thông tin địa chỉ kho hàng</h4>
                    <label data-dismiss="modal" style="cursor: pointer;"><i class="fa fa-times"></i></label>
                </div>
                <div class="modal-body">
                    <div id="city1" type="stock"></div>
                    <div class="form-group m-form__group">
                        <label for="">Địa chỉ</label>
                        <input type="text" class="form-control m-input m-input--square" id="stress_stock" placeholder="Địa chỉ"
                            name="stress_stock" value="{{ old('stress_stock') }}">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="saveDistrictID" class="btn btn-outline-danger active" data-dismiss="modal">Lưu</button>
                    <button type="button" class="btn btn-outline-brand active" data-dismiss="modal">Thoát</button>
                </div>
            </div>
        </div>
    </div>
</div>