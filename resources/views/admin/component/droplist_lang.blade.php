<li class="m-nav__item m-topbar__user-profile m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light"
    m-dropdown-toggle="click" aria-expanded="true">
    <a href="#" class="m-dropdown__toggle btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
        Thêm Ngôn Ngữ <i class="m-menu__link-icon fa fa-chevron-down"></i>
    </a>
    <div class="m-dropdown__wrapper" style="z-index: 101;">
        <span class="m-menu__arrow m-menu__arrow--adjust" style="left: 87.5px;"></span>
        <div class="m-dropdown__inner">
            <!-- <div class="m-dropdown__header m--align-center"> -->
            <ul class="m-menu__subnav" style="list-style-type:none">
                @foreach(config('ecommerce.locale_displays') as $key => $lang )
                <li class="m-menu__item ">
                    @if(!$item->getTranslateText())
                    <i class="m-menu__link-icon fa fa-plus"></i>
                    @endif
                    <input type="submit" class="btn btn-link input-submit-form" name="update_lang" value="{{ $lang }}"/>
                </li>
                @endforeach
            </ul>
            <!-- </div> -->
        </div>
    </div>
</li>
