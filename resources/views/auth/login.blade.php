@extends('layouts.main')
@section('title')
{{ __('Login') }}
@endsection
@section('meta')

@endsection
@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-5 mx-auto">
            <div id="first">
                <div class="myform form ">
                    <div class="logo mb-3">
                        <div class="col-md-12 text-center">
                            <h1>Đăng nhập</h1>
                        </div>
                    </div>
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                </div>
                <form action="{{ route('login') }}" method="post" name="login">
                    @csrf
                    <div class="form-group">
                        @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                        <label for="exampleInputEmail1">Email:</label>
                        <input type="email" name="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                            name="email" value="{{ old('email') }}" id="email" aria-describedby="emailHelp" placeholder="Enter email">
                    </div>
                    <div class="form-group">
                        @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                        <label for="exampleInputEmail1">Mật khẩu:</label>
                        <input type="password" name="password" id="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                            aria-describedby="emailHelp" placeholder="Enter Password">
                    </div>
                    <div class="form-group">
                    </div>
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn btn-block mybtn btn-primary tx-tfm">Đăng nhập</button>
                    </div>
                    <div class="form-group">
                        
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
@endsection
