<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>@yield('title') - {{ config('app.name', 'Ecommerce') }}</title>
</head>

<body>
<table width="600" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td align="left" valign="top"><img src="{{ asset('images/DMCLOGO.png') }}" width="600" height="106" style="display:block;"></td>
  </tr>
  <tr>
    <td align="center" valign="top" bgcolor="#f1f69d" style="background-color:#f1f69d; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000; padding:10px;"><table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:10px;">
      @yield('content')
    </td>
  </tr>
  <tr>
    <td align="left" valign="top" bgcolor="#478730" style="background-color:#478730;"><table width="100%" border="0" cellspacing="0" cellpadding="15">
      <tr>
        <td align="left" valign="top" style="color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:13px;">Company Address <br>
          Contact Person <br>
      </tr>
    </table></td>
  </tr>
</table>
</body>
</html>
