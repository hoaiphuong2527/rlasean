@extends('mail.main') 
@section('title')
Welcome mail
@endsection
@section('content')
<p>Hi {{ $user->name }}</p>
<p>We have create a account from your information!</p>
<p>Go to website to update your information and manager your order!</p>
<div class="col align-center">
    <a class="button button-red" href="{{ route('user_profiles.check',['token' => $user->middleware_token,'active' => $user->email_verified_at]) }}" target="_blank">Go to website</a>
</div>
<p>Regards,</p>
<p>Rlasean</p>
@endsection