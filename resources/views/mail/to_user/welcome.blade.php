@extends('mail.main') 
@section('title')
Welcome
@endsection
@section('content')
    <div class="col align-center">
        <a class="button button-red" href="{{route('activate_account',['user_id' => $user->id, 'token' => $user->middleware_token])}}" target="_blank">Activate</a>
    </div>
@endsection