@extends('mail.main') 
@section('title')
New review
@endsection
@section('content')
<p>Hi {{ $user->name }}</p>
<p>{{ $review->user->name  }} vừa {{ $content_mail }}  sản phẩm "{{ $review->reviewtable->name }}"</p><br>
<p>Click vào nút dưới đến website để biết thêm chi tiết!</p>
@if($review->type == config('ecommerce.review_types.comment'))
<div class="col align-center">
    <a class="button button-red" href="{{ $review->reviewtable->urlGuestShow() }}" target="_blank">Go to website</a>
</div>
@else
<div class="col align-center">
    <a class="button button-red" href="{{ route('admin_reviews.index') }}" target="_blank">Go to website</a>
</div>
@endif
<p>Regards,</p>
<p>Rlasean</p>
@endsection