@extends('mail.main') 
@section('title')
Liên hệ
@endsection
@section('content')
Xin chào {{ $admin->name }}
{{ $contact->name }} vừa gửi đến hệ thông một liên hệ!
Nội dung: {{ $contact->message }}
@endsection