<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <script src="main.js"></script>
</head>
<body>
    <p>Chào {{ $user->name }}</p>
    <p>{{ $name }} vừa đặt hàng tại website. Nhấn vào link sau để xem chi tiết </p>
    <a href="{{  $order->urlAdminEdit() }}" >Go to website</a>
</body>
</html>