@extends('layouts.main')
@section('title')
{{ __('Profile') }}
@endsection
@section('meta')

@endsection
@section('content')
<div class="container mt-5 ilv-container-benefit pt-4 pb-4 mb-4 ">
        <div class="row">
        <div class="col-sm-10">
        </div>
    </div>
    <form class="col-sm-12" method="POST" action="{{ route('user_profiles.update',['token' => $user->middleware_token,'id'=> $user->id]) }}" enctype="multipart/form-data">
            <input type="hidden" name="_method" value="PUT" /> {{ csrf_field() }}
        <div class="row">
            <div class="col-sm-3">
                <!--left col-->
                <div class="text-center">
                    <img src="{{ $user->getAvata() }}" class="avatar img-circle img-thumbnail" alt="avatar" onclick="
                        document.getElementById('ul-img').click();
                        ">
                    <h1>{{ $user->name}}</h1>
                    <h6>Upload a different photo...</h6>
                    <input id="ul-img" type="file" name="image" class="text-center center-block file-upload d-none">
                </div>
            </div>
            <!--/col-3-->
            <div class="col-sm-9">
                <div class="tab-content">
                    <div class="tab-pane active" id="home">
                        <hr>
                            <div class="form-group row">
                                <div class="col-md-6 py-2">
                                    <label for="name">
                                        <h4>Tên hiển thị</h4>
                                    </label>
                                    <input type="text" class="form-control" name="name" id="name" placeholder="Tên hiển thị"
                                        value="{{ old('name',$user->name) }}">
                                    @if ($errors->has('name'))
                                    <span class="invalid-feedback" style="display:block;color: red">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                      
                                <div class="col-md-6 py-2">
                                    <label for="phone">
                                        <h4>Điện thoại</h4>
                                    </label>
                                    <input type="text" class="form-control" name="phone" id="phone" placeholder="Điện thoại"
                                        value="{{ old('phone',$user->phone) }}">
                                    @if ($errors->has('phone'))
                                    <span class="invalid-feedback" style="display:block;color: red">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                    @endif
                                </div>
                           
                                <div class="col-md-6 py-2">
                                    <label for="email">
                                        <h4>Email</h4>
                                    </label>
                                    <input type="email" class="form-control" name="email" id="email" placeholder="you@email.com"
                                        value="{{ old('email',$user->email) }}" disabled>
                                    @if ($errors->has('email'))
                                    <span class="invalid-feedback" style="display:block;color: red">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                        
                                <div class="col-md-6 py-2">
                                    <label for="location">
                                        <h4>Địa chỉ</h4>
                                    </label>
                                    <input type="text" class="form-control" name="address" id="location" placeholder="Địa chỉ"
                                        title="Địa chỉ" value="{{ old('address',$user->address) }}">
                                        @if ($errors->has('address'))
                                        <span class="invalid-feedback" style="display:block;color: red">
                                            <strong>{{ $errors->first('address') }}</strong>
                                        </span>
                                        @endif
                                </div>
                         
                                <div class="col-md-6 py-2">
                                    <label for="password">
                                        <h4>Mật khẩu</h4>
                                    </label>
                                    <input type="password" class="form-control" name="password" id="password" placeholder="Mật khẩu"
                                        title="enter your password.">
                                        @if ($errors->has('password'))
                                        <span class="invalid-feedback" style="display:block;color: red">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                        @endif
                                </div>
                      
    
                                <div class="col-md-6 py-2">
                                    <label for="password_confirmation">
                                        <h4>Xác thực mật khẩu</h4>
                                    </label>
                                    <input type="password" class="form-control" name="password_confirmation" id="password_confirmation"
                                        placeholder="Xác thực mật khẩu" title="enter your password confirm.">
                                </div>
                          
                                <div class="col-md-12">
                                    <br>
                                    <input class="btn btn-lg btn-primary" type="submit" value="Lưu">
                                    <button class="btn btn-lg" type="reset"><i class="glyphicon glyphicon-repeat"></i>
                                        Reset</button>
                                </div>
                            </div>
                        <hr>
                    </div>
                </div>
                <!--/tab-pane-->
            </div>
            <!--/tab-content-->
        </div>
    </form>
    <!--/col-9-->
</div>
<!--/row-->
@endsection
