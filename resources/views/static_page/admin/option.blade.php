<div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
    <div class="form-group m-form__group">
        <label for="exampleSelect1">Danh mục</label>
        <select class="form-control m-input m-input--square" name="category_id">
            <option value="{{ $category->id }}" @if($category->id == $item->category_id) selected @endif>{{ $category->name }}</option>
            @foreach( $category->children as $cate)
                <option value="{{ $cate->id }}" @if($cate->id == $item->category_id) selected @endif>&nbsp;&nbsp;----{{ $cate->name }}</option>
            @endforeach
        </select>
    </div>
</div>
