@extends('layouts.admin')
@section('content')
@section('title')
Chỉnh sửa thông tin {{ $item->name }}
@endsection
<div class="m-portlet">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    Chỉnh sửa thông tin {{ $item->name }}
                </h3>
            </div>
        </div>
    </div>
    <form class="m-form m-form--fit m-form--label-align-right" method="POST" action="{{ route('admin_page.update',['id' => $item->id]) }}">
        <input type="hidden" name="_method" value="PUT" /> {{ csrf_field() }}
        <div class="m-portlet__body">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Thông tin mặc định
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <ul class="m-topbar__nav m-nav m-nav--inline">
                        @include('admin.component.droplist_lang')
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="tab-content">
                    <div class="row">
                        <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                            <div class="form-group m-form__group">
                                <label for="exampleInputPassword1">Tiêu đề</label>
                                <input type="text" class="form-control m-input m-input--square" id="" placeholder="Tiêu đề"
                                    name="name" value="{{ old('name',$item->name) }}">
                                @if ($errors->has('name'))
                                <span class="invalid-feedback" style="display:block;">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        @if($item->name)
                        <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                            <div class="form-group m-form__group">
                                <label for="">Đường dẫn</label><br>
                                <span class="m-form__help" style="color:#333">Vui lòng nhập đường dẫn theo định
                                    dạng mẫu:<b>duong-dan-{{ $item->slug }}</b></span><br>
                                <div class="input-group m-input-group m-input-group--square">
                                    <div class="input-group-prepend"><span class="input-group-text">{{
                                            route('guest.view_static_page',[
                                            'slug' =>""]) }}/</span></div>
                                    <input type="text" class="form-control m-input m-input--square" id=""
                                        placeholder="" name="slug" value="{{ old('slug',$item->slug) }}"
                                        required>
                                    @if ($errors->has('slug'))
                                    <span class="invalid-feedback" style="display:block;">
                                        <strong>{{ $errors->first('slug') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        @endif
                        @include('static_page.admin.option')
                        <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                            <div class="form-group m-form__group">
                                <label for="col-form-label col-lg-3 col-sm-12">Nội dung</label>
                                <textarea class="form-control myPost" rows="20" cols="30" name="description">{{old('description',$item->description)}}</textarea>
                                @if ($errors->has('description'))
                                <span class="invalid-feedback" style="display:block;">
                                    <strong>{{ $errors->first('description') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="m-portlet__foot m-portlet__foot--fit">
                <div class="m-form__actions text-center">
                    @if(!$item->status)
                    <input type="submit" class="btn btn-success" name="publish" value="Đăng">
                    <input type="submit" class="btn btn-warning" name="draff" value="Lưu nháp">
                    @else
                    <input type="submit" class="btn btn-success" name="publish" value="Lưu & Đăng">
                    @endif
                </div>
            </div>
        </div>
    </form>
</div>
@endsection
