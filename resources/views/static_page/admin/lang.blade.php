@extends('layouts.admin')
@section('content')
@section('title')
Chỉnh sửa thông tin ngôn ngữ
@endsection
@if(Session::has('status'))
<div class="alert alert-success">
    {{ Session::get('status') }}
</div>
@endif
<div class="m-portlet">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    Cập nhật ngôn ngữ
                </h3>
            </div>
        </div>
    </div>
    <form class="m-form m-form--fit m-form--label-align-right" method="POST" action="{{ route('admin_post.update_lang',$item->id) }}">
        <input type="hidden" name="_method" value="PUT" /> {{ csrf_field() }}
        <div class="m-portlet__body">
            <div class="m-portlet__head">
                <div class="m-portlet__head-tools">
                    <ul class="nav nav-tabs m-tabs m-tabs-line  reload-page m-tabs-line--right m-tabs-line-danger" role="tablist">
                        @foreach(config('ecommerce.locale_displays') as $key => $lang )
                        <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_portlet_tab_{{ $key }}"
                                role="tab" aria-selected="false">
                                {{ $lang }}
                            </a>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="tab-content">
                    <div class="tab-pane active" id="m_portlet_tab_{{ $lang }}">
                        <div class="row">
                            @include('components.lang',['item' => $item, 'lable' => 'Tiêu đề','col_name' =>
                            'name','type' => 'input'])
                            @if($item->name)
                            @include('components.lang',['item' => $item, 'lable' => 'Đường
                            dẫn','col_name'=>'slug','type' => 'page_slug','url' => $url])
                            @endif
                            @include('components.lang',['item' => $item, 'lable' => 'Nội dung','col_name'
                            => 'description','type' => 'summernote'])
                            @foreach(config('ecommerce.locales') as $key => $lang)
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="m-portlet__foot m-portlet__foot--fit">
            <div class="m-form__actions text-center">
                <input type="submit" class="btn btn-info" value="Lưu">
            </div>
        </div>
    </form>
</div>
@endsection
