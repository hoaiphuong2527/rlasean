@extends('layouts.admin')
@section('content')
@section('title')
Bài viết cố định
@endsection
@if(Session::has('status'))
<div class="alert alert-success">
    {{ Session::get('status') }}
</div>
@endif
<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    Danh dách bài viết cố định
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <form method="post" action="{{ route('admin_page.store') }}">
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air"
                            value="Thêm mới" />
                    </form>
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <div class="m_datatable m-datatable m-datatable--default m-datatable--loaded" style="">
            <table class="m-datatable__table" style="display: block; min-height: 300px; overflow-x: auto;">
                <thead class="m-datatable__head">
                    <tr class="m-datatable__row" style="left: 0px;">
                        <th class="m-datatable__cell--center m-datatable__cell  ">
                            <span style="width: 80px;"> #</span>
                            </span></th>
                        <th class="m-datatable__cell--center m-datatable__cell  ">
                            <span style="width: 300px;"> Tên</span>
                            </span></th>
                        <th class="m-datatable__cell--center m-datatable__cell ">
                            <span style="width: 100px ;">Trạng thái</span></th>
                        <th class="m-datatable__cell--center m-datatable__cell ">
                            <span style="width: 150px ;">Hành động</span></th>
                    </tr>
                </thead>
                <tbody style="" class="m-datatable__body">
                    @foreach($pages as $row)
                    <tr class="m-datatable__row" style="left: 0px;">
                        <td class="m-datatable__cell--center m-datatable__cell">
                            <span style="width: 100px ;">
                                {{$row->id }}</span></td>

                        <td class="m-datatable__cell"> <span style="width: 300px ;">
                                <b>{{ $row->name }}</b>
                            </span></td>
                        <td class="m-datatable__cell--center m-datatable__cell">
                            <span style="width: 100px ;">
                                <div data-toggle="tooltip" data-placement="top" style="display: inline-block;" title="Đóng/mở trạng thái trang tĩnh">
                                    <div class="checkbox">
                                        <label class="m-switch m-switch--outline m-switch--primary">
                                            <input data-id="{{$row->id}}" class="chkPost" type="checkbox" @if($row->status) checked
                                            @endif>
                                            <span></span>
                                        </label>
                                    </div>
                                </div>  
                            </span></td>
                        <td class="m-datatable__cell--center m-datatable__cell">
                            <span style="width: 150px ;">
                                <div class=" ilv-btn-group">
                                    <div data-toggle="tooltip" data-placement="top" style="display: inline-block;" title="Chỉnh sửa thông tin trang tĩnh">
                                        <a class="btn btn-sm btn-warning" href="{{ route('admin_page.edit',['slug' => $row->id]) }}">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                    </div>  
                                    @include('post.admin.component.btn_delete_post')
                                </div>
                            </span>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
