@extends('layouts.main')
@section('title')
{{ __('page_text.partner.page_title') }}
@endsection
@section('meta')
<meta name="description" content="{{ $option->name }}">
<meta name="keywords" content="{{ $option->name }}">
<meta name="author" content="{{ $option->name }}">
<meta property="og:url"           content="{{ route('guest_partners.index.'.app()->getLocale()) }}" />
<meta property="og:type"          content="article" />
<meta property="og:title"         content="{{ __('page_text.partner.page_title') }}" />
<meta property="og:description"   content="{{ __('page_text.partner.page_title') }}" />
<meta property="og:image"         content="{{ $option->linkImage('main_logo') }}" />
@endsection

@section('content')

@include('guest.component.banner_title',['imageLink' => $banner->linkImage(), 'title' => __('page_text.partner.page_title')])
<div class="container">
    {!! Breadcrumbs::render('guest_partners.index.'.app()->getLocale())!!}
</div>

    <div class="container mt-4">
        <div class="row">
            @foreach($partners as $row)
                <div class="col-3 mt-4">
                    <div class="text-center">
                        <img src="{{ $row->linkImage() }}" alt="" class="img-fluid">
                        <div class="py-1"><a href="mailto:{{ $row->domain }}">{{ $row->domain }}</a></div>
                        <div class="py-1">{{ $row->address }}</div>
                        <div class="py-1"><strong>{{ $row->phone }}</strong></div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <div class="container mt-5">
        @include('guest.component.title_left', ['title' => __('page_text.partner.section_1_title')])
    </div>
    <div class="container mt-4">
    <p>
        {{ __('page_text.partner.section_1_desc') }}
    </p>
    </div>
    
    <div class="container my-4 text-center">
        @include('guest.component.button_primary', ['text' => __('component_text.buttons.contact_us'), 'link' => route('guest.contact.' . app()->getLocale())])
    </div>

@endsection
