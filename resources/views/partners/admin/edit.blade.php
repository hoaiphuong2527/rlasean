@extends('layouts.admin')
@section('content')
@section('title')
Chỉnh sửa thông tin đối tác
@endsection
<div class="m-portlet">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    Chỉnh sửa thông tin đối tác
                </h3>
            </div>
        </div>
    </div>
    <form class="m-form m-form--fit m-form--label-align-right" method="POST" action="{{ $item->urlAdminUpdate() }}" enctype="multipart/form-data">
        <input type="hidden" name="_method" value="PUT" /> {{ csrf_field() }}
        <div class="m-portlet__body">
            <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                <div class="form-group m-form__group">
                    <label for="exampleInputPassword1">Domain</label>
                    <input type="text" class="form-control m-input m-input--square" id="" placeholder="Domain"
                        name="domain" value="{{ old('domain',$item->domain) }}">
                    @if ($errors->has('domain'))
                    <span class="invalid-feedback" style="display:block;">
                        <strong>{{ $errors->first('domain') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                <div class="form-group m-form__group">
                    <label for="exampleInputPassword1">Điện thoại</label>
                    <input type="text" class="form-control m-input m-input--square" id="" placeholder="Phone"
                        name="phone" value="{{ old('phone',$item->phone) }}">
                    @if ($errors->has('phone'))
                    <span class="invalid-feedback" style="display:block;">
                        <strong>{{ $errors->first('phone') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                <div class="form-group m-form__group">
                    <label for="col-form-label col-lg-3 col-sm-12">Địa chỉ</label>
                    <textarea class="form-control" rows="3" cols="30" name="address">{{old('address',$item->address)}}</textarea>
                    @if ($errors->has('address'))
                    <span class="invalid-feedback" style="display:block;">
                        <strong>{{ $errors->first('address') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                <div class="form-group m-form__group">
                    <label>Hình ảnh</label>
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" name="image" id="ilv-upload-img">
                        <label class="custom-file-label" for="ilv-upload-img">Chọn hình ảnh</label>
                    </div>
                    @if($item->image != null)
                    <img id="preview" src="{{ $item->linkImage()}}" alt="image" style="max-width: 100%">
                    @else
                    <img id="preview" src="" alt="image" height="100px" style="display: none;"> @endif
                    @if ($errors->has('image'))
                    <span class="invalid-feedback" style="display:block;">
                        <strong>{{ $errors->first('image') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            <div class="m-portlet__foot m-portlet__foot--fit">
                <div class="m-form__actions text-center">
                        <input type="submit" class="btn btn-success" name="publish" value="Lưu">
                </div>
            </div>
        </div>
    </form>
</div>
@endsection
