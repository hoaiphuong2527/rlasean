@section('title') Đối tác @endsection @extends('layouts.admin') @section('content')
@if(Session::has('status'))
<div class="alert alert-info">
    {{ Session::get('status') }}
</div>
@endif
@if(Session::has('destroy'))
<div class="alert alert-success">
    {{ Session::get('destroy') }}
</div>
@endif
<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    Danh sách đối tác
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <form method="post" action="{{ route('admin_partners.store') }}">
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air"
                            value="Thêm mới" />
                    </form>
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <!--begin: Datatable -->
        <div class="m_datatable m-datatable m-datatable--default m-datatable--loaded" style="">
            <table class="m-datatable__table" style="display: block; min-height: 300px; overflow-x: auto;">
                <thead class="m-datatable__head">
                    <tr class="m-datatable__row" style="left: 0px;">
                        <th class="m-datatable__cell--center m-datatable__cell "> <span style="width: 100px  ;">#</span></th>
                        <th class="m-datatable__cell--center m-datatable__cell "> <span style="width: 100px  ;">Logo</span></th>
                        <th class="m-datatable__cell--center m-datatable__cell "> <span style="width: 200px  ;">Domain</span></th>
                        <th class="m-datatable__cell--center m-datatable__cell "> <span style="width: 100px  ;">Địa chỉ</span></th>
                        <th class="m-datatable__cell--center m-datatable__cell "> <span style="width: 50px  ;">Điện thoại</span></th>
                        <th class="m-datatable__cell--center m-datatable__cell "> <span style="width: 150px  ;">Hành
                                động</span></th>
                    </tr>
                </thead>
                <tbody style="" class="m-datatable__body">
                    <?php $index = 1; ?>
                    @foreach($partners as $item)
                    <tr class="m-datatable__row" style="left: 0px;">
                        <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 100px  ;">{{
                                $index }}</span></td>
                        <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 100px ;">
                            <img class="img-thumbnail" src="{{ $item->linkImage() }}" width="100px" height="100px">
                            </span></td>
                        <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 200px ;">
                                {{ $item->domain }}
                            </span></td>
                        <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 100px ;">
                                {{ $item->address }}
                            </span></td>
                        <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 50px ;">
                                {{ $item->phone }}
                            </span></td>
                        <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 150px ;">
                                <div class=" ilv-btn-group">
                                    <div data-toggle="tooltip" data-placement="top" style="display: inline-block;" title="Chỉnh sửa thông tin">
                                        <a class="btn btn-warning" href="{{ $item->urlAdminEdit() }}">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                    </div>
                                    @include('partners.admin.btn_delete')
                                </div>
                            </span></td>
                    </tr>
                    <?php $index++ ?>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection
