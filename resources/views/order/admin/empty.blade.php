@section('title')
Danh sách đơn hàng
@endsection
@extends('layouts.admin')
@section('content')
<div class="container text-center mt-5">
    <div class="row">
        <div class="col">
            <h1> Quản lí đơn hàng</h1>
            <p>Tạo và quản lí các đơn hàng trên website của bạn</p>
        </div>
    </div>
    <div class="row my-4">
        <div class="col">
            <img src="{{ asset('images/admin/cart.png') }}" class="" height="150" width="auto">
        </div>
    </div>
    <div class="row mt-4">
        <div class="col">
            <a href="{{ route('admin_order.create') }}" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">Tạo đơn hàng mới</a>
        </div>
    </div>
</div>
@endsection