@section('title')
Tạo đơn hàng mới
@endsection
@extends('layouts.admin')
@section('content')
@if(session('status'))
<div class="alert alert-info">{{session('status')}}</div>
@endif
<script>
    var option = {!! $option !!}
    var locale = "{!! app()->getLocale() !!}"   
</script>
<form class="m-form m-form--fit m-form--label-align-right" action="{{ route('admin_order.store') }}" method="POST">
    @csrf
    <div class="m-portlet__body">
        <div id="admin-react-create-order">
        </div>
    </div>
</form>

@endsection
