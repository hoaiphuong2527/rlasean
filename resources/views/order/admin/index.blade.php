@section('title')
Danh sách đơn hàng
@endsection
@extends('layouts.admin')
@section('content')
@if(Session::has('destroy'))
<div class="alert alert-success">
    {{ Session::get('destroy') }}
</div>
@endif
<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    Danh sách đơn hàng
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <a href="{{ route('admin_order.create') }}" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">Tạo đơn hàng mới</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body" style="padding-bottom: 52px;">
        <form class="row" method="GET" action="" id="orderForm">
            <div class="form-group m-form__group col-lg-3 col-md-3 col-sm-3">
                <div class="form-group m-form__group">
                    <label class="">Nhập thông tin</label>
                    <input class="form-control m-input m-input--square" placeholder="Nhập Code/Tên/SDT.." type="text"
                        name="key" value="{{ $key }}">
                </div>
            </div>
            <div class="form-group m-form__group col-lg-2 col-md-2 col-sm-2">
                <div class="form-group m-form__group">
                    <label class="">PT Thanh toán</label>
                    <select class="form-control" name="status_payment">
                        <option value="all">Tất cả</option>
                        @foreach(config('ecommerce.status_payment_display') as $key => $status)
                        <option value="{{ $key }}" @if($status_payment==$key) selected @endif>{{ $status }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group m-form__group col-lg-2 col-md-2 col-sm-2">
                <div class="form-group m-form__group">
                    <label class="">TT Đơn hàng</label>
                    <select class="form-control" name="order_status">
                        <option value="all">Tất cả</option>
                        @foreach(config('ecommerce.order_status_display_be') as $key => $status)
                        <option value="{{ $key }}" @if($order_status==$key) selected @endif>{{ $status }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group m-form__group col-lg-3 col-md-3 col-sm-3">
                <div class="form-group m-form__group">
                    <label class="">Thời gian</label>
                    <input class="d-none" id="start_date" name="start_date" value="">
                    <input class="d-none" id="end_date" name="end_date" value="">
                    <input type="text" class="form-control" name="daterange" value="" />
                </div>
            </div>
            <div class="form-group m-form__group col-lg-2 col-md-2 col-sm-2" style="margin-top: 25px;">
                <div class="form-group m-form__group row">
                    <div class="col-8">
                        <input type="submit" class="btn btn-info" value="Tìm kiếm">
                    </div>
                    <div class="col-4" style="margin-top: 5%; display: inline-block;" title="Refresh page">
                        <a href="{{ route('admin_order.index') }}"><i class="fa fa-sync-alt" style="font-size: 1.5em;
                            color: red;"></i></a>
                    </div>
                </div>
            </div>
            <input type="text" class="d-none" id="inputSubmitExport" name="" value="true">
        </form>
        <div class="m_datatable m-datatable m-datatable--default m-datatable--loaded" style="">
            <table class="m-datatable__table" style="display: block; min-height: 300px; overflow-x: auto;">
                <thead class="m-datatable__head">
                    <tr class="m-datatable__row" style="left: 0px;">
                        <th class=" m-datatable__cell "> <span style="width: 160px ;">
                                Mã Đơn
                            </span></th>
                        <th class="m-datatable__cell--center m-datatable__cell "> <span style="width: 150px ;">
                                TTKH
                            </span></th>
                        <th class=" m-datatable__cell "> <span style="width: 150px ;">
                                Ngày đặt
                            </span></th>
                        <th class=" m-datatable__cell "> <span style="width: 100px ;">
                                Tổng tiền
                            </span></th>
                        <th class=" m-datatable__cell "> <span style="width: 80px ;">
                                Trạng thái
                            </span></th>
                        <th class=" m-datatable__cell "> <span style="width: 80px ;">
                                Thanh toán
                            </span></th>
                        <th class=" m-datatable__cell "> <span style="width: 90px ;">
                                Hành động
                            </span></th>
                    </tr>
                </thead>
                <tbody style="" class="m-datatable__body">
                    @foreach($orders as $item)
                    <tr class="m-datatable__row" style="left: 0px;">
                        <td class=" m-datatable__cell"> <span style="width: 160px ;">
                                {!! $item->order_number_format !!}
                            </span></td>
                        <td class="m-datatable__cell--left m-datatable__cell"> <span style="width: 150px ;">
                                {{ $item->customerInfo() }}
                            </span></td>
                        <td class=" m-datatable__cell"> <span style="width: 150px ;">
                                {{ $item->create_date }}
                            </span></td>
                        <td class=" m-datatable__cell"> <span style="width: 100px ;">
                                {{ $item->getTotalPay() }}
                            </span></td>
                        <td class=" m-datatable__cell"> <span style="width: 80px ;">
                                {{ $item->getStatus() }}
                            </span></td>
                        <td class=" m-datatable__cell"> <span style="width: 80px ;">
                                {{ $item->getPaymentStatus() }}
                            </span></td>
                        <td class=" m-datatable__cell"> <span style="width: 90px ;">
                            <div class="ilv-btn-group">
                                <div data-toggle="tooltip" data-placement="top" style="display: inline-block;"
                                    title="Chỉnh sửa đơn hàng">
                                    <a class="btn btn-sm btn-warning" href="{{ $item->urlAdminEdit() }}" style="text-decoration: none;">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                </div>
                                @include('order.component.btn_delete')
                            </div>
                        </span></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $orders->links() }}
        </div>
        <div style="float:right;">
            <div class="form-group m-form__group row" style="margin-top: 5px;">
                <input type="button" name="export" class="btn btn-light" id="exportOrder" style="border-color: #8c8282;"
                    value="Tải xuống excel">
            </div>
        </div>
    </div>
</div>

@endsection