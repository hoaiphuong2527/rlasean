@section('title')
Chỉnh sửa thông tin đơn hàng #{{ $item->order_number }}
@endsection
@extends('layouts.admin')
@section('content')
@if(session('status'))
<div class="alert alert-info">{{session('status')}}</div>
@endif
<div class="row">
    <div class="col-8">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Chỉnh sửa thông tin đơn hàng #{{ $item->order_number }}
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <form action="{{ $item->urlAdminUpdate() }}" method="POST">
                    <input type="hidden" name="_method" value="PUT" /> {{ csrf_field() }}
                    <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Tên sản phẩm</th>
                                <th>Số lượng</th>
                                <th>Đơn giá</th>
                                <th>Tổng cộng</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $index = 1; @endphp
                            @foreach($item->product_variants as $product)
                            <tr>
                                <td>{{ $index }}</td>
                                <td>{{ $product->getName() }}</td>
                                <td>{{ $product->pivot->amount }}
                                    <input type="hidden" min=0 class="d-none change-amount-variant" name="amount[]"
                                        value="{{old('amount',$product->pivot->amount)}}">
                                </td>
                                <td>{{ $product->renderPriceInView($product->pivot->amount) }}</td>
                                <td>
                                    {{ $product->calculatTotal($product->pivot->amount) }}
                                </td>
                            </tr>
                            @php $index++ @endphp
                            @endforeach
                        </tbody>
                    </table>
                    <div class=" row text-right">
                        <div class="col-7"></div>
                        <div class="col-5">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td style="border-top:none"><b>Khuyến mãi </b><br>
                                            @if($item->code()->count())
                                            <p>Mã giảm giá {{ $item->code[0]->promotion }}</p>
                                            @endif
                                        </td>
                                        <td style="border-top:none">
                                            @if($item->code()->count())
                                            - {{ $item->code[0]->getPromotionPrice() }}
                                            @elseif($item->checkProductHasPromotion()['result'])
                                            {{ $item->checkProductHasPromotion()['text'] }}
                                            @else N\A
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="border-top:none"><b>Vận chuyển</b></td>
                                        <td style="border-top:none"> {{ $item->getShipFee() }}</td>
                                    </tr>
                                    @if($item->breakable_cost)
                                    <tr>
                                        <td style="border-top:none"><b>Chi phí hàng dễ vỡ</b></td>
                                        <td style="border-top:none"> {{ $item->getBreakableCost() }}</td>
                                    </tr>
                                    @endif
                                    <tr>
                                        <td style="border-top:none"><b>Tổng thanh toán</b></td>
                                        <td style="border-top:none">{{ $item->getTotalAfter() }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-7"></div>
                        <div class="col-5">
                            <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                                <label for="col-form-label col-lg-3 col-sm-12">TT Đơn hàng</label>
                                <select class="form-control m-input m-input--square col-md-12" name="status"
                                    @if($item->order_status
                                    == config('ecommerce.order_statuses.delivered')) disabled @endif id="statusOrder">
                                    @foreach($statuses as $key => $status)
                                    <option value="{{ $key }}" @if($item->order_status == $key) selected @endif>{{
                                    $status }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12" id="typePayment">
                                <label for="col-form-label col-lg-3 col-sm-12">PT Thanh toán</label>
                                <select class="form-control m-input m-input--square col-md-12" name="type_payment"
                                    @if($item->order_status
                                    == config('ecommerce.order_statuses.delivered')) disabled @endif id="typePaymentOption">
                                    @foreach($payment_types as $key => $type)
                                    <option value="{{ $key }}" @if($item->payment_type == $key) selected
                                        @endif>{{ $type }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12" id="paymentStatus">
                                <label for="col-form-label col-lg-3 col-sm-12">TT Thanh toán</label>
                                <select class="form-control m-input m-input--square col-md-12" name="payment_status"
                                    @if($item->order_status
                                    == config('ecommerce.order_statuses.delivered')) disabled @endif id="paymentStatusOption">
                                    @foreach($payment_statuses as $key => $status)
                                    <option value="{{ $key }}" id="{{ $key }}" @if($item->status_payment == $key) selected @endif>{{
                                    $status }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12 text-center">
                                @if($item->order_status == config('ecommerce.order_statuses.delivered'))
                                @include('order.component.btn_backup')
                                @elseif($item->confirmOrder())
                                @include('order.component.btn_update')
                                @else
                                <div data-toggle="tooltip" data-placement="top"
                                    style="display: inline-block; width: 100%" title="Cập nhật thông tin đơn hàng">
                                    <input type="submit" class="btn btn-primary save-order" data-id="{{ $item->id }}"
                                        order-edit-url="{!! $item->urlAdminEdit() !!}" name="save" value="Lưu"
                                        style="width: 100%">
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-4">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Khách hàng
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <div>
                    <b>Tên khách hàng</b>
                    <h5>{{ $item->customer_name }}</h5>
                </div>
                <div>
                    <b>Số điện thoại</b>
                    <h5>{{ $item->customer_phone }}</h5>
                </div>
                <div>
                    <b>Email</b>
                    <h5>{{ $item->customer_email }}</h5>
                </div>
            </div>
        </div>
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Giao hàng
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <div>
                    <b>Địa chỉ</b>
                    <h5>{{ $item->address }}</h5>
                </div>
                <div>
                    <b>Số điện thoại người nhận</b>
                    <h5>{{ $item->customer_phone }}</h5>
                </div>
                <div>
                    <b>Tên người nhận</b>
                    <h5>{{ $item->customer_name }}</h5>
                </div>
                <div>
                    <b>Ghi chú</b>
                    <h5>{{ $item->note }}</h5>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
