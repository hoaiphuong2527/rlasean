@extends('layouts.main')
@section('title')
    @lang('page_text.manageorders.title_single'){{ $order->order_number }}
@endsection
@section('content')
<div class="container">
        
        <div class="row">
            <div class="col-md-3 ilv-col-1">
                <div class="ilv-menu-left">
                    <h6 class="ilv-menu-left-item "><a href="{{ route('user_profiles.index',['token' => Auth::user()->middleware_token]) }}"><i class="fas fa-user-circle"></i>Thông tin tài khoản</a></h6>
                    <h6 class="ilv-menu-left-item ilv-menu-left-active"><a href="{{ route('user_order.index' ,['token' => Auth::user()->middleware_token ]) }}"><i class="fas fa-clipboard-list"></i> Quản lý đơn hàng</a></h6>
                </div>
            </div>
            <div class="col-md-9 ilv-col-2">    
            <h3>@lang('page_text.manageorders.title_single') {{ $order->order_number }}</h3>
            <table class="table ilv-table">
                <thead>
                <tr class="ilv-table-text">
                    <th >@lang('page_text.order_infomation.product_name')</th>
                    <th>@lang('page_text.order_infomation.price')</th>
                    <th>@lang('page_text.order_infomation.amount')</th>
                    <th>@lang('page_text.order_infomation.total')</th>
               
                </tr>
                </thead>
                <tbody>
                    @foreach ($order->product_variants as $product)     
                <tr>
                    <td>
                        <img style="    width: 60px;" src="{{ $product->image[0] }}" ><br>
                        <a href="#">{{ $product->product->name }}-{{ $product->title }}</a>
                    </td>
                    <td>{{ $product->renderPriceInView($product->pivot->amount) }}</td>
                    <td>{{ $product->pivot->amount }}</td>
                    <td>
                        {{ $product->calculatTotal($product->pivot->amount) }}
                    </td>
                </tr>
                @endforeach
                <tr>
                    <td class="ilv-col-span" colspan="4">@lang('page_text.order_infomation.ship') </td>
                    <td>
                        {{$order->getShipFee()}}
                    </td>
                </tr>
                <tr>
                    <td class="ilv-col-span" colspan="4">@lang('page_text.order_infomation.breakable_cost') </td>
                    <td>
                        {{$order->getBreakableCost()}}
                    </td>
                </tr>
                    <!--Nếu trạng thái =paid thì mới xuất hiện cột này (working)-->
                <tr>
                    <td class="ilv-col-span" colspan="4">@lang('page_text.order_infomation.total_pay') </td>
                <td>    
                    {{$order->getTotalAfter()}}
                </td>
                </tr>
    
            </tbody>
    
            </table>
            </div>
        </div>
    </div>
@endsection