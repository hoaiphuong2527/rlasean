@extends('layouts.main')
@section('title')
{{ __('page_text.manageorders.page_title') }}
@endsection

@section('content')

<div class="container">
    {!! Breadcrumbs::render('user_order'.app()->getLocale())!!}
    <div class="row">
        <div class="col-md-3 ilv-col-1">
            <div class="ilv-menu-left">
                <h6 class="ilv-menu-left-item "><a href="{{ route('user_profiles.index',['token' => Auth::user()->middleware_token]) }}"><i class="fas fa-user-circle"></i>Thông tin tài khoản</a></h6>
                <h6 class="ilv-menu-left-item ilv-menu-left-active"><a href="{{ route('user_order.index' ,['token' => Auth::user()->middleware_token ]) }}"><i class="fas fa-clipboard-list"></i> Quản lý đơn hàng</a></h6>
            </div>
        </div>
        <div class="col-md-9 ilv-col-2">    
        <h3>{{ __('page_text.manageorders.myorders') }}</h3>
        <table class="table ilv-table">
            <thead>
            <tr class="ilv-table-text">
                <th>{{ __('page_text.manageorders.order_number') }}</th>
                <th>{{ __('page_text.manageorders.purchase_date') }}</th>
                <th>{{ __('page_text.manageorders.summary') }}</th>
                <th>{{ __('page_text.manageorders.status') }}</th>
            </tr>
            </thead>
            <tbody>
                @foreach($orders as $order)
                <tr>
                    <td><a href="{{ route('user_order.show' ,['token' => Auth::user()->middleware_token,'order_number' =>$order->order_number ]) }}">{{$order->order_number}}</a></td>
                            <td>{{ $order->create_date }}</td>
                            <td>{{ $order->getFinalTotal() }}</td>
                            <td>{{ $order->getPaymentStatusGuest() }}</td>
                        </tr>
                @endforeach
            
        </tbody>

        </table>
        </div>
    </div>
</div>
@endsection