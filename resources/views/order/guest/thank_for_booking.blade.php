@extends('layouts.main')
@section('title')
{{ __('page_text.order_infomation.page_title') }}
@endsection
@section('meta')
<meta charset="UTF-8">
<meta name="description" content="{{ $option->name }}">
<meta name="keywords" content="{{ $option->name }}">
<meta name="author" content="{{ $option->name }}">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
@endsection
@section('content')

<div class="container ilv-mt-60 ilv-mb-60">
    <div class="ilv-text-center ">
        <h1>@lang('page_text.order_infomation.your_order')</h1>
    </div>
    <table class="table ilv-table" id="m_table_2">
        <thead class="text-primary text-uppercase text-center">
            <tr>
                <th colspan="2"><b>@lang('page_text.order_infomation.customer_infor')</b></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>@lang('page_text.order_infomation.full_name')</td>
                <td>{{ $order->customer_name }}</td>
            </tr>
            <tr>
                <td>@lang('page_text.order_infomation.date')</td>
                <td>{{ $order->created_at }}</td>
            </tr>
            <tr>
                <td>@lang('page_text.order_infomation.phone')</td>
                <td>{{ $order->customer_phone }}</td>
            </tr>
            <tr>
                <td>@lang('page_text.order_infomation.address')</td>
                <td>{{ $order->address }}</td>
            </tr>
            <tr>
                <td>@lang('page_text.order_infomation.payment_type')</td>
                <td>{{ $order->getPaymentType() }}</td>
            </tr>
            <tr>
                <td>@lang('page_text.order_infomation.note')</td>
                <td>{{ $order->note }}</td>
            </tr>
        </tbody>
    </table>
    <table class="table ilv-table" id="m_table_1">
        <thead class="text-primary text-uppercase">
            <tr>
                <th>@lang('page_text.order_infomation.id')</th>
                <th>@lang('page_text.order_infomation.product_name')</th>
                <th>@lang('page_text.order_infomation.amount')</th>
                <th>@lang('page_text.order_infomation.price')</th>
            </tr>
        </thead>
        <tbody>
            @php $index = 1; @endphp
            @foreach($order->product_variants as $product)
            <tr>
                <td>{{ $index }}</td>
                <td>{{ $product->getName() }}</td>
                <td>{{ $product->pivot->amount }}</td>
                <td>{{ $product->renderPriceInView($product->pivot->amount) }}</td>
            </tr>
            @php $index++ @endphp
            @endforeach
            <tr>
                <th colspan='3'><b>@lang('page_text.order_infomation.total')</b></th>
                <th>{{ $order->getPrice() }}</th>
            </tr>
            <tr>
                <th colspan='3'><b>@lang('page_text.order_infomation.ship')</b></th>
                <th>{{ $order->getShipFee() }}</th>
            </tr>
            @if($order->breakable_cost)
            <tr>
                <th colspan='3'><b>@lang('page_text.order_infomation.breakable_cost')</b></th>
                <th>{{ $order->getBreakableCost() }}</th>
            </tr>
            @endif
            <tr>
                <th colspan='3'><b>@lang('page_text.order_infomation.total_pay')</b></th>
                <th>{{ $order->getTotalPay() }}</th>
                @if($order->code()->count())
                <th style="color:red">Đơn hàng có sử dụng mã giảm giá {{ $order->code()->first()->promotion }} </th>
                @endif
            </tr>
        </tbody>
    </table>
    <div class="ilv-text-center ">
        <a href="/" class="btn  btn-outline-danger">@lang('component_text.buttons.go_back_homepage')</a>
    </div>
</div>

@endsection
