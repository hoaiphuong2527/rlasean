@extends('layouts.main')
@section('title')
{{ __('Check out') }}
@endsection
@section('meta')
<meta charset="UTF-8">
<meta name="description" content="{{ $option->name }}">
<meta name="keywords" content="{{ $option->name }}">
<meta name="author" content="{{ $option->name }}">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
@endsection
@section('content')

<div id="react-checkout" class="ilv-mb-60 ilv-mt-60"></div>
<div class="clearfix"></div>
<div id="token_form">
    @csrf
</div>
<script>
    var option = {!! $option !!}
    var locale = "{!! app()->getLocale() !!}"   
</script>
@endsection
