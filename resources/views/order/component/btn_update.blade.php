<div data-toggle="tooltip" data-placement="top" style="display: inline-block;width: 100%" title="Cập nhật thông tin đơn hàng">
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#update" style="width: 100%">
        Lưu
    </button>
</div>
<div class="modal fade" id="update" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content  -->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Xác nhận giao hàng</h4>
                <label data-dismiss="modal" style="cursor: pointer;"><i class="fa fa-times"></i></label>
            </div>
            <div class="modal-body">
                <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Tên sản phẩm</th>
                            <th>SL của đơn hàng</th>
                            <th>SL chưa giao</th>
                            <th>SL tồn kho</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $index = 1; @endphp
                        @foreach($item->product_variants as $product)
                        <tr>
                            <td>{{ $index }}</td>
                            <td>{{ $product->getName() }}</td>
                            <td class="variant-amount">{{ $product->pivot->amount }}</td>
                            <td>{{ $product->countAmountByStatus([
                                        config('ecommerce.order_statuses.waiting'),
                                        config('ecommerce.order_statuses.processing'),
                                        config('ecommerce.order_statuses.new')
                                    ]) }}
                            </td>
                            <td class="current-variant-amount">{{ $product->amount }}</td>
                            
                        </tr>
                        @php $index++ @endphp
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <input type="submit" class="btn btn-outline-danger active save-order" data-id="{{ $item->id }}" order-edit-url="{!! $item->urlAdminEdit() !!}" name="save" value="Lưu">
                <button type="button" class="btn btn-outline-brand active" data-dismiss="modal">Hủy bỏ</button>
            </div>
        </div>
    </div>
</div>
