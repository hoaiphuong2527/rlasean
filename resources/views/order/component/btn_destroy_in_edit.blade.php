<div data-toggle="tooltip" data-placement="top" style="display: inline-block;" title="Xóa đơn hàng">
    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="{{ '#delete' . $item->id }}">
        <i class="fa fa-trash-alt"></i>
    </button>
</div>
<div class="modal fade" id="{{ 'delete' . $item->id }}" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content  -->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Cảnh báo</h4>
                <label data-dismiss="modal" style="cursor: pointer;"><i class="fa fa-times"></i></label>
            </div>
            <div class="modal-body">
                <p>Dữ liệu sẽ bị xóa vĩnh viễn. Bạn có chắc là muốn như vậy? </p>
            </div>
            <div class="modal-footer">
                <input type="submit" class="btn btn-outline-danger active" name="destroy" value="Đồng ý">
                <button type="button" class="btn btn-outline-brand active" data-dismiss="modal">Không đồng ý</button>
            </div>
        </div>
    </div>
</div>

