<img src="{{ $banner->linkImage() }}" alt=""> 
<!-- em de tam hinh nhu vay, anh design lai nha -->
<div class="image-aboutus-banner">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="lg-text">{{ $h1 }}</h1>
                <h6>{{ $h6 }}</h6>
            </div>
        </div>
    </div>
</div>
