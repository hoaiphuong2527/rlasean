<div class="ivl-item-card">
    <div class="hotel-img">
        <a href="{{ $item->urlGuestShow() }}"><img src="{{ $item->linkImage() }}" alt="" class="img-fluid"></a>
    </div>
    <h3><a href="{{ $item->urlGuestShow() }}">{{ $item->getAttributeLang('name') }}</a></h3>
    <div class="c-position">
        <!-- <p>{{ $item->short_description }}</p> -->
    </div>
    <div class="c-position">
        <p> Giá: {{ $item->getPrice() }}</p>
    </div>
    <div class="c-position">
        <p>        
            <a class="btn btn-ys ilv-italic ilv-circle-20 ys-text-color" href="{{ $item->urlGuestShow() }}">Xem Chi Tiết</a>
        </p>
    </div>
</div>
