@switch($type)
    @case('textarea')
        <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
            <div class="form-group m-form__group">
                <label for="col-form-label col-lg-3 col-sm-12">{{ $lable }}</label>
                <textarea class="form-control" rows="5" cols="30" name="text_translate_{{ $col_name }}_{{$key}}"  >{{ old('text_translate_'. $col_name .'_'. $key , $item->getElementByLang($key,$col_name)) }}</textarea>
                @if ($errors->has('text_translate_'.$col_name.'_'.$key))
                <span class="invalid-feedback" style="display:block;">
                    <strong>{{ $errors->first('text_translate_'.$col_name.'_'.$key) }}</strong>
                </span>
                @endif
            </div>
        </div>
        @break
    @case('slug')
        <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
            <div class="form-group m-form__group">
                <label for="">{{ $lable }}</label><br>
                <span class="m-form__help" style="color:#333">Vui lòng nhập đường dẫn theo định dạng mẫu:<b>duong-dan-{{ $item->getElementByLang($key,$col_name) }}</b></span><br>
                <div class="input-group m-input-group m-input-group--square">
                    <div class="input-group-prepend"><span class="input-group-text">{{ route($url,['id'=>$item->id,'slug' =>""]) }}/</span></div>
                    <input type="text" class="form-control m-input m-input--square" id="" placeholder="" name="text_translate_{{ $col_name }}_{{$key}}"
                        value="{{ old('text_translate_'.$col_name .'_'. $key , $item->getElementByLang($key,$col_name)) }}"  >
                        @if ($errors->has('text_translate_'.$col_name.'_'.$key))
                        <span class="invalid-feedback" style="display:block;">
                            <strong>{{ $errors->first('text_translate_'.$col_name.'_'.$key) }}</strong>
                        </span>
                        @endif
                </div>
            </div>
        </div>
        @break
    @case('page_slug')
        <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
            <div class="form-group m-form__group">
                <label for="">{{ $lable }}</label><br>
                <span class="m-form__help" style="color:#333">Vui lòng nhập đường dẫn theo định dạng mẫu:<b>duong-dan-{{ $item->getElementByLang($key,$col_name) }}</b></span><br>
                <div class="input-group m-input-group m-input-group--square">
                    <div class="input-group-prepend"><span class="input-group-text">{{ route($url,['slug' =>""]) }}/</span></div>
                    <input type="text" class="form-control m-input m-input--square" id="" placeholder="" name="text_translate_{{ $col_name }}_{{$key}}"
                        value="{{ old('text_translate_'.$col_name .'_'. $key , $item->getElementByLang($key,$col_name)) }}"  >
                        @if ($errors->has('text_translate_'.$col_name.'_'.$key))
                        <span class="invalid-feedback" style="display:block;">
                            <strong>{{ $errors->first('text_translate_'.$col_name.'_'.$key) }}</strong>
                        </span>
                        @endif
                </div>
            </div>
        </div>
        @break
    @case('summernote')
        <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
            <div class="form-group m-form__group">
                <label for="exampleInputPassword1">{{ $lable }}</label>
                <input name="col_name_{{ $col_name }}_{{ $key }}" value="{{ $col_name }}" class="d-none">
                <textarea class="form-control myPost" rows="20" cols="30" name="text_translate_{{ $col_name }}_{{$key}}"  >{{ old('text_translate_'. $col_name .'_'. $key , $item->getElementByLang($key,$col_name)) }}</textarea>
                @if ($errors->has('text_translate_'.$col_name.'_'.$key))
                <span class="invalid-feedback" style="display:block;">
                    <strong>{{ $errors->first('text_translate_'.$col_name.'_'.$key) }}</strong>
                </span>
                @endif
            </div>
        </div>
        @break
    @default
    <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
        <div class="form-group m-form__group">
            <label for="exampleInputPassword1">{{ $lable }}</label>
            <input name="col_name_{{ $col_name }}_{{ $key }}" value="{{ $col_name }}" class="d-none">
            <input type="text" class="form-control m-input m-input--square" id="" placeholder="{{ $lable }}" name="text_translate_{{ $col_name }}_{{$key}}"
                value="{{ old('text_translate_'.$col_name .'_'. $key , $item->getElementByLang($key,$col_name)) }}"  >
            @if ($errors->has('text_translate_'.$col_name.'_'.$key))
            <span class="invalid-feedback" style="display:block;">
                <strong>{{ $errors->first('text_translate_'.$col_name.'_'.$key) }}</strong>
            </span>
            @endif
        </div>
    </div>
@endswitch