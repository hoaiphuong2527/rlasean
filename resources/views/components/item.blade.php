<!-- BEGIN: CONTENT/SHOPS/SHOP-2-1 -->
<div class="container">
    <div class="c-content-person-1-slider" data-slider="owl">
            <div class="owl-carousel owl-theme c-theme c-owl-nav-center" data-rtl="false" data-items="3"
                    data-slide-speed="8000">
                    @foreach($list as $item)
                    @include('components.card')
                    @endforeach
            </div>
    </div>
</div>
<!-- END: CONTENT/SHOPS/SHOP-2-1 -->