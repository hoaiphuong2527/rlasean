@foreach($list as $l)
    @include('components.dropdown_item', ['link' => $l->urlGuestShow(), 'text' => $l->getAttributeLang('display_name')])
    @if($l->children()->count())
        @include('components.dropdown_item_child',['list' => $l->children])
    @endif
@endforeach