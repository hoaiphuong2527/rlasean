@extends('layouts.admin')
@section('content')
@section('title')
Review sản phẩm
@endsection 
@section('content')

<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    Danh sách review
                </h3>
            </div>
        </div>
    </div>
    <div class="m-portlet__body">
        <form method="get" class="row">
            <div class="m-form__group m-form__group--inline col-md-4">
                <div class="m-form__label">
                    <label>Trạng thái</label>
                </div>
                <div class="m-form__control">
                    <select class="form-control" name="status">
                        <option value="all" @if ($status=="all" ) selected @endif>Tất cả</option>
                        <option value="public" @if ($status=="public" ) selected @endif>Public</option>
                        <option value="skip" @if ($status=="skip" ) selected @endif>Bỏ qua</option>
                    </select>
                </div>
            </div>
            <!-- <div class="m-form__group m-form__group--inline col-md-4">
                <div class="m-form__label">
                    <label>Đối tượng</label>
                </div>
                <div class="m-form__control">
                    <select class="custom-select form-control" name="review_type">
                        <option value="all" @if ($review_type=="all" ) selected @endif>All</option>
                        <option value="product" @if ($review_type=="product" ) selected @endif>Loại sản phẩm</option>
                        <option value="post" @if ($review_type=="post" ) selected @endif>Loại bài viết</option>
                    </select>
                </div>
            </div> -->
            <div class="m-form__group m-form__group--inline col-md-4">
                <div class="m-form__label" style="margin-top:7px">
                    <label></label>
                </div>
                <div class="m-form__control">
                    <button class="btn btn-success">Lọc</button>
                </div>
            </div>
        </form>
        <hr>
        <!--begin: Datatable -->
        <div class="m_datatable m-datatable m-datatable--default m-datatable--loaded" style="">
            <table class="m-datatable__table" style="display: block; min-height: 300px; overflow-x: auto;">
                <thead class="m-datatable__head">
                    <tr class="m-datatable__row" style="left: 0px;">
                        <th class="m-datatable__cell--center m-datatable__cell "> <span style="width: 50px ;">#</span></th>
                        <th class="m-datatable__cell--center m-datatable__cell "> <span style="width: 100px ;">Người
                                gửi</span></th>
                        <th class="m-datatable__cell--center m-datatable__cell "> <span style="width: 100px ;">Đối
                                tượng
                                review</span></th>
                        <th class="m-datatable__cell--center m-datatable__cell "> <span style="width: 100px ;">Điểm số</span></th>
                        <th class="m-datatable__cell--center m-datatable__cell "> <span style="width: 100px ;">Trạng
                                thái</span></th>
                        <!-- <th class="m-datatable__cell--center m-datatable__cell "> <span style="width: 100px ;">Loại</span></th> -->
                        <th class="m-datatable__cell--center m-datatable__cell "> <span style="width: 100px ;">Hành
                                động</span></th>
                    </tr>
                </thead>
                <tbody style="" class="m-datatable__body">
                    <?php $index = 1; ?>
                    @foreach($reviews as $row)
                    <tr class="m-datatable__row" style="left: 0px;">
                        <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 50px ;">{{ $index
                                }}</span></td>
                        <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 100px ;">
                                {{ $row->user['name'] }}
                            </span></td>
                        <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 100px ;">
                                {{ $row->reviewtable->name }}
                            </span></td>
                        <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 100px ;">
                                {{ $row->level }} / 5
                            </span></td>
                        <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 100px ;">
                                <div class="checkbox">
                                        <label class="m-switch m-switch--outline m-switch--primary" data-toggle="tooltip"
                                            data-placement="top" title="{{ __('Đóng/Mở') }}" class="ilv-tooltip-wrapper">
                                            <input data-id="{{$row->id}}" class="chkStatusReview" type="checkbox"
                                                @if($row->status == 1) checked @endif>
                                            <span></span>
                                        </label>
                                    </div>
                            </span></td>
                        <!-- <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 100px ;">
                                {{ $row->getReviewType() }}
                            </span></td> -->
                        <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 100px ;">
                                <div class=" ilv-btn-group">
                                    @include('reviews.admin.detail_content')
                                    <div data-toggle="tooltip" data-placement="top" style="display: inline-block;" title="xóa bình luận">
                                        <form method="post" action="{!! route('admin_reviews.destroy', ['id' => $row->id]) !!}">
                                            <input type="hidden" name="_method" value="DELETE" /> {!! csrf_field() !!}
                                            <button type="submit" class="btn btn-sm btn-danger">
                                                <i class="fa fa-trash-alt"></i>
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </span></td>
                    </tr>
                    <?php $index++ ?>
                    @endforeach
                </tbody>
                {{ $reviews->links() }}
            </table>
        </div>
    </div>
</div>
@endsection