<div data-toggle="tooltip" data-placement="top" style="display: inline-block;" title="Xem chi tiết bình luận">
    <button type="button" class="btn btn-sm btn-info" data-toggle="modal" data-target="{{ '#detail' . $row->id }}">
        <i class="fa fa-eye"></i>
    </button>
</div>
<div class="modal fade" id="{{ 'detail' . $row->id }}" role="dialog">
<div class="modal-dialog">
    <!-- Modal content  -->
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Nội dung</h4>
            <label data-dismiss="modal" style="cursor: pointer;"><i class="fa fa-times"></i></label>
        </div>
        <div class="modal-body text-left">
            {{ $row->content }}
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-danger active" data-dismiss="modal">Đóng</button>
        </div>
    </div>
</div>
</div>