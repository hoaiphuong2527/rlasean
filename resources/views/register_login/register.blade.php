<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>

    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <h1>Register Account</h1>
            @if(Session::has('success'))
        <div class="alert alert-success">
            {{ Session::get('success') }}
        </div>
    @endif
    <form action="{{ route('register') }}" method="post">
        @csrf
        @if($errors->has('name'))
            <p style="color:red">Lỗi ! {{ $errors->first('name') }}</p>
        @endif
        <div class="form-group">
            <label for="">Name</label>
            <input type="text" class="form-control" id="name" name="name" aria-describedby="emailHelp" placeholder="Enter name" value="{{ old('name') }}">
        </div>
         @if($errors->has('email'))
            <p style="color:red">Lỗi ! {{ $errors->first('email') }}</p>
        @endif
        <div class="form-group">
            <label for="">Email</label>
            <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" placeholder="Enter email" value="{{ old('email') }}">
        </div>
        <div class="form-group">
            <label for="">BirthDay</label>
            <input type="date" class="form-control" id="date" name="date" aria-describedby="emailHelp"  value="{{ old('date') }}">
        </div>
        <div class="form-group">
            <label for="">Image</label>
            <input type="file" class="form-control" id="image" name="image" placeholder="Image" value="{{ old('image') }}">
        </div>
         @if($errors->has('password'))
            <p style="color:red">Lỗi ! {{ $errors->first('password') }}</p>
        @endif
        <div class="form-group">
            <label for="">Password</label>
            <input type="password" class="form-control" id="password" name="password"placeholder="Password">
        </div>
         @if($errors->has('re_password'))
            <p style="color:red">Lỗi ! {{ $errors->first('re_password') }}</p>
        @endif
        <div class="form-group">
            <label for="">Re-password</label>
            <input type="password" class="form-control" id="re_password" name="re_password" placeholder="Re-password">
        </div>
         @if($errors->has('phone'))
            <p style="color:red">Lỗi ! {{ $errors->first('phone') }}</p>
        @endif
        <div class="form-group">
            <label for="">Phone</label>
            <input type="number" class="form-control" id="phone" name="phone" placeholder="Phone number" value="{{ old('phone') }}">
        </div>
         @if($errors->has('address'))
            <p style="color:red">Lỗi ! {{ $errors->first('address') }}</p>
        @endif
        <div class="form-group">
            <label for="">Address</label>
            <input type="text" class="form-control" id="address" name="address" placeholder="Address" value="{{ old('address') }}">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
        </div>
        <div class="col-md-4"></div>
    </div>
</body>
</html>
