@section('title') Danh sách đăng ký nhận tin tức @endsection @extends('layouts.admin') @section('content')
@if(Session::has('status'))
<div class="alert alert-info">
    {{ Session::get('status') }}
</div>
@endif
<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    Danh sách đăng ký nhận tin tức
                </h3>
            </div>
        </div>
    </div>
    <div class="m-portlet__body">
        <form class="row" method="GET" action="">
            <div class="col-5">
                <div class="form-group m-form__group row">
                    <label class="col-form-label col-lg-3 col-sm-12">Email</label>
                    <div class="col-lg-9 col-md-9 col-sm-12">
                        <input class="form-control m-input m-input--square" type="text" name="email" value="{{ $email }}">
                    </div>
                </div>
            </div>
            <div class="col-3">
                <div class="form-group m-form__group row">
                    <input type="submit" class="btn btn-info" value="Tìm kiếm">
                </div>
            </div>
            <div style="float:right;">
                <div class="form-group m-form__group row" style="margin-top: 5px;">
                    <input type="submit" name="export" class="btn btn-light" id="" style="border-color: #8c8282;"
                        value="Tải xuống excel">
                </div>
            </div>
        </form>
        <hr>
        <!--begin: Datatable -->
        <div class="m_datatable m-datatable m-datatable--default m-datatable--loaded" style="">
            <table class="m-datatable__table" style="display: block; min-height: 300px; overflow-x: auto;">
                <thead class="m-datatable__head">

                    <tr class="m-datatable__row" style="left: 0px;">
                        <th class="m-datatable__cell--center m-datatable__cell ">
                            <span style="width: 50px;">#</span></th>
                        <th class="m-datatable__cell--center m-datatable__cell  ">
                            <span style="width: 200px;">Email</span></th>
                        <th class="m-datatable__cell--center m-datatable__cell  ">
                            <span style="width: 150px;">Hành Động</span></th>
                    </tr>
                </thead>
                <tbody style="" class="m-datatable__body">
                    <?php $index = 1; ?>
                    @foreach($news as $item)
                    <tr class="m-datatable__row" style="left: 0px;">
                        <td class="m-datatable__cell--center m-datatable__cell">
                            <span style="width: 50px;">{{ $index++}}</span></td>
                        <td class="m-datatable__cell--center m-datatable__cell"> 
                            <span style="width: 200px;">{{ $item->email }}
                            </span></td>
                        <td class="m-datatable__cell--center m-datatable__cell">
                            <span style="width: 150px; margin-left:30px">
                                @include('newsletter.admin.btn_delete')
                            </span>
                        </td>
                    </tr>
                    @endforeach()
                </tbody>
            </table>
            {{ $news->links() }}
        </div>
    </div>
</div>
@endsection
