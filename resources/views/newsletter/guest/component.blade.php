<div class="card">
    <h5 class="card-header ilv-card-head text-black  text-uppercase">@lang('page_text.post.news_letter')</h5>
    <form action="{{ route('guest_news.store') }}" method="POST">
        @csrf
        <div class="card-body">
            <div class="input-group">
                <input type="text" class="form-control" name="news_email" placeholder="">
                <button class="btn btn-secondary" type="submit">@lang('component_text.buttons.send')</button>
            </div>
        </div>
    </form>
</div>