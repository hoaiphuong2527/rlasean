@section('title') 
Thư viện ảnh 
@endsection 
@extends('layouts.admin') 
@section('content')
@if(Session::has('status'))
<div class="alert alert-info">
    {{ Session::get('status') }}
</div>
@endif
<div id="admin-react-media-app"></div>
@endsection
