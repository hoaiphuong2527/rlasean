@extends('layouts.admin')
@section('content')
@section('title')
    @if($search_name) Kết quả tìm kiếm @else Tất cả sản phẩm @endif
@endsection
@if(Session::has('status'))
<div class="alert alert-info">
    {{ Session::get('status') }}
</div>
@endif
@if(Session::has('destroy'))
<div class="alert alert-success">
    {{ Session::get('destroy') }}
</div>
@endif
<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    Danh sách sản phẩm
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <form method="post" action="{{ route('admin_product.store') }}">
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air"
                            value="Thêm mới" />
                    </form>
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <form class="row" method="GET" action="">

            <div class="col-5">
                <div class="form-group m-form__group row">
                    <label class="col-form-label col-lg-3 col-sm-12">Tên sản phẩm</label>
                    <div class="col-lg-9 col-md-9 col-sm-12">
                        <input class="form-control m-input m-input--square" type="text" name="name" value="{{ $name }}">
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="form-group m-form__group row">
                    <label class="col-form-label col-lg-3 col-sm-12">Danh mục</label>
                    <div class="col-lg-9 col-md-9 col-sm-12">
                        <select class="form-control" id="m_select2_2_modal" name="category_slug">
                            <option value="all" @if($category_slug=='all' ) selected @endif>Tất cả</option>
                            @foreach($categories as $category)
                            <option value="{{$category->slug}}" @if($category_slug==$category->slug ) selected
                                @endif>{{ $category->renderName() }}</option>
                            @include('products.component.child_option_index',['categories' => $category->children])   
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-3">
                <div class="form-group m-form__group row">
                    <input type="submit" class="btn btn-info" value="Tìm kiếm">
                </div>
            </div>
        </form>
        <div class="m_datatable m-datatable m-datatable--default m-datatable--loaded" style="">
            <table class="m-datatable__table" style="display: block; min-height: 300px; overflow-x: auto;">
                <thead class="m-datatable__head">
                    <tr class="m-datatable__row" style="left: 0px;">
                        <th class="m-datatable__cell--center m-datatable__cell  ">
                            <span style="width: 80px;"> #</span>
                            </span></th>
                        <th class="m-datatable__cell--center m-datatable__cell  ">
                            <span style="width: 100px;"> Tên sản phẩm</span>
                            </span></th>
                        <th class="m-datatable__cell--center m-datatable__cell  ">
                            <span style="width: 100px;"> Số lượng</span>
                            </span></th>
                        <th class="m-datatable__cell--center m-datatable__cell  ">
                            <span style="width: 100px;"> Danh mục </span>
                            </span></th>
                        <th class="m-datatable__cell--center m-datatable__cell ">
                            <span style="width: 100px ;">Trạng thái</span></th>
                        <!-- <th class="m-datatable__cell--center m-datatable__cell ">
                            <span style="width: 150px ;">Vị trí sắp xếp</span></th> -->
                        <th class="m-datatable__cell--center m-datatable__cell ">
                            <span style="width: 130px ;">Hành động</span></th>
                    </tr>
                </thead>
                <tbody style="" class="m-datatable__body">
                    @if($products)
                        @foreach($products as $row)
                        <tr class="m-datatable__row" style="left: 0px;">
                            <td class="m-datatable__cell--center m-datatable__cell">
                                <span style="width: 80px ;">
                                    {{$row->id }}</span></td>
                            <td class="m-datatable__cell m-datatable__cell--center"> <span style="width: 100px ;">
                                    <a href="{{ $row->urlGuestShow() }}" target="_blank"><b>{{ $row->name_format }}</b></a>
                                </span></td>
                            <td class="m-datatable__cell m-datatable__cell--center"> <span style="width: 100px ;">
                                    <b>{{ $row->getTotal() }}</b>
                                </span></td>
                            <td class="m-datatable__cell m-datatable__cell--center">
                                <span style="width: 100px ;">
                                    <b>{{ $row->getCateName() }}</b>
                                </span></td>
                            <td class="m-datatable__cell--center m-datatable__cell">
                                <span style="width: 100px ;">
                                    @if($row->category)
                                    <div data-toggle="tooltip" data-placement="top" style="display: inline-block;" title="Đóng/mở trạng thái sản phẩm">
                                        <div class="checkbox">
                                            <label class="m-switch m-switch--outline m-switch--primary">
                                                <input data-id="{{$row->id}}" class="chkProduct" type="checkbox" @if($row->status) checked
                                                @endif>
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    @else
                                        N/A
                                    @endif
                                </span>
                            </td>
                            <!-- <td class="m-datatable__cell--center m-datatable__cell">
                                <span style="width: 150px ;">
                                    <form action="{{ route('admin_product.sort_feature',['id' => $row->id]) }}" method="post">
                                        {{ csrf_field() }}
                                        <div class=" ilv-btn-group">
                                            <input class="form-control" name="sort_feature" value="{{ $row->sort_feature }}"
                                                style="width:50px">
                                            <button type="submit" class="btn btn-sm btn-success" style="margin-top: 5px;
                                            margin-left: 5px;"><i class="fa fa-save"></i></button>
                                        </div>
                                    </form>
                                </span></td> -->
                            <td class="m-datatable__cell--center m-datatable__cell">
                                <span style="width: 130px ;">
                                    <div class=" ilv-btn-group">
                                        <div data-toggle="tooltip" data-placement="top" style="display: inline-block;" title="Chỉnh sửa thông tin sản phẩm">
                                            <a class="btn btn-sm btn-warning" href="{{ $row->urlAdminEdit() }}">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                        </div>
                                        @if($row->status)
                                        <div data-toggle="tooltip" data-placement="top" style="display: inline-block;" title="Tạo bản sao">
                                            <form method="post" action="{{ route('admin_product_clone.store',['product_slug' => $row->slug]) }}">
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-sm btn-info"><i class="fa fa-copy"></i></button>
                                            </form>
                                        </div>
                                        @endif
                                        @include('products.component.btn_delete')                       
                                    </div>
                                </span>
                            </td>
                        </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
            {{ $products->links() }}
        </div>
    </div>
</div>
@endsection
