@extends('layouts.admin')
@section('content')
@section('title')
Chỉnh sửa thông tin
@endsection
@if(Session::has('status'))
<div class="alert alert-info">
    {{ Session::get('status') }}
</div>
@endif
@if(Session::has('destroy'))
<div class="alert alert-success">
    {{ Session::get('destroy') }}
</div>
@endif
<div class="m-portlet">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    Chỉnh sửa thông tin
                </h3>
            </div>
        </div>
        @if($item->status)
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav m-portlet__nav-item">
                <li class="m-nav__item m-topbar__user-profile m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light"
                    m-dropdown-toggle="click" aria-expanded="true">
                    @if($item->hasVariants())
                    @include('products.component.position',['variants' => $item->product_variants])
                    @endif
                </li>
                <li class="m-portlet__nav-item">
                    <form method="post" action="{{ route('admin_product_clone.store',['product_slug' => $item->slug]) }}">
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air"
                            value="Nhân bản sản phẩm" />
                    </form>
                </li>
                @include('admin.component.droplist_lang')
            </ul>
        </div>
        @endif
    </div>
    <form id="formEdit" class="m-form m-form--fit m-form--label-align-right" method="POST" action="{{ $item->urlAdminUpdate() }}">
        <input type="hidden" name="_method" value="PUT" /> {{ csrf_field() }}
        <input type="text" class=" d-none btn btn-info" id="inputSubmitType" name="" value="true">
        <div class="m-portlet__body">
            <div class="m-portlet__body">
                <div class="tab-content">
                    <div class="row">
                        <div class="col-md-6">
                            <br>
                            <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group m-form__group">
                                    <label for="exampleInputPassword1">Tên sản phẩm</label>
                                    <input type="text" class="form-control m-input m-input--square" id="" placeholder="Tên sản phẩm"
                                        name="name" value="{{ old('name',$item->name) }}">
                                    @if ($errors->has('name'))
                                    <span class="invalid-feedback" style="display:block;">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            @if($item->name)
                            <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group m-form__group">
                                    <label for="">Đường dẫn</label><br>
                                    <span class="m-form__help" style="color:#333">Vui lòng nhập đường dẫn theo định
                                        dạng
                                        mẫu:<b>duong-dan-{{ $item->slug }}</b></span><br>
                                    <div class="input-group m-input-group m-input-group--square">
                                        <div class="input-group-prepend"><span class="input-group-text">{{
                                                route('guest_products.show.'.app()->getLocale(),['id' => $item->id,
                                                'slug'
                                                =>""]) }}/</span></div>
                                        <input type="text" class="form-control m-input m-input--square" id=""
                                            placeholder="" name="slug" value="{{ old('slug',$item->slug) }}" required>
                                        @if ($errors->has('slug'))
                                        <span class="invalid-feedback" style="display:block;">
                                            <strong>{{ $errors->first('slug') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            @endif
                            <br>
                            <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group m-form__group">
                                    <div class="m-checkbox-inline">
                                        <label class="m-checkbox col-12">
                                            <input type="checkbox" name="is_breakable"  @if($item->is_breakable) checked @endif> Sản phẩm dễ vỡ
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div style="border-left: 1px solid #ebedf2;">
                                <br>
                                @include('products.component.option')
                                <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                                    <div class="form-group m-form__group">
                                        <label for="col-form-label col-lg-3 col-sm-12">Mô tả ngắn</label>
                                        <textarea class="form-control" rows="5" cols="30" name="short_description">{{old('short_description',$item->short_description)}}</textarea>
                                        @if ($errors->has('short_description'))
                                        <span class="invalid-feedback" style="display:block;">
                                            <strong>{{ $errors->first('short_description') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <br>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <br>
                        <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                            <div class="form-group m-form__group">
                                <label for="col-form-label col-lg-3 col-sm-12">Mô tả chi tiết</label>
                                <textarea class="form-control myPost" rows="20" cols="30" name="description">{{old('description',$item->description)}}</textarea>
                                @if ($errors->has('description'))
                                <span class="invalid-feedback" style="display:block;">
                                    <strong>{{ $errors->first('description') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                            <div class="form-group m-form__group">
                                <label for="col-form-label col-lg-3 col-sm-12">Thông số kỹ thuật</label>
                                <textarea class="form-control myPost" rows="20" cols="30" name="specification">{{old('specification',$item->specification)}}</textarea>
                                @if ($errors->has('specification'))
                                <span class="invalid-feedback" style="display:block;">
                                    <strong>{{ $errors->first('specification') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                            <div class="form-group m-form__group">
                                <label for="col-form-label col-lg-3 col-sm-12">Tổng quan</label>
                                <textarea class="form-control myPost" rows="20" cols="30" name="overview">{{old('overview',$item->overview)}}</textarea>
                                @if ($errors->has('overview'))
                                <span class="invalid-feedback" style="display:block;">
                                    <strong>{{ $errors->first('overview') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                            <div class="form-group m-form__group">
                                <label for="col-form-label col-lg-3 col-sm-12">Tương thích</label>
                                <textarea class="form-control myPost" rows="20" cols="30" name="compatibility">{{old('compatibility',$item->compatibility)}}</textarea>
                                @if ($errors->has('compatibility'))
                                <span class="invalid-feedback" style="display:block;">
                                    <strong>{{ $errors->first('compatibility') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <br>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="m-portlet">
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Danh sách biến thể của {{ $item->name }}
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                        <input type="submit" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air input-submit-form"
                            value="Thêm mới" name="create_variant" />
                    </li>
                </ul>
            </div>
        </div>
        <div class="m-portlet__body">
            <div class="m_datatable m-datatable m-datatable--default m-datatable--loaded" style="">
                <table class="m-datatable__table" style="display: block; min-height: 300px; overflow-x: auto;">
                    <thead class="m-datatable__head">
                        <tr class="m-datatable__row" style="left: 0px;">
                            <th class="m-datatable__cell--center m-datatable__cell  ">
                                <span style="width: 80px;"></span></th>
                            <th class="m-datatable__cell--center m-datatable__cell  ">
                                <span style="width: 100px;"> Tên giả định</span></th>
                            <th class="m-datatable__cell--center m-datatable__cell  ">
                                <span style="width: 200px;"> Giá</span></th>
                            <th class="m-datatable__cell--center m-datatable__cell  ">
                                <span style="width: 200px;"> Số lượng</span></th>
                            <th class="m-datatable__cell--center m-datatable__cell ">
                                <span style="width: 100px ;">Sắp xếp</span></th>
                            <th class="m-datatable__cell--center m-datatable__cell ">
                                <span style="width: 100px ;">Trạng thái</span></th>
                            <th class="m-datatable__cell--center m-datatable__cell ">
                                <span style="width: 100px ;">Hành động</span></th>
                        </tr>
                    </thead>
                    <tbody style="" class="m-datatable__body">
                        @if(count($item->product_variants))
                        @foreach($item->product_variants as $row)
                        <tr class="m-datatable__row" style="left: 0px;">
                            <td class="m-datatable__cell--center m-datatable__cell">
                                <span style="width: 100px ;">
                                    <img src="{{ $row->linkImage() }}" alt="" style=" width:50px; height:50px">
                                </span>
                            </td>
                            <form action="{{ route('admin_product_variant.sort_position',['id' => $row->id]) }}"
                                    method="post">
                                    {{ csrf_field() }}
                            <td class="m-datatable__cell"> <span style="width: 100px ;">
                                <input type="string" class="form-control" name="title" value="{{ $row->title }}">
                                </span></td>
                            <td class="m-datatable__cell"> <span style="width: 200px ;">
                                    <input type="number" class="form-control" name="price" value="{{ $row->price_round }}">
                                </span></td>
                            <td class="m-datatable__cell"> <span style="width: 200px ;">
                                    <input type="number" class="form-control" name="amount" value="{{ $row->amount }}">
                                </span></td>
                            <td class="m-datatable__cell--center m-datatable__cell">
                                <span style="width: 100px ;">
                                    <div class=" ilv-btn-group">
                                        <input class="form-control" name="position" value="{{ $row->position }}"
                                            style="width:50px">
                                        <button type="submit" class="btn btn-sm btn-success" style="margin-top: 5px; margin-left: 5px;">
                                            <i class="fa fa-save"></i>
                                        </button> 
                                    </div>
                                </span></td>
                            </form>
                            <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 100px ;">
                                    <div data-toggle="tooltip" data-placement="top" style="display: inline-block;" title="Đóng/mở trạng thái biến thể">
                                    <div class="checkbox">
                                        <label class="m-switch m-switch--outline m-switch--primary">
                                            <input data-id="{{$row->id}}" class="chkVariant" type="checkbox"
                                                @if($row->status == 1) checked @endif>
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                                </span></td>
                            <td class="m-datatable__cell--center m-datatable__cell">
                                <span style="width: 100px ;">
                                    <div class=" ilv-btn-group">
                                        <a class="btn btn-sm btn-warning" href="{{ $row->urlAdminEdit($item->slug) }}">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        @include('products.variant.admin.btn_delete')
                                    </div>
                                </span>
                            </td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="m-portlet__body">
        <div class="m-form__actions text-right">
            <input type="button" class="btn btn-info input-submit-form" name="continue" value="Lưu & tạo mới">
            @if(!$item->status)
            <input type="button" class="btn btn-success input-submit-form" name="publish" value="Đăng">
            <input type="button" class="btn btn-warning input-submit-form" name="draff" value="Lưu nháp">
            @else
            <input type="button" class="btn btn-success input-submit-form" name="publish" value="Lưu & Đăng">
            @endif
        </div>
    </div>
</div>
@endsection
