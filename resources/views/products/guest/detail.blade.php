@extends('layouts.main')
@section('title')
{{ $item->getAttributeLang('name') }}
@endsection
@section('meta')
<meta charset="UTF-8">
<meta name="description" content="{{ $item->getAttributeLang('name') }}">
<meta name="keywords" content="{{ $item->getAttributeLang('name') }}">
<meta name="author" content="{{ $item->getAttributeLang('name') }}">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta property="og:url"
    content="{{ route('guest_products.show.'.app()->getLocale(),['id' => $item->id, 'slug' => $item->getAttributeLang('slug') ]) }}" />
<meta property="og:type" content="article" />
<meta property="og:title" content="{{ $item->getAttributeLang('name')}}" />
<meta property="og:description" content="{{ $item->getAttributeLang('name')}}" />
<meta property="og:image" content="{{ $item->linkImageFill() }}" />
@section('content')
<style>
    .iw-breadcrumbs-bg {
        background: url("{{ asset('images/bg/breadcrumb.jpg') }}");
        /* background: url("https://www.iwalkthailand.com/themes/leo_zigg/assets/img/modules/appagebuilder/images/Banner-Cable1.jpg"); */
        background-repeat: no-repeat;
        background-size: cover;
        background-position: center center;
    }

</style>
<div class="container iw-breadcrumbs  py-2">
    {!! Breadcrumbs::render('guest_products.show.'.app()->getLocale(),['id' => $item->id, 'slug' =>
    $item->getAttributeLang('slug') ])!!}
</div>
<div class="container">
    <script>
        var locale = "{!! app()->getLocale() !!}"
        var item = {!!$item!!};
        var varriants = {!!$item->product_variant_onlines!!};
    </script>
    <div id="react-detail"></div>
    <div class="row mt-4">
        <div class="col-md-12">
            <div class="card">
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link active ilv-nav-link ilv-border-none" data-toggle="tab"
                            href="#thongtin">@lang('page_text.product_detail.description')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link ilv-border-none" data-toggle="tab"
                            href="#kythuat">@lang('page_text.product_detail.specification')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link ilv-border-none" data-toggle="tab"
                            href="#tongquan">@lang('page_text.product_detail.overview')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link ilv-border-none" data-toggle="tab"
                            href="#tuongthich">@lang('page_text.product_detail.compatibility')</a>
                    </li>
                </ul>
                <div class="tab-content ilv-content">
                    <div class="tab-pane container active" id="thongtin">
                        {!! $item->getAttributeLang('description') !!}
                    </div>
                    <div class="tab-pane container fade" id="kythuat">
                        {!! $item->getAttributeLang('specification') !!}
                    </div>
                    <div class="tab-pane container fade" id="tongquan">
                        {!! $item->getAttributeLang('overview') !!}
                    </div>
                    <div class="tab-pane container fade" id="tuongthich">
                        {!! $item->getAttributeLang('compatibility') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Danh gia va nhan xet -->
    <br>
    <div class="container mt-5 ilv-container-benefit pt-4 pb-4 ">
        <div class="row">
            <div class="col-md-4">
                <div class="rating-block col-12 text-center">
                    <h5> {{ $item->calculatedLevel() }}/5</h5>
                    <hr>
                    <b>@include('products.component.review',['lenght' => $item->calculatedLevel()])</b>
                </div>
            </div>
            <div class="col-md-4 border-left border-right ">
                <div class="row">
                    @for($i = 5 ; $i >= 1 ; $i--)
                    <div class="ilv-center-block">
                            <div class="pull-left">
                                    <div class="pull-left" style="width:35px; line-height:1;">
                                        <div style="height:9px; margin:5px 0;">{{ $i }} <span class="fa fa-star fa-fw"></span>
                                        </div>
                                    </div>
                                    <div class="pull-left" style="width:180px;">
                                        <div class="progress" style="height:9px; margin:8px 0;">
                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="5"
                                                aria-valuemin="0" aria-valuemax="5"
                                                style="width: {{ $item->countPercent($i)."%" }}">
                                                <span class="sr-only">80% Complete (danger)</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pull-right" style="margin-left:10px;">{{ $item->countReview($i) }}</div>
                                </div>
                    </div>
                    
                    @endfor
                </div>
            </div>
            <div class="col-md-4">
                <h4>Bạn đã dùng sản phẩm này?</h4>
                <a class="button-animate" id="show-review">Gửi đánh giá của bạn</a>
            </div>
            <hr>
            <form id="review-form" class="col-12 mt-3" method="POST"
                action="{{ route('guest_products.review',['id' => $item->id, 'type' => config('ecommerce.review_types.review')]) }}">
                @csrf
                <section class='rating-widget'>
                    <h3>Bạn chấm sản phẩm này bao nhiêu sao?</h3>
                    <div class='rating-stars text-center'>
                        <ul id='stars'>
                            <li class='star' title='Poor' data-value='1'>
                                <i class='fa fa-star fa-fw'></i>
                            </li>
                            <li class='star' title='Fair' data-value='2'>
                                <i class='fa fa-star fa-fw'></i>
                            </li>
                            <li class='star' title='Good' data-value='3'>
                                <i class='fa fa-star fa-fw'></i>
                            </li>
                            <li class='star' title='Excellent' data-value='4'>
                                <i class='fa fa-star fa-fw'></i>
                            </li>
                            <li class='star' title='WOW!!!' data-value='5'>
                                <i class='fa fa-star fa-fw'></i>
                            </li>
                        </ul>
                    </div>
                </section>
                <input type="hidden" name="level" id="rating-star" value='1'>
                <div class="form-group">
                    <input type="hidden" value="{{ config('ecommerce.reviewtable_type.product') }}"
                        name="reviewtable_type">
                    <input type="hidden" value="0" name="parent_id">
                    <textarea name="content" class="form-control" rows="3" minlength="3" required></textarea>
                    <div class="col-md-12 ilv-min-h-50">
                        @include('products.component.dialog',['type' => config('ecommerce.review_types.review') ])
                    </div>
                </div>
            </form>
            <div class="col-12 mt-3">
                    <div class="row">
                        <div class="col-md-12">
                            <h3>Khách hàng nhận xét</h3>
                        </div>
                        @foreach($item->type_review_online(config('ecommerce.review_types.review'))->get() as $review)
                        <div class="container">
                                <div class="card my-3">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-1">
                                                <img src="https://image.ibb.co/jw55Ex/def_face.jpg" class="img img-rounded img-fluid"/>

                                            </div>
                                            <div class="col-md-11">
                                                <p>
                                                    <a class="float-left"><strong>Tên: {{ $review->user->name }}</strong></a>
                                                    <span class="float-right">
                                                            @include('products.component.review',['lenght' => $review->level])

                                                    </span>
                                               </p>
                                               <div class="clearfix"></div>
                                               <span>Email: {{ $review->user->email }}</span>
                                               <hr>
                                               <p>  {{ $review->content }}</p>
                                                <a  class="float-right btn btn-outline-primary ml-2 reply-comment"> <i class="fa fa-reply"></i>Trả lời bình luận</a>
                                            </div>
                                            <form class="col-12 mt-2 form-comment" method="POST"
                                                action="{{ route('guest_products.review',['id' => $item->id,'type' => config('ecommerce.review_types.comment') ]) }}">
                                                @csrf
                                                <input type="hidden" name="level" id="rating-star" value='0'>
                                                <div class="form-group">
                                                    <input type="hidden" value="{{ $review->id }}" name="parent_id">
                                                    <input type="hidden" value="{{ config('ecommerce.reviewtable_type.product') }}"
                                                        name="reviewtable_type">
                                                    <textarea name="content" class="form-control" rows="3" minlength="3"
                                                        required></textarea>
                                                        @include('products.component.dialog',['type' => 'child'] )
                                                </div>
                                            </form>
                                        </div>
                                        @foreach($review->children_online as $child)
                                            <div class="card card-inner my-2">
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-md-1">
                                                            <img src="https://image.ibb.co/jw55Ex/def_face.jpg" class="img img-rounded img-fluid"/>
                                                        </div>
                                                        <div class="col-md-11">
                                                            <p>
                                                            <a href="https://maniruzzaman-akash.blogspot.com/p/contact.html"><strong>Tên: {{ $child->user->name }}</strong></a></p>
                                                            <span>Email: {{ $child->user->email }}</span>
                                                            <hr>
                                                            <p>{{ $child->content }}</p>                                                            <p>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                    
                    @endforeach
                </div>
            </div>

            <div class="col-12">
                <div class="row">
                    
                    <form class="col-12" method="POST"
                        action="{{ route('guest_products.review',['id' => $item->id,'type' => config('ecommerce.review_types.comment') ]) }}">
                        @csrf
            <hr>
                        <h3>Hỏi & đáp về {{ $item->getAttributeLang('name') }}</h3>
                        <input type="hidden" name="level" id="rating-star" value='0'>
                        <div class="form-group">
                            <input type="hidden" value="0" name="parent_id">
                            <input type="hidden" value="{{ config('ecommerce.reviewtable_type.product') }}"
                                name="reviewtable_type">
                            <textarea name="content" class="form-control" rows="3" minlength="3" required></textarea>
                            <div class="col-md-12 ilv-min-h-50">
                                @include('products.component.dialog',['type' =>
                                config('ecommerce.review_types.comment')] )
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-12">
                    <div class="row">
                        @foreach($item->type_review_online(config('ecommerce.review_types.comment'))->whereParentId(0)->get()
                        as
                        $review)
                        <div class="container">
                                <div class="card my-3">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-1">
                                                <img src="https://image.ibb.co/jw55Ex/def_face.jpg" class="img img-rounded img-fluid"/>
                                            </div>
                                            <div class="col-md-11">
                                                <p>
                                                    <a class="float-left"><strong>Tên: {{ $review->user->name }}</strong></a>
                                               </p>
                                               <div class="clearfix"></div>
                                               <span>Email: {{ $review->user->email }}</span>
                                               <hr>
                                               <p> {{ $review->content }}</p>
                                                <a  class="float-right btn btn-outline-primary ml-2 reply-comment"> <i class="fa fa-reply"></i>Trả lời bình luận</a>
                                            </div>
                                            <form class="col-12 mt-2 form-comment" method="POST"
                                                action="{{ route('guest_products.review',['id' => $item->id,'type' => config('ecommerce.review_types.comment') ]) }}">
                                                @csrf
                                                <input type="hidden" name="level" id="rating-star" value='0'>
                                                <div class="form-group">
                                                    <input type="hidden" value="{{ $review->id }}" name="parent_id">
                                                    <input type="hidden" value="{{ config('ecommerce.reviewtable_type.product') }}"
                                                        name="reviewtable_type">
                                                    <textarea name="content" class="form-control" rows="3" minlength="3"
                                                        required></textarea>
                                                        @include('products.component.dialog',['type' => 'child'] )
                                                </div>
                                            </form>
                                        </div>
                                        @foreach($review->children_online as $child)
                                            <div class="card card-inner my-2">
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-md-1">
                                                            <img src="https://image.ibb.co/jw55Ex/def_face.jpg" class="img img-rounded img-fluid"/>
                                                        </div>
                                                        <div class="col-md-11">
                                                            <p>
                                                            <a href="https://maniruzzaman-akash.blogspot.com/p/contact.html"><strong>Tên: {{ $child->user->name }}</strong></a></p>
                                                            <span>Email: {{ $child->user->email }}</span>
                                                            <hr>
                                                            <p> {{ $child->content }}</p>                                                            <p>
                                                           </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if(count($product_seens))
    <div class="container my-5 ilv-container-benefit pt-4 pb-4 ">
        <div class="row">
            <div class="container mb-4">
                <h4>@lang('page_text.product_details.product_seen')</h4>
                <div class="col-md-12">
                    @php
                    $list = $product_seens;
                    @endphp
                    <div class="owl-carousel owl-init slide-items" id="slide_custom_nav" data-auto-play="true"
                        data-custom-nav="custom-nav-first" data-items-desk="4" data-items-tab="4" data-items-mobile="1"
                        data-margin="20" data-dots="true" data-nav="false" data-loop="false">
                        @foreach($list as $item)
                        @include('categories.guest.product.component.card')
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
</div>

@endsection
