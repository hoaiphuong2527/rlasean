@extends('layouts.main')
@section('title')
{{ __('Sản phẩm thuộc ') . $item->name }}
@endsection
@section('meta')
<meta charset="UTF-8">
<meta name="description" content="{{ $item->name }}">
<meta name="keywords" content="{{ $item->name }}">
<meta name="author" content="{{ $item->name }}">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta property="og:url"           content="{{ route('guest_products.category.'.app()->getLocale(),['id' => $item->id, 'slug' =>
$item->getAttributeLang('slug') ]) }}" />
<meta property="og:type"          content="article" />
<meta property="og:title"         content="{{ $item->getAttributeLang('name')}}" />
<meta property="og:description"   content="{{ $item->getAttributeLang('name')}}" />
<meta property="og:image"         content="{{ $item->linkImage() }}" />
@section('content')

<div id="portfolio" class="portfolio-area area-padding fix">
    <div class="container">
        {!! Breadcrumbs::render('guest_products.category.'.app()->getLocale(),['id' => $item->id, 'slug' => $item->getAttributeLang('slug') ])!!}
        <script>
            var cateSlug = "{!! $item->slug !!}"
            var arr_product_id = {!! collect($arr_product_id) !!}
    </script>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-3">
                @foreach($secondCategories as $s_cate)
                <i>icon</i>
                <a href=""> {{ $s_cate->getAttributeLang('name')}} </a>
                @endforeach
            </div>
            <div class=" col-sm-12 col-md-9">
                @if(count($category_children))
                    @foreach($category_children as $cate_children)
                        <div class="row">
                            @foreach($cate_children->productOnlines as $product)
                                @if($product->product_variant_onlines()->count())
                                <div class="col-12 col-md-6 col-lg-4">
                                    <div class="card">
                                        <a href="{{ $product->urlGuestShow() }}">
                                            <img class="card-img-top" src="{{ $product->linkImage() }}" alt="Card image cap" />
                                        </a>
                                        <div class="card-body">
                                            <h4 class="card-title">
                                                <a class="ilv-black-text" href="{{ $product->urlGuestShow() }}" title="View Product">{{
                                                    $product->getAttributeLang('name') }}</a></h4>
                                            <div class="row">
                                                <div class="col c-position">
                                                    <p>Giá: {{ $product->getPrice() }}</p>
                                                </div>
                                                <div class="col c-position">
                                                    <div class="col text-center" id="btn-add-to-card-{{$product->id}}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endif
                            @endforeach
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
</div>

@endsection
