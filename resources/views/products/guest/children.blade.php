@foreach($categories as $category)
<div class="c-content-box c-size-sm c-content-box c-bg-grey-1">
    <div class="container">
        @include('components.item',['list' => $category->products])
    </div>
</div>
@endforeach