<div class="col-lg-3 mt-lg-0 mt-4 p-lg-0 " id="sidebar">
    <div class="side-bar px-3">
        <div class="sidebar-head">
        <h5 class="agileits-sear-head ilv-fs-24 iw-cl font-weight-bold"> {{ __('page_text.product_by_cate.category') }}</h5>
        </div>
        <div class="left-side  py-2">
            <div class="list-group panel iw-cl">
                @foreach($secondCategories as $category)
                    @if(count($category->children))
                    <a href="#menu{{$category->id}}" class="list-group-item collapsed" data-toggle="collapse" data-parent="#sidebar"
                        aria-expanded="false">
                        <span class="hidden-sm-down">{{$category->name}}<i class="fa fa-plus d-inline float-xs-right"></i></span>
                    </a>
                    <div class="collapse" id="menu{{$category->id}}">
                        @foreach($category->children as $chilCate)
                        <a href="{{ $chilCate->urlGuestShow() }}" class="list-group-item" data-parent="#menu1">{{$chilCate->name}}</a>
                        @endforeach
                    </div>
                    @else
                        @php
                            $active = '';
                            if ($item->id == $category->id) 
                                $active = 'ilv-active'
                        @endphp
                    <a href="{{ $category->urlGuestShow() }}" class="{{ 'list-group-item ' . $active }}" data-parent="#menu1">{{$category->name}}</a>
                        
                    @endif
                @endforeach
            </div>
        </div>
    </div>
    <div class="side-bar p-sm-4 p-3">
        
        <form method="GET">
            <div class="sidebar-head">
                <h5 class="agileits-sear-head ilv-fs-24 iw-cl"> Lọc theo thông số</h5>
            </div>
            <div class="left-side py-2">
                <div class="list-group panel iw-cl">
                    <ul class="list-group category_block">
                        @php
                            $p = null;
                            if (isset($info_key)) {
                                $p = collect($info_key);
                            } else if (old('key')) {
                                $p = collect(old('key'));
                            }
                        @endphp
                        @foreach($info as $key => $inf)
                        <li class="list-group-item">
                            <h5>{{ $inf->name }}</h5>
                            @foreach($inf->getRecordByKey() as $row)
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" name="key[]" id="" value="{{ $row->id }}"
                                        {{ (!empty($p) && $p->contains($row->id)) ? 'checked':'' }}>
                                        {{ $row->getDisplayName() }}
                                </label>
                            </div>
                            @endforeach
                        </li>
                        @endforeach
                    </ul>
                </div>
                <div class="card bg-light mb-3">
                    <input type="submit" class="btn btn-default ilv-hn-custom-button-style" value="Tìm kiếm">
                </div>
            </div>
        </form>
    </div>
</div>
