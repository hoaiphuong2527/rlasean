@extends('layouts.main')
@section('title')
{{ __('page_text.pages.product_by_cate') . $item->name }}
@endsection
@section('meta')
<meta charset="UTF-8">
<meta name="description" content="{{ $item->name }}">
<meta name="keywords" content="{{ $item->name }}">
<meta name="author" content="{{ $item->name }}">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta property="og:url" content="{{ route('guest_products.category.'.app()->getLocale(),['id' => $item->id, 'slug' =>
$item->getAttributeLang('slug') ]) }}" />
<meta property="og:type" content="article" />
<meta property="og:title" content="{{ $item->getAttributeLang('name')}}" />
<meta property="og:description" content="{{ $item->getAttributeLang('name')}}" />
<meta property="og:image" content="{{ $item->linkImage() }}" />
@section('content')
@include('guest.component.banner_title',['imageLink' => $image, 'title' => $item->name ])
<div id="portfolio" class="portfolio-area area-padding fix">
    <div class="container">
        {!! Breadcrumbs::render('guest_products.category.'.app()->getLocale(),['id' => $item->id, 'slug' =>
        $item->getAttributeLang('slug') ])!!}
    </div>
    <div class="container my-5 ilv-container-benefit pt-4 pb-4 ">
        <div class="row" id="react-filter">
        </div>
    </div>
</div>
<script>
        var priceFilter  = {!! collect(config('ecommerce.price_fillter')) !!}
        var cateInfo = {!! collect($item->getArrayInformations())!!};
        var cateId ={!! $item->id!!};

</script>
@endsection
