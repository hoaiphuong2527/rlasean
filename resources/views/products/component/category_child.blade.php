@if(count($categories))
<ul class="list-group ">
    @foreach($categories as $category)
    <li class="list-group-item">
        <div class="custom-control custom-checkbox">
            <a class="custom-control-input" href="{{ route('guest_products.category.'.app()->getLocale(),['id' => $category->id, 'slug' => $category->getAttributeLang('slug') ]) }}">{{ $category->getAttributeLang('name') }}</a>
        </div>
    </li>
    @endforeach
</ul>
@endif
