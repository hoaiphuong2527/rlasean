@foreach($categories as $cate)
<option value="{{ $cate->slug }}"  @if($category_slug == $cate->slug )  selected @endif>
    &nbsp;&nbsp;
{{ $cate->renderName() }}</option>
@if(count($cate->children) > 0)
    @include('products.component.child_option_index',['categories' => $cate->children])   
@endif
@endforeach