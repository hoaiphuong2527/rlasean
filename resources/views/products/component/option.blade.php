<div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
    <div class="form-group m-form__group">
        <label for="exampleSelect1">Danh mục</label>
        <select class="form-control m-input m-input--square" name="category_id">
            @foreach( $categories as $cate)
                <option value="{{ $cate->id }}" @if($cate->id == $item->category_id) selected @endif>{{ $cate->renderName() }}</option>
                @include('products.component.child_option',['categories' => $cate->children])   
            @endforeach
        </select>
    </div>
</div>
