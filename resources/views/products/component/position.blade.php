<input type="button" class="btn btn-info m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air" data-toggle="modal" data-target="{{ '#featured' . $item->id }}"
                            value="Nổi bật" />
<form action="{{ route('admin_products.featured',['p_id' => $item->id]) }}" method="POST">
    @csrf
    <div class="modal fade" id="{{ 'featured' . $item->id }}" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content  -->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Sắp xếp đối tượng hiển thị</h4>
                    <label data-dismiss="modal" style="cursor: pointer;"><i class="fa fa-times"></i></label>
                </div>
                <div class="modal-body">
                    @foreach($variants as $variant)
                    <div class="row">
                        <div class="col-4">{{ $variant->getName() }}</div>
                        <div class="col-8">
                            <input type="number" min="1" value="{{ $variant->position }}" name="value_position[]" class="form-control">
                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="modal-footer">
                    <button type="submit" name="position" class="btn btn-outline-danger active input-submit-form" >Lưu</button>
                </div>
            </div>
        </div>
    </div>
</form>                            
