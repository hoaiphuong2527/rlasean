<div data-toggle="tooltip" data-placement="top">
    <button type="button" class="btn btn-outline-primary mt-3" data-toggle="modal"
        data-target="#information_{{$type}}">Gửi</button>
</div>
<div class="modal fade" id="information_{{$type}}" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content  -->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Gửi nhận xét</h4>
                <label data-dismiss="modal" style="cursor: pointer;"><i class="fa fa-times"></i></label>
            </div>
            <div class="modal-body">
                <div class="form-group m-form__group">
                    <label for="exampleInputPassword1">Tên: </label>
                    <input type="text" class="form-control m-input m-input--square" id="" placeholder="Nhập họ tên"
                        name="review_name" @if(Auth::check()) value="{{ Auth::user()->name }}" @else value="" @endif
                        required>
                </div>
                <div class="form-group m-form__group">
                    <label for="exampleInputPassword1">Email: </label>
                    <input type="email" class="form-control m-input m-input--square" id="" placeholder="Nhập email"
                        name="email" @if(Auth::check()) value="{{ Auth::user()->email }}" @else value="" @endif
                        required>
                </div>
                <div class="form-group m-form__group">
                    <label for="exampleInputPassword1">Số điện thoại: </label>
                    <input type="text" class="form-control m-input m-input--square" id=""
                        placeholder="Nhập số điện thoại" name="phone" @if(Auth::check())
                        value="{{ Auth::user()->phone }}" @else value="" @endif>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-outline-danger active">Gửi nhận xét</button>
            </div>
        </div>
    </div>
</div>
