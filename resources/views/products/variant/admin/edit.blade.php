@extends('layouts.admin')
@section('content')
@section('title')
Chỉnh sửa thông tin biến thể
@endsection
@if(Session::has('status'))
<div class="alert alert-success">
    {{ Session::get('status') }}
</div>
@endif
<div class="m-portlet">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    Chỉnh sửa thông tin
                </h3>
            </div>
        </div>
    </div>
    <form class="m-form m-form--fit m-form--label-align-right" method="POST" action="{{ $item->urlAdminUpdate($product->slug) }}">
        <input type="hidden" name="_method" value="PUT" /> {{ csrf_field() }}
        <div class="m-portlet__body">
            <div class="m-portlet__body">
                <div class="row">
                    <div class="col-6">
                        <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                            <div class="form-group m-form__group">
                                <label for="exampleInputPassword1">Tiêu đề</label>
                                <input type="text" class="form-control m-input m-input--square" name="title" id=""
                                    placeholder="Tiêu đề" value="{{ old('title',$item->title) }}">
                                @if ($errors->has('title'))
                                <span class="invalid-feedback" style="display:block;">
                                    <strong>{{ $errors->first('title') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                            <div class="form-group m-form__group">
                                <label for="exampleInputPassword1">Giá</label>
                                <input type="number" class="form-control m-input m-input--square" id="" placeholder="Giá"
                                    name="price" value="{{ old('price',$item->price_round) }}">
                                @if ($errors->has('price'))
                                <span class="invalid-feedback" style="display:block;">
                                    <strong>{{ $errors->first('price') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                            <div class="form-group m-form__group">
                                <label for="exampleInputPassword1">Số lượng</label>
                                <input type="number" class="form-control m-input m-input--square" id="" placeholder="Số lượng"
                                    name="amount" value="{{ old('amount',$item->amount) }}">
                                @if ($errors->has('amount'))
                                <span class="invalid-feedback" style="display:block;">
                                    <strong>{{ $errors->first('amount') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-6" style="border-left: 1px solid #ebedf2;">
                        <div class="row">
                            <div class="form-group m-form__group col-lg-6 col-md-6 col-sm-12">
                                <label for="exampleInputPassword1">Màu sắc</label>
                                <input id="colorpickerField" type="text" class="form-control m-input m-input--square"
                                    placeholder="Màu sắc" name="color" value="{{ old('color',$item->color) }}">
                                @if ($errors->has('color'))
                                <span class="invalid-feedback" style="display:block;">
                                    <strong>{{ $errors->first('color') }}</strong>
                                </span>
                                @endif
                            </div>
                            <!-- <div class="form-group m-form__group col-lg-6 col-md-6 col-sm-12" style="padding-top:0">
                                <label for="exampleInputPassword1">Kích cỡ</label>
                                <input type="text" class="form-control m-input m-input--square" id="" placeholder="Kích cỡ"
                                    name="size" value="{{ old('size',$item->size) }}">
                                @if ($errors->has('size'))
                                <span class="invalid-feedback" style="display:block;">
                                    <strong>{{ $errors->first('size') }}</strong>
                                </span>
                                @endif
                            </div> -->
                            <div class="form-group m-form__group col-lg-6 col-md-6 col-sm-12" style="padding-top:0">
                                <label for="">Khối lượng</label>
                                <div class="input-group">
                                    <input name="weight" type="text" class="form-control m-input" value="{{ old('weight',$item->weight) }}" />
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            kg
                                        </span>
                                    </div>
                                </div>
                                @if ($errors->has('weight'))
                                <span class="invalid-feedback" style="display:block;">
                                    <strong>{{ $errors->first('weight') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <label>Thông tin thêm</label>
                        <div class="row">
                            @foreach($arr_info_unique as $row)
                            <div class="col-lg-4 col-md-4 col-sm-4" style="padding-top:0">
                                    <label>{{ $row->key }}: </label>
                                <select name="infomation_{{ $row->key }}" id="" class="form-control">
                                    <option value="null">Chọn thông tin</option>
                                    @foreach($row->getRecordByKeyInVariant($item->id) as $record)
                                    <option value="{{ $record->id }}"
                                            @foreach($item->informations as $info)
                                                @if($info->pivot->information_id == $record->id)
                                                selected
                                                @endif
                                            @endforeach
                                            {{ collect(old('infomation_'.$row->key))->contains($record->id)
                                                                            ? 'selected':'' }}
                                    >{{ $record->getDisplayName() }}</option>
                                    @endforeach
                                </select>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <br>
                <hr>
                {{-- @foreach($item->media as $photo)
                <div class="col-md-3">
                    <div class="ilv-img-block" id="photo-{{$photo->id}}" style="background-image: url({{ $photo->link }}); height: 125px;">
                        <span class="ilv-chkbox" id="del-photo" photo-id="{{ $photo->id }}">
                            <i class="fa fa-times"></i>
                        </span>
                    </div>
                </div>
                @endforeach --}}
                <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                <script>
                    var previewImage1 = {!! $item->media !!}
                </script>
                <div id="react-admin-upload-btn" data-img="" data-input-name="image" data-type="multiple"></div>
                </div>

                {{-- <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                    @if($item->galleries)
                    <div class="text-center " style="color:red; margin-bottom: 10px;">
                        <b>Hình ảnh đầu tiên sẽ được chọn là ảnh đại diện cho biến thể</b>
                    </div>
                    <div class="row">
                        @foreach($item->galleries as $photo)
                        <div class="col-md-3">
                            <div class="ilv-img-block" id="photo-{{$photo->id}}" style="background-image: url({{ $photo->linkImage() }}); height: 125px;">
                                <span class="ilv-chkbox" id="del-photo" photo-id="{{ $photo->id }}">
                                    <i class="fa fa-times"></i>
                                </span>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    @endif
                    <br />
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="m-dropzone dropzone m-dropzone--primary dz-clickable" action="{{ route('admin_product_variant.upload_image',['id' => $item->id]) }}"
                                id="uploadImagesForm">
                                <div class="m-dropzone__msg dz-message needsclick">
                                    <h3 class="m-dropzone__msg-title">Chọn hoặc kéo/thả ảnh vào đây.</h3>
                                    <span class="m-dropzone__msg-desc">Giới hạn 10 hình ảnh</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> --}}
            </div>
            <div class="m-portlet__foot m-portlet__foot--fit">
                <div class="m-form__actions text-center">
                    <input type="submit" class="btn btn-info" name="continue" value="Lưu & tạo mới">
                    @if(!$item->status)
                    <input type="submit" class="btn btn-success" name="publish" value="Đăng">
                    <input type="submit" class="btn btn-warning" name="draff" value="Lưu nháp">
                    @else
                    <input type="submit" class="btn btn-success" name="publish" value="Lưu & Đăng">
                    @endif
                </div>
            </div>
        </div>
    </form>
</div>
@endsection
