<div data-toggle="tooltip" data-placement="top" style="display: inline-block;" title="Cập nhật số lượng nhập vào">
    <a class="btn btn-sm btn-warning" data-toggle="modal" data-target="{{ '#update' . $item->id }}">
        <i class="fa fa-edit"></i>
    </a>
</div>
<form method="post" action="{!! $item->urlAdminUpdate() !!}">
    <input type="hidden" name="_method" value="PUT" /> {!! csrf_field() !!}
    <div class="modal fade" id="{{ 'update' . $item->id }}" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content  -->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Cập nhật thông tin</h4>
                    <label data-dismiss="modal" style="cursor: pointer;"><i class="fa fa-times"></i></label>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-4">SL nhập vào:</div>
                        <div class="col-8">
                            <input type="number" min="0" class="form-control m-input m-input--square" id="" placeholder="Số lượng nhập vào" name="new_import"
                            value="{{ old('new_import',$item->amount_import) }}">      
                            <input type="text" class="d-none" name="curent_import"
                            value="{{ $item->amount_import }}">               
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" name="position" class="btn btn-outline-danger active" value="Lưu">
                </div>
            </div>
        </div>
    </div>
</form>