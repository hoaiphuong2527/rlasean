@extends('layouts.admin')
@section('content')
@section('title')
Cập nhật số lượng sản phẩm
@endsection
@if (session('status'))
    <div class="alert alert-info">{{session('status')}}</div>
@endif
@if (session('destroy'))
    <div class="alert alert-info">{{session('destroy')}}</div>
@endif
<div class="m-portlet">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    Cập nhật số lượng sản phẩm
                </h3>
            </div>
        </div>
    </div>
    <form class="m-form m-form--fit m-form--label-align-right" method="POST" action="{{ route('admin_warehouse.update',['id' => $item->id]) }}">
        <input type="hidden" name="_method" value="PUT" /> {{ csrf_field() }}
        <div class="m-portlet__body">
            <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                @foreach($item->product_variants as $variant)
                <div class="form-group m-form__group">
                    <div class="row">
                        <div class="col-4 text-center">
                            <img class="img-thumbnail" src="{{ $variant->linkImage() }}" width="50%">
                            <br>
                            <label for="">{{ $variant->getName() }}</label><br>
                            <b>SL Hiện tại: {{ $variant->amount }}</b>
                        </div>
                        <div class="col-8">
                            <label for="">Số lượng nhập vào: </label>
                            <input type="number" min="0" class="form-control m-input m-input--square ilv-margin" id="" placeholder="Số lượng nhập vào" name="import[]"
                            value="{{ old('import',0) }}">
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="m-portlet__foot m-portlet__foot--fit">
                <div class="m-form__actions text-center">
                    <input type="submit" class="btn btn-primary" name="publish" value="Lưu">
                    <input type="submit" class="btn btn-info" name="create" value="Thêm mới biến thể">
                </div>
            </div>
        </div>
    </form>
</div>
@endsection
