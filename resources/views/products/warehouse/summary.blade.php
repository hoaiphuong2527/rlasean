@extends('layouts.admin')
@section('content')
@section('title')
Kho hàng
@endsection
<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Nhập/Xuất sản phẩm
                    </h3>
                </div>
            </div>
            <!-- <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                        <a href="{{ route('admin_warehouse.index') }}" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">Nhập
                            hàng</a>
                    </li>
                </ul>
            </div> -->
        </div>
        <div class="m-portlet__body">
            <form class="row" method="GET" action="">
                <div class="col-5">
                    <div class="form-group m-form__group row">
                        <label class="col-form-label col-lg-3 col-sm-12">Tên sản phẩm</label>
                        <div class="col-lg-9 col-md-9 col-sm-12">
                            <input class="form-control m-input m-input--square" type="text" name="name" value="{{ $name }}">
                        </div>
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group m-form__group row">
                        <input type="submit" class="btn btn-info" value="Tìm kiếm">
                    </div>
                </div>
            </form>
            <div class="m_datatable m-datatable m-datatable--default m-datatable--loaded" style="">
                <table class="m-datatable__table" style="display: block; min-height: 300px; overflow-x: auto;">
                    <thead class="m-datatable__head">
                        <tr class="m-datatable__row" style="left: 0px;">
                            <th class=" m-datatable__cell  ">
                                <span style="width: 80px;"> Ngày diễn ra</span>
                                </span></th>
                            <th class=" m-datatable__cell  ">
                                <span style="width: 200px;"> Tên sản phẩm</span>
                                </span></th>
                            <th class=" m-datatable__cell  ">
                                <span style="width: 100px;"> Nhập vào</span>
                                </span></th>
                            <th class=" m-datatable__cell  ">
                                <span style="width: 100px;"> Đã giao</span>
                                </span></th>
                            <th class=" m-datatable__cell  ">
                                <span style="width: 100px;"> Chưa giao</span>
                                </span></th>
                            
                            <th class=" m-datatable__cell ">
                                <span style="width: 100px ;"> Lịch sử đặt hàng</span></th>
                        </tr>
                    </thead>
                    <tbody style="" class="m-datatable__body">
                        @foreach($products as $item)
                        <tr class="m-datatable__row" style="left: 0px;">
                            <td class="m-datatable__cell"> <span style="width: 80px ;">
                                <b>{{ $item->create_date }}</b>
                            </span></td>
                            <td class="m-datatable__cell"> <span style="width: 200px ;">
                                <a href="{{ $item->product_variant->product->urlGuestShow() }}"><h5>{{ $item->product_variant->product->name }}</h5></a>
                                <p>{{ $item->product_variant->title }}</p>
                            </span></td>
                            <td class="m-datatable__cell"> <span style="width: 100px ;">
                                    <b>{{ $item->amount_import }}</b>
                                </span></td>
                            <td class="m-datatable__cell"> <span style="width: 100px ;">
                                    <b>{{ $item->amount_export }}</b>
                                </span></td>
                            <td class="m-datatable__cell"> <span style="width: 100px ;">
                                <b>{{ $item->pendding }}</b>
                                </span>
                            </td>
                            <td class="m-datatable__cell--center m-datatable__cell">
                                <span style="width: 100px ;">
                                    <div class=" ilv-btn-group">
                                        <div data-toggle="tooltip" data-placement="top" style="display: inline-block;" title="Đi đến danh sách đơn hàng của sản phẩm {{ $item->product_variant->getName()  }}">
                                            <a class="btn btn-sm btn-primary" target="_blank" href="{{ route('admin_order.index',['product_id' => $item->product_variant->id]) }}">
                                                <i class="fa fa-eye"></i>
                                            </a>
                                        </div>
                                    </div>
                                </span>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $products->links() }}
            </div>
        </div>
    </div>
</div>
@endsection
