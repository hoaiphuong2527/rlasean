@extends('layouts.admin')
@section('content')
@section('title')
Kho hàng
@endsection
@if (session('status'))
    <div class="alert alert-info">{{session('status')}}</div>
@endif
@if (session('destroy'))
    <div class="alert alert-success">{{session('destroy')}}</div>
@endif
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Danh dách sản phẩm
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                        <a href="{{ route('admin_warehouse.index') }}" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">Lịch sử</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="m-portlet__body">
            <form class="row" method="GET" action="">

                <div class="col-5">
                    <div class="form-group m-form__group row">
                        <label class="col-form-label col-lg-3 col-sm-12">Tên sản phẩm</label>
                        <div class="col-lg-9 col-md-9 col-sm-12">
                            <input class="form-control m-input m-input--square" type="text" name="name" value="{{ $name }}">
                        </div>
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group m-form__group row">
                        <input type="submit" class="btn btn-info" value="Tìm kiếm">
                    </div>
                </div>
            </form>
            <div class="m_datatable m-datatable m-datatable--default m-datatable--loaded" style="">
                <table class="m-datatable__table" style="display: block; min-height: 300px; overflow-x: auto;">
                    <thead class="m-datatable__head">
                        <tr class="m-datatable__row" style="left: 0px;">
                            <th class=" m-datatable__cell  ">
                                <span style="width: 50px;"></span>
                            </th>
                            <th class="m-datatable__cell  ">
                                <span style="width: 200px;"> Tên sản phẩm</span>
                            </th>
                            <th class=" m-datatable__cell  ">
                                <span style="width: 100px;">Số lượng</span>
                            </th>
                            <th class=" m-datatable__cell m--align-center ">
                                <span style="width: 250px;">Nhập thêm</span>                             
                            </th>
                        </tr>
                    </thead>
                    <tbody style="" class="m-datatable__body">
                        @foreach($products as $row)
                        <tr class="m-datatable__row" style="left: 0px;">
                            <td class=" m-datatable__cell">
                                <span style="width: 50px ;">
                                    <img src="{{ $row->linkImage() }}" alt="" style=" width:50px; height:50px">
                                </span>
                            </td>
                            <td class="m-datatable__cell"> <span style="width: 200px ;">
                                <a href="{{ $row->product->urlGuestShow() }}"><h5>{{ $row->product->name }}</h5></a>
                                    <p>{{ $row->title }}</p>
                                </span></td>
                            <td class=" m-datatable__cell"> <span style="width: 100px ;">
                                {{ $row->amount }}
                                </span>
                            </td>
                            <td class="m-datatable__cell">
                                <span style="width: 250px ;">
                                    <form class="m-form m-form--fit m-form--label-align-right d-flex" method="POST" action="{{ route('admin_warehouse.update',['id' => $row->id]) }}">
                                        <input type="hidden" name="_method" value="PUT" /> {{ csrf_field() }}
                                        <div class=" ilv-btn-group text-dark-inner">
                                            <input class="form-control pr-0" type="number" min="0" name="import" value="{{ old('import') }}" style="width:80px">
                                            
                                        </div>
                                        <button type="submit" class="btn btn-sm btn-brand" style="margin-top: 5px; margin-left: 5px;">
                                            LƯU
                                        </button> 
                                    </form>
                                </span>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
