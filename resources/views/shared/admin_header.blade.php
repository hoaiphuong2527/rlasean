<header id="m_header" class="m-grid__item    m-header " m-minimize-offset="200" m-minimize-mobile-offset="200">
    <div class="m-container m-container--fluid m-container--full-height">
        <div class="m-stack m-stack--ver m-stack--desktop">
            <!-- BEGIN: Brand -->
            <div class="m-stack__item m-brand  m-brand--skin-dark ">
                <div class="m-stack m-stack--ver m-stack--general">
                    <div class="m-stack__item m-stack__item--middle m-brand__logo">
                        <a href="{{ route('admin_dashboard.index') }}" class="m-brand__logo-wrapper">
                            <img alt="logo" src="{{ $urlLogo }}" data-src="" class="lazyload">
                        </a>
                    </div>
                    <div class="m-stack__item m-stack__item--middle m-brand__tools">
                        <a href="javascript:;" id="m_aside_left_minimize_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-desktop-inline-block">
                            <span></span>
                        </a>
                        <a href="javascript:;" id="m_aside_left_offcanvas_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block">
                            <span></span>
                        </a>
                        <a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
                            <i class="fa fa-arrows-alt"></i>
                        </a>
                        <!-- BEGIN: Topbar Toggler -->
                    </div>
                </div>
            </div>
            <!-- END: Brand -->
            <div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">
                <!-- BEGIN: Horizontal Menu -->
                <button class="m-aside-header-menu-mobile-close  m-aside-header-menu-mobile-close--skin-dark " id="m_aside_header_menu_mobile_close_btn"><i
                        class="la la-close"></i></button>
                <div id="m_header_menu" class="m-header-menu m-aside-header-menu-mobile m-aside-header-menu-mobile--offcanvas  m-header-menu--skin-light m-header-menu--submenu-skin-light m-aside-header-menu-mobile--skin-dark m-aside-header-menu-mobile--submenu-skin-dark ">
                    <ul class="m-menu__nav  m-menu__nav--submenu-arrow ">
                        <li class="m-menu__item m-menu__item--submenu m-menu__item--rel">
                            <a href="{{ route('guest_home.index.'.app()->getLocale()) }}" target="_blank"
                                class="m-menu__link"><i class=" m-menu__link-icon fa fa-paper-plane-o"></i><span
                                    class="m-menu__link-title"> <span class="m-menu__link-wrap">
                                            <i class="fa fa-tachometer-alt" style="margin-top: 28px;
                                            padding-right: 5px; font-size: medium;
                                    "></i>                                            
                                            <span class="m-menu__link-text">Trang chủ</span>                                  
                                        </a>
                            
                        </li>
                        <li class="m-menu__item m-menu__item--submenu m-menu__item--rel m-menu__item--open-dropdown"
                            m-menu-submenu-toggle="click" m-menu-link-redirect="1" aria-haspopup="true"><a href="javascript:;"
                                class="m-menu__link m-menu__toggle"><i class=" m-menu__link-icon fa fa-paper-plane-o"></i><span
                                    class="m-menu__link-title"> <span class="m-menu__link-wrap">
                                        <span class="m-menu__link-badge"><span class="m-badge m-badge--brand m-badge--wide">new</span></span>
                                    </span></span><i class="m-menu__link-icon fa fa-chevron-down" style="margin-top: 28px;
                                        padding-left: 5px;
                                "></i></a>
                            <div class="m-menu__submenu m-menu__submenu--classic m-menu__submenu--left"><span class="m-menu__arrow m-menu__arrow--adjust"
                                    style="left: 87.5px;"></span>
                                <ul class="m-menu__subnav">
                                    <li class="m-menu__item " m-menu-link-redirect="1" aria-haspopup="true"><a href="{{ route('admin_product.index') }}"
                                            class="m-menu__link "><i class="m-menu__link-icon fa fa-box-open"></i><span
                                                class="m-menu__link-text">Sản phẩm</span></a></li>
                                    <li class="m-menu__item " m-menu-link-redirect="1" aria-haspopup="true"><a href="{{ route('admin_post.index') }}"
                                                    class="m-menu__link "><i class="m-menu__link-icon fa fa-feather-alt"></i><span
                                                        class="m-menu__link-text">Bài viết</span></a></li>
                                    <li class="m-menu__item " m-menu-link-redirect="1" aria-haspopup="true"><a href="{{ route('admin_page.index') }}"
                                                    class="m-menu__link "><i class="m-menu__link-icon fa fa-window-maximize "></i><span
                                                        class="m-menu__link-text">Trang tĩnh</span></a></li>
                                </ul>
                            </div>
                        </li>
                        @if(isset($item))
                        <li class="m-menu__item m-menu__item--submenu m-menu__item--rel">
                            @if($paramater = Request::segment(2) == 'static-page')
                            <a href="{{ route('guest.view_static_page',['slug' => $item->slug]) }}" target="_blank"
                                    class="m-menu__link"><i class=" m-menu__link-icon fa fa-paper-plane-o"></i><span
                                        class="m-menu__link-title"> <span class="m-menu__link-wrap">
                                                <i class="fa fa-share" style="margin-top: 28px;
                                                padding-right: 5px; font-size: medium;
                                        "></i>                                            
                                                <span class="m-menu__link-text">Xem {{ substr(strip_tags($item->name), 0, 50) }} ...</span>                                  
                                            </a>
                            @elseif($item->name && !(isset($item->email)))
                            <a href="{{ $item->urlGuestShow() }}" target="_blank"
                                class="m-menu__link"><i class=" m-menu__link-icon fa fa-paper-plane-o"></i><span
                                    class="m-menu__link-title"> <span class="m-menu__link-wrap">
                                            <i class="fa fa-share" style="margin-top: 28px;
                                            padding-right: 5px; font-size: medium;
                                    "></i>                                            
                                            <span class="m-menu__link-text">Xem {{ substr(strip_tags($item->name), 0, 50) }} ...</span>                                  
                                        </a>
                            @endif
                        </li>
                        @endif
                    </ul>
                </div>
                <div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general m-stack--fluid">
                    <div class="m-stack__item m-topbar__nav-wrapper">
                        <ul class="m-topbar__nav m-nav m-nav--inline">
                            <li class="m-nav__item m-dropdown m-dropdown--large m-dropdown--arrow m-dropdown--align-center m-dropdown--mobile-full-width m-dropdown--skin-light	m-list-search m-list-search--skin-light"
                                m-dropdown-toggle="click" id="m_quicksearch" m-quicksearch-mode="dropdown"
                                m-dropdown-persistent="1">
                                <a href="#" class="m-nav__link m-dropdown__toggle">
                                    <span class="m-nav__link-icon"><i class="fa fa-search"></i></span>
                                </a>
                                <div class="m-dropdown__wrapper" style="z-index: 101;">
                                    <span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
                                    <div class="m-dropdown__inner ">
                                        <div class="m-dropdown__header">
                                            <form class="m-list-search__form" method="GET" action="{{ route('admin_search_product') }}">
                                                <div class="m-list-search__form-wrapper">
                                                    <span class="m-list-search__form-input-wrapper">
                                                        <input id="m_quicksearch_input" autocomplete="off" type="text"
                                                            name="product_name" class="m-list-search__form-input" value=""
                                                            placeholder="Search...">
                                                    </span>
                                                    <span class="m-list-search__form-icon-close" id="m_quicksearch_close">
                                                        <i class="fa fa-times"></i>
                                                    </span>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="m-nav__item m-topbar__user-profile m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light"
                                m-dropdown-toggle="click" aria-expanded="true" style="padding-top: 15px;">
                                <a href="#" class="m-nav__link m-dropdown__toggle">
                                    Hi, {{ Auth::user()->name }}
                                    <img src="{{ Auth::user()->getAvata() }}" width="20px" height="20px" class="img-responsive"
                                        alt="avt">
                                </a>
                                <div class="m-dropdown__wrapper" style="z-index: 101;">
                                    <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"
                                        style="left: auto; right: 4.5px;"></span>
                                    <div class="m-dropdown__inner">
                                        <div class="m-dropdown__header m--align-center">
                                            <a href="{{ route('user_profiles.index',['token' => Auth::user()->middleware_token]) }}">
                                                <h5 class="m-card-user__name m--font-weight-500">{{ Auth::user()->name
                                                    }}</h5>
                                            </a>
                                            <ul class="m-nav m-nav--skin-light">
                                                <li class="m-nav__item">
                                                    <a class="btn m-btn--pill btn-danger m-btn m-btn--custom m-btn--label-brand m-btn--bolder"
                                                        href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                                        {{ __('Đăng xuất') }}
                                                    </a>
                                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                                        style="display: none;">
                                                        @csrf
                                                    </form>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- END: Topbar -->
            </div>
        </div>
    </div>
</header>
