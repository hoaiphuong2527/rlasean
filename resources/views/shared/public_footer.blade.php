<footer>
    <div class="footer-area">
        <div class="container-fluid">
            <div class="row ilv-hn-widget-style">
                <div class="col-xl-6 col-lg-12 ilv-pt-10 ilv-pb-10">
                    <div class="row">
                        <div class="col-12">
                            <h5 class="ilv-hn-project-title-color">{{ $option->sologan }}</h5>
                        </div>
                        <div class="col-12">
                            <p><i class="fa fa-home"></i> @lang('component_text.footer.head_office.label') {{ $option->address }}</p>
                            <!-- <p><i class="fa fa-home"></i> @lang('component_text.footer.showroom.label') @lang('component_text.footer.showroom.content') </p> -->
                            <p><i class="fa fa-user"></i> @lang('component_text.footer.hotline.label')@lang('component_text.footer.hotline.content')</p>
                        </div>
                    </div>
                    <div class="row ilv-pb-10">
                        
                    </div>
                    <div class="row ilv-pt-10">
                        <div class="col-md-6 col-12">
                            <div class="row">
                                <div class="col-12 ilv-pt-10 ilv-pb-10">
                                    <h5 class="ilv-hn-project-title-color">@lang('component_text.footer.linkage')</h5>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2 col-2">
                                    <a href="{{ $option->facebook_acc }}"><img src="{{ asset('images/fb.png') }}" alt="" height="40px" width="auto"></a>
                                </div>
                                <div class="col-md-2 col-2">
                                    <a href="@lang('component_text.footer.twitter_acc')"><img src="{{ asset('images/tw.png') }}" alt="" height="40px" width="auto"></a>
                                </div>
                                <div class="col-md-2 col-2">
                                    <a href="{{ $option->youtube_acc }}"><img src="{{ asset('images/yt.png') }}" alt="" height="40px" width="auto"></a>
                                </div>
                                <div class="col-md-2 col-2">
                                    <a href="@lang('component_text.footer.google_acc')"><img src="{{ asset('images/gg.png') }}" alt="" height="40px" width="auto"></a>
                                </div>
                                <div class="col-md-2 col-2">
                                    <a href="{{ $option->intagram_acc }}"><img src="{{ asset('images/ins.png') }}" alt="" height="40px" width="auto"></a>
                                </div>    
                            </div>
                        </div>
                        <!-- <div class="col-md-6 col-12">
                            <div class="row">
                                <div class="col-12 ilv-pt-10 ilv-pb-10">
                                    <h5 class="ilv-hn-project-title-color">@lang('component_text.footer.payment')</h5>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3">
                                    <img src="{{ asset('images/vs.jpg') }}" alt="" height="40px" width="auto">
                                </div>
                                <div class="col-3">
                                    <img src="{{ asset('images/mt.png') }}" alt="" height="40px" width="auto">
                                </div>
                                <div class="col-3">
                                    <img src="{{ asset('images/vs.jpg') }}" alt="" height="40px" width="auto">
                                </div>
                            </div>
                        </div> -->
                    </div>
                </div>
                <div class="col-xl-6 col-lg-12 ilv-pt-10 ilv-pb-10">
                    <div class="row custom-mb-pd">
                        <div class="col-md-4 col-12">
                            <h5 class="ilv-hn-project-title-color">@lang('component_text.footer.info')</h5>
                            @if($category_info)
                                @foreach($category_info->getOnlinePosts as $item)
                                    <a href="{{ route('guest.view_static_page',['slug' => $item->getAttributeLang('slug') ]) }}"><p>{{ $item->getAttributeLang('name') }}</p></a>
                                @endforeach
                            @endif
                        </div>
                        <div class="col-md-4 col-12">
                            <h5 class="ilv-hn-project-title-color">@lang('component_text.footer.policy')</h5>
                            @if($category_policy)
                                @foreach($category_policy->getOnlinePosts as $item)
                                    <a href="{{ route('guest.view_static_page',['slug' => $item->getAttributeLang('slug') ]) }}"><p>{{ $item->getAttributeLang('name') }}</p></a>
                                @endforeach
                            @endif
                        </div>
                        <div class="col-md-4 col-12">
                            <h5 class="ilv-hn-project-title-color">@lang('component_text.footer.suport')</h5>
                            <p>@lang('component_text.footer.PBX.label') @lang('component_text.footer.PBX.content')</p>
                            <p>@lang('component_text.footer.consult1.label') @lang('component_text.footer.consult1.content')</p>
                            <p>{{ $option->email }}</p>
                        </div>
                    </div>
                    <div class="row ilv-pt-10">
                        <div class="col-md-8 col-12">
                            <h5 class="ilv-hn-project-title-color">@lang('component_text.footer.email.label')</h5>
                            <form action="{{ route('guest_news.store') }}" method="POST">
                                @csrf
                                <div class="row ilv-pt-10 ilv-pb-10">
                                    <div class="col-8 ilv-hn-pr-0">
                                        <input type="email" class="ilv-hn-custom-input-style" placeholder="@lang('component_text.placeholders.register_email')">
                                    </div>
                                    <div class="col-4 ilv-hn-pl-0">
                                        <button type="submit" class="btn  ilv-hn-custom-button-style">@lang('component_text.buttons.register')</button>
                                    </div>
                                </div>
                            </form>
                            <p>@lang('component_text.footer.email.content')</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row ilv-hn-copyright-style">
                <!-- <div class="col-md-8 col-12">
                    <p>@lang('component_text.footer.label1')</p>
                    <p>@lang('component_text.footer.label2')</p>
                    <p>@lang('component_text.labels.phone'): {{ $option->phone }} - @lang('component_text.labels.email'):{{ $option->email }}</p>
                </div>
                <div class="col-md-4 col-12 ilv-hn-text-align-r ilv-hn-text-align-md-c">
                    <img src="{{ $option->getLogo() }}" alt="" height="80px" width="auto" class="ilv-hn-custom-logo-ft">
                </div> -->
                <div class="col-12 ilv-hn-text-align-c">
                    <p><i class="fa fa-copyright"></i> @lang('component_text.footer.copyright') <span style="color:#bc7323;">{{ $option->name }}</span></p>
                </div>
            </div>
        </div>
    </div>

</footer>

<a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
