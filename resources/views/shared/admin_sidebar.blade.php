<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
    <!-- BEGIN: Left Aside -->
    <button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">
        <i class="la la-close"></i>
    </button>
    <div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">
        <!-- BEGIN: Aside Menu -->
        <div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark "
            m-menu-vertical="1" m-menu-scrollable="1" m-menu-dropdown-timeout="500" style="position: relative;">
            <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
                <li class="m-menu__item " aria-haspopup="true">
                    <a href="{{ route('admin_dashboard.index') }}" class="m-menu__link ">
                        <i class="m-menu__link-icon fa fa-chart-line"></i>
                        <span class="m-menu__link-title">
                            <span class="m-menu__link-wrap">
                                <span class="m-menu__link-text ">Tổng quan</span>
                            </span>
                        </span>
                    </a>
                </li>
                
                @can('CAE')
                <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true">
                    <a href="javascript:;" class="m-menu__link m-menu__toggle">
                        <i class="m-menu__link-icon fa fa-box-open"></i>
                        <span class="m-menu__link-text">Sản phẩm</span>
                        <i class="m-menu__ver-arrow fa fa-angle-up"></i>
                    </a>
                    <div class="m-menu__submenu ">
                        <span class="m-menu__arrow"></span>
                        <ul class="m-menu__subnav">
			   <li class="m-menu__item " aria-haspopup="true">
                                <a href="{{ route('admin_category.index',['type' => 'product']) }}" class="m-menu__link ">
                                    <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="m-menu__link-text">Danh mục sản phẩm</span>
                                </a>
                            </li>
                            <li class="m-menu__item " aria-haspopup="true">
                                <a href="{{ route('admin_product.index') }}" class="m-menu__link ">
                                    <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                        <span></span>
                                </i>
                                    <span class="m-menu__link-text">Tất cả sản phẩm</span>
                                </a>
                            </li>
                            <!-- <li class="m-menu__item " aria-haspopup="true">
                                <a href="{{ route('admin_information.index') }}" class="m-menu__link ">
                                    <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="m-menu__link-text">Thông số sản phẩm</span>
                                </a>
                            </li> -->
                            <li class="m-menu__item " aria-haspopup="true">
                                <a href="{{ route('admin_history.index') }}" class="m-menu__link ">
                                    <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="m-menu__link-text">Tồn kho</span>
                                </a>
                            </li>
                            <li class="m-menu__item " aria-haspopup="true">
                                <a href="{{ route('admin_code.index') }}" class="m-menu__link ">
                                    <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="m-menu__link-text">Khuyến mãi</span>
                                </a>
                            </li>
                            <li class="m-menu__item " aria-haspopup="true">
                                <a href="{{ route('admin_reviews.index') }}" class="m-menu__link ">
                                    <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="m-menu__link-text">Review Sản Phẩm</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="m-menu__item" aria-haspopup="true">
                    <a href="{{ route('admin_warranties.index') }}" class="m-menu__link ">
                        <i class="m-menu__link-icon fa fa-wrench"></i>
                        <span class="m-menu__link-title">
                            <span class="m-menu__link-wrap">
                                <span class="m-menu__link-text">Liên hệ Bảo Hành</span>
                            </span>
                        </span>
                    </a>
                </li>
                @endcan
                @can('controller_admin')
                <li class="m-menu__item" aria-haspopup="true">
                    <a href="{{ route('admin_order.index') }}" class="m-menu__link ">
                        <i class="m-menu__link-icon fa fa-clipboard-list"></i>
                        <span class="m-menu__link-title">
                            <span class="m-menu__link-wrap">
                                <span class="m-menu__link-text">Đơn hàng</span>
                            </span>
                        </span>
                    </a>
                </li>
                @endcan
                <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true">
                    <a href="javascript:;" class="m-menu__link m-menu__toggle">
                        <i class="m-menu__link-icon fa fa-window-maximize"></i>
                        <span class="m-menu__link-text">Trang tĩnh</span>
                        <i class="m-menu__ver-arrow fa fa-angle-up"></i>
                    </a>
                    <div class="m-menu__submenu ">
                        <span class="m-menu__arrow"></span>
                        <ul class="m-menu__subnav">
                            <li class="m-menu__item " aria-haspopup="true">
                                <a href="{{ route('admin_category.index',['type' => 'page']) }}" class="m-menu__link ">
                                    <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="m-menu__link-text">Danh mục trang tĩnh</span>
                                </a>
                            </li>
                            <li class="m-menu__item " aria-haspopup="true">
                                <a href="{{ route('admin_page.index') }}" class="m-menu__link ">
                                    <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="m-menu__link-text">Tất cả trang tĩnh</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true">
                    <a href="javascript:;" class="m-menu__link m-menu__toggle">
                        <i class="m-menu__link-icon fa fa-feather-alt"></i>
                        <span class="m-menu__link-text">Bài viết</span>
                        <i class="m-menu__ver-arrow fa fa-angle-up"></i>
                    </a>
                    <div class="m-menu__submenu ">
                        <span class="m-menu__arrow"></span>
                        <ul class="m-menu__subnav">
                            <li class="m-menu__item " aria-haspopup="true">
                                <a href="{{ route('admin_category.index',['type' => 'post']) }}" class="m-menu__link ">
                                    <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="m-menu__link-text">Danh mục bài viết</span>
                                </a>
                            </li>
                            <li class="m-menu__item " aria-haspopup="true">
                                <a href="{{ route('admin_post.index') }}" class="m-menu__link ">
                                    <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="m-menu__link-text">Tất cả bài viết</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="m-menu__item " aria-haspopup="true">
                        <a href="{{ route('admin_media_index') }}" class="m-menu__link ">
                            <i class="m-menu__link-icon fa fa-file-image"></i>
                            <span class="m-menu__link-title">
                                <span class="m-menu__link-wrap">
                                    <span class="m-menu__link-text ">Thư viện ảnh</span>
                                </span>
                            </span>
                        </a>
                </li>
                @can('controller')
                <li class="m-menu__item" aria-haspopup="true">
                    <a href="{{ route('admin_user.index') }}" class="m-menu__link ">
                        <i class="m-menu__link-icon fa fa-user-circle"></i>
                        <span class="m-menu__link-title">
                            <span class="m-menu__link-wrap">
                                <span class="m-menu__link-text">Người dùng</span>
                            </span>

                        </span>
                    </a>
                </li>
                @endcan
                <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true">
                    <a href="javascript:;" class="m-menu__link m-menu__toggle">
                        <i class="m-menu__link-icon fas fa-cogs"></i>
                        <span class="m-menu__link-text">Cài đặt</span>
                        <i class="m-menu__ver-arrow fa fa-angle-up"></i>
                    </a>
                    <div class="m-menu__submenu ">
                        <span class="m-menu__arrow"></span>
                        <ul class="m-menu__subnav">
                            <li class="m-menu__item " aria-haspopup="true">
                                <a href="{{ route('admin_slideshow.index') }}" class="m-menu__link ">
                                    <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="m-menu__link-text">Slideshow</span>
                                </a>
                            </li>
                            @can('CAE')
                            <li class="m-menu__item " aria-haspopup="true">
                                <a href="{{ route('admin_contacts.index') }}" class="m-menu__link ">
                                    <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="m-menu__link-text">Liên hệ</span>
                                </a>
                            </li>
                            @endcan
                            <li class="m-menu__item " aria-haspopup="true">
                                <a href="{{ route('admin_banners.index') }}" class="m-menu__link ">
                                    <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="m-menu__link-text">Banner</span>
                                </a>
                            </li>
                            <li class="m-menu__item " aria-haspopup="true">
                                <a href="{{ route('admin_partners.index') }}" class="m-menu__link ">
                                    <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="m-menu__link-text">Đối tác</span>
                                </a>
                            </li>
                            <li class="m-menu__item " aria-haspopup="true">
                                <a href="{{ route('admin_news.index') }}" class="m-menu__link ">
                                    <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="m-menu__link-text">DSDK nhận tin</span>
                                </a>
                            </li>
                            <li class="m-menu__item " aria-haspopup="true">
                                <a href="{{ route('admin_menu.index') }}" class="m-menu__link ">
                                    <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="m-menu__link-text">Menu</span>
                                </a>
                            </li>
                            <!-- <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true">
                                <a href="javascript:;" class="m-menu__link m-menu__toggle">
                                    <span class="m-menu__link-text">Cập nhật ngôn ngữ</span>
                                    <i class="m-menu__ver-arrow fa fa-angle-up"></i>
                                </a>
                                <div class="m-menu__submenu ">
                                    <span class="m-menu__arrow"></span>
                                    <ul class="m-menu__subnav">
                                        <li class="m-menu__item " aria-haspopup="true">
                                            <a href="{{ route('admin_lang_index',['file_name' => 'component_text','locale' => 'vi']) }}" class="m-menu__link ">
                                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="m-menu__link-text">Component file</span>
                                            </a>
                                        </li>
                                        <li class="m-menu__item " aria-haspopup="true">
                                            <a href="{{ route('admin_lang_index',['file_name' => 'page_text','locale' => 'vi']) }}" class="m-menu__link ">
                                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="m-menu__link-text">Page</span>
                                            </a>
                                        </li>
                                        <li class="m-menu__item " aria-haspopup="true">
                                            <a href="{{ route('admin_lang_index',['file_name' => 'route_text','locale' => 'vi']) }}" class="m-menu__link ">
                                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="m-menu__link-text">Route</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li> -->
                            @can('controller_admin')
                            <li class="m-menu__item " aria-haspopup="true">
                                <a href="{{ route('admin_option.index') }}" class="m-menu__link ">
                                    <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                        <span></span>
                                    </i>
                                    <span class="m-menu__link-text">Cập nhật thông tin</span>
                                </a>
                            </li>
                            @endcan
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
        <!-- END: Aside Menu -->
    </div>
    <!-- END: Left Aside -->
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-content">
                {!! Breadcrumbs::render()!!}
                @yield('content')

        </div>
    </div>
</div>
