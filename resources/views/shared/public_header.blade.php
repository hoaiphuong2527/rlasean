<header class="section-header">
    <section class="header-main">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <a href="/" class="brand-wrap">
                        <img src="{{ $option->getLogo() }}" alt="" height="80px" width="auto" class="ilv-hn-custom-logo-ft">
                    </a>
                </div>
                <div class="col-lg-6 col-sm-6">
                    <form action="{{ route('guest_search.index.'. app()->getLocale()) }}" class="search-wrap"
                        method="GET">
                        <div class="input-group py-lg-4">
                            <input type="text" class="form-control" name="product_name"
                                placeholder="Nhập tên sản phẩm..." />
                            <div class="input-group-append">
                                <button class="btn btn-primary" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="widgets-wrap d-flex justify-content-end py-lg-4">
                        @if(!Auth::check())
                        <div class="widget-header ilv-widget-header dropdown">
                            <a href="#" class="button-animate" data-toggle="dropdown" data-offset="20,10">
                                <div class="text-wrap">
                                    <span>Login </span>
                                </div>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <form class="px-4 py-3"  action="{{ route('login') }}" method="post">
                                        @csrf
                                    <div class="form-group">
                                        <label>Email address</label>
                                        <input type="email" name="email"
                                            class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                            placeholder="email@example.com" name="email" value="{{ old('email') }}"
                                            id="email">
                                        @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label>Password</label>
                                        @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                        @endif
                                        <input type="password" name="password" id="password"
                                            class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                            placeholder="Password">
                                    </div>
                                    <button type="submit" class="btn btn-primary">Sign in</button>
                                </form>
                                <hr class="dropdown-divider">
                                <a class="dropdown-item" href="#">Have account? Sign up</a>
                                <a class="dropdown-item" href="#">Forgot password?</a>
                            </div>
                        </div>
                        @else
                        <div class="dropdown">
                            <button class="btn btn-outline-primary dropdown-toggle icontext button-animate d-flex"
                                type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">
                                Hi! {{ Auth::user()->name }}
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item"
                                    href="{{ route('user_profiles.index',['token' => Auth::user()->middleware_token]) }}">Your
                                    account</a>
                                @if(Auth::user()->is_admin)
                                <a class="dropdown-item" href="{{ route('admin_dashboard.index') }}">Dashboard</a>
                                @endif
                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                    style="display: none;">
                                    @csrf
                                </form>
                                <a class="dropdown-item" style="cursor:pointer"
                                    onclick="event.preventDefault();document.getElementById('logout-form').submit();">Log
                                    out</a>
                                <a class="dropdown-item" 
                                    href="{{ route('user_order.index' ,['token' => Auth::user()->middleware_token ]) }}"  >Manage my orders
                                    </a>
                            </div>
                        </div>
                        @endif
                        <div class="widget-header ilv-widget-header">
                            <a href="/dat-hang" class="icontext button-animate btn btn-outline-primary d-flex">
                                <i class="fa fa-shopping-cart mx-1"></i>
                                <div id="react-btn"></div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <nav class="navbar navbar-expand-lg navbar-dark ilv-header">
        <div class="container">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main_nav"
                aria-controls="main_nav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="main_nav">
                <ul class="navbar-nav ilv-navbar-collapse">
                    @foreach($menu as $row)
                    <li class="nav-item">
                        <a class="nav-link pl-0 "
                            href="{{ $row->urlGuestShow() }}">{{ $row->getAttributeLang('display_name') }}</a>
                        @if($row->children()->count())
                        <div class="dropdown-menu ">
                            @foreach($row->children as $m)
                            @include('components.dropdown_item', ['link' => $m->urlGuestShow(), 'text' =>
                            $m->getAttributeLang('display_name')])
                            @if($row->children()->count())
                            @include('components.dropdown_item_child',['list' => $row->children])
                            @endif
                            @endforeach
                        </div>
                        @endif
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </nav>

</header>
