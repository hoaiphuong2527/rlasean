@extends('layouts.admin') @section('content')
@if(Session::has('success'))
<div class="alert alert-success">
    {{ Session::get('success') }}
</div>
@endif

<div class="m-portlet">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    Tạo chương trình khuyến mãi
            </div>
        </div>
    </div>
    <div class="m-portlet__body">
        <form method="POST" action="{{ $code->urlAdminUpdate() }}"
                enctype="multipart/form-data">
            <input type="hidden" name="_method" value="PUT" /> {{ csrf_field() }}
            <div id="admin-react-create-promotion"></div>
            <div class="m-portlet__foot m-portlet__foot--fit">
                    <div class="m-form__actions mt-4">
                        <input type="submit" class="btn btn-primary" value="Tạo khuyến mãi">
                    </div>
                </div>
        </form>
    </div>
</div>

{{-- <!-- <div class="m-portlet">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    Chỉnh sửa thông tin
            </div>
        </div>
    </div>
    <form class="m-form m-form--fit m-form--label-align-right" id="form-edit" method="POST" action="{{ $code->urlAdminUpdate() }}"
        enctype="multipart/form-data">
        <input type="hidden" name="_method" value="PUT" /> {{ csrf_field() }}
        <div class="m-portlet__body">
            <div class="form-group m-form__group row">
                <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                    <div class="form-group m-form__group">
                        @if ($errors->has('promotion'))
                        <span class="invalid-feedback" style="display:block;">
                            <strong>{{ $errors->first('promotion') }}</strong>
                        </span>
                        @endif
                        <label for="col-form-label col-lg-3 col-sm-12">Tên Mã Giảm</label>
                        <input name="promotion" type="text" class="form-control m-input" value="{{ old('promotion',$code->promotion) }}">
                        <span class="invalid-feedback" style="display:block;">
                            <strong></strong>
                        </span>
                    </div>
                </div>
                <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                    <div class="form-group m-form__group">
                        <label for="col-form-label col-lg-3 col-sm-12">Loại Giảm Giá</label><br>
                        @foreach(config('ecommerce.reduction_type_display') as $key => $show)
                        <label for="">{{ $show }}</label>
                        <input class="type-radio" type="radio" name="reduction_type" value="{{ $key }}" @if($code->reduction_type
                        == $key ) checked
                        @endif
                        />
                        @endforeach
                    </div>
                </div>

                <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                    <div class="form-group m-form__group">
                        @if ($errors->has('promotion_price'))
                        <span class="invalid-feedback" style="display:block;">
                            <strong>{{ $errors->first('promotion_price') }}</strong>
                        </span>
                        @endif
                        <label for="col-form-label col-lg-3 col-sm-12">Số Tiền Giảm</label>
                        <input name="promotion_price" type="number" max='100' class="form-control m-input" value="{{ old('promotion_price',$code->promotion_price) }}">
                        <span class="invalid-feedback" style="display:block;">
                            <strong></strong>
                        </span>
                    </div>
                </div>


                <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                    <div class="form-group m-form__group">
                        @if ($errors->has('amount'))
                        <span class="invalid-feedback" style="display:block;">
                            <strong>{{ $errors->first('amount') }}</strong>
                        </span>
                        @endif
                        <label for="col-form-label col-lg-3 col-sm-12">Số Lượng</label>
                        <input name="amount" type="number" min=1 class="form-control m-input" value="{{ old('amount',$code->amount) }}">
                        <span class="invalid-feedback" style="display:block;">
                            <strong></strong>
                        </span>
                    </div>
                </div>
                <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                    <div class="form-group m-form__group">
                        <div class="row">
                            <div class="col-6">
                                <label for="col-form-label col-lg-3 col-sm-12">Ngày Bắt Đầu</label>
                                <input name="begin_day" type="date" class="form-control m-input" value="{{ old('begin_day',$code->begin_day) }}">
                                <span class="invalid-feedback" style="display:block;">
                                    <strong></strong>
                                </span>
                            </div>
                            <div class="col-6">
                                <label for="col-form-label col-lg-3 col-sm-12">Ngày Kết Thúc</label>
                                <input name="end_day" type="date" class="form-control m-input" value="{{ old('end_day',$code->end_day) }}">
                                <span class="invalid-feedback" style="display:block;">
                                    <strong></strong>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                    <div class="form-group m-form__group">

                        <span class="invalid-feedback" style="display:block;">
                            <strong></strong>
                        </span>
                    </div>
                </div>
                <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                    <div class="form-group m-form__group">
                        <label for="col-form-label col-lg-3 col-sm-12">Áp dụng cho</label><br>
                        @foreach(config('ecommerce.promotion_type_display') as $key => $show)
                        <label for="">{{ $show }}</label>
                        <input type="radio" name="promotion_type" id="apply_{{ $key }}" value="{{ $key }}" @if($code->promotion_type
                        == $key)
                        checked
                        @endif
                        >
                        @endforeach
                        <a class="hidden" id="fake-btn" data-toggle="modal" data-target="#modalCode"></a>
                    </div>
                </div>

            </div>

            <div class="m-portlet__foot m-portlet__foot--fit">
                <div class="m-form__actions text-center">
                    <input type="submit" class="btn btn-primary" value="Lưu">
                </div>
            </div>
        </div>

    </form>
</div> --> --}}
@endsection
