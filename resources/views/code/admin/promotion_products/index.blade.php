@section('title') Sản phẩm khuyến mãi @endsection @extends('layouts.admin') @section('content')
<div class="row">
    <div class="col-lg-6">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Sản Phẩm Khuyến Mãi
                        </h3>
                    </div>
                </div>
            </div>
            <form class="m-form m-form--fit m-form--label-align-right" method="POST" action="{{ route('admin_productcode.store') }}"
                enctype="multipart/form-data">
                 {{ csrf_field() }}
                <div class="m-portlet__body">
                    <div class="form-group m-form__group row">
                        <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                            <div class="form-group m-form__group">
                                <label for="">Thêm Sản Phẩm</label>

                                <select multiple="multiple" class="form-control" name="product_id[]" id="product_id">
                                    @foreach($product as $row)
                                    <option value="{{ $row->id }}" >{{ $row->name }}</option>
                                    @endforeach
                                </select>
                                <label for="">Code áp dụng</label>
                                <select class="form-control" name="code_id">
                                    @foreach($code as $row)
                                    <option value="{{ $row->id }}">{{ $row->promotion }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__foot m-portlet__foot--fit">
                    <div class="m-form__actions">
                        <input type="submit" class="btn btn-primary" value="Lưu">
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="col-md-6">


        <div class="m-portlet m-portlet--tab">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon m--hide">
                            <i class="la la-gear"></i>
                        </span>
                        <h3 class="m-portlet__head-text">
                            Danh sách Sản phẩm có khuyến mãi
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <!--begin: Datatable -->
                <div class="m_datatable m-datatable m-datatable--default m-datatable--loaded" style="">
                    <table class="m-datatable__table" style="display: block; min-height: 300px; overflow-x: auto;">
                        <thead class="m-datatable__head">
                            <tr class="m-datatable__row" style="left: 0px;">
                                <th class="m-datatable__cell--center m-datatable__cell "> <span style="width: 20px ;">#</span></th>
                                <th class="m-datatable__cell--center m-datatable__cell "> <span style="width: 100px ;">Mã Khuyến Mãi</span></th>
                                <th class="m-datatable__cell--center m-datatable__cell "> <span style="width: 180px ;">Tên Sản Phẩm</span></th>
                                <th class="m-datatable__cell--center m-datatable__cell "> <span style="width: 100px ;">Hành
                                        động</span></th>
                            </tr>
                        </thead>
                        <tbody style="" class="m-datatable__body">
                            <?php $index = 1; ?>
                            @foreach($productcode as $row)
                            <tr class="m-datatable__row" style="left: 0px;">
                                <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 20px ;"></span>{{ $index++ }}</td>
                                <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 100px ;">
                                    {{ $row->code['promotion'] }}
                                    </span></td>
                                <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 180px ;">
                                    {{ $row->product['name'] }}
                                    </span></td>
                            @endforeach
                            <?php $index++ ;?>

                        </tbody>
                    </table>
                    {{ $productcode->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('#product_id').select2()
    });
</script>
@endsection
