@section('title') Danh sách mã giảm giá @endsection @extends('layouts.admin') @section('content')
@if(Session::has('success'))
<div class="alert aler-success">
    {{ Session::get('success') }}
</div>
@endif
<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    Danh sách mã giảm giá
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">

            <ul class="m-portlet__nav">

                <li class="m-portlet__nav-item">
                    <form method="post" action="{{ route('admin_code.store') }}">
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air"
                            value="Thêm mới" />
                    </form>
                </li>

            </ul>
        </div>

    </div>
    <div class="m-portlet__body">
        <form class="row" method="GET" action="">
            <div class="col-5">
                <div class="form-group m-form__group row">
                    <label class="col-form-label col-lg-3 col-sm-12">Nhập mã giảm giá</label>
                    <div class="col-lg-9 col-md-9 col-sm-12">
                        <input class="form-control m-input m-input--square" type="text" name="promotion" value="{{ $promotion }}">
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="form-group m-form__group row">
                    <label class="col-form-label col-lg-3 col-sm-12">Loại</label>
                    <div class="col-lg-9 col-md-9 col-sm-12">
                        <select class="form-control" id="m_select2_2_modal" name="reduction_type">
                            @foreach(config('ecommerce.fillter_code_display') as $key => $type)
                            <option value="{{ $key }}" @if($key==$reduction_type) selected @endif>{{ $type }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-3">
                <div class="form-group m-form__group row">
                    <input type="submit" class="btn btn-info" value="Tìm kiếm">
                </div>
            </div>
        </form>
        <hr>
        <!--begin: Datatable -->
        <div class="m_datatable m-datatable m-datatable--default m-datatable--loaded">
            <table class="m-datatable__table" style="display: block; min-height: 300px; overflow-x: auto;">
                <thead class="m-datatable__head">
                    <tr class="m-datatable__row" style="left: 0px;">
                        <th class="m-datatable__cell--center m-datatable__cell ">
                            <span style="width: 55px;">TT</span>
                            </span></th>
                        <th class="m-datatable__cell--center m-datatable__cell  ">
                            <span style="width: 90px;">Mã Giảm Giá</span></th>
                        <th class="m-datatable__cell--center m-datatable__cell  ">
                            <span style="width: 100px;">Loại Mã Giảm</span></th>
                        <th class="m-datatable__cell--center m-datatable__cell  ">
                            <span style="width: 100px;">Áp Dụng Cho</span></th>
                        <th class="m-datatable__cell--center m-datatable__cell  ">
                            <span style="width: 80px;">Số Lượng</span></th>
                        <th class="m-datatable__cell--center m-datatable__cell  ">
                            <span style="width: 80px;">Số Tiền Giảm </span></th>
                        <th class="m-datatable__cell--center m-datatable__cell  ">
                            <span style="width: 100px;">Ngày Bắt Đầu</span></th>
                        <th class="m-datatable__cell--center m-datatable__cell  ">
                            <span style="width: 100px;">Ngày Kết Thúc</span></th>
                        <th class="m-datatable__cell--center m-datatable__cell  ">
                            <span style="width: 80px;">Hành Động</span></th>

                    </tr>
                </thead>
                <tbody style="" class="m-datatable__body">
                    @foreach($code as $row)
                    <tr class="m-datatable__row" style="left: 0px;">
                        <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 55px ;">
                            <div data-toggle="tooltip" data-placement="top" style="display: inline-block;" title="Đóng/mở trạng thái">
                            <div class="checkbox">
                                <label class="m-switch m-switch--outline m-switch--primary">
                                    <input data-id="{{ $row->id }}" class="chkStatusPromotion" type="checkbox"
                                        @if($row->status == 'R') checked @endif>
                                    <span></span>
                                </label>
                            </div>
                        </div>
                        </span></td>
                        <td class="m-datatable__cell--center m-datatable__cell"><span style="width: 90px;">
                                {{ $row->promotion }}
                            </span></td>
                        <td class="m-datatable__cell--center m-datatable__cell"><span style="width: 100px;">
                                {{ $row->getPromotionType() }}
                            </span></td>
                        <td class="m-datatable__cell--center m-datatable__cell"><span style="width: 100px;">
                                {{ $row->getPromotionApply() }}
                            </span></td>
                        <td class="m-datatable__cell--center m-datatable__cell"><span style="width: 80px;">
                                {{$row->amount_ordered }} / {{ $row->amount }}
                            </span></td>
                        <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 80px;">
                                {{ $row->getPromotionPrice() }}
                            </span></td>
                        <td class="m-datatable__cell--center m-datatable__cell"><span style="width: 100px;">
                                {{ $row->getBeginDay() }}
                            </span></td>
                        <td class="m-datatable__cell--center m-datatable__cell"><span style="width: 100px;">
                                {{ $row->getEndDay() }}
                            </span></td>
                        <td class="m-datatable__cell--center m-datatable__cell">
                            <span style="width: 80px;">
                                <div class="ilv-btn-group" style="    margin-left: 30%;">
                                    @include('code.admin.component.btn_delete_code')
                                </div>
                            </span>
                        </td>
                    </tr>
                    @endforeach()
                </tbody>
            </table>
            {{ $code->links() }}
        </div>
    </div>
</div>
@endsection
