@section('title') Liên hệ @endsection @extends('layouts.admin') @section('content')
@if(Session::has('status'))
<div class="alert alert-info">
    {{ Session::get('status') }}
</div>
@endif
@if(Session::has('destroy'))
<div class="alert alert-success">
    {{ Session::get('destroy') }}
</div>
@endif
<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    Danh sách liên hệ
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    @include('contacts.admin.confirm_del_multi')
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <div class="m-portlet__head">
            <div class="m-portlet__head-tools">
                <ul class="nav nav-tabs m-tabs m-tabs-line  reload-page m-tabs-line--right m-tabs-line-danger" role="tablist">
                    <li class="nav-row m-tabs__item">
                        <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_portlet_tab_1_1" role="tab"
                            aria-selected="false">
                            Liên hệ thông thường
                        </a>
                    </li>
                    <li class="nav-row m-tabs__item">
                        <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_portlet_tab_1_2" role="tab"
                            aria-selected="false">
                            Liên hệ quà tặng
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="tab-content">
            <div class="tab-pane active" id="m_portlet_tab_1_1">
                <div class="m_datatable m-datatable m-datatable--default m-datatable--loaded" style="">
                    <table class="m-datatable__table" style="display: block; min-height: 300px; overflow-x: auto;">
                        <thead class="m-datatable__head">
                            <tr class="m-datatable__row" style="left: 0px;">
                                <th class="m-datatable__cell--center m-datatable__cell "> <span style="width: 100px  ;">#</span></th>
                                <th class="m-datatable__cell--center m-datatable__cell "> <span style="width: 100px  ;">Người
                                        gửi</span></th>
                                <th class="m-datatable__cell--center m-datatable__cell "> <span style="width: 200px  ;">Email</span></th>
                                <th class="m-datatable__cell--center m-datatable__cell "> <span style="width: 100px  ;">Điện
                                        thoại</span></th>
                                <th class="m-datatable__cell--center m-datatable__cell "> <span style="width: 50px  ;">Trạng
                                        thái</span></th>
                                <th class="m-datatable__cell--center m-datatable__cell "> <span style="width: 150px  ;">Hành
                                        động</span></th>
                            </tr>
                        </thead>
                        <tbody style="" class="m-datatable__body">
                            <?php $index = 1; ?>
                            @foreach($contacts as $row)
                            <tr class="m-datatable__row" style="left: 0px;">
                                <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 100px  ;">{{
                                        $index }}</span></td>
                                <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 100px ;">
                                        {{ $row->name }}
                                    </span></td>
                                <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 200px ;">
                                        {{ $row->email }}
                                    </span></td>
                                <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 100px ;">
                                        {{ $row->phone }}
                                    </span></td>
                                <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 50px ;">
                                        {{ $row->getStatus() }}
                                    </span></td>
                                <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 150px ;">
                                        <div class=" ilv-btn-group">
                                            <div data-toggle="tooltip" data-placement="top" style="display: inline-block;" title="Xem chi tiết tin nhắn">
                                                <a class="btn btn-sm btn-info" href="{{ $row->urlAdminEdit() }}">
                                                    <i class="fa fa-reply"></i>
                                                </a>
                                            </div>
                                            @include('contacts.admin.btn_delete')
                                        </div>
                                    </span></td>
                            </tr>
                            <?php $index++ ?>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $contacts->links() }}
                </div>
            </div>
            <div class="tab-pane" id="m_portlet_tab_1_2">
                    <div class="m_datatable m-datatable m-datatable--default m-datatable--loaded" style="">
                        <table class="m-datatable__table" style="display: block; min-height: 300px; overflow-x: auto;">
                            <thead class="m-datatable__head">
                                <tr class="m-datatable__row" style="left: 0px;">
                                    <th class="m-datatable__cell--center m-datatable__cell "> <span style="width: 100px  ;">#</span></th>
                                    <th class="m-datatable__cell--center m-datatable__cell "> <span style="width: 100px  ;">Người
                                            gửi</span></th>
                                    <th class="m-datatable__cell--center m-datatable__cell "> <span style="width: 200px  ;">Email</span></th>
                                    <th class="m-datatable__cell--center m-datatable__cell "> <span style="width: 100px  ;">Điện
                                            thoại</span></th>
                                    <th class="m-datatable__cell--center m-datatable__cell "> <span style="width: 50px  ;">Trạng
                                            thái</span></th>
                                    <th class="m-datatable__cell--center m-datatable__cell "> <span style="width: 150px  ;">Hành
                                            động</span></th>
                                </tr>
                            </thead>
                            <tbody style="" class="m-datatable__body">
                                <?php $index = 1; ?>
                                @foreach($gift_contacts as $row)
                                <tr class="m-datatable__row" style="left: 0px;">
                                    <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 100px  ;">{{
                                            $index }}</span></td>
                                    <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 100px ;">
                                            {{ $row->name }}
                                        </span></td>
                                    <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 200px ;">
                                            {{ $row->email }}
                                        </span></td>
                                    <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 100px ;">
                                            {{ $row->phone }}
                                        </span></td>
                                    <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 50px ;">
                                            {{ $row->getStatus() }}
                                        </span></td>
                                    <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 150px ;">
                                            <div class=" ilv-btn-group">
                                                <a class="btn btn-sm btn-info" href="{{ $row->urlAdminEdit() }}">
                                                    <i class="fa fa-reply"></i>
                                                </a>
                                                @include('contacts.admin.btn_delete')
                                            </div>
                                        </span></td>
                                </tr>
                                <?php $index++ ?>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $gift_contacts->links() }}
                    </div>
                </div>
        </div>
        <!--begin: Datatable -->
    </div>
</div>

@endsection
