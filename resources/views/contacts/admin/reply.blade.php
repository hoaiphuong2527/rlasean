@section('title')
Phản hồi thông tin
@endsection
@extends('layouts.admin') 
@section('content')
<div class="m-portlet">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    Phản hồi liên hệ
                </h3>
            </div>
        </div>
    </div>
    <form class="m-form m-form--fit m-form--label-align-right" method="POST" action="{!! $contact->urlAdminUpdate() !!}" enctype="multipart/form-data">
            <input type="hidden" name="_method" value="PUT" /> {{ csrf_field() }}
        <div class="m-portlet__body">
            <div class="form-group m-form__group row">
                <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                    <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                            <h4 class="m-portlet__head-text">Thông tin người nhận:</h4>
                    </div>
                </div>
                <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                    <div class="form-group m-form__group">
                        <label for="col-form-label col-lg-3 col-sm-12">Tên người nhận</label>
                        <input name="full_name" type="text" class="form-control m-input" value="{{ $contact->name }}" disabled>
                    </div>
                </div>
                <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                    <div class="form-group m-form__group">
                        <label for="col-form-label col-lg-3 col-sm-12">Email người nhận</label>
                        <input name="email" type="text" class="form-control m-input" value="{{ $contact->email }}" disabled>
                    </div>
                </div>
                <hr>
                @if($contact->message)
                <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                    <div class="form-group m-form__group">
                        <label for="col-form-label col-lg-3 col-sm-12">Nội dung:</label>
                        <h5>{{ $contact->message }}</h5>
                    </div>
                </div>
                @endif
                <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                    <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                            <h4 class="m-portlet__head-text">Phản hồi:</h4>
                    </div>
                </div>
                <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                    <div class="form-group m-form__group">
                        <label for="col-form-label col-lg-3 col-sm-12">Nội dung phản hồi</label>
                        <textarea id="myPost" class="form-control" rows="20" cols="30" name="reply_content">{{old('reply_content',$contact->reply_content)}}</textarea>
                        @if ($errors->has('reply_content'))
                            <span class="invalid-feedback" style="display:block;">
                                <strong>{{ $errors->first('reply_content') }}</strong>
                            </span> 
                        @endif
                    </div>
                </div>
            </div>
            <div class="m-portlet__foot m-portlet__foot--fit">
                <div class="m-form__actions text-center">
                    <input type="submit" class="btn btn-primary" value="Gửi">
                </div>
            </div>
        </div>
    </form>
</div>
@endsection