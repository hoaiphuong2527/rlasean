<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#del">
    Xóa tin đã xem
</button>
<form method="post" action="{{ route('admin_contacts.deleteMulti') }}">
    {!! csrf_field() !!}
    <div class="modal fade" id="del" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content  -->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Cảnh báo</h4>
                    <label data-dismiss="modal" style="cursor: pointer;"><i class="fa fa-times"></i></label>
                </div>
                <div class="modal-body">
                    <p>Các liên hệ có trạng thái <b>"Đã xem"</b>, <b>"Đã phản hồi" </b>sẽ bị xóa vĩnh viễn. Bạn có chắc
                        là muốn như vậy?</p>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-outline-danger active">Đồng ý</button>
                    <button type="button" class="btn btn-outline-brand active" data-dismiss="modal">Không đồng ý</button>
                </div>
            </div>
        </div>
    </div>
</form>
