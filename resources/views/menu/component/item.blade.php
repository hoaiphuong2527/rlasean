<div class="m-accordion__item" style="margin-bottom: 15px" id="{{ $row->id }}">
    <div class="m-accordion__item-head collapsed" role="tab" id="m_accordion_8_item_{{ $row->id }}_body" data-toggle="collapse"
        href="#m_accordion_8_item_{{ $row->id }}_body" aria-expanded="false">
        <span class="m-accordion__item-title">{{ $row->display_name }}</span>
        <span><i class="fa fa-angle-down"></i></span>
    </div>
    <div class="m-accordion__item-body collapse " id="m_accordion_8_item_{{ $row->id }}_body" role="tabpanel"
        aria-labelledby="m_accordion_8_item_{{ $row->id }}_head" data-parent="#m_accordion_8" style="">
        <div class="m-accordion__item-content">
            <br>
            <form id="formMenu{{ $row->id }}" action="{{ $row->urlAdminUpdate() }}" method="POST">
                    <input type="hidden" name="_method" value="PUT" /> {{ csrf_field() }}
            <h5><strong>Tiếng việt:</strong></h5>
            <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                <div class="form-group m-form__group">
                    <label for="col-form-label col-lg-3 col-sm-12">Đường dẫn</label>
                    <input class="form-control" name="url" value="{{ old('url',$row->url) }}" />
                    @if ($errors->has('url'))
                    <span class="invalid-feedback" style="display:block;">
                        <strong>{{ $errors->first('url') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                <div class="form-group m-form__group">
                    <label for="col-form-label col-lg-3 col-sm-12">Tên hiển thị</label>
                    <input class="form-control" name="display_name" value="{{ old('display_name',$row->display_name) }}" />
                    @if ($errors->has('display_name'))
                    <span class="invalid-feedback" style="display:block;">
                        <strong>{{ $errors->first('display_name') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            @foreach(config('ecommerce.locale_displays') as $key => $lang)
            <h5><strong>{{ $lang }}:</strong></h5>
            <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                <div class="form-group m-form__group">
                    <label for="col-form-label col-lg-3 col-sm-12">Đường dẫn</label>
                    <input class="form-control" name="text_translate_url_{{ $key }}" value="{{ old('text_translate_url_'.$key,$row->getElementByLang($key,'url')) }}"
                        required />
                    @if ($errors->has('text_translate_url_'.$key))
                    <span class="invalid-feedback" style="display:block;">
                        <strong>{{ $errors->first('text_translate_url_'.$key) }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                <div class="form-group m-form__group">
                    <label for="col-form-label col-lg-3 col-sm-12">Tên hiển thị</label>
                    <input class="form-control" name="text_translate_display_name_{{ $key }}" value="{{ old('text_translate_display_name_'.$key, $row->getElementByLang($key,'display_name')) }}"
                        required />
                    @if ($errors->has('text_translate_display_name_'.$key))
                    <span class="invalid-feedback" style="display:block;">
                        <strong>{{ $errors->first('text_translate_display_name_'.$key) }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            </form>
            @endforeach
            <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12 text-right">
                @include('menu.component.btn_update')
                <label style="margin:0px 5px 0 2px">/</label>
                @include('menu.component.btn_delete')
            </div>
        </div>
    </div>
</div>
