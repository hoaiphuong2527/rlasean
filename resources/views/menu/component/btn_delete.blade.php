<div data-toggle="tooltip" data-placement="top" style="display: inline-block;">
    <label data-toggle="modal" data-target="{{ '#delete' . $row->id }}" style="cursor: pointer; color: red;font-style: italic">
        Xóa</label>
</div>
<div class="ilv-btn-group">
    <form action="{{ $row->urlAdminDestroy() }}" method="post">

        <input type="hidden" name="_method" value="DELETE">
        {!! csrf_field() !!}
        <div class="modal fade" id="{{ 'delete' . $row->id }}" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content  -->
                <div class="modal-content text-center">
                    <div class="modal-header">
                        <h4 class="modal-title">Cảnh báo</h4>
                        <label data-dismiss="modal" style="cursor: pointer;"><i class="fa fa-times"></i></label>
                    </div>
                    <div class="modal-body">
                        <p>Dữ liệu sẽ bị xóa vĩnh viễn.Bạn có chắc là muốn như vậy?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-outline-danger active">Đồng ý</button>
                        <button type="button" class="btn btn-outline-brand active" data-dismiss="modal">Không đồng ý</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
