<div style="padding-left:40px">
    @foreach($list as $row)
            @include('menu.component.item')
        @if($row->children()->count())
            @include('menu.component.child',['list' => $row->children])
        @endif
    @endforeach
</div>