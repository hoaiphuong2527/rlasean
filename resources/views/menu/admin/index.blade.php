@section('title')
Quản lý menu website
@endsection
@extends('layouts.admin')
@section('content')
<br>
<div class="m-portlet__body">
    <div class="row">
        <form class="col-6" action="{{ route('admin_menu.store') }}" method="POST">
            @csrf
            <div class="m-portlet m-portlet m-portlet--tab">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Thêm mục menu
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <div class="m-accordion m-accordion--default m-accordion--solid m-accordion--section  m-accordion--toggle-arrow"
                        id="m_accordion_7" role="tablist">
                        <div class="m-accordion__item">
                            <div class="m-accordion__item-head" role="tab" id="m_accordion_7_item_1_body" data-toggle="collapse"
                                href="#m_accordion_7_item_1_body" aria-expanded="false">
                                <span class="m-accordion__item-title">Đường dẫn tùy chỉnh</span>
                                <span><i class="fa fa-angle-down"></i></span>
                            </div>
                            <div class="m-accordion__item-body " id="m_accordion_7_item_1_body" role="tabpanel"
                                aria-labelledby="m_accordion_7_item_1_head" data-parent="#m_accordion_7" style="">
                                <div class="m-accordion__item-content">
                                    <br>
                                    <h5><strong>Tiếng việt:</strong></h5>
                                    <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                                        <div class="form-group m-form__group">
                                            <label for="col-form-label col-lg-3 col-sm-12">Đường dẫn</label>
                                            <input class="form-control" name="url" value="{{ old('url') }}" />
                                            @if ($errors->has('url'))
                                            <span class="invalid-feedback" style="display:block;">
                                                <strong>{{ $errors->first('url') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                                        <div class="form-group m-form__group">
                                            <label for="col-form-label col-lg-3 col-sm-12">Tên hiển thị</label>
                                            <input class="form-control" name="display_name" value="{{ old('display_name') }}" />
                                            @if ($errors->has('display_name'))
                                            <span class="invalid-feedback" style="display:block;">
                                                <strong>{{ $errors->first('display_name') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    @foreach(config('ecommerce.locale_displays') as $key => $lang)
                                    <h5><strong>{{ $lang }}:</strong></h5>
                                    <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                                        <div class="form-group m-form__group">
                                            <label for="col-form-label col-lg-3 col-sm-12">Đường dẫn</label>
                                            <input class="form-control" name="text_translate_url_{{ $key }}" value="{{ old('text_translate_url_'.$key) }}" required/>
                                            @if ($errors->has('text_translate_url_'.$key))
                                            <span class="invalid-feedback" style="display:block;">
                                                <strong>{{ $errors->first('text_translate_url_'.$key) }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                                        <div class="form-group m-form__group">
                                            <label for="col-form-label col-lg-3 col-sm-12">Tên hiển thị</label>
                                            <input class="form-control" name="text_translate_display_name_{{ $key }}" value="{{ old('text_translate_display_name_'.$key) }}" required/>
                                            @if ($errors->has('text_translate_display_name_'.$key))
                                            <span class="invalid-feedback" style="display:block;">
                                                <strong>{{ $errors->first('text_translate_display_name_'.$key) }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    @endforeach
                                    <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12 text-right">
                                        <button type="submit" class="btn btn-primary text-right">
                                            Thêm mục <i class="fa fa-angle-right"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-accordion__item">
                            <div class="m-accordion__item-head" role="tab" id="m_accordion_7_item_3_head" data-toggle="collapse"
                                href="#m_accordion_7_item_3_body" aria-expanded="false">
                                <span class="m-accordion__item-title">Danh mục</span>
                                <span><i class="fa fa-angle-down"></i></span>
                            </div>
                            <div class="m-accordion__item-body" id="m_accordion_7_item_3_body" role="tabpanel"
                                aria-labelledby="m_accordion_7_item_3_head" data-parent="#m_accordion_7" style="">
                                <div class="m-accordion__item-content">
                                    <br>
                                    <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                                        <div class="form-group m-form__group">
                                            <label for="exampleSelect1">Danh mục</label>
                                            <select class="form-control m-input m-input--square" name="parent">
                                                <option value="0">Danh mục cha</option>
                                                @foreach($list as $row)
                                                <option value="{{ $row->id }}">{{ $row->display_name }}</option>
                                                @if($row->children()->count())
                                                    @include('menu.component.option',['list' => $row->children,'padding' => '---   '])
                                                @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <div class="col-6">
            <div class="m-portlet">
                <div class="m-portlet__head-tools">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    Cấu trúc menu
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div class="m-accordion m-accordion--default m-accordion--solid m-accordion--section  m-accordion--toggle-arrow ilv-sort-order"
                            id="m_accordion_8" role="tablist">
                            @foreach($list as $row)
                            @include('menu.component.item')
                            @if($row->children()->count())
                            @include('menu.component.child',['list' => $row->children])
                            @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
