@extends('layouts.main')
@section('title')
{{ $item->getAttributeLang('name') }}
@endsection
@section('meta')
<meta charset="UTF-8">
<meta name="description" content="{{ $item->name }}">
<meta name="keywords" content="{{ $item->name }}">
<meta name="author" content="{{ $item->user->name }}">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta property="og:url"
    content="{{ route('guest_posts.show.'.app()->getLocale(),['id' => $item->id, 'slug' => $item->slug ]) }}" />
<meta property="og:type" content="article" />
<meta property="og:title" content="{{ $item->getAttributeLang('name')}}" />
<meta property="og:description" content="{{ $item->getAttributeLang('name')}}" />
<meta property="og:image" content="{{ $item->linkImage() }}" />
@endsection
@section('content')

<div class="container">
    {!! Breadcrumbs::render('guest_posts.show.'.app()->getLocale(),['id' => $item->id, 'slug' => $item->slug ])!!}
</div>
<div class="container mt-5">
    <div class="row">
        <div class="col-md-8">
            <div class="row mb-2">
                <div class="col-md-12">
                    <div class="card">
                        <img class="card-img-top" src="{{ $item->linkImage() }}" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title"> @include('guest.component.title_left', ['title' =>
                                $item->getAttributeLang('name')])
                            </h5>
                            <p class="card-text"> {!! $item->getAttributeLang('description') !!}
                            </p>
                            <!-- bai viet tiep theo -->
                            @if($latest_post)
                            <a
                                href="{{ $latest_post->urlGuestShow() }}">{{ $latest_post->getAttributeLang('name') }}</a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">@lang('page_text.post.related_post')</h4>
                </div>
                <div class="card-body">
                    @foreach($related_post as $post)
                    <a href="{{ $post->urlGuestShow() }}">
                        <div class="d-flex flex-row">
                            <div class="p-2 col-4 mx-0">
                                <img class="card-img-top" src="{{ $post->linkImage() }}" alt="Card image cap">
                            </div>
                            <div class="p-2 col-8 mx-0">
                                <h4 class="card-title ilv-title-card ">{{ $post->short_name('70') }}</h4>
                            </div>
                        </div>
                    </a>
                    @endforeach
                </div>
            </div>
            @if(count($product_seens))

            <div class="card my-4">
                <div class="card-header">
                    <h4 class="card-title">@lang('page_text.product_details.product_seen')</h4>
                </div>
                <div class="card-body">
                    @php
                    $list = $product_seens;
                    @endphp
                    @foreach($list as $item)
                    <a href="{{ $item->urlGuestShow() }}">
                        <div class="d-flex flex-row">
                            <div class="p-2 col-4 mx-0">
                                <img class="card-img-top" src="{{ $item->linkImage() }}" alt="Card image cap">
                            </div>
                            <div class="p-2 col-8 mx-0">
                                <h4 class="card-title ilv-title-card ">{{ $item->name }}</h4>
                                <small>{{ $item->discount_price_display }}</small>
                            </div>
                        </div>
                    </a>
                    @endforeach
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
@endsection
