<div class="container">
    <div class="row">
        @foreach($posts as $row)
        <div class="col-md-4">
            @include('post.guest.component.card')
        </div>
        @endforeach
    </div>
</div>