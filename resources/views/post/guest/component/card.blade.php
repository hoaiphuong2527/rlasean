<a href="{{ $row->urlGuestShow() }}">
    <div class="card mb-4">
        <img class="card-img-top" src="{{ $row->linkImage() }}" alt="Card image cap">
        <div class="card-footer text-muted">
            @lang('page_text.post.by') {{ $row->user->name ." , ".
            $row->created_at->diffforhumans()}}
        </div>
        <div class="card-body">
            <h4 class="card-title ilv-title-card ">{{ $row->getAttributeLang('name') }}</h4>
            <p>{!! $row->short_decs(200) !!}</p>
        </div>
    </div>
</a>
