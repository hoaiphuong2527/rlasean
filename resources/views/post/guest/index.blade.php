@extends('layouts.main')
@section('title')
{{ $item->getAttributeLang('name')}}
@endsection
@section('meta')
<meta charset="UTF-8">
<meta name="description" content="{{ $option->name }}">
<meta name="keywords" content="{{ $option->name }}">
<meta name="author" content="{{ $option->name }}">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta property="og:url"
    content="{{ route('guest_posts.category.'.app()->getLocale(),['id' => $item->id, 'slug' => $item->getAttributeLang('slug')] ) }}" />
<meta property="og:type" content="article" />
<meta property="og:title" content="{{ $item->getAttributeLang('name')}}" />
<meta property="og:description" content="{{ $item->getAttributeLang('name')}}" />
<meta property="og:image" content="{{ $item->linkImage() }}" />
@endsection
@section('content')

@include('guest.component.banner_title', ['title' => $item->getAttributeLang('name'), 'imageLink' =>
$item->linkImage()])
<div class="container">
    {!! Breadcrumbs::render('guest_posts.index.'.app()->getLocale())!!}
</div>
<div class="container mt-5">
    <div class="row">
        <div class="col-md-12">
            <ul class="nav bg radius  mb-3 ilv-post-nav">
                @foreach($categories as $category)
                <li class="nav-item">
                    <a href="{{ $category->urlGuestShow()}} " class="nav-link text-uppercase">
                        {{ $category->getAttributeLang('name')}}
                    </a>
                </li>
                @endforeach
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            @if($latest_post)
            <a href="{{ $latest_post->urlGuestShow()}} ">
                <div class="card">
                    <img src="{{ $latest_post->linkImage() }}" class="img-fluid" alt="">
                    <div class="card-body">
                        <h4 class="card-title">{{ $latest_post->getAttributeLang('name') }}</h4>
                        <p class="card-text">{{ $latest_post->short_decs(100) }}</p>
                    </div>
                </div>
            </a>
            @endif
        </div>
        <div class="col-md-4 my-2 my-lg-0">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title"> @lang('page_text.post.new_post')
                    </h4>
                </div>
                <div class="card-body">
                    @foreach($latest_posts as $row)
                    <a href="{{ $row->urlGuestShow() }}">
                        <div class="d-flex flex-row">
                            <div class="p-2 col-4 mx-0">
                                <img class="card-img-top" src="{{ $row->linkImage() }}" alt="Card image cap">
                            </div>
                            <div class="p-2 col-8 mx-0">
                                <h4 class="card-title ilv-title-card ">{{ $row->short_name(65) }}</h4>
                            </div>
                        </div>
                    </a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card my-4">
                <div class="card-header">
                    <h4 class="card-title">
                        @lang('page_text.post.related_post')
                    </h4>
                </div>
                <div class="card-body row ">
                    @foreach($related_posts as $row)
                    <div class="col-md-4">
                        @include('post.guest.component.card')
                    </div>
                    @endforeach
                </div>
                <div class="card-footer pagination-center">
                    <div class="row">
                        {{ $related_posts->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
