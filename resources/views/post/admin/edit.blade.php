@extends('layouts.admin') @section('content')
@if(Session::has('success'))
<div class="alert alert-success">
    {{ Session::get('success') }}
</div>
@endif
<div class="m-portlet">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    Chỉnh sửa thông tin
            </div>
        </div>
        @if($item->status)
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <form method="post" action="{{ route('admin_post_clone.store',['post_slug' => $item->slug]) }}">
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air"
                            value="Tạo bản nháp" />
                    </form>
                </li>
            </ul>
        </div>
        @endif
    </div>
    <form class="m-form m-form--fit m-form--label-align-right" method="POST" action="{{ $item->urlAdminUpdate() }}"
        enctype="multipart/form-data">
        <input type="hidden" name="_method" value="PUT" /> {{ csrf_field() }}
        <div class="m-portlet__body">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Thông tin mặc định
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <ul class="m-topbar__nav m-nav m-nav--inline">
                        @include('admin.component.droplist_lang')
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="tab-content">
                    <div class="row">
                        <div class="col-md-6">
                            <br>
                            <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group m-form__group">
                                    @if ($errors->has('name'))
                                    <span class="invalid-feedback" style="display:block;">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                    <label for="col-form-label col-lg-3 col-sm-12">Tiêu đề</label>
                                    <input name="name" type="text" class="form-control m-input" value="{{ old('name',$item->name) }}">
                                    <span class="invalid-feedback" style="display:block;">
                                        <strong></strong>
                                    </span>
                                </div>
                            </div>
                            @include('post.admin.component.option')
                            @if($item->name)
                            <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group m-form__group">
                                    <label for="">Đường dẫn</label><br>
                                    <span class="m-form__help" style="color:#333">Vui lòng nhập đường dẫn theo định
                                        dạng mẫu:<b>duong-dan-{{ $item->slug }}</b></span><br>
                                    <div class="input-group m-input-group m-input-group--square">
                                        <div class="input-group-prepend"><span class="input-group-text">{{
                                                route('guest_posts.show.'.app()->getLocale(),['id' => $item->id,
                                                'slug' =>""]) }}/</span></div>
                                        <input type="text" class="form-control m-input m-input--square" id=""
                                            placeholder="" name="slug" value="{{ old('slug',$item->slug) }}"
                                            required>
                                        @if ($errors->has('slug'))
                                        <span class="invalid-feedback" style="display:block;">
                                            <strong>{{ $errors->first('slug') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            @endif
                            <br>
                        </div>
                        <div class="col-md-6">
                            <div style="border-left: 1px solid #ebedf2;">
                                <br>
                                <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                                    <div class="form-group m-form__group">
                                        <label>Hình ảnh</label>
                                        <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                                            <script>
                                                var previewImage =[]
                                                var single = {!! collect($item->getImg()) !!}
                                            </script>
                                            <div id="react-admin-upload-icon-btn" data-img="" data-input-name="image"
                                                data-type="single"></div>
            
                                        </div>
                                    </div>
                                </div>
                                <br>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <br>
                            <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group m-form__group">
                                    @if ($errors->has('description'))
                                    <span class="invalid-feedback" style="display:block;">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                    @endif
                                    <label for="col-form-label col-lg-3 col-sm-12">Nội dung</label>
                                    <textarea class="form-control myPost" rows="20" cols="30"
                                        placeholder="Nội dung" name="description">{{ old('description',$item->description) }}</textarea>
                                </div>
                            </div>
                            <br>
                    </div>
                </div>
            </div>
            <div class="m-portlet__foot m-portlet__foot--fit">
                <div class="m-form__actions text-center">
                    <input type="submit" class="btn btn-info" name="continue" value="Lưu & tạo mới">
                    @if(!$item->status)
                    <input type="submit" class="btn btn-success" name="publish" value="Đăng">
                    <input type="submit" class="btn btn-warning" name="draff" value="Lưu nháp">
                    @else
                    <input type="submit" class="btn btn-success" name="publish" value="Lưu & Đăng">
                    @endif
                </div>
            </div>
        </div>
    </form>
</div>
@endsection
