@section('title') Danh sách bài post @endsection @extends('layouts.admin') @section('content')
@if(Session::has('destroy'))
<div class="alert alert-success">
    {{ Session::get('destroy') }}
</div>
@endif
<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    Danh sách bài viết
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <form method="post" action="{{ route('admin_post.store') }}">
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air"
                            value="Thêm mới" />
                    </form>
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <form class="row" method="GET" action="">
            <div class="col-5">
                <div class="form-group m-form__group row">
                    <label class="col-form-label col-lg-3 col-sm-12">Tên bài viết</label>
                    <div class="col-lg-9 col-md-9 col-sm-12">
                        <input class="form-control m-input m-input--square" type="text" name="search" value="{{ $search }}">
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="form-group m-form__group row">
                    <label class="col-form-label col-lg-3 col-sm-12">Danh mục</label>
                    <div class="col-lg-9 col-md-9 col-sm-12">
                        <select class="form-control" id="m_select2_2_modal" name="category_id">
                            <option value="all" @if($category_id == 'all') selected @endif>All</option>
                            @foreach($categories as $category)
                            <option value="{{ $category->id }}" @if($category_id == $category->id) selected @endif>{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-3">
                <div class="form-group m-form__group row">
                    <input type="submit" class="btn btn-info" value="Tìm kiếm">
                </div>
            </div>
        </form>
        <hr>
        <!--begin: Datatable -->
        <div class="m_datatable m-datatable m-datatable--default m-datatable--loaded" style="">
            <table class="m-datatable__table" style="display: block; min-height: 300px; overflow-x: auto;">
                <thead class="m-datatable__head">

                    <tr class="m-datatable__row" style="left: 0px;">
                        <th class="m-datatable__cell--center m-datatable__cell ">
                            <span style="width: 50px;">#</span></th>
                        <th class="m-datatable__cell--center m-datatable__cell  ">
                            <span style="width: 100px;">Tiêu Đề</span></th>
                        <th class="m-datatable__cell--center m-datatable__cell  ">
                            <span style="width: 150px;">Danh mục</span></th>
                        <th class="m-datatable__cell--center m-datatable__cell  ">
                            <span style="width: 200px;">Trạng thái</span></th>
                        <th class="m-datatable__cell--center m-datatable__cell  ">
                            <span style="width: 150px;">Hành Động</span></th>
                    </tr>
                </thead>
                <tbody style="" class="m-datatable__body">
                    <?php $index = 1; ?>
                    @foreach($post as $row)
                    <tr class="m-datatable__row" style="left: 0px;">
                        <td class="m-datatable__cell--center m-datatable__cell">
                            <span style="width: 50px;">{{ $index++}}</span></td>
                        <td class="m-datatable__cell--center m-datatable__cell"> 
                            <span style="width: 100px;">{{ $row->name_format }}
                            </span></td>
                        <td class="m-datatable__cell--center m-datatable__cell"> 
                            <span style="width: 200px;">{{ $row->getCateName() }}
                            </span></td>
                        <td class="m-datatable__cell--center m-datatable__cell"> 
                            <span style="width: 200px;">
                                @if($row->post_category && $row->name)
                                <div data-toggle="tooltip" data-placement="top" style="display: inline-block;" title="Đóng/mở trạng thái bài viết">
                                    <div class="checkbox">
                                        <label class="m-switch m-switch--outline m-switch--primary">
                                            <input data-id="{{$row->id}}" class="chkPost" type="checkbox" @if($row->status) checked
                                            @endif>
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                                @else
                                    N/A
                                @endif
                            </span>
                        </td>
                        <td class="m-datatable__cell--center m-datatable__cell">
                            <span style="width: 150px; margin-left:30px">
                                <div class="ilv-btn-group">
                                    <div data-toggle="tooltip" data-placement="top" style="display: inline-block;" title="Xem thông tin bài viết">
                                        <a class="btn btn-sm btn-warning" href="{{ $row->urlAdminEdit() }}">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                    </div>
                                    @if($row->status)
                                    <div data-toggle="tooltip" data-placement="top" style="display: inline-block;" title="Tạo bản sao">
                                        <form method="post" action="{{ route('admin_post_clone.store',['post_slug' => $row->slug]) }}">
                                            {{ csrf_field() }}
                                            <button type="submit" class="btn btn-sm btn-info"><i class="fa fa-copy"></i></button>
                                        </form>
                                    </div>
                                    @endif
                                    @include('post.admin.component.btn_delete_post')
                                </div>

                            </span>
                        </td>
                    </tr>
                    @endforeach()
                </tbody>
            </table>
            {{ $post->links() }}
        </div>
    </div>
</div>
@endsection
