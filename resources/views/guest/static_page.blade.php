@extends('layouts.main')
@section('title')
{{ $item->getAttributeLang('name')}}
@endsection
@section('meta')
<meta charset="UTF-8">
<meta name="description" content="{{ $item->getAttributeLang('name')}}">
<meta name="keywords" content="{{ $item->getAttributeLang('name')}}">
<meta name="author" content="{{ $item->getAttributeLang('name')}}">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta property="og:url" content="{{ route('guest.contact.'.app()->getLocale()) }}" />
<meta property="og:type" content="article" />
<meta property="og:title" content="{{ __('page_text.contact.page_title') }}" />
<meta property="og:description" content="{{ __('page_text.contact.page_title') }}" />
<meta property="og:image" content="{{ $item->linkImage() }}" />
@endsection
@section('content')
<style>
    .image-aboutus-banner {
        background: url("{{ $banner->linkImage() }}");
        background-repeat: no-repeat;
        background-size: cover;
        background-position: center center;
        color: #fff;
        padding-top: 110px;
        padding-bottom: 110px;
    }

</style>
<div class="image-aboutus-banner">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="lg-text">{{ $item->getAttributeLang('name')}}</h1>
            </div>
        </div>
    </div>
</div>
<div id="services" class="services-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="section-headline services-head text-center">
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="container">
            <div class="row text-center">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="services-contents">
                        {!! $item->getAttributeLang('description') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
