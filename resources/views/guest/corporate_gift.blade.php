@extends('layouts.main')
@section('title')
{{ __('page_text.corporate_gift.page_title') }}
@endsection
@section('meta')
<meta charset="UTF-8">
<meta name="description" content="{{ $option->name }}">
<meta name="keywords" content="{{ $option->name }}">
<meta name="author" content="{{ $option->name }}">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta property="og:url"           content="{{ route('guest.corporate_gift.'.app()->getLocale()) }}" />
<meta property="og:type"          content="article" />
<meta property="og:title"         content="{{ __('page_text.corporate_gift.page_title') }}" />
<meta property="og:description"   content="{{ __('page_text.corporate_gift.page_title') }}" />
<meta property="og:image"         content="{{ $option->linkImage('main_logo') }}" />
@endsection
@section('content')
<style>
    .image-aboutus-banner {
    background:  url("{{ $banner->linkImage() }}");
    background-repeat: no-repeat;
    background-size: cover;
    background-position: center center;
    color: #fff;
    padding-top: 110px;
    padding-bottom:110px;
}
</style>
<div class="image-aboutus-banner">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="lg-text">{{ __('page_text.corporate_gift.page_title') }}</h1>
            </div>
        </div>
    </div>
</div>

<div class="container ilv-mt-60 ilv-mb-60">
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header ilv-card-head text-black  text-uppercase "><i class="fa fa-envelope"></i> @lang('page_text.corporate_gift.contact_with_us')
                </div>
                <div class="card-body">
                    <form action="{{ route('guest.post_contact') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="name">@lang('component_text.labels.lastname')</label>
                            <input type="text" name="l_name" class="form-control" id="name" aria-describedby="emailHelp"
                                placeholder="@lang('component_text.placeholders.lastname')" required>
                            @if($errors->has('l_name'))
                            <small style=" color: red !important;" class="form-text text-muted">{{
                                $errors->first('name') }}</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="name">@lang('component_text.labels.firstname')</label>
                            <input type="text" name="f_name" class="form-control" id="name" aria-describedby="emailHelp"
                                placeholder="@lang('component_text.placeholders.firstname')" required>
                            @if($errors->has('f_name'))
                            <small style=" color: red !important;" class="form-text text-muted">{{
                                $errors->first('name') }}</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="email">@lang('component_text.labels.email')</label>
                            <input type="email" name="email" class="form-control" id="email" aria-describedby="emailHelp"
                                placeholder="@lang('component_text.placeholders.email')" required>
                            @if($errors->has('email'))
                            <small style=" color: red !important;" class="form-text text-muted">{{
                                $errors->first('email') }}</small>
                            @endif </div>
                        <div class="form-group">
                            <label for="email">@lang('component_text.labels.phone')</label>
                            <input type="number" name="phone" class="form-control" id="phone" aria-describedby="emailHelp"
                                placeholder="@lang('component_text.placeholders.phone')" required>
                            @if($errors->has('phone'))
                            <small style=" color: red !important;" class="form-text text-muted">{{
                                $errors->first('phone') }}</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="email">@lang('component_text.labels.company')</label>
                            <input type="text" name="company" class="form-control" id="company" aria-describedby="emailHelp"
                                placeholder="@lang('component_text.placeholders.company')" required>
                            @if($errors->has('company'))
                            <small style=" color: red !important;" class="form-text text-muted">{{
                                $errors->first('company') }}</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="email">@lang('component_text.labels.website')</label>
                            <input type="text" name="website" class="form-control" id="website" aria-describedby="emailHelp"
                                placeholder="@lang('component_text.placeholders.website')" required>
                            @if($errors->has('website'))
                            <small style=" color: red !important;" class="form-text text-muted">{{
                                $errors->first('website') }}</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="email">@lang('component_text.labels.country')</label>
                            <input type="text" name="country" class="form-control" id="country" aria-describedby="emailHelp"
                                placeholder="@lang('component_text.placeholders.country')" required>
                            @if($errors->has('country'))
                            <small style=" color: red !important;" class="form-text text-muted">{{
                                $errors->first('country') }}</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="message">@lang('component_text.labels.message')</label>
                            <textarea class="form-control" name="message" id="message" rows="6" required placeholder="@lang('component_text.placeholders.message')"></textarea>
                            @if($errors->has('message'))
                            <small style=" color: red !important;" class="form-text text-muted">{{
                                $errors->first('message') }}</small>
                            @endif
                        </div>
                        <input type="hidden" name="type" value="{{ config('ecommerce.contact_types.gift') }}">
                        <div class="mx-auto">
                            <button type="submit" class="btn btn-ys ilv-italic ys-yellow-text">@lang('component_text.buttons.send')</button></div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-4">
            <div class="card bg-light mb-3">
                <div class="card-header ilv-card-head text-black  text-uppercase "><i class="fa fa-home"></i> @lang('component_text.labels.address')</div>
                <div class="card-body">
                    <p>{{ $option->name }}</p>
                    <p>{{ $option->address }}</p>
                    <p>@lang('component_text.labels.email'): {{ $option->email }}</p>
                    <p>@lang('component_text.labels.phone'): {{ $option->phone }}</p>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
