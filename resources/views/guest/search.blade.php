@extends('layouts.main')
@section('title')
@lang('component_text.labels.result') {{ $product_name }}
@endsection

@section('content')
<div id="services" class="services-area">
    <!-- <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="section-headline services-head text-center">
                </div>
            </div>
        </div>
    </div> -->
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <h2 class="mt-4">@lang('page_text.search.search')</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 card my-3">
                    <div class="section-headline services-head text-center card-body">
                        <form action="{{ route('guest_search.index.'. app()->getLocale()) }}" class="search-wrap"
                            method="GET">
                            <div class="input-group">
                                <input type="text" class="form-control" name="product_name"
                                    placeholder="Nhập tên sản phẩm..." />
                                <div class="input-group-append">
                                    <button class="btn btn-primary text-white" type="submit">
                                        @lang('component_text.buttons.search')
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="container my-5 ilv-container-benefit pt-4 pb-4 ">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="services-contents">
                    <hgroup class="mb20">
                        @if(!$products->count())
                        <h2>@lang('page_text.search.not_found')</h2>
                        @else
                        <br>
                        <h4>@lang('page_text.search.key')<strong>"{{ $product_name }}"</strong></h4>
                    </hgroup>
                </div>
                <div class="row">
                    @foreach($products as $row)
                    <div class="col-md-3 col-sm-6 col-xs-12 p-lg-1">
                        <figure class="card card-product my-1 m-lg-1">
                            @if ($row->isValidCode())
                            <span class="badge-offer">
                                <b> {{ $row->getPromotionShowInView() }}</b>
                            </span>
                            @endif
                            <div class="thumbnail item img-wrap">
                                <a href="{{ $row->urlGuestShow() }}" class="blog-thumb-img">
                                    <img src="{{ $row->linkImage() }}" class="img-responsive">
                                </a>
                            </div>
                            <figcaption class="info-wrap caption">
                                <h4 class="title">
                                    <a href="{{ $row->urlGuestShow() }}">
                                        <p class="js-clamp">{{ $row->getAttributeLang('name') }}</p>
                                    </a>
                                </h4>
                                {{-- <p class="desc">{{ $row->short_description }}</p> --}}
                            </figcaption>
                            <div class="bottom-wrap">
                                @if ($row->isValidCode())
                                <span class="price" itemprop="offers" itemscope="">
                                    <span itemprop="price"
                                        content="{{ $row->discount_price_display }}">{{ $row->discount_price_display }}</span>
                                </span>
                                <span class="ilv-promotion-price"> {{ $row->getPrice() }}</span>
                                @else
                                <span class="price" itemprop="offers" itemscope="">
                                    <span itemprop="price"
                                        content="{{ $row->getPrice() }}">{{  $row->getPrice() }}</span>
                                </span>
                                @endif
                                <br>
                                <div class="row">
                                    <div class="ilv-center-block">
                                        @foreach ($row->product_variants as $variant)
                                        <span class="ilv-color-tag mx-1"
                                            style="background-color:{{ $variant->color }}">&nbsp;</span>
                                        @endforeach
                                    </div>
                                </div>
                            </div> <!-- bottom-wrap.// -->
                        </figure>
                    </div>
                    @endforeach
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
