@extends('layouts.main')
@section('title')
{{ __('page_text.contact.page_title') }}
@endsection
@section('meta')
<meta charset="UTF-8">
<meta name="description" content="{{ $option->name }}">
<meta name="keywords" content="{{ $option->name }}">
<meta name="author" content="{{ $option->name }}">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta property="og:url"           content="{{ route('guest.contact.'.app()->getLocale()) }}" />
<meta property="og:type"          content="article" />
<meta property="og:title"         content="{{ __('page_text.contact.page_title') }}" />
<meta property="og:description"   content="{{ __('page_text.contact.page_title') }}" />
<meta property="og:image"         content="{{ $option->linkImage('main_logo') }}" />
@endsection
@section('content')

@include('guest.component.banner_title', ['title' => __('page_text.contact.page_title'), 'imageLink' => $banner->linkImage()])
<div class="container">
    {!! Breadcrumbs::render('guest.contact.'.app()->getLocale())!!}
</div>

<div class="container mt-5 text-center">
    @include('guest.component.title_left', ['title' => __('page_text.contact.contact_with_us')])
    <p>
        {{ __('page_text.contact.section_text_1') }}
    </p>
</div>
<div class="container mt-5">
    <div class="row">
        <div class="col-md-4 text-center">
                <i class="fa fa-phone ilv-fs-40" aria-hidden="true"></i>
                <div class="font-weight-bold mt-2"> {{ __('page_text.contact.call_us') }}</div>
                <div>{{ $option->phone }}</div>
        </div>
        <div class="col-md-4 text-center">
                <i class="fa fa-commenting ilv-fs-40" aria-hidden="true"></i>
            <div class="font-weight-bold mt-2">{{ __('page_text.contact.live_chat') }}</div>
            <div>{{ __('page_text.contact.working_time') }}</div>
        </div>
        <div class="col-md-4 text-center">
                <i class="fa fa-envelope ilv-fs-40" aria-hidden="true"></i>
            <div class="font-weight-bold mt-2">{{ __('page_text.contact.email') }}</div>
            <div>{{ $option->email }}</div>
        </div>
    </div>
</div>
<div class="container mt-5" id="display-iframe" data="{{$option->iframe_gg}}">
</div>
<div class="container ilv-mt-60 ilv-mb-60">
    @if(Session::has('status'))
        <div class="alert alert-success">
            {{ Session::get('status') }}
        </div>
    @endif
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header ilv-card-head text-black  text-uppercase "><i class="fa fa-envelope"></i> @lang('page_text.contact.contact_with_us')
                </div>
                <div class="card-body">
                    <form action="{{ route('guest.post_contact') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="name">@lang('component_text.labels.full_name')</label>
                            <input type="text" name="f_name" class="form-control" id="name" aria-describedby="emailHelp"
                                placeholder="@lang('component_text.placeholders.full_name')" value="{{ old('f_name') }}" required>
                            @if($errors->has('f_name'))
                            <small style=" color: red !important;" class="form-text text-muted">{{
                                $errors->first('name') }}</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="email">@lang('component_text.labels.email')</label>
                            <input type="email" name="email" class="form-control" id="email" aria-describedby="emailHelp" value="{{ old('email') }}"
                                placeholder="@lang('component_text.placeholders.email')" required>
                            @if($errors->has('email'))
                            <small style=" color: red !important;" class="form-text text-muted">{{
                                $errors->first('email') }}</small>
                            @endif </div>
                        <div class="form-group">
                            <label for="email">@lang('component_text.labels.phone')</label>
                            <input type="number" name="phone" class="form-control" id="phone" aria-describedby="emailHelp" value="{{ old('phone') }}"
                                placeholder="@lang('component_text.placeholders.phone')" required>
                            @if($errors->has('phone'))
                            <small style=" color: red !important;" class="form-text text-muted">{{
                                $errors->first('phone') }}</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="message">@lang('component_text.labels.message')</label>
                            <textarea class="form-control" name="message" id="message" rows="6" required>{{ old('message') }}</textarea>
                            @if($errors->has('message'))
                            <small style=" color: red !important;" class="form-text text-muted">{{
                                $errors->first('message') }}</small>
                            @endif
                        </div>
                        <input type="hidden" name="type" value="{{ config('ecommerce.contact_types.contact') }}">
                        <div class="mx-auto">
                            <button type="submit" class="btn btn-primary ilv-btn text-white">@lang('component_text.buttons.send')</button></div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-4">
            <div class="card bg-light mb-3">
                <div class="card-header ilv-card-head text-black  text-uppercase "><i class="fa fa-home"></i> @lang('page_text.contact.info')</div>
                <div class="card-body">
                    <p>{{ $option->name }}</p>
                    <p>{{ $option->address }}</p>
                    <p>@lang('component_text.labels.email') : {{ $option->email }}</p>
                    <p>@lang('component_text.labels.phone'): {{ $option->phone }}</p>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
