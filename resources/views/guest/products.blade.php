@extends('layouts.main')
@section('title')
{{ $title }}
@endsection

@section('content')
<div id="services" class="services-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="section-headline services-head text-center">
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="container">
            <br>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="row">
                        @foreach($products as $row)
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <figure class="card card-product my-1 m-lg-1">
                                @if ($row->isValidCode())
                                <span class="badge-offer">
                                    <b> {{ $row->getPromotionShowInView() }}</b>
                                </span>
                                @endif
                                <div class="thumbnail img-wrap">
                                    <a href="{{ $row->urlGuestShow() }}" class="blog-thumb-img">
                                        <img src="{{ $row->linkImage() }}" class="img-responsive">
                                    </a>
                                </div>
                                <figcaption class="info-wrap caption ">
                                    <h4 class="title">
                                        <a href="{{ $row->urlGuestShow() }}">{{ $row->getAttributeLang('name') }}</a>
                                    </h4>
                                    {{-- <p class="desc">{{ $row->short_description }}</p> --}}
                                </figcaption>
                                <div class="bottom-wrap">
                                    @if ($row->isValidCode())
                                    <div class="price-wrap h5">
                                        <span class="price-new">{{ $row->getDiscountPrice() }}</span>
                                        <del class="price-old">{{ $row->discount_price }}</del>
                                    </div> <!-- price-wrap.// -->
                                    @else
                                    <div class="price-wrap h5">
                                        <span class="price-new">{{ $row->getPrice() }}</span>
                                    </div> <!-- price-wrap.// -->
                                    @endif
                                    <div class="ilv-center-block">
                                        @foreach ($row->product_variants as $variant)
                                        <span class="ilv-color-tag mx-1"
                                            style="background-color:{{ $variant->color }}">&nbsp;</span>
                                        @endforeach
                                    </div>
                                </div>
                            </figure>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
