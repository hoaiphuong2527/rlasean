@foreach($categories as $item)
<a class="dropdown-item" href="{{ route('guest_products', ['slug' => $item->slug] )}}">{{$item->name}}</a>
    @if(count($item->children))
        @include('components.item_category_header',['categories' => $item->children])
    @endif
@endforeach