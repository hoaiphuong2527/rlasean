<!-- BEGIN: CONTENT/SHOPS/SHOP-2-1 -->
<div class="container">
    <div class="c-content-person-1-slider" data-slider="owl">
        <div class="owl-carousel owl-carousel-post owl-theme c-theme c-owl-nav-center" data-rtl="false" data-items="3" data-slide-speed="8000">
                @foreach($posts as $item)

            <div class="ivl-item-card">
                <div class="hotel-img">
                    <img src="{{ $item->linkImage() }}" alt="" class="img-fluid">
                </div>
                <h3><a href="{{ $item->urlGuestShow() }}">{{ $item->name }}</a></h3>
                <div class="post">
                    {{ $item->short_decs() }}
                </div>
                <div class="c-position">
                    <p>
                        <a class="btn btn-ys ilv-italic  ys-text-color" href="{{ $item->urlGuestShow() }}">Xem Tiếp</a>
                    </p>
                </div>
            </div>

            @endforeach
        </div>
    </div>
</div>
<!-- END: CONTENT/SHOPS/SHOP-2-1 -->
