<style>
        .image-aboutus-banner {
        background:  url("{{ $imageLink }}");
        background-repeat: no-repeat;
        background-size: cover;
        background-position: center center;
        color: #fff;
        padding-top: 110px;
        padding-bottom:110px;
    }
</style>
<div class="image-aboutus-banner">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
            <h1 class="ilv-title text-uppercase banner-title">{{ $title }}</h1>
            </div>
        </div>
    </div>
</div>