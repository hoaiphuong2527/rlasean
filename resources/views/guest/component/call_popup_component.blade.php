<div class="modal fade " id="modalCall" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered login-popup" role="document">
        <div class="modal-content">
            <div class="modal-content">
                <div class="modal-header bg-red text-center ">
                    <h5 class="modal-title  ys-yellow-text">
                        <i class="ty ty-phone"></i>
                        <span>Tổng đài tư vấn</span>
                    </h5>
                    <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>

                </div>
                <div class="modal-body">
                    <div class="col-xs-12 text-xs-center form-call">
                        <div class="screen-reader-response"></div>
                        <form action="{{ route('guest.post_contact') }}" method="post">
                            @csrf
                            <h4 class="ilv-fs-16">
                                Nhập số điện thoại, nhận ngay tư vấn miễn phí!<br>
                            </h4>
                            <div class="form-group col-xs-12 col-md-6 offset-md-3">
                                <span class="wpcf7-form-control-wrap txt-phone"><input type="tel" name="phone"
                                        value="" size="40" class="" id="phone" aria-required="true" aria-invalid="false"
                                        placeholder="0901234567">
                                    </span>
                            </div>
                            <p class="col-xs-12 text-center">
                                <button type="submit" class="btn ilv-btn-green">Tôi
                                    muốn được tư vấn</button><span class="ajax-loader"></span></p>
                            <div class="wpcf7-response-output wpcf7-display-none"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
