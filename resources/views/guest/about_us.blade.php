@extends('layouts.main')
@section('title')
about
@endsection
@section('meta')
<!-- <meta charset="UTF-8">
<meta name="description" content="{{ $item->name }}">
<meta name="keywords" content="{{ $item->name }}">
<meta name="author" content="{{ $item->name }}">
<meta name="viewport" content="width=device-width, initial-scale=1.0"> -->
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="ilv-hn-banner-style">
            <div class="ilv-hn-banner-img">
                @include('guest.component.banner',['imageLink' => $banner->linkImage()])
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12 my-4 py-4">
            <div class="text-center">
                <h2>GIỚI THIỆU</h2>
                <div class="fusion-text">
                    <hr >   
                </div>
            </div>
            <div class="fusion-text">
                <p style="font-size: 16px; text-align: left;">Công ty <strong>TNHH RLASEAN</strong> chuyên cung cấp các
                    sản
                    phẩm và thiết bị điện tử phục vụ cho nhu cầu cuộc sống hiện đại và tiện nghi. Các sản phẩm của chúng
                    tôi
                    đến từ thương hiệu RL với gần 40 năm kinh nghiệm trong lĩnh vực công nghệ an ninh, đã được kiểm
                    chứng và
                    đánh giá cao về công nghệ thiết kế cũng như chất lượng sản phẩm.</p>
                <p style="font-size: 16px; text-align: left;">Chuông gọi cửa hình IP, máy quay hình IP, hệ thống báo
                    động
                    hình IP, hệ thống chuông gọi cửa hình thông minh, chuông cửa hình vô tuyến,…Các sản phẩm của chúng
                    tôi
                    đã được xuất khẩu hơn 100 quốc gia khác nhau bao gồm các thị trường Châu Âu và Châu Mỹ.</p>
                <p style="font-size: 16px; text-align: left;">Tại <strong>RLASEAN</strong> chúng tôi tin rằng, những
                    giải
                    pháp công nghệ điện tử sẽ giúp cuộc sống trở nên tiện nghi, hiện đại và mọi người hạnh phúc hơn.
                    Cùng
                    với mong muốn trở thành đơn vị hàng đầu trong lĩnh vực, chúng tôi không ngừng nổ lực phát triển và
                    cam
                    kết cung cấp sản phẩm với chất lượng cao và thiết thực để phục vụ khách hàng.</p>
            </div>
            <div class="fusion-clearfix"></div>
        </div>
        <div class="col-md-12 my-4 text-center py-4">
            <div class="fusion-text">
                <h2>KÊNH PHÂN PHỐI</h2>
                <hr>
            </div>
            <div class="owl-carousel owl-init slide-items" id="slide_custom_nav" data-auto-play="true"
                data-custom-nav="custom-nav-first" data-items-desk="4" data-items-tab="4" data-items-mobile="2"
                data-margin="20" data-dots="true" data-nav="false">
                <img class="img-fluid mx-auto d-block" src="../images/hlogo1.png" alt="" height="auto" width="100%">
                <img class="img-fluid mx-auto d-block" src="../images/hlogo2.png" alt="" height="auto" width="100%">
                <img class="img-fluid mx-auto d-block" src="../images/hlogo3.png" alt="" height="auto" width="100%">
                <img class="img-fluid mx-auto d-block" src="../images/hlogo4.png" alt="" height="auto" width="100%">
                <img class="img-fluid mx-auto d-block" src="../images/hlogo5.png" alt="" height="auto" width="100%">
                <img class="img-fluid mx-auto d-block" src="../images/hlogo6.png" alt="" height="auto" width="100%">
            </div>
        </div>
        <div class="col-md-12 my-4 text-center py-4">
            <div class="fusion-text">
                <h2>ĐỐI TÁC CHIẾN LƯỢC</h2>
                <hr>
            </div>
            <div class="owl-carousel owl-init slide-items" id="slide_custom_nav" data-auto-play="true"
                data-custom-nav="custom-nav-first" data-items-desk="4" data-items-tab="4" data-items-mobile="2"
                data-margin="20" data-dots="true" data-nav="false">
                <img class="img-fluid mx-auto d-block" src="../images/hlogo1.png" alt="" height="auto" width="100%">
                <img class="img-fluid mx-auto d-block" src="../images/hlogo2.png" alt="" height="auto" width="100%">
                <img class="img-fluid mx-auto d-block" src="../images/hlogo3.png" alt="" height="auto" width="100%">
                <img class="img-fluid mx-auto d-block" src="../images/hlogo4.png" alt="" height="auto" width="100%">
                <img class="img-fluid mx-auto d-block" src="../images/hlogo5.png" alt="" height="auto" width="100%">
                <img class="img-fluid mx-auto d-block" src="../images/hlogo6.png" alt="" height="auto" width="100%">
            </div>
        </div>
        <div class="col-md-4 my-4 py-4 ">
            <img class="ilv-center-block" src="https://rlasean.iluva.website/wp-content/uploads/2019/04/knowing.png">
        </div>
        <div class="col-md-8 my-4 py-4">
            <div class="text-center">
                <h2>THAM GIA VÀO GIA ĐÌNH RLASEAN</h2>
                <p class="">Is simply dummy text of the
                    printing
                    and typesetting industry Lorem Ipsum has been the industry’s standard.Is simply dummy text
                    of
                    the printing and typesetting industry Lorem Ipsum has been the industry’s standard.Is simply
                    dummy text of the printing and typesetting industry Lorem Ipsum has been the industry’s
                    standard.Is
                </p>
            </div>
            <div class="row">
                <a class="ilv-center-block btn btn-outline-primary" target="_self" href="https://rlasean.iluva.website/tuyen-dung/"><span
                        class="fusion-button-text">TÌM HIỂU THÊM</span></a>
            </div>
        </div>
    </div>
</div>


@endsection
