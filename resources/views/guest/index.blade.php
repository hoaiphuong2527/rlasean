@extends('layouts.main')
@section('title')
{{ __('page_text.home.page_title') }}
@endsection
@section('meta')
<meta charset="UTF-8">
<meta name="description" content="{{ $option->name }}">
<meta name="keywords" content="{{ $option->name }}">
<meta name="author" content="{{ $option->name }}">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta property="og:url" content="{{ route('guest_home.index.'.app()->getLocale()) }}" />
<meta property="og:type" content="article" />
<meta property="og:title" content="{{ __('page_text.home.page_title') }}" />
<meta property="og:description" content="{{ __('page_text.home.page_title') }}" />
<meta property="og:image" content="{{ $option->linkImage('main_logo') }}" />
@endsection
@section('content')
<!-- Slideshow Start -->
@include('slideshow.guest.slideshow')
<div class="container">
    <div class="container mt-5 ilv-container-benefit pt-4 pb-4 ilv-mb-d-none">
        <div class="row">
            <div class="col-md-3">
                <div class="row">
                    <div class="col-md-3">
                        <img src="{{ asset('images/icons/7ngay.png') }}">
                    </div>
                    <div class="col-md-9">
                        <div class="ilv-font-weight-500">Chất lượng đảm bảo</div>
                        <div class="ilv-font-size-small">Chuyên nghiệp - Tốc độ</div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <div class="col-md-3">
                        <img src="{{ asset('images/icons/chinhhang.png') }}">
                    </div>
                    <div class="col-md-9">
                        <div class="ilv-font-weight-500">Trả góp 0% lãi suất</div>
                        <div class="ilv-font-size-small">Chuyên nghiệp - Tốc độ</div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <div class="col-md-3">
                        <img src="{{ asset('images/icons/vanchuyen.png') }}">
                    </div>
                    <div class="col-md-9">
                        <div class="ilv-font-weight-500">Thanh toán online</div>
                        <div class="ilv-font-size-small">Chuyên nghiệp - Tốc độ</div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <div class="col-md-3">
                        <img src="{{ asset('images/icons/vanchuyen.png') }}">
                    </div>
                    <div class="col-md-9">
                        <div class="ilv-font-weight-500">Giao hàng tận nơi</div>
                        <div class="ilv-font-size-small">Chuyên nghiệp - Tốc độ</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container mt-5 ilv-container-benefit pt-4 pb-4 ">
        <div class="row">
            <div class="container mb-4">
                <h4>Danh mục sản phẩm</h4>
                <div class="col-md-12">
                    <div class="owl-carousel owl-init slide-items" id="slide_custom_nav" data-auto-play="true"
                        data-custom-nav="custom-nav-first" data-items-desk="9" data-items-tab="4" ata-items-mobile="4"
                        data-margin="20" data-nav="false">
                        @foreach($categories as $category)
                        <div>
                            <a class="btn-outline-primary btn mx-2 cate-icon" href="{{$category->urlGuestShow()}}">
                                <img src="{{$category->linkIcon() }}">
                                <p class="my-2">{{$category->getAttributeLang('name') }}</p>
                            </a>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- Slideshow End -->
    <div class="my-5 container">
        @include('categories.guest.product.tabs_with_carousel', ['categories' => $categories])
    </div>
    @if(count($product_has_promotion))
    <div class="container mt-5 ilv-container-benefit pt-4 pb-4 ">
            @include('categories.guest.product.tabs_with_carousel_single', ['title' => trans('component_text.labels.product_promotion'), 'urlGuestShow' => route('product_promotion.index.'.app()->getLocale()), 'list' => $product_has_promotion])
    </div>
    @endif
    <div class="container my-5 ilv-container-benefit pt-4 pb-4 ">
            @include('categories.guest.product.tabs_with_carousel_single', ['title' => trans('component_text.labels.new_product'), 'urlGuestShow' => route('new_product.index.'.app()->getLocale()), 'list' => $latest_product])
    </div>
     <!-- <div class="container my-4">
        <div class="row">
            <div class="col-md-12">
                @include('guest.component.title', ['title' => __('page_text.home.post')])
            </div>
        </div>
    </div>  -->

     <!-- @include('post.guest.component.list', ['posts' => $posts])

     <div class="container my-3">
        @include('guest.component.static_image_slide')
    </div>
     -->
    {{-- DOI TAC TIEU BIEU --}}
 <!-- <div class="container my-4">
        <div class="row">
            <div class="col-md-12">
                @include('guest.component.title', ['title' => __('page_text.home.partner_feature')])
            </div>
        </div>
    </div>

    <div class="container mb-4">
        <div class="col-md-12">
            <div class="owl-carousel owl-theme c-theme c-owl-nav-center" auto-play="true" loop="true" data-rtl="false"
                data-items-desk="9" data-items-tab="6" data-items-mobile="3" data-slide-speed="8000">
                @foreach($partners as $partner)
                <img class="img-fluid mx-auto d-block" src="{{ $partner->linkImage() }}" alt="" height="auto"
                    width="100%">
                @endforeach
            </div>
        </div>
    </div> -->
</div>
@endsection
