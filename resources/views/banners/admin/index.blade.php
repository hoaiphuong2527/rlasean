@section('title')
Banner
@endsection
@extends('layouts.admin')
@section('content')
@if (session('status'))
<div class="alert alert-info">{{session('status')}}</div>
@endif
<div class="m-portlet">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    Quản lý banner
                </h3>
            </div>
        </div>
    </div>
    <form class="m-form m-form--fit m-form--label-align-right" method="POST" action="{{ route('admin_banners.update') }}"
        enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="m-portlet__body">
            <div class="form-group m-form__group">
                    <h4>Hình ảnh</h4>
                    <div class="row">
                        <h5 class="col">
                            <strong>Lưu ý:</strong>
                            Chiều dài và chiều rộng của ảnh upload nên theo tỉ lệ 3:1
                        </h5>
                    </div>
                    <div class="row">
                            <h5  class="col">
                                <strong>Ví dụ:</strong>
                                    Chiều dài : 1500px<br>
                                    Chiều rộng : 500px
                            </h5>
                        </div>
                @foreach($banners as $key=> $banner)
                <div class="row pt-3 pb-3">
                    <div class="col-md-4">
                        <label>{!! $banner->name !!}</label>
                    </div>
                    
                    <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                        <script>
                            var previewImage{{$key}} = []
                            var single{{$key}} = {!! collect($banner->getImg()) !!}
                        </script>
                    <div id="react-admin-upload-btn-{{$key}}" data-img="" data-input-name="{{ $banner->key }}" data-type="single"></div>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="m-portlet__foot m-portlet__foot--fit">
                <div class="m-form__actions text-center">
                    <input type="submit" class="btn btn-primary" value="Lưu">
                </div>
            </div>
        </div>
    </form>
</div>
@endsection