@extends('layouts.admin')
@section('content')
@section('title')
Chỉnh sửa thông tin danh mục
@endsection
@if (session('status'))
<div class="alert alert-info">{{session('status')}}</div>
@endif
<div class="m-portlet">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    Chỉnh sửa thông tin danh mục
                </h3>
            </div>
        </div>
    </div>
    <form class="m-form m-form--fit m-form--label-align-right" method="POST" action="{{ $item->urlAdminUpdate() }}"
        enctype="multipart/form-data">
        <input type="hidden" name="_method" value="PUT" /> {{ csrf_field() }}
            <div class="m-portlet m-portlet--tabs">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-tools">
                        <ul class="nav nav-tabs m-tabs m-tabs-line  reload-page m-tabs-line--right m-tabs-line-danger"
                            role="tablist">
                            <li class="nav-item m-tabs__item">
                                <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_portlet_tab_1_1"
                                    role="tab" aria-selected="false">
                                    Thông tin cơ bản
                                </a>
                            </li>
                            <li class="nav-item m-tabs__item">
                                <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_portlet_tab_1_2" role="tab"
                                    aria-selected="false">
                                    Đa ngôn ngữ
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <div class="tab-content">
                        <div class="tab-pane active" id="m_portlet_tab_1_1">
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                                        <div class="form-group m-form__group">
                                            <label for="exampleInputPassword1">Tên danh mục</label>
                                            <input type="text" class="form-control m-input m-input--square" id=""
                                                placeholder="Tên danh mục" name="name" value="{{ old('name',$item->name) }}">
                                            @if ($errors->has('name'))
                                            <span class="invalid-feedback" style="display:block;">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    @include('categories.component.option')

                                </div>
                                <div class="col-6">
                                    <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                                        <div class="form-group m-form__group">
                                            <label>Icon</label>
                                            <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                                                <script>
                                                    var previewImage = []
                                                    var single = {!! collect($item->getImg('icon')) !!}
                                                </script>
                                            <div id="react-admin-upload-icon-btn" data-img="" data-input-name="icon" data-type="single"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                                        <div class="form-group m-form__group">
                                            <label>Hình ảnh</label>
                                            <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                                                <script>
                                                    var previewImage1 = []
                                                    var single1 = {!! collect($item->getImg('image')) !!}
                                                </script>
                                            <div id="react-admin-upload-img-btn" data-img="" data-input-name="image" data-type="single"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if($item->context_type == config('ecommerce.context_type.product'))
                            <script>
                                var allInfo = {!! json_encode( $item->getArrayInformations() ) !!}
                                var categoryId =  {!! $item->id !!}
                                var information_array = {!! collect($information_id) !!}
                            </script>
                            <div id="admin-react-information">

                            </div>
                            {{-- <div class="form-group m-form__group col-lg-6 col-md-6 col-sm-6">
                                    <div class="form-group m-form__group">
                                        <label for="exampleInputPassword1">Thông số sản phẩm</label>
                                        <div class="row">
                                            @if(!empty($item->informations))
                                            @foreach($item->informations as $inf)
                                            <div class="col-6">
                                                <span>{{ $inf->name }}</span>
                                            </div>
                                            <div class="col-6">
                                                @foreach($inf->getRecordByKey() as $row)
                                                <div>
                                                    <span>{{ $row->getDisplayName() }} </span>
                                                </div>
                                                @endforeach
                                                <input class="form-control m-input m-input--square" name="value">
                                                <input class="form-control m-input m-input--square" name="name"> <!-- ten hien thi -->
                                            </div>
                                            @endforeach
                                            @endif
                                        </div>
                                        <input type="text" class="form-control m-input m-input--square" id=""
                                        placeholder="" name="key" value="">
                                    </div>
                                </div>   --}}
                            @endif
                            <!-- <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group m-form__group">
                                    <label for="col-form-label col-lg-3 col-sm-12">Mô tả</label>
                                    <textarea class="form-control myPost" rows="20" cols="30" name="description">{{old('description',$item->description)}}</textarea>
                                    @if ($errors->has('description'))
                                    <span class="invalid-feedback" style="display:block;">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div> -->
                        </div>
                        <div class="tab-pane " id="m_portlet_tab_1_2">
                            <div class="m-portlet m-portlet--tabs">
                                <div class="m-portlet__head">
                                    <div class="m-portlet__head-tools">
                                        <ul class="nav nav-tabs m-tabs m-tabs-line  reload-page m-tabs-line--right m-tabs-line-danger"
                                            role="tablist">
                                            @foreach(config('ecommerce.locale_displays') as $key => $lang)
                                            <li class="nav-item m-tabs__item">
                                                <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_portlet_tab_lang_{{$key}}"
                                                    role="tab" aria-selected="false">
                                                    {{ $lang }}
                                                </a>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                                <div class="m-portlet__body">
                                    <div class="tab-content">
                                        @foreach(config('ecommerce.locales') as $key => $lang)
                                        <div class="tab-pane" id="m_portlet_tab_lang_{{ $lang }}">
                                            @include('components.lang',['item' => $item, 'lable' => 'Tên danh
                                            mục','col_name' => 'name','type' => 'input'])
                                            <!-- @include('components.lang',['item' => $item, 'lable' => 'Mô tả','col_name'=>'description','type' => 'summernote']) -->
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="m-form__actions text-center">
                <input type="submit" class="btn btn-info" name="continue" value="Lưu & tạo mới">
                @if($item->status != 1)
                <input type="submit" class="btn btn-success" name="publish" value="Đăng">
                <input type="submit" class="btn btn-warning" name="draff" value="Lưu nháp">
                @else
                <input type="submit" class="btn btn-success" name="publish" value="Lưu & Đăng">
                @endif
            </div>
    </form>
</div>
@endsection
