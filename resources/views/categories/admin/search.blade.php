@extends('layouts.admin')
@section('content')
@section('title')
Kết quả tìm kiếm
@endsection
@if(Session::has('status'))
<div class="alert alert-info">
    {{ Session::get('status') }}
</div>
@endif
@if(Session::has('destroy'))
<div class="alert alert-success">
    {{ Session::get('destroy') }}
</div>
@endif
<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    Quản lý danh mục
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                @if($type_cate == config('ecommerce.context_type.product'))
                <li class="m-portlet__nav-item">
                    <form method="post" action="{{ route('admin_category.store',['type' => $type_cate]) }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="context_type" value="{{ config('ecommerce.context_type.product') }}">
                        <input type="submit" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air"
                            value="Thêm mới" />
                    </form>
                </li>
                @elseif($type_cate == config('ecommerce.context_type.post'))
                <li class="m-portlet__nav-item">
                    <form method="post" action="{{ route('admin_category.store',['type' => $type_cate]) }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="context_type" value="{{ config('ecommerce.context_type.post') }}">
                        <input type="submit" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air"
                            value="Thêm mới" />
                    </form>
                </li>
                @else
                @endif
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <form class="row" method="GET" action="">

            <div class="col-5">
                <div class="form-group m-form__group row">
                    <label class="col-form-label col-lg-3 col-sm-12">Tên danh mục</label>
                    <div class="col-lg-9 col-md-9 col-sm-12">
                        <input class="form-control m-input m-input--square" type="text" name="name" value="{{ $name }}">
                        <input class="d-none" value="{{ $type_cate }}" name="type" />
                    </div>
                </div>
            </div>
            <div class="col-3">
                <div class="form-group m-form__group row">
                    <input type="submit" class="btn btn-info" value="Tìm kiếm">
                </div>
            </div>
        </form>
        <div class="m_datatable m-datatable m-datatable--default m-datatable--loaded" style="">
            <table class="m-datatable__table" style="display: block; min-height: 300px; overflow-x: auto;">
                <thead class="m-datatable__head">
                    <tr class="m-datatable__row" style="left: 0px;">
                        <th class="m-datatable__cell--center m-datatable__cell  ">
                            <span style="width: 50px;"> #</span>
                        </th>
                        <th class="m-datatable__cell--center m-datatable__cell  ">
                            <span style="width: 200px;"> Tên danh mục</span>
                        </th>
                        <th class="m-datatable__cell--center m-datatable__cell  ">
                            <span style="width: 100px;"> Danh mục cha </span>
                        </th>
                        <th class="m-datatable__cell--center m-datatable__cell ">
                            <span style="width: 100px ;">Trạng thái</span>
                        </th>
                        <th class="m-datatable__cell--center m-datatable__cell ">
                            <span style="width: 100px ;">Hành động</span>
                        </th>
                    </tr>
                </thead>
                <tbody style="" class="m-datatable__body">
                    @foreach($categories as $item)
                    <tr class="m-datatable__row" style="left: 0px;">
                        <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 50px ;">{{
                                $item->id }}</span></td>

                        <td class="m-datatable__cell"> <span style="width: 200px ;">
                                <b>{{ $item->name_format }}</b>
                            </span></td>
                        <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 100px ;">
                                {{ $item->getNameParent() }}
                            </span></td>
                        <td class="m-datatable__cell--center m-datatable__cell">
                            <span style="width: 100px ;">
                                @if($item->status != 2)
                                <div data-toggle="tooltip" data-placement="top" style="display: inline-block;" title="Đóng/mở trạng thái danh mục">
                                    <div class="checkbox">
                                        <label class="m-switch m-switch--outline m-switch--primary">
                                            <input data-id="{{$item->id}}" class="chkCategory" type="checkbox"
                                                @if($item->status) checked
                                            @endif>
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                                @else
                                N/A
                                @endif
                            </span>
                        </td>
                        <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 100px ;">
                                <div class=" ilv-btn-group">
                                    <div data-toggle="tooltip" data-placement="top" style="display: inline-block;"
                                        title="Chỉnh sửa thông tin danh mục">
                                        <a class="btn btn-sm btn-warning" href="{{ $item->urlAdminEdit() }}">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                    </div>
                                    @include('categories.component.btn_delete')
                                </div>
                            </span>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $categories->links() }}
        </div>
    </div>
</div>
@endsection
