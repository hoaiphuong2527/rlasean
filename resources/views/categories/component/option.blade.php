<div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
    <div class="form-group m-form__group">
        <label for="exampleSelect1">Danh mục cha</label>
        <select class="form-control m-input m-input--square" name="parent_id">
                @if($item->context_type == config('ecommerce.context_type.product'))
                <option value="1" @if(1 == $item->parent_id) selected @endif> Sản Phẩm </option>
                @else if($item->context_type == config('ecommerce.context_type.post'))
                <option value="2" @if(2 == $item->parent_id) selected @endif> Bài viết </option>
                @endif
            @foreach( $categories as $cate)
                <option value="{{ $cate->id }}" @if($cate->id == $item->parent_id) selected @endif>{{ $cate->renderName() }}</option>
                @include('categories.component.child_option',['categories' => $cate->children])   
            @endforeach
        </select>
    </div>
</div>
