
@foreach($categories as $cate)
<option value="{{ $cate->id }}" @if($cate->id == $item->parent_id) selected @endif>
        &nbsp;&nbsp;
    {{ $cate->renderName() }}</option>
    @if(count($cate->children) > 0)
        @include('categories.component.child_option',['categories' => $cate->children])   
    @endif
@endforeach