@foreach($categories as $item)
<tr class="m-datatable__row" style="left: 0px;">
    <td class="m-datatable__cell"> <span style="width: 200px ;">
            <b>{{ $item->name_format }}</b>
        </span></td>
    <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 100px ;">
            {{ $item->getNameParent() }}
        </span></td>
    <td class="m-datatable__cell--center m-datatable__cell">
        <span style="width: 100px ;">
            @if($item->status != 2)
            <div data-toggle="tooltip" data-placement="top" style="display: inline-block;" title="Đóng/mở trạng thái danh mục">
                <div class="checkbox">
                    <label class="m-switch m-switch--outline m-switch--primary">
                        <input data-id="{{$item->id}}" class="chkCategory" type="checkbox" @if($item->status) checked
                        @endif>
                        <span></span>
                    </label>
                </div>
            </div>
            @else
            N/A
            @endif
        </span>
    </td>
    <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 100px ;">
            <div class=" ilv-btn-group">
                <div data-toggle="tooltip" data-placement="top" style="display: inline-block;" title="Chỉnh sửa thông tin danh mục">
                    <a class="btn btn-sm btn-warning" href="{{ $item->urlAdminEdit() }}">
                        <i class="fa fa-edit"></i>
                    </a>
                </div>
                @include('categories.component.btn_delete')
            </div>
        </span>
    </td>
</tr>
@include('categories.component.children_in_index_view',['categories' => $item->children])
@endforeach
