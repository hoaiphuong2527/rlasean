@foreach($categories as $item)
<tr class="m-datatable__row" style="left: 0px;">
    <!-- <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 50px ;">{{ $item->id }}</span></td> -->

    <td class=" m-datatable__cell"> <span style="width:200px ;">
            &nbsp;&nbsp;{{ $item->renderName() }}
        </span></td>
    <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 100px ;">
            {{ $item->getNameParent() }}
        </span></td>
    <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 100px ;">
            <div class="checkbox">
                <label class="m-switch m-switch--outline m-switch--primary">
                    <input data-id="{{$item->id}}" class="chkCategory" type="checkbox" @if($item->status) checked
                    @endif>
                    <span></span>
                </label>
            </div>
        </span></td>
    <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 100px ;">
            <div class=" ilv-btn-group">
                <a class="btn btn-sm btn-warning" href="{{ $item->urlAdminEdit() }}">
                    <i class="fa fa-edit"></i>
                </a>
                @include('categories.component.btn_delete')
            </div>
        </span>
    </td>
</tr>
@if(count($item->children) > 0)
@include('categories.component.children_in_index_view',['categories' => $item->children])
@endif
@endforeach
