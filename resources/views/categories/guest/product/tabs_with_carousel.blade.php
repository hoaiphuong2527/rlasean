<div class="row">
    @foreach($categories as $category)
    <div class="container mb-5 ilv-container-benefit pt-4 pb-4 ">
            <header class="row">
            <div class="col-12">
        <div class="title-text">
            <span class="h4">{{$category->getAttributeLang('name') }}</span>
            <div class="btn-group btn-group-sm float-right">
                <a class="btn  btn-outline-primary" href="{{$category->urlGuestShow()}}">@lang('component_text.buttons.see_more')</a>
            </div>
        </div>
        </div>
    </header>
    <!-- ============== owl slide items 2  ============= -->
    @php
    $list = $category->product_has_variants();
    $list2 = $category->product_has_variants()->take(4)
    @endphp
    <div class="row">
        <div class="col-md-12 d-none d-lg-block">
                <div class="owl-carousel owl-init slide-items" id="slide_custom_nav" data-auto-play="true"
                data-custom-nav="custom-nav-first" data-items-desk="4" data-items-tab="4" data-items-mobile="2"
                data-margin="20" data-dots="true" data-nav="false">
                @foreach($list as $item)
                @include('categories.guest.product.component.card')
                @endforeach
            </div>
        </div>
        @foreach($list2 as $item)
        <div class="col-6 px-1 d-lg-none">
            @include('categories.guest.product.component.card')
        </div>
        @endforeach
    </div>
    </div>
    @endforeach
    <div class="clearfix"></div>
</div>
