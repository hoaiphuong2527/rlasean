    <div class="row">
        <header class="col-12">
            <div class="title-text">
                <span class="h4">{{ $title }}</span>
                <div class="btn-group btn-group-sm float-right">
                    <a class="btn  btn-outline-primary"
                        href="{{ $urlGuestShow }}">@lang('component_text.buttons.see_more')</a>
                </div>
            </div>
        </header>
        <!-- ============== owl slide items 2  ============= -->

        <div class="col-md-12 d-none d-lg-block">
            <div class="owl-carousel owl-init slide-items" id="slide_custom_nav" data-auto-play="true"
                data-custom-nav="custom-nav-first" data-items-desk="4" data-items-tab="4" data-items-mobile="2"
                data-margin="20" data-dots="true" data-nav="false">
                @foreach($list as $item)
                @include('categories.guest.product.component.card')
                @endforeach
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
