<div class="container">
    <div class="c-content-person-1-slider" data-slider="owl">
        <div class="owl-carousel owl-theme c-theme c-owl-nav-center"  data-rtl="false"
            data-items-desk="3" data-items-tab="2" data-items-mobile="1" data-slide-speed="8000">
            @foreach($list as $item)
            @include('categories.guest.product.component.card')
            @endforeach
        </div>
    </div>
</div>