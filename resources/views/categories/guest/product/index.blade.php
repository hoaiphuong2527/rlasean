@extends('layouts.main')
@section('title')
{{ __('page_text.all_products.page_title') }}
@endsection
@section('meta')
<meta property="og:url" content="{{ route('guest_all_categories.index.'.app()->getLocale()) }}" />
<meta property="og:type" content="article" />
<meta property="og:title" content="{{ __('page_text.all_products.page_title') }}" />
<meta property="og:description" content="{{ __('page_text.all_products.page_title') }}" />
<meta property="og:image" content="{{ $option->linkImage('main_logo') }}" />
@endsection
@section('content')

@include('guest.component.banner_title', ['title' => __('page_text.category.page_title'), 'imageLink' =>
$item->linkImage()])
<div class="container">
    {!! Breadcrumbs::render('guest_all_categories.index.'.app()->getLocale())!!}
</div>

<div class="container ilv-container-benefit pt-4 pb-4 ">
    <div class="row">
            <div class="col-md-12">
                <div class="owl-carousel owl-init slide-items" id="slide_custom_nav" data-auto-play="true"
                    data-custom-nav="custom-nav-first" data-items-desk="9" data-items-tab="4" ata-items-mobile="4"
                    data-margin="20" data-nav="false">
                    @foreach($categories as $category)
                    <div>
                        <a class="btn-outline-primary btn mx-2 cate-icon" href="{{$category->urlGuestShow()}}">
                            <img src="{{$category->linkIcon() }}">
                            <p class="my-2">{{$category->getAttributeLang('name') }}</p>
                        </a>
                    </div>
                    @endforeach
                </div>
        </div>
    </div>

</div>
<div class="container my-5 ilv-container-benefit pt-4 pb-4 ">

@foreach ($categories as $category)
<div class="container mt-2">
    @include('guest.component.title_link', ['title' => $category->name, 'link' =>
    $category->urlGuestShow()])
</div>

<div class="container mt-2 text-center ">
    @php
    $list = $category->product_has_variants()
    @endphp
    <div class="owl-carousel owl-init slide-items" id="slide_custom_nav" data-auto-play="true"
        data-custom-nav="custom-nav-first" data-items-desk="4" data-items-tab="4" data-items-mobile="2" data-margin="20"
        data-dots="true" data-nav="false">
        @foreach($list as $item)
        @include('categories.guest.product.component.card')
        @endforeach
    </div>
    @include('guest.component.button_primary', ['text' => __('component_text.buttons.see_all'), 'link' =>
    $category->urlGuestShow()])
    <hr>
</div>
@endforeach
</div>
@endsection
