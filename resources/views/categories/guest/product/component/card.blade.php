    <figure class="card card-product my-1 m-lg-1">
        @if ($item->isValidCode())
        <span class="badge-offer">
            <b> {{ $item->getPromotionShowInView() }}</b>
        </span>
        @endif
        <div class="thumbnail item img-wrap">
            <a href="{{ $item->urlGuestShow() }}" class="blog-thumb-img">
                <img src="{{ $item->linkImage() }}" class="img-responsive">
            </a>
        </div>
        <figcaption class="info-wrap caption">
            <h4 class="title">
                <a href="{{ $item->urlGuestShow() }}"><p class="js-clamp">{{ $item->getAttributeLang('name') }}</p></a>
            </h4>
            {{-- <p class="desc">{{ $item->short_description }}</p> --}}
        </figcaption>
        <div class="bottom-wrap">
            @if ($item->isValidCode())
                    <span class="price" itemprop="offers" itemscope="">
                        <span itemprop="price" content="{{ $item->discount_price_display }}">{{ $item->discount_price_display }}</span>
                    </span>
                    <span class="ilv-promotion-price"> {{ $item->getPrice() }}</span>
            @else
                <span class="price" itemprop="offers" itemscope="">
                    <span itemprop="price" content="{{ $item->getPrice() }}">{{  $item->getPrice() }}</span>
                </span>
                @endif
                <br>
                <div class="row">
                        <div class="ilv-center-block">
                    @foreach ($item->product_variants as $variant)
                        <span class="ilv-color-tag mx-1" style="background-color:{{ $variant->color }}">&nbsp;</span>
                    @endforeach
                    </div>
                </div>
        </div> <!-- bottom-wrap.// -->
    </figure>
