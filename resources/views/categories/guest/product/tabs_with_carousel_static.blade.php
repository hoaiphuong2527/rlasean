<div class="container">
    <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item">
            <a class="{{ 'nav-link ilv-text text-uppercase active'}}" data-toggle="tab" href="#static-tab-1">@lang('page_text.home.new_arrivals')</a>
        </li>
        <li class="nav-item">
            <a class="{{ 'nav-link ilv-text text-uppercase '}}" data-toggle="tab" href="#static-tab-2">@lang('page_text.home.best_seller')</a>
        </li>
        <li class="nav-item">
            <a class="{{ 'nav-link ilv-text text-uppercase '}}" data-toggle="tab" href="#static-tab-3">@lang('page_text.home.special')</a>
        </li>
    </ul>
    <div class="tab-content">
        <div id="static-tab-1" class="tab-pane active"><br>
            @php($list = $new_arrivals)
            <div class="c-content-person-1-slider" data-slider="owl">
                <div class="owl-carousel owl-theme c-theme c-owl-nav-center"  data-rtl="false"
                    data-items-desk="3" data-items-tab="2" data-items-mobile="1" data-slide-speed="8000">
                    @foreach($new_arrivals as $item)
                        @include('categories.guest.product.component.card')
                    @endforeach
                </div>
            </div>
        </div>
        <div id="static-tab-2" class="tab-pane fade"><br>
            @php($list = $best_sellers)
            <div class="c-content-person-1-slider" data-slider="owl">
                <div class="owl-carousel owl-theme c-theme c-owl-nav-center"  data-rtl="false"
                    data-items-desk="3" data-items-tab="2" data-items-mobile="1" data-slide-speed="8000">
                    @foreach($list as $item)
                        @include('categories.guest.product.component.card')
                    @endforeach
                </div>
            </div>
        </div>
        <div id="static-tab-3" class="tab-pane fade"><br>
            @php($list = $specials)
            <div class="c-content-person-1-slider" data-slider="owl">
                <div class="owl-carousel owl-theme c-theme c-owl-nav-center"  data-rtl="false"
                    data-items-desk="3" data-items-tab="2" data-items-mobile="1" data-slide-speed="8000">
                    @foreach($list as $item)
                        @include('categories.guest.product.component.card')
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
