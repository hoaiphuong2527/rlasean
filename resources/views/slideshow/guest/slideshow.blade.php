<div id="home" class="slider-area">
    <div class="bend niceties preview-2">
        <div id="ensign-nivoslider" class="slides">
            @foreach($slideshows as $item)
            <a class="" href="{{ $item->url }}">
                <img src="{{ $item->linkImage() }}" alt="" title="#slider-direction-{{ $item->id }}" />
            </a>
            @endforeach
        </div>
    </div>
</div>
