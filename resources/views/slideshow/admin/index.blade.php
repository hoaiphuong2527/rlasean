@section('title') Slideshow @endsection @extends('layouts.admin') @section('content')
@if (session('status'))
    <div class="alert alert-info">{{session('status')}}</div>
@endif
@if (session('destroy'))
    <div class="alert alert-success">{{session('destroy')}}</div>
@endif
<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    Danh sách slideshow
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <a  href="{{ route('admin_slideshow.create') }}" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air"
                        >Thêm mới</a>
                </li>
                <li class="m-portlet__nav-item"></li>
                <li class="m-portlet__nav-item"></li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <!--begin: Datatable -->
        <div class="m_datatable m-datatable m-datatable--default m-datatable--loaded" style="">
            <table class="m-datatable__table" style="display: block; min-height: 300px; overflow-x: auto;">
                <thead class="m-datatable__head">
                    <tr class="m-datatable__row" style="left: 0px;">
                        <th class="m-datatable__cell--center m-datatable__cell "> <span style="width: 100px ;">#</span></th>
                        <th class="m-datatable__cell--center m-datatable__cell "> <span style="width: 250px ;">Hình ảnh</span></th>
                        <th class="m-datatable__cell--center m-datatable__cell "> <span style="width: 100px ;">Link</span></th>
                        <th class="m-datatable__cell--center m-datatable__cell "> <span style="width: 100px ;">Đóng/mở</span></th>
                        <th class="m-datatable__cell--center m-datatable__cell "> <span style="width: 100px ;">Hành động</span></th>
                    </tr>
                </thead>
                <tbody style="" class="m-datatable__body ilv-sort-slideshow">
                    @php $index = 1 @endphp
                    @foreach($slideshows as $item)
                    <tr class="m-datatable__row" style="left: 0px;" id="{{ $item->id }}">
                        <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 100px ;">{{
                                $index }}</span></td>
                        <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 250px ;">
                                <img src="{{ $item->linkImage()}}" alt="" class="img-thumbnail thumb-fixed-height img-responsive" width="200px;">
                            </span></td>
                        <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 100px ;">
                                <a href="{{ $item->url }}">Link </a>
                            </span></td>
                        <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 100px ;">
                                <div data-toggle="tooltip" data-placement="top" style="display: inline-block;" title="Đóng/mở trạng thái hình ảnh">
                                <div class="checkbox">
                                    <label class="m-switch m-switch--outline m-switch--primary">
                                        <input data-id="{{$item->id}}" class="chkStatusSlideshow" type="checkbox"
                                            @if($item->status == 1) checked @endif>
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                            </span></td>
                        <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 100px ;">
                                <div class=" ilv-btn-group">
                                    <div data-toggle="tooltip" data-placement="top" style="display: inline-block;" title="Chỉnh sửa thông tin đôi tượng">
                                        <a class="btn btn-sm btn-warning" href="{{ route('admin_slideshow.edit', ['id' => $item->id]) }}">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                    </div>
                                    @include('slideshow.component.btn_delete_slide')
                                </div>
                            </span></td>
                    </tr>
                    @php $index++ @endphp
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
