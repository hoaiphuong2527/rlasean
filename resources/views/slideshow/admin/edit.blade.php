@section('title')
Chỉnh sửa slideshow
@endsection
@extends('layouts.admin')
@section('content')
@if (session('status'))
<div class="alert alert-info">{{session('status')}}</div>
@endif
<div class="m-portlet">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    Cập nhật thông tin
                </h3>
            </div>
        </div>
    </div>
    <form class="m-form m-form--fit m-form--label-align-right" method="POST"
        action="{{ route('admin_slideshow.update',['id' => $item->id]) }}" enctype="multipart/form-data">
        <input type="hidden" name="_method" value="PUT" /> {{ csrf_field() }}
        <div class="m-portlet__body">
            <div class="form-group m-form__group row">
                <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                    <div class="form-group m-form__group">
                        <h4>Hình ảnh</h4>
                        <div class="row">
                            <h5  class="col">
                                <strong>Lưu ý:</strong>
                                Chiều dài và chiều rộng của ảnh upload nên theo tỉ lệ 3:1
                            </h5>
                        </div>
                        <div class="row">
                            <h5  class="col">
                                    <strong>Ví dụ:</strong>
                                        Chiều dài : 1500px<br>
                                        Chiều rộng : 500px
                                </h5>
                            </div>
                        <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                            <script>
                                var previewImage = []
                                var single = {!! collect($item->getImg()) !!}
                            </script>
                            <div id="react-admin-upload-icon-btn" data-img="" data-input-name="image"
                                data-type="single"></div>
                        </div>
                    </div>
                </div>
                <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                    <div class="form-group m-form__group">
                        <h4 for="col-form-label col-lg-3 col-sm-12">Đường dẫn</h4>
                        <input name="url" type="text" class="form-control m-input" value="{{ old('url',$item->url) }}">
                        @if ($errors->has('url'))
                        <span class="invalid-feedback" style="display:block;">
                            <strong>{{ $errors->first('url') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="m-portlet__foot m-portlet__foot--fit">
                <div class="m-form__actions text-center">
                    <input type="submit" class="btn btn-primary" value="Lưu">
                </div>
            </div>
        </div>
    </form>
</div>
@endsection
