@extends('layouts.admin')
@section('content')
@section('title')
Thông số sản phẩm
@endsection
@if(Session::has('status'))
<div class="alert alert-info">
    {{ Session::get('status') }}
</div>
@endif
@if(Session::has('destroy'))
<div class="alert alert-success">
    {{ Session::get('destroy') }}
</div>
@endif
<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    Thông số sản phẩm
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <form method="post" action="{{ route('admin_information.store') }}">
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air"
                            value="Thêm mới" />
                    </form>
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <form class="row" method="GET" action="">
            <div class="col-5">
                <div class="form-group m-form__group row">
                    <label class="col-form-label col-lg-3 col-sm-12">Tên thuộc tính</label>
                    <div class="col-lg-9 col-md-9 col-sm-12">
                        <input style="width:100%" class="form-control m-input m-input--square" type="text" name="name"
                            value="{{ $name }}">
                    </div>
                </div>
            </div>
            {{-- <div class="col-4">
                <div class="form-group m-form__group row">
                    <label class="col-form-label col-lg-3 col-sm-12">Loại</label>
                    <div class="col-lg-9 col-md-9 col-sm-12">
                        <select class="form-control" id="m_select2_2_modal" name="key">
                            @foreach(config('ecommerce.fillter_information_display') as $key => $type)
                            <option value="{{ $key }}" @if($key==$text_key) selected @endif>{{ $type }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div> --}}
            <div class="col-3">
                <div class="form-group m-form__group row">
                    <input type="submit" class="btn btn-info" value="Tìm kiếm">
                </div>
            </div>
        </form>
        <div class="m_datatable m-datatable m-datatable--default m-datatable--loaded" style="">
            <table class="m-datatable__table" style="display: block; min-height: 300px; overflow-x: auto;">
                <thead class="m-datatable__head">
                    <tr class="m-datatable__row" style="left: 0px;">
                        <th class="m-datatable__cell--center m-datatable__cell  ">
                            <span style="width: 50px;"> #</span>
                            </span></th>
                        <th class="m-datatable__cell--center m-datatable__cell  ">
                            <span style="width: 100px;"> Loại </span>
                            </span></th>
                        <th class="m-datatable__cell--center m-datatable__cell  ">
                            <span style="width: 100px;"> Tên hiển thị/NSX </span>
                            </span></th>
                        <th class="m-datatable__cell--center m-datatable__cell ">
                            <span style="width: 100px ;"> Giá trị </span></th>
                        <th class="m-datatable__cell--center m-datatable__cell ">
                            <span style="width: 150px ;">Hành động</span></th>
                    </tr>
                </thead>
                <tbody style="" class="m-datatable__body">
                    <?php $index = 1; ?>
                    @foreach($infos as $row)
                    <tr class="m-datatable__row" style="left: 0px;">
                        <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 50px  ;">{{
                                $index }}</span></td>
                        <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 100px ;">
                                {{ $row->key }}
                            </span></td>
                        <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 100px ;">
                                {{ $row->name }}
                            </span></td>
                        <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 100px ;">
                                {{ $row->value }}
                            </span></td>
                        <td class="m-datatable__cell--center m-datatable__cell"> <span style="width: 150px ;">
                                <div class=" ilv-btn-group">
                                    @include('information.admin.create')
                                    <div data-toggle="tooltip" data-placement="top" style="display: inline-block;" title="Chỉnh sửa thông tin">
                                        <a class="btn btn-sm btn-warning" href="{{ $row->urlAdminEdit() }}">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                    </div>
                                    @include('information.admin.btn_delete')
                                </div>
                            </span></td>
                    </tr>
                    <?php $index++ ?>
                    @endforeach
                </tbody>
            </table>


        </div>
    </div>
</div>
@endsection
