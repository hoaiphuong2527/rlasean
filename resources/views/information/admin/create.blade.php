<div data-toggle="tooltip" data-placement="top" style="display: inline-block;" title="Thêm mới từ đây">
    <a class="btn btn-sm btn-info" href="" data-toggle="modal" data-target="{{ '#create' . $row->id }}">
        <i class="fa fa-plus-square"></i>
    </a>
</div>
<form method="post" action="{{ route('admin_information.store') }}">
    {!! csrf_field() !!}
    <div class="modal fade" id="{{ 'create' . $row->id }}" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content  -->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Thêm mới</h4>
                    <label data-dismiss="modal" style="cursor: pointer;"><i class="fa fa-times"></i></label>
                </div>
                <div class="modal-body">
                    <p>Dữ liệu mới sẽ có "Loại" và "Tên hiển thị" giống với đối tượng bạn đã chọn</p>
                    <div class="form-group m-form__group row text-left" >
                        <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                            <div class="form-group m-form__group">
                                @if ($errors->has('name'))
                                <span class="invalid-feedback" style="display:block;">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @endif
                                <label for="col-form-label col-lg-3 col-sm-12">Loại</label>
                                <input name="key" type="text" class="form-control m-input" value="{{ $row->key }}" readonly>
                                <span class="invalid-feedback" style="display:block;">
                                    <strong></strong>
                                </span>
                            </div>
                        </div>
                        <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                            <div class="form-group m-form__group">
                                @if ($errors->has('key'))
                                <span class="invalid-feedback" style="display:block;">
                                    <strong>{{ $errors->first('key') }}</strong>
                                </span>
                                @endif
                                <label for="col-form-label col-lg-3 col-sm-12">Tên hiển thị/NSX</label>
                                <input name="name" type="text" class="form-control m-input" value="{{ $row->name }}" readonly>
                                <span class="invalid-feedback" style="display:block;">
                                    <strong></strong>
                                </span>
                            </div>
                        </div>
                        <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                            <div class="form-group m-form__group">
                                @if ($errors->has('value'))
                                <span class="invalid-feedback" style="display:block;">
                                    <strong>{{ $errors->first('value') }}</strong>
                                </span>
                                @endif
                                <label for="col-form-label col-lg-3 col-sm-12">Giá trị</label>
                                <input name="value" type="text" class="form-control m-input" value="{{ old('value') }}">
                                <span class="invalid-feedback" style="display:block;">
                                    <strong></strong>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-outline-danger active">Lưu</button>
                    <button type="button" class="btn btn-outline-brand active" data-dismiss="modal">Hủy bỏ</button>
                </div>
            </div>
        </div>
    </div>
</form>
