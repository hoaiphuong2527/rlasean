@extends('layouts.admin') @section('content')
@if(Session::has('success'))
<div class="alert alert-success">
    {{ Session::get('success') }}
</div>
@endif
<div class="m-portlet">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    Chỉnh sửa thông tin
            </div>
        </div>
    </div>
    <form class="m-form m-form--fit m-form--label-align-right" method="POST" action="{{ $info->urlAdminUpdate() }}"
        enctype="multipart/form-data">
        <input type="hidden" name="_method" value="PUT" /> {{ csrf_field() }}
        <div class="m-portlet__body">
            <div class="form-group m-form__group row">
                <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                    <div class="form-group m-form__group">
                        @if ($errors->has('key'))
                        <span class="invalid-feedback" style="display:block;">
                            <strong>{{ $errors->first('key') }}</strong>
                        </span>
                        @endif
                        <label for="col-form-label col-lg-3 col-sm-12">Loại</label>
                        <input name="key" type="text" class="form-control m-input" value="{{ old('key',$info->key) }}">
                        <span class="invalid-feedback" style="display:block;">
                            <strong></strong>
                        </span>
                    </div>
                </div>
                <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                    <div class="form-group m-form__group">
                        @if ($errors->has('name'))
                        <span class="invalid-feedback" style="display:block;">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                        @endif
                        <label for="col-form-label col-lg-3 col-sm-12">Tên hiển thị/NSX</label>
                        <input name="name" type="text" class="form-control m-input" value="{{ old('name',$info->name) }}">
                        <span class="invalid-feedback" style="display:block;">
                            <strong></strong>
                        </span>
                    </div>
                </div>
                <div class="form-group m-form__group col-lg-12 col-md-12 col-sm-12">
                    <div class="form-group m-form__group">
                        @if ($errors->has('value'))
                        <span class="invalid-feedback" style="display:block;">
                            <strong>{{ $errors->first('value') }}</strong>
                        </span>
                        @endif
                        <label for="col-form-label col-lg-3 col-sm-12">Giá trị</label>
                        <input name="value" type="text" class="form-control m-input" value="{{ old('value',$info->value) }}">
                        <span class="invalid-feedback" style="display:block;">
                            <strong></strong>
                        </span>
                    </div>
                </div>
            </div>
            <div class="m-portlet__foot m-portlet__foot--fit">
                <div class="m-form__actions text-center">
                    <input type="submit" class="btn btn-primary" name="publish" value="Lưu">
                </div>
            </div>
        </div>
    </form>
</div>
@endsection
