<button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="{{ '#delete' . $row->id }}">
    <i class="fa fa-trash-alt"></i>
</button>
<form method="post" action="{!! $row->urlAdminDestroy() !!}">
    <input type="hidden" name="_method" value="DELETE" /> {!! csrf_field() !!}
    <div class="modal fade" id="{{ 'delete' . $row->id }}" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content  -->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Cảnh báo</h4>
                    <label data-dismiss="modal" style="cursor: pointer;"><i class="fa fa-times"></i></label>
                </div>
                <div class="modal-body">
                    <p>Dữ liệu này sẽ bị xóa vĩnh viễn. Bạn có chắc là muốn xóa không</p>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-outline-danger active">Đồng ý</button>
                    <button type="button" class="btn btn-outline-brand active" data-dismiss="modal">Không đồng ý</button>
                </div>
            </div>
        </div>
    </div>
</form>
