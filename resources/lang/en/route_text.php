<?php

return [
    /*
    |--------------------------------------------------------------------------
    | HƯỚNG DẪN SỬ DỤNG:
    | - Bạn chỉnh sửa các câu chữ bạn cần thay đổi sau đó nhấn nút "LƯU".
    | - Lưu ý: PHẢI giữ nguyên cấu trúc của file không được thay đổi
    |--------------------------------------------------------------------------
    |
    |
    */





     /*
    |--------------------------------------------------------------------------
    | ĐƯỜNG DẪN
    |--------------------------------------------------------------------------
    |
    |
    */
    
    'routes' => [
        'get_post_by_category'          => 'category/posts',
        'posts'                         => 'posts',
        'category'                      => 'category',
        'product_detail'                => 'product-detail',
        'contact'                       => 'contact',
        'order'                         => 'orders',
        'order_successfully'            => 'order-successfully',
        'partners'                      => 'partners',
        'categories'                    => 'all-category',
        'corporate_gift'                => 'corporate-gift',
        'warranty'                      => 'warranty',
        'search'                        => 'search',
        'product_promotion'             => 'promotion-products',
        'new_product'                   => 'new-products'
    ],
    





     /*
    |--------------------------------------------------------------------------
    | BREADCRUMBS
    |--------------------------------------------------------------------------
    |
    |
    */
    
    'breadcrumbs' => [
        'home'                          => 'Home'
    ],
];