<?php

return [
     /*
    |--------------------------------------------------------------------------
    | HƯỚNG DẪN SỬ DỤNG:
    | - Bạn chỉnh sửa các câu chữ bạn cần thay đổi sau đó nhấn nút "LƯU".
    | - Lưu ý: PHẢI giữ nguyên cấu trúc của file không được thay đổi
    |--------------------------------------------------------------------------
    |
    |
    */




    /*
    |--------------------------------------------------------------------------
    | LABELS
    |--------------------------------------------------------------------------
    |
    |
    */
    'labels' => [
        'full_name'         => 'Full Name',
        'firstname'         => 'First Name',
        'lastname'          => 'Last Name',
        'phone'             => 'Phone Number',
        'amount'            => 'How many products are you registering?',
        'purchase_date'     => 'Purchase Date',
        'location'          => 'Purchase Location',
        'yes'               => 'Yes',
        'no'                => 'No',
        'invite'            => 'May we invite you join our Inner Circle?',
        'company'           => 'Company',
        'website'           => 'Website',
        'country'           => 'Country',
        'message'           => 'Messages',
        'address'           => 'Address',
        'product_promotion' => 'Product has promotion',
        'new_product'       => 'New product',
    ],





    /*
    |--------------------------------------------------------------------------
    | PLACEHOLDERS
    |--------------------------------------------------------------------------
    |
    |
    */
    'placeholders' => [
        'full_name'         => 'Full Name',
        'firstname'         => 'First Name',
        'lastname'          => 'Last Name',
        'email'             => 'Email',
        'phone'             => 'Phone Number',
        'register_email'    => 'Email register',
        'company'           => 'Enter your company',
        'website'           => 'Enter your website',
        'country'           => 'Enter your country',
        'message'           => 'Messages'
    ],





    /*
    |--------------------------------------------------------------------------
    | BUTTON
    |--------------------------------------------------------------------------
    |
    |
    */
    'buttons' => [
        'send'              => 'Submit',
        'contact_us'        => 'Contact us',
        'see_all'           => 'See all',
        'register'          => 'Register',
        'go_back_homepage'  => 'Go back Homepage',
        'search'            => 'Search',
        'see_more'          => 'See more'
    ],
    




    /*
    |--------------------------------------------------------------------------
    | HEADER
    |--------------------------------------------------------------------------
    |
    |
    */
    'header' => [
        'warrant'               => 'Warrant',
        'posts'                 => 'Posts',
        'contact'               => 'Contact',
        'partner'               => 'Partner',
        'account'               => 'Account:',
        'sign_in'               => 'Sign in',
        'my_account'            => 'My Account',
        'sign_up'               => 'Sign up',
        'about_us'              => 'About Us',
        'products'              => 'Products',
        'english'               => 'English',
        'vietnam'               => 'Tiếng Việt'
    ],
    




    /*
    |--------------------------------------------------------------------------
    | FOOTER
    |--------------------------------------------------------------------------
    |
    |
    */
    'footer' => [
        'contact'       => 'Contact',
        'head_office'   => [
            'label'     => 'Head office: ',
        ],
        'showroom'      => [
            'label'     => 'Showroom: ',
            'content'   => '64/4 Street D3, Van Thanh Bac, Ward 25, Binh Thanh District, Ho Chi Minh City'
        ],
        'hotline'       => [
            'label'     => 'Hotline: ',
            'content'   => '(028)2216.04.27 - 0926.87.87.99 - 0931.87.87.99'
        ],
        'business'      => [
            'label'     => 'Wholesale business: (Zalo Chat)',
            'content'   => '0931.87.87.99'
        ],
        'technical'     => [
            'label'     => 'Technical support / Warranty: ',
            'content'   => '0926.87.87.99'
        ],
        'linkage'       => 'LINKAGE',
        'payment'       => 'PAYMENT',
        'info'          => 'INFOMATION',
        'policy'        => 'POLICY',
        'suport'        => 'SUPPORT',
        'email'         => [
            'label'     => 'HÃY ĐỂ LẠI MAIL NHẬN NHIỀU ƯU ĐÃI HƠN',
            'content'   => '* Hãy để lại email để nhận được những khuyến mãi mới nhất'
        ],
        'PBX'           => [
            'label'     => 'PBX: ',
            'content'   => '(028)2216.04.27',
        ],
        'consult1'       => [
            'label'     => 'Support: ',
            'content'   => '0931.87.87.99',
        ],
        'consult2'       => [
            'label'     => 'Support: ',
            'content'   => '0931.87.87.99',
        ],
        'label1'        => '',
        'label2'        => '',
        'copyright'     => 'Coppyright',
        'twitter_acc'   => '',
        'google_acc'    => ''         
    ],
    




    /*
    |--------------------------------------------------------------------------
    | FILLTER
    |--------------------------------------------------------------------------
    |
    |
    */
    'fillters' => [
        'search_by' => 'Search By:',
        'names' => [
            'asc'   => 'Name, A to Z',
            'desc'  => 'Name, A to Z'
        ],
        'prices' => [
            'asc'   => 'Price, low to high',
            'desc'  => 'Price, high to low'
        ],
        'count'     => 'Have/has :count product'
    ],
    




    /*
    |--------------------------------------------------------------------------
    | ALERTS
    |--------------------------------------------------------------------------
    |
    |
    */
    'alerts' => [
        'contact' => 'Send request success!'
    ]
];