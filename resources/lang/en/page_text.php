<?php

return [
     /*
    |--------------------------------------------------------------------------
    | HƯỚNG DẪN SỬ DỤNG:
    | - Bạn chỉnh sửa các câu chữ bạn cần thay đổi sau đó nhấn nút "LƯU".
    | - Lưu ý: PHẢI giữ nguyên cấu trúc của file không được thay đổi
    |--------------------------------------------------------------------------
    |
    |
    */







     /*
    |--------------------------------------------------------------------------
    | Tên trang
    |--------------------------------------------------------------------------
    |
    |
    */
    'pages'  => [
        'warrant'           => 'Warrant',
        'product_by_cate'   => 'Product in category ',
        'contact'           => 'Contact'
    ],





     /*
    |--------------------------------------------------------------------------
    | Trang Bảo hành
    |--------------------------------------------------------------------------
    |
    |
    */
    'warrant' => [
        'title'       => '',
        'register' => 'Please submit your product below',
        'page_title' => 'Warranty'
    ],

    'contact' => [
        'page_title'        => 'Contact',
        'contact_with_us'   => 'Contact us',
    ],

    'post' => [
        'page_title'    => 'Post',
        'by'            => 'By ',
        'most_read'     => 'Most read',
        'news_letter'   => 'Register news',
        'related_post'  => 'Related post',
        'new_post'      => 'New post'
    ],
    
    'home' => [
        'page_title'        => 'Homepage',
        'post'              => 'Post',
        'partner_feature'   => 'Feature partner',
        'new_arrivals'      => 'New Arrivals',
        'best_seller'       => 'Best Sellers',
        'special'           => 'Specials'
    ],

    'product_detail' => [
        'description'               => 'Description',
        'specification'             => 'Specification',
        'overview'                  => 'Overview',
        'compatibility'             => 'Compatibility'
    ],

    'partner'   => [
        'page_title'    => 'Partner'
    ],
    





    /*
    |--------------------------------------------------------------------------
    | Sản phẩm theo danh mục
    |--------------------------------------------------------------------------
    |
    |
    */
    'product_by_cate' => [
        'category'       => 'Categories',
    ],






    /*
    |--------------------------------------------------------------------------
    | Chi tiết đơn hàng
    |--------------------------------------------------------------------------
    |
    |
    */
    'order_infomation' => [
        'page_title'        => 'Thank for booking',
        'your_order'        => 'Your order',
        'customer_infor'    => "Customer's infomation",
        'full_name'         => 'Full Name',
        'date'              => 'Date booking',
        'phone'             => 'Phone',
        'address'           => 'Address',
        'payment_type'      => 'Payment type',
        'note'              => 'Note',
        'id'                => '#',
        'product_name'      => "Product's name",
        'amount'            => 'Amount',
        'price'             => 'Price',
        'total'             => 'Total',
        'ship'              => 'Shipping cost',
        'breakable_cost'    => 'Breakable cost',
        'total_pay'         => 'Summary',
        'disscount'         => 'Disscount'
    ],





    /*
    |--------------------------------------------------------------------------
    | Tất cả sản phẩm
    |--------------------------------------------------------------------------
    |
    |
    */
    'all_products' => [
        'page_title'       => 'Products',
    ],
    




    /*
    |--------------------------------------------------------------------------
    | Quà tặng
    |--------------------------------------------------------------------------
    |
    |
    */
    'corporate_gift' => [
        'page_title'        => 'Corporate Gift',
        'contact_with_us'   => 'Contact with us'
    ],
        




    /*
    |--------------------------------------------------------------------------
    | chi tiết sản phẩm
    |--------------------------------------------------------------------------
    |
    |
    */
    'product_details' => [
        'product_seen'        => 'Viewed products'
    ],





    /*
    |--------------------------------------------------------------------------
    | tìm kiếm
    |--------------------------------------------------------------------------
    |
    |
    */
    'search' => [
        'search'        => 'Search product',
        'not_found'     => 'Not found any product',
        'key'           => 'Result '
    ],

    /*
    |--------------------------------------------------------------------------
    | Quản lý đơn hàng
    |--------------------------------------------------------------------------
    |
    |
    */
    'manageorders'=>[
        'page_title' =>'Order Management',
        'myorders'  => 'My Orders',
        'order_number'=>'Order Number',
        'purchase_date' => 'Purchase Date',
        'summary'       => 'Summary',
        'status'        => 'Status',
        'paid'        => 'Paid',
        'unpaid'        => 'Unpaid',
        'title_single'  => 'Order Detail'
        
     ]
];