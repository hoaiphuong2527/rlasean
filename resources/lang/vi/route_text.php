<?php

return [
    /*
    |--------------------------------------------------------------------------
    | HƯỚNG DẪN SỬ DỤNG:
    | - Bạn chỉnh sửa các câu chữ bạn cần thay đổi sau đó nhấn nút "LƯU".
    | - Lưu ý: PHẢI giữ nguyên cấu trúc của file không được thay đổi
    |--------------------------------------------------------------------------
    |
    |
    */





     /*
    |--------------------------------------------------------------------------
    | ĐƯỜNG DẪN
    |--------------------------------------------------------------------------
    |
    |
    */
    
    'routes' => [
        'get_post_by_category'          => 'bai-viet/danh-muc',
        'posts'                         => 'bai-viet',
        'category'                      => 'danh-muc',
        'product_detail'                => 'chi-tiet-san-pham',
        'contact'                       => 'lien-he',
        'order'                         => 'dat-hang',
        'order_successfully'            => 'dat-hang-thanh-cong',
        'partners'                      => 'doi-tac',
        'categories'                    => 'san-pham',
        'corporate_gift'                => 'qua-tang',
        'warranty'                      => 'bao-hanh',
        'search'                        => 'tim-kiem',
        'product_promotion'             => 'san-pham-khuyen-mai',
        'new_product'                   => 'san-pham-moi',
        'manage_orders'                 => 'quan-ly-don-hang'
    ],

    





     /*
    |--------------------------------------------------------------------------
    | BREADCRUMBS
    |--------------------------------------------------------------------------
    |
    |
    */
    
    'breadcrumbs' => [
        'home'                          => 'Trang chủ'
    ],
];