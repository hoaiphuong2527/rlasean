<?php

return [
     /*
    |--------------------------------------------------------------------------
    | HƯỚNG DẪN SỬ DỤNG:
    | - Bạn chỉnh sửa các câu chữ bạn cần thay đổi sau đó nhấn nút "LƯU".
    | - Lưu ý: PHẢI giữ nguyên cấu trúc của file không được thay đổi
    |--------------------------------------------------------------------------
    |
    |
    */


     /*
    |--------------------------------------------------------------------------
    | Tên trang
    |--------------------------------------------------------------------------
    |
    |
    */
    'pages'  => [
        'warrant'           => 'Bảo hành',
        'product_by_cate'   => 'Sản phẩm thuộc ',
        'contact'           => 'Contact'
    ],

     /*
    |--------------------------------------------------------------------------
    | Trang Bảo hành
    |--------------------------------------------------------------------------
    |
    |
    */

    'category' => [
        'category' => 'Danh Mục Sản Phẩm',
        'page_title' => 'Sản phẩm'
    ],
    
    'warrant' => [
        'title'         => '',
        'register'      => 'Vui lòng gửi sản phẩm của bạn theo form bên dưới',
        'page_title'    => 'Bảo hành'
    ],

    'contact' => [
        'page_title'        => 'Liên hệ',
        'contact_with_us'   => 'Liên hệ với chúng tôi',
        'section_text_1' => 'Sử dụng tùy chọn thuận tiện nhất bên dưới để liên hệ với chúng tôi',
        'call_us' => 'GỌI CHO CHÚNG TÔI',
        'working_time' => 'Thứ Hai tới thứ Bảy, 8:00 - 17:30',
        'live_chat' => 'Live Chat',
        'email' => 'Email',
        'feedback' => 'Hãy phản hồi tới ILUVA, chúng tôi luôn luôn muốn lắng nghe khách hàng. agency@iluva.net',
        'info' => 'Thông tin liên hệ'
    ],

    'post' => [
        'page_title'    => 'Tin tức',
        'by'            => 'Được đăng bởi ',
        'most_read'     => 'Bài xem nhiều nhất',
        'news_letter'   => 'Đăng ký nhận tin',
        'related_post'  => 'Bài viết liên quan',
        'new_post'      => 'Bài viết mới nhất'
    ],
    
    'home' => [
        'page_title'        => 'Trang chủ',
        'post'              => 'Tin tức',
        'partner_feature'   => 'Đối tác tiêu biểu',
        'new_arrivals'      => 'Sản phẩm mới',
        'best_seller'       => 'Bán chạy',
        'special'           => 'Đặc biệt'
    ],

    'product_detail' => [
        'description'               => 'Thông tin sản phẩm',
        'specification'             => 'Thông số kỹ thuật',
        'overview'                  => 'Tổng quan',
        'compatibility'             => 'Tương thích'
    ],
    'partner' => [
        'page_title' => 'Đối tác',
        'section_1_title' => 'Bạn muốn trở thành đối tác của chúng tôi?',
        'section_1_desc' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua
        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua'
    ],




    /*
    |--------------------------------------------------------------------------
    | Sản phẩm theo danh mục
    |--------------------------------------------------------------------------
    |
    |
    */
    'product_by_cate' => [
        'category'       => 'Danh Mục Sản Phẩm',
    ],
    





    /*
    |--------------------------------------------------------------------------
    | Chi tiết đơn hàng
    |--------------------------------------------------------------------------
    |
    |
    */
    'order_infomation' => [
        'page_title'        => 'Đặt hàng thành công',
        'your_order'        => 'Đơn hàng của bạn',
        'customer_infor'    => 'Thông tin người mua',
        'full_name'         => 'Họ tên',
        'date'              => 'Ngày đặt',
        'phone'             => 'Số điện thoại',
        'address'           => 'Địa chỉ giao hàng',
        'payment_type'      => 'Phương thức thanh toán',
        'note'              => 'Ghi chú',
        'id'                => '#',
        'product_name'      => 'Tên sản phẩm',
        'amount'            => 'Số lượng',
        'price'             => 'Đơn giá',
        'total'             => 'Tổng giá',
        'ship'              => 'Phí vận chuyển',
        'breakable_cost'    => 'Phí hàng dễ vỡ',
        'total_pay'         => 'Tổng trả',
        'disscount'         => 'Giảm giá'

    ],





    /*
    |--------------------------------------------------------------------------
    | Tất cả sản phẩm
    |--------------------------------------------------------------------------
    |
    |
    */
    'all_products' => [
        'page_title'       => 'Sản phẩm',
    ],
    




    /*
    |--------------------------------------------------------------------------
    | Quà tặng
    |--------------------------------------------------------------------------
    |
    |
    */
    'corporate_gift' => [
        'page_title'        => 'Quà tặng doanh nghiệp',
        'contact_with_us'   => 'Liên lạc với chúng tôi'
    ],
     




    /*
    |--------------------------------------------------------------------------
    | chi tiết sản phẩm
    |--------------------------------------------------------------------------
    |
    |
    */
    'product_details' => [
        'product_seen'        => 'Sản phẩm đã xem'
    ],
    




    /*
    |--------------------------------------------------------------------------
    | tìm kiếm
    |--------------------------------------------------------------------------
    |
    |
    */
    'search' => [
        'search'        => 'Tìm kiếm sản phẩm',
        'not_found'     => 'Không tìm thấy kết quả',
        'key'           => 'Kết quả tìm kiếm cho từ khóa '
    ],
    /*
    |--------------------------------------------------------------------------
    | tìm kiếm
    |--------------------------------------------------------------------------
    |
    |
    */
    /*
    |--------------------------------------------------------------------------
    | Quản lý đơn hàng
    |--------------------------------------------------------------------------
    |
    |
    */
     'manageorders'=>[
        'page_title' =>'Quản lý đơn hàng',
        'myorders'  => 'Đơn hàng của tôi',
        'order_number'=>'Mã đơn hàng',
        'purchase_date' => 'Ngày mua',
        'summary'       => 'Tổng tiền',
        'status'        =>'Trạng thái',
        'paid'        => 'Đã thanh toán',
        'unpaid'        => 'Chưa thanh toán',
        'title_single'  => 'Chi tiết đơn hàng'
        
     ]
    /*
    |--------------------------------------------------------------------------
    | Quản lý đơn hàng
    |--------------------------------------------------------------------------
    |
    |
    */
];