<?php

return [
     /*
    |--------------------------------------------------------------------------
    | HƯỚNG DẪN SỬ DỤNG:
    | - Bạn chỉnh sửa các câu chữ bạn cần thay đổi sau đó nhấn nút "LƯU".
    | - Lưu ý: PHẢI giữ nguyên cấu trúc của file không được thay đổi
    |--------------------------------------------------------------------------
    |
    |
    */




    /*
    |--------------------------------------------------------------------------
    | LABELS
    |--------------------------------------------------------------------------
    |
    |
    */
    'labels' => [
        'full_name'         => 'Họ và Tên',
        'firstname'         => 'Họ',
        'lastname'          => 'Tên',
        'email'             => 'Email',
        'phone'             => 'Số điện thoại',
        'amount'            => 'Số lượng sản phẩm đã mua',
        'purchase_date'     => 'Ngày mua',
        'location'          => 'Địa điểm mua',
        'yes'               => 'Có',
        'no'                => 'Không',
        'invite'            => 'Bạn có muốn tham gia vào Inner Circle không?',
        'message'           => 'Lời nhắn',
        'address'           => 'Địa chỉ',
        'company'           => 'Công ty',
        'website'           => 'Website',
        'country'           => 'Quốc gia',
        'product_promotion' => 'Sản phẩm khuyến mãi',
        'new_product'       => 'Sản phẩm mới',
    ],
    
    



    /*
    |--------------------------------------------------------------------------
    | PLACEHOLDERS
    |--------------------------------------------------------------------------
    |
    |
    */
    'placeholders' => [
        'full_name'         => 'Họ và Tên',
        'firstname'         => 'Họ',
        'lastname'          => 'Tên',
        'email'             => 'Email',
        'phone'             => 'Số điện thoại',
        'register_email'    => 'Đăng ký email',
        'company'           => 'Nhập tên công ty',
        'website'           => 'Nhập địa chỉ website',
        'country'           => 'Nhập quốc gia',
        'message'           => 'Tin nhắn'
    ],





    /*
    |--------------------------------------------------------------------------
    | BUTTON
    |--------------------------------------------------------------------------
    |
    |
    */
    'buttons' => [
        'send'              => 'Gửi',
        'contact_us'        => 'Liên lạc với chúng tôi',
        'see_all'           => 'Xem tất cả',
        'register'          => 'Đăng ký',
        'go_back_homepage'  => 'Quay về trang chủ',
        'search'            => 'Tìm kiếm',
        'see_more'          => 'Xem thêm'
    ],
    




    /*
    |--------------------------------------------------------------------------
    | HEADER
    |--------------------------------------------------------------------------
    |
    |
    */
    'header' => [
        'warrant'               => 'Bảo hành',
        'posts'                 => 'Tin tức',
        'contact'               => 'Liên hệ',
        'partner'               => 'Đối tác',
        'account'               => 'Tài khoản:',
        'sign_in'               => 'Đăng nhập',
        'my_account'            => 'Tài khoản của tôi',
        'sign_up'               => 'Đăng xuất',
        'about_us'              => 'Về chúng tôi',
        'products'              => 'Sản phẩm',
        'english'               => 'English',
        'vietnam'               => 'Tiếng Việt'
    ],
    




    /*
    |--------------------------------------------------------------------------
    | FOOTER
    |--------------------------------------------------------------------------
    |
    |
    */
    'footer' => [
        'contact'              => 'Liên hệ',
        'head_office'   => [
            'label'     => 'Head office: ',
        ],
        'showroom'      => [
            'label'     => '',
            'content'   => ''
        ],
        'hotline'       => [
            'label'     => 'Hotline: ',
            'content'   => '0898428523'
        ],
        'business'      => [
            'label'     => 'Kinh doanh sỉ: (Zalo Chat)',
            'content'   => '0898428523'
        ],
        'technical'     => [
            'label'     => 'Hỗ trợ kỹ thuật/Bảo hành: ',
            'content'   => '0898428523'
        ],
        'linkage'       => 'LIÊN KẾT',
        'payment'       => 'THANH TOÁN',
        'info'          => 'THÔNG TIN',
        'policy'        => 'CHÍNH SÁCH',
        'suport'        => 'HỖ TRỢ',
        'email'         => [
            'label'     => 'HÃY ĐỂ LẠI MAIL NHẬN NHIỀU ƯU ĐÃI HƠN',
            'content'   => '* Hãy để lại email để nhận được những khuyến mãi mới nhất'
        ],
        'PBX'           => [
            'label'     => 'Tổng đài: ',
            'content'   => '0898428523',
        ],
        'consult1'       => [
            'label'     => 'Tư vấn: ',
            'content'   => '0898428523',
        ],
        'consult2'       => [
            'label'     => 'Tư vấn: ',
            'content'   => '0898428523',
        ],
        'label1'        => 'ILUVA VIỆT NAM',
        'label2'        => '66/10, Phạm Ngọc Thạch, phường 06, quận 3, Hồ Chí Minh',
        'copyright'     => 'Bản quyền thuộc về',
        'twitter_acc'   => '',
        'google_acc'    => ''         
    ],
    




    /*
    |--------------------------------------------------------------------------
    | FILLTER
    |--------------------------------------------------------------------------
    |
    |
    */
    'fillters' => [
        'search_by' => 'Tìm kiếm theo:',
        'names' => [
            'asc'   => 'Tên, A đến Z',
            'desc'  => 'Tên, A đến Z'
        ],
        'prices' => [
            'asc'   => 'Giá, thấp đến cao',
            'desc'  => 'Giá, cao đến thấp'
        ],
        'count'     => 'Có :count sản phẩm'
    ],
      




    /*
    |--------------------------------------------------------------------------
    | ALERTS
    |--------------------------------------------------------------------------
    |
    |
    */
    'alerts' => [
        'contact' => 'Gửi thông tin thành công!'
    ]
];