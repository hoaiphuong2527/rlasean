<?php

namespace App\Mail\ToAdmin;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class WarrantyMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $warranty;
    protected $admin;
    public function __construct($warranty,$admin)
    {
        $this->warranty = $warranty;
        $this->admin = $admin;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.to_admin.warranty_mail',['warranty' => $this->warranty,'admin' => $this->admin])->subject('Liên hệ quà tặng');
    }
}
