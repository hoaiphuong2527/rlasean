<?php

namespace App\Mail\ToAdmin;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $order,$user,$name;
    public function __construct($order,$user,$name)
    {
        $this->order = $order;
        $this->user = $user;
        $this->name = $name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.to_admin.order')->with([
            'order' => $this->order,
            'user'  => $this->user,
            'name'  => $this->name
        ])->subject('Đơn hàng mới');
    }
}
