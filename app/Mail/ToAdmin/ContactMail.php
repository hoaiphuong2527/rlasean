<?php

namespace App\Mail\ToAdmin;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $contact;
    protected $admin;
    public function __construct($contact,$admin)
    {
        $this->contact = $contact;
        $this->admin = $admin;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.to_admin.contact_mail',['contact' => $this->contact,'admin' => $this->admin])->subject('Liên hệ');
    }
}
