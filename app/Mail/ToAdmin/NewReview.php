<?php

namespace App\Mail\ToAdmin;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewReview extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $user,$review,$content_mail;
    public function __construct($user,$review,$content_mail)
    {
        $this->user  = $user;
        $this->review = $review;
        $this->content_mail = $content_mail;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.to_admin.review')->with([
            'user'          => $this->user,
            'review'        => $this->review,
            'content_mail'  => $this->content_mail
        ])->subject('Review mới');
    }
}
