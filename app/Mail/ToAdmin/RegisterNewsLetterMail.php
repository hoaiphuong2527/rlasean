<?php

namespace App\Mail\ToAdmin;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RegisterNewsLetterMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $user,$name;
    public function __construct($user,$name)
    {
        $this->user = $user;
        $this->name = $name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.to_admin.news_letter')->with([
            'user'  => $this->user,
            'name'  => $this->name
        ])->subject('Đăng ký nhận tin mới');
    }
}
