<?php

namespace App\Mail\ToGuest;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ReplyMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $contact;
    protected $content;
    public function __construct($contact,$content)
    {
        $this->contact = $contact;
        $this->content = $content;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.to_guest.reply_mail',['contact' => $this->contact,'content' => $this->content])->subject('Thông tin phản hồi');
    }
}
