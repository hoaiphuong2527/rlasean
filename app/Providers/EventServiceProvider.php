<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\GuestSendContactEvent' => [
            'App\Listeners\GuestSendContact',
        ],
        'App\Events\Admin\SendMailContact' => [
            'App\Listeners\Admin\SendMailContactListener',
        ],
        'App\Events\OrderSuccessfullyEvent' => [
            'App\Listeners\OrderSuccessfullyListener',
        ],
        'App\Events\User\SendMailActiveEvent' => [
            'App\Listeners\User\SendMailActiveListener'
        ],
        'App\Events\GuestSendWarrantyEvent' => [
            'App\Listeners\GuestSendWarrantyListener'
        ],
        'App\Events\RegisterNewsLetterEvent' => [
            'App\Listeners\RegisterNewsLetterListener'
        ],
        'App\Events\User\CreateNewUserNatural' => [
            'App\Listeners\User\SendEmailToNaturalUser',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
