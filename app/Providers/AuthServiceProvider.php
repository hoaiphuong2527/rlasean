<?php

namespace App\Providers;

use Laravel\Passport\Passport;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\Models\User;
use Auth;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('can-see',function(User $user,$token){
            $user = User::whereMiddlewareToken($token)->firstOrFail();
            if($user->id == Auth::user()->id)
            return true;
        });
        
        Gate::define('contributor', function (User $user) {
            if($user->role == config('ecommerce.admin_role.contributor')){
                return true;
            }
        });
        Gate::define('editor', function (User $user) {
            if($user->role == config('ecommerce.admin_role.editor')){
                return true;
            }
        });

        Gate::define('admin', function (User $user) {
            if($user->role == config('ecommerce.admin_role.admin')){
                return true;
            }
        });

        Gate::define('controller', function (User $user) {
            if($user->role == config('ecommerce.admin_role.controller')){
                return true;
            }
        });

        Gate::define('CAE', function (User $user) {
            if(($user->role == config('ecommerce.admin_role.controller')) ||
                ($user->role == config('ecommerce.admin_role.admin') || 
                ($user->role == config('ecommerce.admin_role.editor')))){
                return true;
            }
        });

        Gate::define('controller_admin', function (User $user) {
            if(($user->role == config('ecommerce.admin_role.controller')) || ($user->role == config('ecommerce.admin_role.admin'))){
                return true;
            }
        });

        Passport::routes();
    }
}
