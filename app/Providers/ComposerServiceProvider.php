<?php

namespace App\Providers;

use App\Models\Post;
use App\Models\Option;
use App\Models\DynamicMenu;
use App\Models\Products\Product;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use App\Models\Categories\PageCategory;
use Illuminate\Support\Facades\Session;
use App\Models\Categories\ProductCategory;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('shared.public_header', function ($view) {
            $option = Option::first();
            $menu = DynamicMenu::whereParent(0)->orderBy('sort')->get();
            $view->with([
                'urlLogo'=> $option->getLogo(),
                'menu'      => $menu
            ]);
        });
        View::composer('shared.public_footer', function ($view) {
            $category_policy = PageCategory::whereSlug('chinh-sach')->whereStatus(1)->first();
            $category_info  = PageCategory::whereSlug('thong-tin')->whereStatus(1)->first();
            $option = Option::first();
            $view->with([
                'option'            => $option,
                'category_policy'   => $category_policy,
                'category_info'     => $category_info
            ]);
        });
        View::composer('shared.admin_header', function ($view) {
            $option = Option::first();
            $view->with([
                'urlLogo'=> $option->getLogo(),
            ]);
        });
        View::composer(['layouts.main','products.guest.detail','post.guest.single'], function ($view) {
            $option = Option::first();
            $arr_product_id = Session::get('product_seen');
            if($arr_product_id)
                $product_seens = Product::whereHas('product_variant_onlines')->whereStatus(1)->whereIn('id',$arr_product_id)->get();
            else $product_seens = [];
            $view->with([
                'option'=> $option,
                'product_seens' => $product_seens
            ]);
        });
        View::composer('layouts.admin', function ($view) {
            $option = Option::first();
            $view->with([
                'option'=> $option,
            ]);
            $categories = ProductCategory::whereHas('products')->whereStatus(1)->get();
            $view->with('categories',$categories);
            $view->with('contact', Post::findOrFail(1));
            $view->with('about', Post::findOrFail(2));
        });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
