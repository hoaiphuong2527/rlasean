<?php

namespace App\Exports;

use App\Models\Order;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;

class OrdersExport implements FromQuery,WithHeadings
{
    use Exportable;
    /**
    * @return \Illuminate\Support\Collection
    */
    // public function collection()
    // {
    //     return Order::all();
    // }

    public function __construct($request)
    {
        $this->request = $request;
    }

    public function query()
    {
        $query = Order::query();

        $key = $this->request->key;
        $status_payment = $this->request->status_payment;
        $order_status = $this->request->order_status;
        if($key){
            $query->where('order_number', 'like', '%'.$key.'%')
                ->orWhere('customer_name', 'like', '%'.$key.'%')
                ->orWhere('customer_phone', 'like', '%'.$key.'%')
                ->orWhere('customer_email', 'like', '%'.$key.'%');
        }
        if($status_payment && $status_payment != 'all'){
            $query->whereStatusPayment($status_payment);
        }
        if($order_status && $order_status != 'all'){
            $query->whereOrderStatus($order_status);
        }
        if($this->request->start_date && $this->request->end_date){
            $start_date = $this->request->start_date." 00:00:00";
            $end_date    = $this->request->end_date." 23:59:59";
            $query->whereBetween('created_at', array($start_date, $end_date));
        }
        return $query;
    }

    public function headings(): array
    {
        return [
            'STT',
            'Mã ĐH',
            'TT Thanh toán',
            'Ghi chú',
            'Tổng giá',
            'Tên KH',
            'SDT',
            'Email',
            'Địa chỉ',
            'TT Đơn hàng',
            'Ngày đặt',
            'HT thanh toán',
            'Phí vận chuyển',
            'Tổng giá sau khi giảm'
        ];
    }
}
