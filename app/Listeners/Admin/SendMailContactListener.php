<?php

namespace App\Listeners\Admin;

use Mail;
use App\Mail\ToGuest\ReplyMail;
use App\Events\Admin\SendMailContact;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMailContactListener implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SendMailContact  $event
     * @return void
     */
    public function handle(SendMailContact $event)
    {
        $contact = $event->contact;
        $content = $event->content;
        Mail::to($contact->email,$contact->name)
        ->send(new ReplyMail($contact,$content));
    }
}
