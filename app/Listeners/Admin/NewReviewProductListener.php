<?php

namespace App\Listeners\Admin;

use Mail;
use App\Mail\ToAdmin\NewReview;
use App\Models\AdminReceiveMail;
use App\Events\Admin\NewReviewProduct;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewReviewProductListener implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewReviewProduct  $event
     * @return void
     */
    public function handle(NewReviewProduct $event)
    {
        $review = $event->review;
        $content_mail = $event->content_mail;
        foreach(AdminReceiveMail::all() as $admin){
            Mail::to($admin->user->email,$admin->user->name)
            ->send(new NewReview($admin->user,$review,$content_mail));    
        }
    }
}
