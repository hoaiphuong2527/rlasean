<?php

namespace App\Listeners;

use Mail;
use App\Models\AdminReceiveMail;
use App\Mail\ToAdmin\ContactMail;
use App\Events\GuestSendContactEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class GuestSendContact implements ShouldQueue
{
    use InteractsWithQueue;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  GuestSendContactEvent  $event
     * @return void
     */
    public function handle(GuestSendContactEvent $event)
    {
        foreach (AdminReceiveMail::all() as $admin){
            Mail::to($admin->user->email,$admin->user->name)
            ->send(new ContactMail($event->contact,$admin->user));    
        }
    }
}
