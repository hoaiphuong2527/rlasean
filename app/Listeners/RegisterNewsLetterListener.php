<?php

namespace App\Listeners;

use Mail;
use App\Models\AdminReceiveMail;
use App\Mail\ToAdmin\RegisterNewsLetterMail;
use App\Events\RegisterNewsLetterEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RegisterNewsLetterListener implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RegisterNewsLetterEvent  $event
     * @return void
     */
    public function handle(RegisterNewsLetterEvent $event)
    {
        $email = $event->email;

        foreach(AdminReceiveMail::all() as $admin){
            Mail::to($admin->user->email,$admin->user->name)
            ->send(new RegisterNewsLetterMail($admin->user,$email));    
        }
    }
}
