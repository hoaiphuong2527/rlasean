<?php

namespace App\Listeners\User;

use Mail;
use App\Events\User\NoticeActiveSuccessEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Mail\ToUser\ActiveSuccessMail;

class NoticeActiveSuccessListener implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NoticeActiveSuccessEvent  $event
     * @return void
     */
    public function handle(NoticeActiveSuccessEvent $event)
    {
        Mail::to($event->user->email,$event->user->name)
        ->send(new ActiveSuccessMail($event->user));
    }
}
