<?php

namespace App\Listeners\User;

use Mail;
use App\Events\User\CreateNewUserNatural;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Mail\ToUser\InfomationUserNaturalMail;

class SendEmailToNaturalUser implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CreateNewUserNatural  $event
     * @return void
     */
    public function handle(CreateNewUserNatural $event)
    {
        Mail::to($event->user->email,$event->user->name)
        ->send(new InfomationUserNaturalMail($event->user,$event->password));
    }
}
