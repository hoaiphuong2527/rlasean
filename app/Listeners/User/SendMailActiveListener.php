<?php

namespace App\Listeners\User;

use Mail;
use App\Events\User\SendMailActiveEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Mail\ToUser\WelcomeMail;

class SendMailActiveListener implements ShouldQueue
{
    use InteractsWithQueue;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SendMailActiveEvent  $event
     * @return void
     */
    public function handle(SendMailActiveEvent $event)
    {
        Mail::to($event->user->email,$event->user->name)
        ->send(new WelcomeMail($event->user));
    }
}