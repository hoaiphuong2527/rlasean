<?php

namespace App\Listeners;

use Mail;
use App\Models\AdminReceiveMail;
use App\Mail\ToAdmin\OrderMail;
use App\Mail\ToGuest\OrderSuccessMail;
use App\Events\OrderSuccessfullyEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderSuccessfullyListener implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OrderSuccessfullyEvent  $event
     * @return void
     */
    public function handle(OrderSuccessfullyEvent $event)
    {
        $order = $event->order;
        $email = $event->email;
        $name = $event->name;
        Mail::to($email,$name)
        ->send(new OrderSuccessMail($order,$name));
        foreach(AdminReceiveMail::all() as $admin){
            Mail::to($admin->user->email,$admin->user->name)
            ->send(new OrderMail($order,$admin->user,$name));    
        }
    }
}
