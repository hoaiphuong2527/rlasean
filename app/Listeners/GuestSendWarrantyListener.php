<?php

namespace App\Listeners;

use Mail;
use App\Models\AdminReceiveMail;
use App\Mail\ToAdmin\WarrantyMail;
use App\Events\GuestSendWarrantyEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class GuestSendWarrantyListener implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  GuestSendWarrantyEvent  $event
     * @return void
     */
    public function handle(GuestSendWarrantyEvent $event)
    {
        foreach (AdminReceiveMail::all() as $admin){
            Mail::to($admin->user->email,$admin->user->name)
            ->send(new WarrantyMail($event->warranty,$admin->user));    
        }
    }
}
