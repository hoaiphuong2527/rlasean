<?php
namespace App\Helpers;
use App\Helpers\SessionManager;

trait BeautyLink {

    public function linkImage()
    {
        if ($this->image) {
            return $this->image;
        }
        return asset('images/empty_image.jpg');
    }

    public function linkIcon()
    {
        if ($this->icon) {
            return $this->icon;
        }
        return asset('images/empty_image.jpg');
    }
    public function urlAdminEdit()
    {
        return route(self::$urlAdminEdit, ['id' => $this->id]);
    }

    public function urlAdminShow()
    {
        return route(self::$urlAdminShow, ['id' => $this->id]);
    }

    public function urlAdminDestroy()
    {
        return route(self::$urlAdminDestroy, ['id' => $this->id]);
    }

    public function urlAdminUpdate()
    {
        return route(self::$urlAdminUpdate, ['id' => $this->id]);
    }

    public function urlGuestShow(){
        return route(self::$urlGuestShow.'.'. app()->getLocale(),['id' => $this->id, 'slug' => $this->getAttributeLang('slug')]);
    }

    public function getNameFormatAttribute()
    {
        if($this->name)
            return $this->name;
        return "N/A";
    }

    //translate
    public function getAttributeLang($attribute)
    {
        $lang = $this->getLangItemByLocaleAndName(app()->getLocale(),$attribute);
        if($lang && $lang->translate_text != null)
            $text = $lang->translate_text;
        else
            $text = $this->$attribute;
        return $text;
    }

    public function getModel()
    {
        return self::$namespace;
    }
    //end

    public function getTranslateText(){
        $relation = self::$translate_relation;
        $lang = $this->$relation->first();
        if($lang){
            if ($lang->translate_text)
                return true;
        }
        return false;
    }
}
