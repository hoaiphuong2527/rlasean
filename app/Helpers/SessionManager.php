<?php

namespace App\Helpers;
use Illuminate\Support\Facades\Session;

class SessionManager extends Session
{
    public static function setLang($lang){
        Session::put("LANG",$lang);
    }

    public static function getLang(){
        $currentLang = null;
        if(Session::has("LANG")){
            $currentLang  = Session::get("LANG");
        }
        return $currentLang;
    }

    public static function validSession ($key)
    {
        if(Session::has($key)){
           return true;
        }
        return false;
    }
}