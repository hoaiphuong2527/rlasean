<?php
namespace App\Helpers;
use Image;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;

class UploadImage {

    public static function uploadImage($file_name,$image,$path,$slug,$item,$width,$height,$col)
    {
        Image::configure(array('driver' => 'gd'));
        $nameImage = Input::file($file_name)->getClientOriginalExtension();
        $imageURL =  $slug.'.'.date("H_i_s",time()). ".". $nameImage;
        if (! File::exists(public_path().'/'.$path)) {
            File::makeDirectory(public_path().'/'.$path,0777,true);
        }
        if($width == null || $height == null){
            Image::make($image)->encode('jpg', 60)->save(public_path($path . $imageURL),60);
        }else{
            Image::make($image)->encode('jpg', 60)->fit($width,$height)->save(public_path($path . $imageURL),60);
        }
        $oldImage = $item->$col;
        if($oldImage != '')
        {
            if(File::exists(public_path($path) . $oldImage))
            {
                unlink(public_path($path) . $oldImage);
            }
        }
        return $imageURL;

    }

    public static function formatYoutubeLink($link)
    {
        $yt_embed = '';
        preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $link, $match);
        if (isset($match[1])) {
            $youtube_id = $match[1];
            $yt_embed = 'https://youtube.com/embed/' . $youtube_id;
        }
        return $yt_embed;
    }
}
