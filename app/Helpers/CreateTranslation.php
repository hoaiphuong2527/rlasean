<?php
namespace App\Helpers;
use App\Models\Translations\GeneralTranslation;

class CreateTranslation{

    public static function createRecord($model,$item_id,$foreign_key,$col,$request)
    {
        foreach(config('ecommerce.locales') as $key => $locale){
            $model::create([
                'locale'            => $locale,
                'column_name'       => $col,
                'translate_text'    => $request->input('text_translate_'. $col .'_'.$locale),
                $foreign_key        => $item_id
            ]);
        }
    }

    public static function saveLang($col,$request,$item,$model,$foreign_key)
    {
        foreach(config('ecommerce.locales') as $key => $locale){
            $lang = $item->getLangItemByLocaleAndName($locale,$col);
            if(!$lang){
                $new = $model::create([
                    'locale' => $locale,
                    'column_name' => $col,
                    $foreign_key  => $item->id
                ]);
                self::saveText($new,$request,$col,$locale);
            }else{
                self::saveText($lang,$request,$col,$locale);
            }
        }

    }

    public static function saveText($lang,$request,$col,$locale)
    {
        if($col == 'slug'){
            if($lang->translate_text){
                $slug = $request->input('text_translate_slug_'.$locale);
                if($slug)
                    $lang->translate_text = str_slug($request->input('text_translate_slug_'.$locale));
                else
                    $lang->translate_text = str_slug($request->input('text_translate_name_'.$locale));
            }
            else
                $lang->translate_text = str_slug($request->input('text_translate_name_'.$locale));
        }else{
            $lang->translate_text = $request->input('text_translate_'.$col.'_'.$locale);
        }
        $lang->save();
    }

    public static function createPolymorphism($col,$id,$type)
    {
        foreach(config('ecommerce.locales') as $key => $lang){
            GeneralTranslation::create([
                'locale'    => $lang,
                'table_id'  => $id,
                'column_name'   => $col,
                'table_type'    => $type,
            ]);
        }
    }

    public static function updatePolymorphism($col,$request,$item,$model)
    {
        foreach(config('ecommerce.locales') as $key => $locale){
            $lang = $item->getLangItemByLocaleAndName($locale,$col);
            if(!$lang){
                $new = GeneralTranslation::create([
                    'locale'    => $locale,
                    'table_id'  => $item->id,
                    'column_name'   => $col,
                    'table_type'    => $model,
                ]);
                self::saveText($new,$request,$col,$locale);
            }else{
                self::saveText($lang,$request,$col,$locale);
            }
        }
    }
}
