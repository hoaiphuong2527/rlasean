<?php
namespace App\Helpers;

use Illuminate\Http\Request;

trait UpdateSomething {
    
    public function updateStatus($item)
    {
        if ($item == null){
            $data = [
                'status' => -1,
            ];
        }else{
            if($item->status == 0){
                $item->status = 1;   
            }
            else {
                $item->status = 0;
            }
            $data = [
                'status' => 1,
            ];
        }
        $item->save();
        return response()->json($data, 200);
    }

    public function sorting(Request $request,$item)
    {
        $item->sort_feature = $request->input('sort_feature');
        $item->save();
    }

    public function updateFlag($item)
    {
        if ($item == null)
            return response()->json(['error' => 'not found']);
        if($item->flag == 0){
            $item->flag = 1;
        }
        else 
            $item->flag = 0;
        $item->save();
        return response()->json(['success' => 'ok']);
        
    }

    public function updateFeatured( $item)
    {

        if ($item == null)
            return response()->json(['error' => 'not found']);
        if($item->is_featured == 0){
            $item->is_featured = 1;
        }
        else 
            $item->is_featured = 0;
        $item->save();
        return response()->json(['success' => 'ok']);
        
    }
}