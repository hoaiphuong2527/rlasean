<?php

namespace App\Models;

use App\Helpers\BeautyLink;
use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    use BeautyLink;

    public static $urlAdminEdit = 'admin_reviews.edit';
    public static $urlAdminShow = 'admin_reviews.show';
    public static $urlAdminDestroy = 'admin_reviews.destroy';
    public static $urlAdminUpdate = 'admin_reviews.update';

    protected $fillable = [
        'content',
        'reviewtable_id',
        'reviewtable_type',
        'level',
        'status',
        'parent_id',
        'type',
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function reviewtable()
    {
        return $this->morphTo();
    }

    public function parent() {
        return $this->belongsTo('App\Models\Review', 'parent_id', 'id');
    }

    public function children()
    {   
        return $this->hasMany('App\Models\Review', 'parent_id', 'id');
    }

    
    public function children_online()
    {   
        return $this->hasMany('App\Models\Review', 'parent_id', 'id')->whereStatus(1);
    }

    public function getReviewType()
    {
        if($this->reviewtable_type === config('ecommerce.reviewtable_type.product'))
            return config('ecommerce.reviewtable_type_display.product');
        // return config('ecommercer.reviewtable_type_display.post');
    }
}
