<?php

namespace App\Models;
use App\Helpers\BeautyLink;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    use BeautyLink;
    public static $urlAdminEdit = 'admin_contacts.edit';
    public static $urlAdminShow = 'admin_contacts.show';
    public static $urlAdminDestroy = 'admin_contacts.destroy';
    public static $urlAdminUpdate = 'admin_contacts.update';
    
    protected $fillable = [
        'f_name', 
        'email',
        'phone',
        'message',
        'status',
        'l_name',
        'company',
        'website',
        'country',
        'type'
    ];
    /**
     * 
     */

    public function getStatus()
    {
        if($this->status ==0 )
            return "Chưa xem";
        elseif($this->status == 1)
            return "Đã xem";
        return "Đã phản hồi";
    }

    public static function deleteMulti()
    {
        $contacts = Contact::whereIn('status',[1,2])->delete();
    }

    public function getNameAttribute()
    {
        return  "{$this->f_name} {$this->l_name}";
    }
}
