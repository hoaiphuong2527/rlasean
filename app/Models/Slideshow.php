<?php

namespace App\Models;

use App\Helpers\BeautyLink;
use App\Helpers\SearchTrait;
use Illuminate\Database\Eloquent\Model;

class Slideshow extends Model
{
    use BeautyLink, SearchTrait;
    public static $imgPath = 'upload/images/slideshow/';
    public static $urlAdminEdit = 'admin_slideshow.edit';
    public static $urlAdminShow = 'admin_slideshow.show';
    public static $urlAdminDestroy = 'admin_slideshow.destroy';
    public static $urlAdminUpdate = 'admin_slideshow.update';

    protected $fillable = [
        'image',
        'url',
        'status',
        'sort_feature'
    ];
    /**
     * 
     * 
     */
    public function getArrImg()
    {
        $pic = Media::where('link',$this->image)->first();
        if($pic)
            return array($pic);
        return array();
    }

    public function getImg()
    {
        $pic = Media::where('link',$this->image)->first();
        return $pic;
    }
}
