<?php

namespace App\Models\Information;

use Illuminate\Database\Eloquent\Model;

class ProductInformation extends Model
{

    protected $table = 'product_information';

    function product()
    {
        return $this->belongsTo('App\Models\Products\Product');
    }
    function information()
    {
        return $this->belongsTo('App\Models\Informations\Information');
    }
}
