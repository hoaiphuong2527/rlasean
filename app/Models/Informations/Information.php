<?php

namespace App\Models\Informations;

use App\Helpers\BeautyLink;
use Illuminate\Database\Eloquent\Model;
use App\Helpers\SearchTrait;
use App\Models\Products\ProductVariant;

class Information extends Model
{

    use SearchTrait,BeautyLink;
    public static $urlAdminEdit = 'admin_information.edit';
    public static $urlAdminShow = 'admin_information.show';
    public static $urlAdminDestroy = 'admin_information.destroy';
    public static $urlAdminUpdate = 'admin_information.update';

    protected $table = "informations";
    protected $fillable = [
        'name',
        'key',
        'value',
    ];

    public function product_variants(){
        return $this->belongsToMany('App\Models\Products\ProductVariant','product_information');
    }

    function categories(){
        return $this->belongsToMany('App\Models\Categories\ProductCategory','category_information');
    }

    public function getRecordByKey()
    {
        return self::where('key',$this->key)->get();
    }

    public function getRecordByKeyAndId($arr)
    {
        return self::where('key',$this->key)->whereIn('id',$arr)->get();
    }

    public function getDisplayName()
    {
        return $this->name . '-' . $this->value;
    }

    public function getRecordByKeyInVariant($id)
    {        
        $variant = ProductVariant::findOrFail($id);
        $category = $variant->product->category;
        $arr_info_unique = $category->informations->pluck('id')->toArray();
        return self::where('key',$this->key)->whereIn('id',$arr_info_unique)->get();
    }
}
