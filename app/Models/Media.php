<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\Formatter;

class Media extends Model
{
    protected $fillable = [
        'alt', 
        'description',
        'link',
        'user_id',
        'file_name'
    ];
    protected $appends = ['size', 'resolution'];

    public function getSizeAttribute()
    {
        $img_dir = base_path( config('ecommerce.media.upload_dir') ) . DIRECTORY_SEPARATOR . $this->file_name;
        $data = filesize($img_dir);
        $formated_data = Formatter::formatSizeUnits($data);
        return $formated_data;  
    }
    public function getResolutionAttribute() {
        $img_dir = base_path( config('ecommerce.media.upload_dir') ) . DIRECTORY_SEPARATOR . $this->file_name;
        $data = getimagesize($img_dir);
        return $data[0] . 'x' . $data[1];  
    }

    public function user() {
        return $this->belongsTo('App\Models\User');
    }

    public function product_variants()
    {
        return $this->morphedByMany('App\Models\Products\ProductVariant', 'mediable');
    }
}
