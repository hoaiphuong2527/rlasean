<?php

namespace App\Models\Products;

use Illuminate\Database\Eloquent\Model;

class VariantTranslation extends \Eloquent
{

    protected $table = "variant_translations";
    public static $namespace = 'App\Models\Products\VariantTranslation';
    public static $foreign_key = 'product_variant_id';
    protected $fillable = [
        'locale',
        'column_name',
        'translate_text',
        'product_variant_id'
    ];

    public function product_variant() {
        return $this->belongsTo('App\Models\Products\ProductVariant');
    }
}
