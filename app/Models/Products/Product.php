<?php

namespace App\Models\Products;

use Carbon\Carbon;
use App\Helpers\BeautyLink;
use App\Helpers\SearchTrait;
use App\Models\Products\ProductVariant;
use App\Models\Categories\ProductCategory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Translations\ProductTranslation;

class Product extends Model
{
    use SearchTrait,BeautyLink;

    public static $urlAdminEdit = 'admin_product.edit';
    public static $urlAdminShow = 'admin_product.show';
    public static $urlAdminDestroy = 'admin_product.destroy';
    public static $urlAdminUpdate = 'admin_product.update';
    public static $urlGuestShow = 'guest_products.show';
    public static $namespace = 'App\Models\Products\Product';

    protected $fillable = [
        'name',
        'slug',
        'status',
        'short_description',
        'sort_feature',
        'is_featured',
        'category_id',
        'description',
        'specification',
        'overview',
        'compatibility',
        'is_breakable'
    ];

    public static $translate_column = [
        'name',
        'slug',
        'short_description',
        'description',
        'specification',
        'overview',
        'compatibility'
    ];

    // protected $appends = ['images','default_price','discount_price','discount_value','url','colors','sizes'];
    public function getImagesAttribute()
    {
        $arr_galleries = [];
        foreach( $this->product_variant_onlines[0]->media as $img){
            $arr_galleries[] = $img->link;
        }
        return $arr_galleries;
    }

    public function getDefaultPriceAttribute()
    {
        $variant = $this->product_variant_onlines[0];
        $symbol = ' đ';
        $symbol_thousand = '.';
        $decimal_place = 0;
        $pledge_value = number_format( $variant->price, $decimal_place, '', $symbol_thousand);
        return $pledge_value . $symbol;
    }

    public function getDiscountPriceAttribute()
    {
        return $this->product_variant_onlines[0]->getDiscountPrice();
    }

    public function getDiscountPriceDisplayAttribute()
    {
        $symbol = ' đ';
        $symbol_thousand = '.';
        $decimal_place = 0;
        $pledge_value = number_format( $this->discount_price, $decimal_place, '', $symbol_thousand);
        return $pledge_value . $symbol;
    }

    public function getDiscountValueAttribute()
    {
        return $this->getPromotionShowInView();
    }

    public function getUrlAttribute()
    {
        return $this->urlGuestShow();
    }

    public function getColorsAttribute(){
        return $this->getParam('color');
    }

    public function getSizesAttribute(){
        return $this->getParam('sizes');
    }


    public static $translate_relation = "product_translation";
    /**
     * getStatus: display status of item
     * getVariantWithPositionEqual1: get variant has position = 1
     * getTotal: count total varient
     * hasVariants: check item has variant?
     * linkImage:  return link image of variant has position = 1
     * getPrice: format price
     * getProductsByArrayCategory: get products by array category id
     * isOnline:get item has status 1
     * getProductLatestByCategoryId: get product latest by category id
     * updateStatusVariants: update status variants to status of product
     * productCode : get name to  Models ProductCode
     * clone: clone new product,
     * clone_variants: clone variants
     * getBestSeller: get products which is ordered
     * checkCodePromotion: check promotion type and disscount price product
     * isInvalidCode: check product has code return true or fasle
     * getInvalidCode: check product has code return code or null use in api
     */

    
    function productInformation()
    {
        return $this->hasMany('App\Models\Informations\ProductInformation');
    }

    public function productCodes(){
        return $this->hasMany('App\Models\ProductCode');
    }
    // public function codes()
    // {
    //     return $this->belongsToMany('App\Models\Codes\Code','product_code');
    // }
    public static function isOnline()
    {
        return self::whereStatus(1);
    }
    public function hasCode()
    {
        return self::where('id',$this->id)->has('product_codes')->count();
    }

    public static function productHasCode()
    {
        $collection = collect();
        foreach(self::latestProduct() as $product){
            if($product->isValidCode()) $collection->push($product);
        }
        return $collection;
    }

    public static function latestProduct()
    {
        return self::whereHas('category',function($q){
            $q->whereStatus(1);
        })->has('product_variant_onlines')->whereStatus(1)->latest()->get();
    }

    public static function getProductLatestByCategoryId($id)
    {
        return self::has('product_variant_onlines')->whereCategoryId($id)->whereStatus(1)->latest()->first();
    }

    public static function sortProductByAtt($cate_id,$att,$val)
    {
        return self::whereCategoryId($cate_id)->has('product_variant_onlines')->whereStatus(1)->orderBy($att,$val);
    }

    public static function sortProductPrice($id,$val)
    {
        if($val == 'desc')
            $products = self::whereCategoryId($id)->whereStatus(1)->get()->sortByDesc('price');
        else $products = self::whereCategoryId($id)->whereStatus(1)->get()->sortBy('price');

        return $products;
    }

    // public static function getBestSeller()
    // {
    //     return self::has('category')->whereHas('product_variant_onlines',function($query){
    //         $query->has('order_sortings');
    //     })->take(10)->get();
    // }

    public static function getItemBySlug($slug)
    {
        $locales = ['en','vi'];
        if (in_array(app()->getLocale(), $locales)){
            $post = self::whereHas('product_translation', function ($q) use ($slug) {
                $q->whereColumnName('slug')->whereTranslateText($slug);
            })->whereStatus(1)->first();
        }if(!$post){
            $post = self::whereSlug($slug)->first();
        }
        return $post;
    }

    public static function getProductsByArrayCategory(array $arr)
    { //cate
        return self::isOnline()->whereIn('category_id',$arr)->get();
    }

    public static function findMinMaxValueArrayPrice(array $price)
    {
        $min = current($price);
        $max = end($price);
        return $data = [
            'min'   => $min,
            'max'   => $max
        ];
    }

    public static function fillterPrice(array $price)
    { //price
        $value = self::findMinMaxValueArrayPrice($price);
        $min = $value['min'];
        $max = $value['max'];
        if($max){
            $products = self::whereHas('product_variants', function ($q) use ($min,$max) {
                $q->whereStatus(1)->whereBetween('price',[$min, $max]);
            })->whereStatus(1)->get();
        }else {
            // $products = self::whereHas('product_variants', function ($q) use ($min_end) {
            //     $q->whereStatus(1)->where('price','>=',$min_end);
            // })->whereStatus(1)->get();
        }
        return $products;
    }

    public static function fillterCateMaster(array $category_id)
    { //master
        $arr_cate_id = [];
        foreach($category_id as $id){
            $cate = ProductCategory::findOrFail($id);
            $arr_cate_id[] = $cate->children()->pluck('id')->toArray();
        }
        self::getProductsByArrayCategory($arr_cate_id[0]);
    }
    
    public static function filterInformation(array $information_id)
    { //information
        return self::whereHas('product_variants',function($q) use ($information_id){
            $q->whereHas('informations' ,function($q1) use ($information_id){
                $q1->whereIn('information_id',$information_id);
            })->whereStatus(1);
        })->whereStatus(1)->get();
    }
    public static function catePrice($category_id,$price)
    { //cate_price
        $value = self::findMinMaxValueArrayPrice($price);
        $min = $value['min'];
        $max = $value['max'];
        // $min_end = $value['min_end'];
        if($max){
            $products = self::whereHas('product_variants', function ($q) use ($min,$max) {
                $q->whereStatus(1)->whereBetween('price',[$min, $max]);
            })->whereStatus(1)->whereIn('category_id',$category_id)->get();
        }else{
            // $products = self::whereHas('product_variants', function ($q) use ($min_end) {
            //     $q->whereStatus(1)->where('price','>=',$min_end);
            // })->whereStatus(1)->whereIn('category_id',$category_id)->get();
        }
        return $products;
    }

    public static function fillterCateMasterWithPrice($category_id,$price)
    { //master_price
        $arr_cate_id = [];
        foreach($category_id as $id){
            $cate = ProductCategory::findOrFail($id);
            $arr_cate_id[] = $cate->children()->pluck('id')->toArray();
        }
        $value = self::findMinMaxValueArrayPrice($price);
        $min = $value['min'];
        $max = $value['max'];
        // $min_end = $value['min_end'];
        if($max){
            $products = self::whereHas('product_variants', function ($q) use ($min,$max) {
                $q->whereStatus(1)->whereBetween('price',[$min, $max]);
            })->whereStatus(1)->whereIn('category_id',$arr_cate_id[0])->get();
        }else{
            // $products = self::whereHas('product_variants', function ($q) use ($min_end) {
            //     $q->whereStatus(1)->where('price','>=',$min_end);
            // })->whereStatus(1)->whereIn('category_id',$arr_cate_id[0])->get();
        }
        return $products;
    }

    public static function catePriceMaster($category_id,$master_id,$price)
    { 
        $arr_cate_id = [];
        foreach($master_id as $id){
            $cate = ProductCategory::findOrFail($id);
            $arr_cate_id[] = $cate->getIdChildren();
        }
        $new_category_id = array_unique(array_merge($category_id,$arr_cate_id[0]));
        return self::catePrice($new_category_id,$price);
    }

    public static function catePriceInformation($category_id,$information,$price)
    {
        $value = self::findMinMaxValueArrayPrice($price);
        $min = $value['min'];
        $max = $value['max'];
        // $min_end = $value['min_end'];
        if($max){
            $products = self::whereHas('product_variants', function ($q) use ($min,$max,$information) {
                $q->whereHas('informations', function($q1) use ($information){
                    $q1->whereIn('information_id',$information);
                })->whereStatus(1)->whereBetween('price',[$min, $max]);
            })->whereStatus(1)->whereIn('category_id',$category_id)->get();
        }else{
            // $products = self::whereHas('product_variants', function ($q) use ($min_end,$information) {
            //     $q->whereHas('informations', function($q1) use ($information){
            //         $q1->whereIn('information_id',$information);
            //     })->whereStatus(1)->where('price','>=',$min_end);
            // })->whereStatus(1)->whereIn('category_id',$category_id)->get();
        }
        return $products;
    }

    public static function cateMasterInfomation($category_id,$master_id,$information)
    {
        $arr_cate_id = [];
        foreach($master_id as $id){
            $cate = ProductCategory::findOrFail($id);
            $arr_cate_id[] = $cate->getIdChildren();
        }
        $new_category_id = array_unique(array_merge($category_id,$arr_cate_id[0]));
        return self::cateInfomation($new_category_id,$information);
    }

    public static function filterAll($category_id,$master_id,$information,$price)
    {//cate_master_information_price
        $arr_cate_id = [];
        foreach($master_id as $id){
            $cate = ProductCategory::findOrFail($id);
            $arr_cate_id[] = $cate->getIdChildren();
        }
        $new_category_id = array_unique(array_merge($category_id,$arr_cate_id[0]));
        return self::catePriceInformation($new_category_id,$price,$information);
    }

    public static function cateInfomation($category_id,$information)
    {
        $products = self::whereHas('product_variants', function ($q) use ($information) {
            $q->whereHas('informations', function($q1) use ($information){
                $q1->whereIn('information_id',$information);
            })->whereStatus(1);
        })->whereStatus(1)->whereIn('category_id',$category_id)->get();
        return $products;
    }

    public static function priceMasterInfo($price,$master_id,$information)
    {
        $arr_cate_id = [];
        foreach($master_id as $id){
            $cate = ProductCategory::findOrFail($id);
            $arr_cate_id[] = $cate->getIdChildren();
        }
        $new_category_id = array_unique($arr_cate_id[0]);
        return self::catePriceInformation($new_category_id,$information,$price);
    }

    public static function priceInformation($price,$information)
    {
        $value = self::findMinMaxValueArrayPrice($price);
        $min = $value['min'];
        $max = $value['max'];
        // $min_end = $value['min_end'];
        if($max){
            $products = self::whereHas('product_variants', function ($q) use ($min,$max,$information) {
                $q->whereHas('informations', function($q1) use ($information){
                    $q1->whereIn('information_id',$information);
                })->whereStatus(1)->whereBetween('price',[$min, $max]);
            })->whereStatus(1)->get();
        }else{
            // $products = self::whereHas('product_variants', function ($q) use ($min_end,$information) {
            //     $q->whereHas('informations', function($q1) use ($information){
            //         $q1->whereIn('information_id',$information);
            //     })->whereStatus(1)->where('price','>=',$min_end);
            // })->whereStatus(1)->get();
        }
    }

    public static function masterCate($master_id,$category_id)
    {
        $arr_cate_id = [];
        foreach($master_id as $id){
            $cate = ProductCategory::findOrFail($id);
            $arr_cate_id[] = $cate->getIdChildren();
        }
        $new_category_id = array_unique(array_merge($category_id,$arr_cate_id[0]));
        return self::fillterCateMaster($new_category_id);
    }

    public static function masterInformation($master_id,$information)
    {
        $arr_cate_id = [];
        foreach($master_id as $id){
            $cate = ProductCategory::findOrFail($id);
            $arr_cate_id[] = $cate->getIdChildren();
        }
        $new_category_id = array_unique($arr_cate_id[0]);
        $products = self::whereHas('product_variants', function ($q) use ($information) {
            $q->whereHas('informations', function ($q1) use($information){
                $q1->whereIn('information_id',$information);
            })->whereStatus(1);
        })->whereStatus(1)->whereIn('category_id',$arr_cate_id[0])->get();
    }


    public function getPriceRoundAttribute()
    {
        return round($this->product_variant_onlines[0]->price);
    }

    public function  getRelatedProduct(){
        $cate_id = $this->category_id;
        return self::whereCategoryId($cate_id)->where('id','!=',$this->id)->whereStatus(1)->take(4)->get();
    }

    public function category() {
        return $this->belongsTo('App\Models\Categories\ProductCategory');
    }

    public function product_variants()
    {
        return $this->hasMany('App\Models\Products\ProductVariant');
    }

    public function reviews()
    {
        return $this->morphMany('App\Models\Review', 'reviewtable');
    }

    public function type_reviews($type)
    {
        return $this->morphMany('App\Models\Review', 'reviewtable')->whereType($type);
    }

    public function type_review_online($type)
    {
        return $this->morphMany('App\Models\Review', 'reviewtable')->whereType($type)->whereStatus(1);
    }

    public function product_variants_get_first()
    {
        return $this->hasMany('App\Models\Products\ProductVariant')->whereStatus(1)->limit(1);
    }

    public function product_variant_onlines()
    {
        return $this->hasMany('App\Models\Products\ProductVariant')->whereStatus(1);
    }

    public function comments()
    {
        return $this->morphMany('App\Models\Comment', 'reviewtable');
    }

    public function product_codes()
    {
        return $this->morphMany('App\Models\Codes\ProductCode', 'objecttable');
    }

    public function getStatus()
    {
        if($this->status == 1)
            echo "Publish";
        elseif($this->status == 0)
            echo "Draff";
    }

    public function getTotal()
    {
        $variants = $this->product_variants;
        $total = 0;
        foreach($variants as $item){
            $total += $item->amount;
        }
        return $total;

    }
    public function hasVariants()
    {
        $product = self::where('id',$this->id)->has('product_variants')->count();
        return $product;
    }

    public function getVariantWithPositionEqual1()
    {
        if($this->product_variant_onlines()->count()){
            $variant = $this->product_variant_onlines->where('position',1)->first();
            if($variant)
                $variant = $variant;
            else
                $variant = $this->product_variant_onlines[0];
        }else
            $variant = null;
        return $variant;
    }
    public function checkCode()
    {
        $pivot_items = $this->product_codes;
        foreach($pivot_items as $pivot_item){
            $code = $pivot_item->code;
            if($code && $code->invalidTime() && 
                $code->promotion_type == config('ecommerce.promotion_type.product') && 
                $code->apply_type == config('ecommerce.apply_options.product') &&
                $code->status == config('ecommerce.code_statuses.running')
            ){
                if($pivot_item->amount_ordered <= $pivot_item->amount_default)
                    return $code;
            }
            return false;
        }
        return false;
    }
    public function linkImage()
    {
        $variant = $this->getVariantWithPositionEqual1();
        if(!$variant || count($variant->media) == 0)
            return asset('images/empty_image.jpg');
        else
            return $variant->media[0]->link;

    }
    public function linkImageFill()
    {
        $variant = $this->getVariantWithPositionEqual1();
        if(!$variant || count($variant->media) == 0)
            return asset('images/empty_image.jpg');
        elseif(empty($variant->media[1]))
            return $variant->media[0]->link;
        return $variant->media[1]->link;
    }

    public function getCode(){
        $code =$this->productCodes()->codes()->get();
        return $code;
    }

    public function getPrice()
    {
        $variant = $this->getVariantWithPositionEqual1();

        $number = $variant->price;
        $symbol = ' đ';
        $symbol_thousand = '.';
        $decimal_place = 0;
        $pledge_value = number_format($number, $decimal_place, '', $symbol_thousand);
        return $pledge_value . $symbol;
    }
    public function getDiscountPrice()
    {
        return $this->getVariantWithPositionEqual1()->getDiscountPrice();
    }

    public function updateStatusVariants()
    {
        if($this->hasVariants()){
            foreach($this->product_variants as $variant){
                $variant->status = $this->status;
                $variant->save();
            }
        }
    }
    //translate
    public function product_translation()
    {
        return $this->hasMany('App\Models\Translations\ProductTranslation');
    }

    public function getElementByLang($locale,$element)
    {
        $item = $this->getLangItemByLocaleAndName($locale,$element);
        if($item)
            return $item->translate_text;
        return "";
    }

    public function getLangItemByLocaleAndName($locale,$col)
    {
        $item = $this->product_translation()->where('locale',$locale)->where('column_name',$col)->first();
        return $item;
    }

    //end translate

    public function clone()
    {
        $product = $this->replicate();
        $product->status = 0;
        $product->slug = str_slug($this->name).rand(100000, 999999);
        $product->save();
        $this->clone_variants($product->id);
        $this->clone_translates($product->id);
        return $product;
    }

    public function clone_variants($p_id)
    {
        foreach ($this->product_variants as  $variant) {
            $new_variant = $variant->replicate();
            $new_variant->status = 0;
            $new_variant->product_id = $p_id;
            $new_variant->save();
            $variant->clone_galleries($new_variant->id);
        }
    }

    public function clone_translates($p_id)
    {
        foreach ($this->product_translation as  $item) {
            $new_item = $item->replicate();
            $new_item->product_id = $p_id;
            $new_item->save();
        }
    }

    public function getParam($param)
    {
        $arr_param = [];
        foreach($this->product_variant_onlines as $item){
            $arr_param['value'] = $item->$param;
            $arr_param['id'] = $item->id;
        }
        return $arr_param;
    }

    public function getCateName()
    {
        if($this->category)
            return $this->category->name;
        return "N/A";
    }

    public function getInvalidCode()
    {
        foreach($this->product_codes as $item){
            if($item->code->checkCodeValidProduct($item->amount_default,$item->amount_ordered))
                return $item->code;
        }
        return null;
    }


    public function checkCodePromotion($variant)
    {
        if($this->checkCode()){
            $promotion = $this->checkCode()->promotion_price;
            $variant_price = $variant->price;
            $type = $this->checkCode()->reduction_type;
            if ($type == 'percent') {
                $number = $variant_price - ($promotion*(0.01))*$variant_price;
            } elseif ($type == 'absolute') {
                $number = $variant_price - $promotion ;
            }
        }
        else{
            $number = $variant->price_format;
        }
        return $number;
    }

    public function isValidCode()
    {
        if($this->product_codes()->count()){
            foreach($this->product_codes as $item){
                if($item->code->checkCodeValidProduct($item->amount_default,$item->amount_ordered))
                    return true;
            }
            return false;
        }else{
            foreach($this->product_variants as $variant){
                foreach($variant->product_codes as $item){
                    if($item->code->checkCodeValidProduct($item->amount_default,$item->amount_ordered))
                        return true;
                }
            }
            return false;
        }
    }

    public function getValidCode()
    {
        if($this->product_codes()->count()){
            foreach($this->product_codes as $item){
                if($item->code->checkCodeValidProduct($item->amount_default,$item->amount_ordered))
                    return $item->code;
            }
            return false;
        }else{
            foreach($this->product_variants as $variant){
                foreach($variant->product_codes as $item){
                    if($item->code->checkCodeValidProduct($item->amount_default,$item->amount_ordered))
                        return $item->code;
                }
            }
            return false;
        }
    }
    public function isInvalidCode() {
        // Hàm này anh viết tạm để không lỗi. Ý muốn đặt tên cho đúng nha Luân!
        return $this->isValidCode();
    }

    public function getPromotionShowInView()
    {
        if($this->getVariantWithPositionEqual1()->hasPromotion()){
            $code = $this->getVariantWithPositionEqual1()->hasPromotion();
            if($code->reduction_type == config('ecommerce.reduction_type.Percent'))
                    return "-" . $code->promotion_price . "%";
            else{
                $symbol = ' đ';
                $symbol_thousand = ',';
                $decimal_place = 0;
                $pledge_value = number_format($code->promotion_price, $decimal_place, '', $symbol_thousand);
                return "-" . $pledge_value . $symbol;
            }
        }else{
            if($this->isValidCode()){
                $code = $this->getValidCode();
                if($code->reduction_type == config('ecommerce.reduction_type.Percent'))
                    return "-" . $code->promotion_price . "%";
                else{
                    $symbol = ' đ';
                    $symbol_thousand = ',';
                    $decimal_place = 0;
                    $pledge_value = number_format($code->promotion_price, $decimal_place, '', $symbol_thousand);
                    return "-" . $pledge_value . $symbol;
                }
            }
        }
    }

    public function calculatedLevel()
    {
        $review_arr = $this->type_reviews(config('ecommerce.review_types.review'))->pluck('level');
        $total = 0;
        $average = 0;
        $length = $review_arr->count();
        if($length != 0){
            for ($i = 0 ; $i < $review_arr->count() ; $i++){
                $total += $review_arr[$i];
            }
            $average = round($total / $review_arr->count());
        }
        return $average;
    }

    public function countPercent($level)
    {
        $review_arr = $this->type_reviews(config('ecommerce.review_types.review'))->sum('level');
        $review_arr_level = $this->type_reviews(config('ecommerce.review_types.review'))->where('level',$level)->sum('level');
        if($review_arr_level == 0) return 0;
        return  round($review_arr_level / $review_arr * 100);
    }

    public function countReview($level)
    {
        return $this->type_reviews(config('ecommerce.review_types.review'))->where('level',$level)->count();
    }
}
