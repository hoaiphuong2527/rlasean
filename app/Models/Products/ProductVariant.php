<?php

namespace App\Models\Products;

use Carbon\Carbon;
use App\Helpers\BeautyLink;
use App\Helpers\SearchTrait;
use App\Models\Products\Product;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;

class ProductVariant extends Model
{
    use SearchTrait,BeautyLink;

    public static $urlAdminEdit = 'admin_product_variant.edit';
    public static $urlAdminShow = 'admin_product_variant.show';
    public static $urlAdminDestroy = 'admin_product_variant.destroy';
    public static $urlAdminUpdate = 'admin_product_variant.update';
    public static $namespace = 'App\Models\Products\ProductVariant';
    public static $urlGuestShow = 'guest_products.show';

    protected $fillable = [
        'product_id',
        'color',
        'size',
        'price',
        'amount',
        'status',
        'position',
        'title',
        'weight'
    ];
    protected $with = [
        'product'
    ];
    protected $appends = ['image', 'price_round','name','discount','breakable'];

    public function getImageAttribute()
    {
        $arr_media = [];
        foreach( $this->media as $img){
            $arr_media[] = $img->link;
        }
        return $arr_media;
    }
    public function getDiscountAttribute()
    {
        return $this->getDiscountPrice();
    }

    public function getPriceRoundAttribute()
    {
        return round($this->price);
    }

    public function getBreakableAttribute()
    {
        return $this->product->is_breakable;
    }

    public function getNameAttribute()
    {
        return $this->product->name;
    }

    /**
     * clone_galleries: clone new photo
     * getName: return name
     * getStatus: display status
     * urlAdminEdit: url admin edit
     * urlAdminDestroy: url admin destroy
     * urlAdminUpdate: url admin update
     * getPriceFormatAttribute: format price
     * linkImage: return link image
     * returnOldAmount: return old amont before update inventory
     */
    
    public function informations(){
        return $this->belongsToMany('App\Models\Informations\Information', 'product_information');
    }

    public function product() {
        return $this->belongsTo('App\Models\Products\Product');
    }

    public function history()
    {
        return $this->hasOne('App\Models\Products\HistoryImportProduct');
    }

    public function galleries()
    {
        return $this->hasMany('App\Models\Products\Gallery');
    }

    public function media()
    {
        return $this->morphToMany('App\Models\Media', 'mediable'); 
    }

    public function getName()
    {
        $string = $this->product->name . '-' . $this->title ;
        if($this->size != null)
            $string .= '-'. $this->size;
        return $string;
    }

    public function getStatus()
    {
        if($this->status == 1)
            echo "Publish";
        elseif($this->status == 0)
            echo "Draff";
    }

    public function urlAdminEdit($slug){
        return route(self::$urlAdminEdit,['product_slug' => $slug,'id' => $this->id]);
    }
    public function urlAdminDestroy($slug){
        return route(self::$urlAdminDestroy,['product_slug' => $slug,'id' => $this->id]);
    }

    public function urlAdminUpdate($slug){
        return route(self::$urlAdminUpdate,['product_slug' => $slug,'id' => $this->id]);
    }

    public function urlGuestShow(){
        return $this->product->urlGuestShow();
    }

    public function orders()
    {
        return $this->belongsToMany('App\Models\Order','order_product')->withPivot('amount');
    }

    public function order_in_inventories()
    {
        $status = [config('ecommerce.order_statuses.waiting'),config('ecommerce.order_statuses.processing'),config('ecommerce.order_statuses.new')];
        return $this->belongsToMany('App\Models\Order','order_product')->whereIn('order_status',$status)->withPivot('amount');
    }

    public function order_sortings()
    {
        return $this->belongsToMany('App\Models\Order','order_product')->withPivot('amount')->orderBy('amount','desc');;
    }

    public function product_codes()
    {
        return $this->morphMany('App\Models\Codes\ProductCode', 'objecttable');
    }

    public function getPriceFormatAttribute()
    {
        return (int) $this->price;
    }

    public function countAmountOrderWaiting(){
        $orders = $this->orders()->where('order_status','W')->get();
        $total = 0;
        if(count($orders)){
            foreach($orders as $order){
                $total += $order->pivot->amount;
            }
        }
        return $total;
    }

    public function hasPromotion()
    {
        $pivot_items = $this->product_codes;
        if(count($pivot_items)){
            foreach($pivot_items as $pivot_item){
                $code = $pivot_item->code;
                if($code && $code->invalidTime() && 
                    $code->promotion_type == config('ecommerce.promotion_type.product') && 
                    $code->apply_type == config('ecommerce.apply_options.variant') &&
                    $code->status == config('ecommerce.code_statuses.running')
                ){
                    if($pivot_item->amount_ordered <= $pivot_item->amount_default)
                        return $code;
                }
                return false;
            }
        }else{
            return false;
        }
    }

    public function getPrice(){
        $product = $this->product;
        $symbol = ' đ';
        $symbol_thousand = '.';
        $decimal_place = 0;
        if($this->hasPromotion()){
            $promotion = $this->hasPromotion()->promotion_price;
            $variant_price = $this->price;
            $type = $this->hasPromotion()->reduction_type;
            if ($type == 'percent') {
                $number = $variant_price - ($promotion*(0.01))*$variant_price;
            } elseif ($type == 'absolute') {
                $number = $variant_price - $promotion ;
            }
        }else{
            $number = $product->checkCodePromotion($this);
        }
        $pledge_value = number_format($number, $decimal_place, '', $symbol_thousand);
        return $pledge_value . $symbol;
    }

    public function clone_galleries($v_id)
    {
        $new_variant = self::find($v_id);
        $arr = [];
        foreach($this->media as $photo){
            $arr[] = $photo->id;
        }
        $new_variant->media()->sync($arr);
    }
    //translate
    public function variant_translations()
    {
        return $this->hasMany('App\Models\Products\VariantTranslation');
    }

    public function getElementByLang($locale,$element)
    {
        $item = $this->getLangItemByLocaleAndName($locale,$element);
        if($item)
            return $item->translate_text;
        return "";
    }

    public function getLangItemByLocaleAndName($locale,$col)
    {
        $item = $this->variant_translations()->where('locale',$locale)->where('column_name',$col)->first();
        return $item;
    }
    //end translate

    public function linkImage()
    {
        if($this->media()->count())
            return $this->media[0]->link;
        else
            return asset('images/empty_image.jpg');
    }

    public function returnOldAmount($import)
    {
        $this->amount = $this->amount - $import;
        $this->save();
        return $this;
    }

    public function countAmountByStatus(array $status)
    {
        $begin = ($this->created_at->todatestring()." 00:00:00");
        $end = ($this->created_at->todatestring()." 23:59:59");
        $total = 0;
        if(strtotime($this->created_at) >= $begin && $this->created_at <= $end){
            $pending_orders = $this->order_in_inventories;
            foreach($pending_orders as $item){
                    $total += $item->pivot->amount;
            }    
        }
        return $total;
    }

    public function checkTypeApply()
    {
        $code = null;
        $type = null;
        if($this->hasPromotion()){
            $code = $this->hasPromotion();
            $type = 'variant';
        }else{
            $code = $this->product->getInvalidCode();  
            $type = 'product';
        }
        return $data = [
            'code' => $code,
            'type' => $type
        ];
    }

    /*-------------------------------------*/
    public function getDiscountPrice()
    {
        $code = null;
        if($this->hasPromotion()){
            $code = $this->hasPromotion();
            if($code){
                $amount_default = $code->firstRecordByObjectId($this->id)->amount_default;
                $amount_ordered = $code->firstRecordByObjectId($this->id)->first()->amount_ordered;
                if ($code->checkCodeValidProduct($amount_default,$amount_ordered)) {
                    $code_value = $code->promotion_price;
                    if ($code->reduction_type === config('ecommerce.reduction_type.Percent')) {
                        $result  = (int)  $this->price - ($code_value * 0.01 *  $this->price);
                    } else {
                        $result = (int) $this->price - $code_value;
                    }
                }else{
                    $result = (int) $this->price;
                }
            }else{
                $result = (int) $this->price;
            }
        }else{
            $code = $this->product->getInvalidCode();  
            if($code){
                $amount_default = $code->firstRecordByObjectId($this->product->id)->amount_default;
                $amount_ordered = $code->firstRecordByObjectId($this->product->id)->amount_ordered;
                if ($code->checkCodeValidProduct($amount_default,$amount_ordered)) {
                    $code_value = $code->promotion_price;
                    if ($code->reduction_type === config('ecommerce.reduction_type.Percent')) {
                        $result  = (int)  $this->price - ($code_value * 0.01 *  $this->price);
                    } else {
                        $result = (int) $this->price - $code_value;
                    }
                }else{
                    $result = (int) $this->price;
                }
            }else{
                $result = (int) $this->price;
            }
        }
        return $result;
    }
    public function getDiscountValue()
    {
        $code = null;
        if($this->hasPromotion()){
            $code = $this->hasPromotion();
            if($code){
                $amount_default = $code->firstRecordByObjectId($this->id)->amount_default;
                $amount_ordered = $code->firstRecordByObjectId($this->id)->first()->amount_ordered;
                if ($code->checkCodeValidProduct($amount_default,$amount_ordered)) {
                    $code_value = $code->promotion_price;
                    if ($code->reduction_type === config('ecommerce.reduction_type.Percent')) {
                        $result  = $code_value;
                        return $result.'%';
                    } else {
                        $result = $code_value;
                        $symbol = 'đ';
                        $symbol_thousand = ',';
                        $decimal_place = 0;
                        $formated = number_format($result, $decimal_place, '', $symbol_thousand);
                        return $formated. $symbol;
                    }
                }
            }else{
                return 0;
            }

        }else{
            $code = $this->product->getInvalidCode();  
            if($code){
                $amount_default = $code->firstRecordByObjectId($this->product->id)->amount_default;
                $amount_ordered = $code->firstRecordByObjectId($this->product->id)->amount_ordered;
                if ($code->checkCodeValidProduct($amount_default,$amount_ordered)) {
                    $code_value = $code->promotion_price;
                    if ($code->reduction_type === config('ecommerce.reduction_type.Percent')) {
                        $result  = $code_value;
                        return $result.'%';
                    } else {
                        $result = $code_value;
                        $symbol = 'đ';
                        $symbol_thousand = ',';
                        $decimal_place = 0;
                        $formated = number_format($result, $decimal_place, '', $symbol_thousand);
                        return $formated. $symbol;
                    }
                }
            }else{
                return 0;
            }
        }
    }

    public function getDiscountPriceUseIntoOrderBackEnd()
    {
        $code = null;
        if($this->hasPromotion()){
            $code = $this->hasPromotion();
            $amount_default = $code->firstRecordByObjectId($this->id)->amount_default;
            $amount_ordered = $code->firstRecordByObjectId($this->id)->amount_ordered;

        }else{
            $code = $this->product->getInvalidCode();  
            $amount_default = $code->firstRecordByObjectId($this->product->id)->amount_default;
            $amount_ordered = $code->firstRecordByObjectId($this->product->id)->amount_ordered;
        }
        if($code){
            if ($code->checkCodeValidProduct($amount_default,$amount_ordered)) {
                $code_value = $code->promotion_price;
                if ($code->reduction_type === config('ecommerce.reduction_type.Percent')) {
                    $result  = (int) $this->price - ($code_value * 0.01 * $this->price);
                } else {
                    $result = (int) $this->price - $code_value;
                }
            }else{
                $result = (int)  $this->price;
            }
        }else{
            $result = (int)  $this->price;
        }
        return $result;
    }

    
    public function historyByDate($date)
    {
        return $this->history()->whereDate('created_at',$date)->first();
    }

    public function renderPriceInView($amount)
    {
        $code = $this->checkTypeApply()['code'];
        // dd($amount);
        if($code && $code->amount == 0) {
            $session = Session::get("begin_amount_".$code->id);
            if($session == $amount){
                return $this->disscountPriceInView($code);
            }else{
                return $this->getPrice();
            }
        }else{
            return $this->getPrice();
        }
    }

    public function disscountPriceInView($product_code)
    {
        $promotion = $product_code->promotion_price;
        if ($code->reduction_type === config('ecommerce.reduction_type.Percent')) {
            $result  = (int)  $this->price - ($promotion * 0.01 *  $this->price);
        } else {
            $result = (int) $this->price - $promotion;
        }
        $symbol = ' đ';
        $symbol_thousand = '.';
        $decimal_place = 0;
        $formated = number_format($result, $decimal_place, '', $symbol_thousand);
        return $formated. $symbol;
    }

    public function calculatTotal($amount)
    {
        $product = $this->product;
        if($this->hasPromotion()){
            $price = $this->getDiscountPrice();
        }else{
            $price = $product->checkCodePromotion($this);
        }
        $symbol = ' đ';
        $symbol_thousand = '.';
        $decimal_place = 0;
        $number =  $price * $amount;
        $pledge_value = number_format($number, $decimal_place, '', $symbol_thousand);
        return $pledge_value . $symbol;
    }

    public function getArrayInformations()
    {
        $array = [];
        if($this->informations()->count()){
            foreach($this->informations->unique('key') as $key => $info){
                $array[$key]['name'] = $info->name;
                $array[$key]['key'] = $info->key;
                    foreach($info->getRecordByKey() as $key1 => $row){
                        $array[$key]['params'][$key1] = ['id' => $row->id,'value' => $row->value];
                    }
            }
        }
        return $array;
    }
}
