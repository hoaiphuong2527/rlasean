<?php

namespace App\Models\Products;

use App\Helpers\BeautyLink;
use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    use BeautyLink;
    public static $imgPath = 'upload/images/galleries/';
    protected $fillable = [
        'media_id', 
        'object_id',
        'object'
    ];

    // public function product_variant() {
    //     return $this->belongsTo('App\Models\Products\ProductVariant');
    // }

    // public function media() {
    //     return $this->belongsTo('App\Models\Media');
    // }
}
