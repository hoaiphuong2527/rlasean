<?php

namespace App\Models\Products;

use Carbon\Carbon;
use App\Helpers\BeautyLink;
use App\Helpers\SearchTrait;
use Illuminate\Database\Eloquent\Model;

class HistoryImportProduct extends Model
{
    use SearchTrait,BeautyLink;
    public static $urlAdminEdit = 'admin_history.edit';
    public static $urlAdminShow = 'admin_history.show';
    public static $urlAdminDestroy = 'admin_history.destroy';
    public static $urlAdminUpdate = 'admin_history.update';

    protected $fillable = [
        'product_variant_id', 
        'amount_import',
        'amount_export',
        'pendding'
    ];

    public function product_variant() {
        return $this->belongsTo('App\Models\Products\ProductVariant');
    }

    public static function updateOrCreateInDate($id,$amount_import)
    {
        $item = self::where([['product_variant_id',$id],['updated_at', 'LIKE', date("Y-m-d").' %']])->first();
        if($item){
            $item->amount_import = $item->amount_import + $amount_import;
            $item->save();
        }else{
            self::create([
                'product_variant_id'    => $id,
                'amount_import'         => $amount_import,
                'amount_export'         => 0,
                'pendding'              => 0
            ]);
        }
    }

    public static function updateOrCreateExportInDate($id,$pendding)
    {
        $item = self::where([['product_variant_id',$id],['updated_at', 'LIKE', date("Y-m-d").' %']])->first();
        if(!$item){
            self::create([
                'product_variant_id'    => $id,
                'amount_import'         => 0,
                'amount_export'         => 0,
                'pendding'              => $pendding
            ]);
        }else{
            $item->pendding += $pendding;
            $item->save();
        }
    }

    public static function scopeSearch($query, $keyword)
    {
        $products = self::whereHas('product_variant', function($query) use ($keyword){
            $query->whereHas('product', function($q) use ($keyword){
                $q->where('name','like','%'.$keyword.'%');
            });
        });
        return $products;
    }

    public function getCreateDateAttribute()
    {
        return $this->updated_at->format('d-m-Y');
    }
}
