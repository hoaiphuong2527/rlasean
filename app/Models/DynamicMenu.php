<?php

namespace App\Models;
use App\Helpers\BeautyLink;
use Illuminate\Database\Eloquent\Model;

class DynamicMenu extends Model
{
    use BeautyLink;
    public static $urlAdminEdit = 'admin_menu.edit';
    public static $urlAdminShow = 'admin_menu.show';
    public static $urlAdminDestroy = 'admin_menu.destroy';
    public static $urlAdminUpdate = 'admin_menu.update';

    protected $fillable = [
        'url', 
        'display_name',
        'parent',
        'sort'
    ];

    public static $translate_column = [
        'url',
        'display_name',
        // 'description'
    ];

    public function parent() {
        return $this->belongsTo('App\Models\DynamicMenu', 'parent', 'id');
    }

    public function children()
    {
        return $this->hasMany('App\Models\DynamicMenu', 'parent', 'id');
    }

    public function deleteChilden()
    {
        if($this->children()->count()){
            foreach($this->children as $item){
                $item->deleteChilden();
                $item->delete();
            }
        }
        $this->delete();
    }

    public function translates()
    {
        return $this->hasMany('App\Models\Translations\MenuTranslation','dynamic_menu_id');
    }

    public function getElementByLang($locale,$element)
    {
        $item = $this->getLangItemByLocaleAndName($locale,$element);
        if($item)
            return $item->translate_text;
        return $this->$element;
    }

    public function getLangItemByLocaleAndName($locale,$col)
    {
        $item = $this->translates()->where('locale',$locale)->where('column_name',$col)->first();
        return $item;
    }

    public function urlGuestShow()
    {
        return $this->getElementByLang(app()->getLocale(),'url');
    }
}
