<?php

namespace App\Models\Translations;

use Illuminate\Database\Eloquent\Model;

class MenuTranslation extends Model
{
    protected $table = "menu_translations";
    public static $namespace = 'App\Models\Translations\MenuTranslation';
    public static $foreign_key = 'dynamic_menu_id';
    protected $fillable = [
        'locale',
        'column_name',
        'translate_text',
        'dynamic_menu_id'
    ];

    public function menu() {
        return $this->belongsTo('App\Models\DynamicMenu', 'dynamic_menu_id');
    }
}
