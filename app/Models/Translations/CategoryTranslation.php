<?php

namespace App\Models\Translations;

use Illuminate\Database\Eloquent\Model;

class CategoryTranslation extends Model
{
    protected $table = "category_translations";
    public static $namespace = 'App\Models\Translations\CategoryTranslation';
    public static $foreign_key = 'category_id';
    protected $fillable = [
        'locale',
        'column_name',
        'translate_text',
        'category_id'
    ];

    public function category() {
        return $this->belongsTo('App\Models\Categories\Category', 'category_id');
    }
}
