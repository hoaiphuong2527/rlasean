<?php

namespace App\Models\Translations;

use Illuminate\Database\Eloquent\Model;

class ProductTranslation extends Model
{
    protected $table = "product_translations";
    public static $namespace = 'App\Models\Translations\ProductTranslation';
    public static $foreign_key = 'product_id';
    protected $fillable = [
        'locale',
        'column_name',
        'translate_text',
        'product_id'
    ];

    public function product() {
        return $this->belongsTo('App\Models\Products\Product');
    }


}
