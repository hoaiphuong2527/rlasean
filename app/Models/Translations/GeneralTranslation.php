<?php

namespace App\Models\Translations;

use Illuminate\Database\Eloquent\Model;

class GeneralTranslation extends Model
{
    protected $table = "general_translations";
    protected $fillable = [
        'locale',
        'table_id',
        'table_type',
        'column_name',
        'translate_text'
    ];

    public function table()
    {
        return $this->morphTo('table_id');
    }
}
