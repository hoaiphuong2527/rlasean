<?php

namespace App\Models\Translations;

use Illuminate\Database\Eloquent\Model;

class PostTranslation extends Model
{
    protected $table = "post_translations";
    public static $namespace = 'App\Models\Translations\PostTranslation';
    public static $foreign_key = 'post_id';
    protected $fillable = [
        'locale',
        'column_name',
        'translate_text',
        'post_id'
    ];

    public function post() {
        return $this->belongsTo('App\Models\Post');
    }
}
