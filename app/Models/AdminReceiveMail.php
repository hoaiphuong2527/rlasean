<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdminReceiveMail extends Model
{
    protected $fillable = [
        'user_id'
    ];
    /**
     * user: relationship with user table
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id');
    }
}
