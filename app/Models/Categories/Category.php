<?php

namespace App\Models\Categories;

use App\Models\Post;
use App\Helpers\BeautyLink;
use App\Helpers\SearchTrait;
use App\Models\Products\Product;
use Illuminate\Database\Eloquent\Model;
use App\Models\Categories\PageCategory;
use App\Models\Categories\ProductCategory;
use App\Helpers\SessionManager;
use App\Models\Media;

class Category extends Model
{
    use SearchTrait,BeautyLink;
    public static $imgPath = 'upload/images/category/';
    public static $imgIcon = 'upload/images/category/';
    public static $urlAdminEdit = 'admin_category.edit';
    public static $urlAdminShow = 'admin_category.show';
    public static $urlAdminDestroy = 'admin_category.destroy';
    public static $urlAdminUpdate = 'admin_category.update';

    public static $urlGuestShow = 'guest_products.category';

    public static $namespace = 'App\Models\Categories\Category';
    protected $fillable = [
        'name',
        'slug',
        'status',
        'parent_id',
        'context_type',
        'description',
        'image',
        'icon'
    ];

    protected $appends = ['full_name'];

    public static $translate_column = [
        'name',
        'slug',
        // 'description'
    ];

    public static $translate_relation = "translates";
    /**
     * getCategoryChildren: get children to delete
     * getTree: get tree array category
     * getCategoriesByKey: get all category by key
     * deleteChildren: delete children
     * getNameParent: return name parent
     * getStatus: display status
     * updateStatusItem: update status of posts or products to status of category
     */

    public function getFullNameAttribute()
    {
        $string = '---';
        if($this->isSecondParent()){
            $this->full_name = $string;
            $new_string = $string;
        }else{
            if($this->parent)
                $s= $this->parent->full_name . $string;
            else $s = $string;
            $new_string = $s . $string;
        }
        return $new_string;
    }

    public function renderName()
    {
       return $this->full_name . $this->name;
    }

    public function parent() {
        return $this->belongsTo('App\Models\Categories\Category', 'parent_id', 'id');
    }

    public function children()
    {
        return $this->hasMany('App\Models\Categories\Category', 'parent_id', 'id');
    }

    public function childrenOnline()
    {
        return $this->hasMany('App\Models\Categories\Category', 'parent_id', 'id')->where('status',1);
    }

    public function posts(){
        return $this->hasMany('App\Models\Post','category_id');
    }

    public function products(){
        return $this->hasMany('App\Models\Products\Product','category_id');
    }

    public function informations(){
        return $this->belongsToMany('App\Models\Informations\Information','category_information');
    }

    public function getOnlinePosts()
    {
        return $this->hasMany('App\Models\Post','category_id')->where('status',1);
    }
    public function media()
    {
        return $this->morphToMany('App\Models\Media', 'mediable'); 
    }
    
    public function getImg($col)
    {
        $pic = Media::where('link',$this->$col)->first();
        return $pic;
    }

    public static function getCategoryChildren($id)
    {
        $category = self::findOrFail($id);
        $arr = [];
        if($category->children){
            foreach ($category->children as $cate ) {
                if($cate->parent_id == $id){
                    $arr[] = $cate->id;
                    self::getCategoryChildren($cate->id);
                }
            }
        }
        return $arr;
    }

    public static function getTree($master_categories)
    {
        $array = [];
        foreach($master_categories as $key => $category){
            $array[$key]= $category;
            if($category->children()->count()){
                $category->children;
                self::getTree($category->children);
            }
        }
        return $array;
    }

    public static function getTreeOnline($master_category)
    {
        $array[]= $master_category;
        foreach($master_category->childrenOnline as $key => $category){
            if($category->childrenOnline()->count()){
                $category->childrenOnline;
                self::getTreeOnline($category);
            }
        }
        return $array;
    }

    public static function getCategoriesByKey($key,$id = 0)
    {
        if($key == config('ecommerce.context_type.product')){
            $categories = ProductCategory::where('parent_id',1)->where('status','!=',2)->get();
        }elseif($key == config('ecommerce.context_type.page')){
            $categories = PageCategory::where('parent_id',3)->where('status','!=',2)->get();
        }
        elseif($key == config('ecommerce.context_type.post')){
            $categories = PostCategory::where('parent_id',2)->where('status','!=',2)->get();
        }
        $array = self::getTree($categories);
        return $array;
    }

    public function deleteChildren()
    {
        $arr = self::getCategoryChildren($this->id);
        // translate
        $this->deleteLang();
        //end
        self::whereIn('id',$arr)->delete();
    }

    public function getNameParent()
    {
        if($this->status == 2)
            echo "N/A";
        elseif($this->parent_id == 0)
            echo "[Danh mục cha]";
        else echo $this->parent->name;
    }

    public function getStatus()
    {
        if($this->status == config('ecommerce.category_status.draff')){
            echo config('ecommerce.category_status_display.0');
        }elseif($this->status == config('ecommerce.category_status.publish')){
            echo config('ecommerce.category_status_display.1');
        }else{}
    }

    public function updateStatusChildren()
    {
        $categories = self::getTree($this->children);
        if(count($categories) > 0){
            foreach($categories as $cate){
                $cate->status = $this->status;
                $cate->save();
                $cate->updateStatusItem();
            }
        }else{
            $this->updateStatusItem();
        }

    }

    public function updateStatusItem()
    {
        if(count($this->posts)){
            Post::whereCategoryId($this->id)->update(['status'=>$this->status]);
        }elseif(count($this->products)){
            foreach($this->products as $product){
                $product->status = $this->status;
                $product->save();
                $product->updateStatusVariants();
            }
        }
    }

    public static function getCategoryNotMaster()
    {
        return self::where('parent_id','!=',0)->whereStatus(1)->get();
    }

    public function isSecondParent()
    {
        if($this->parent_id != 0){
            if($this->parent->parent_id == 0) return true;
        }
        return false;
    }

    //translate
    public function translates()
    {
        return $this->hasMany('App\Models\Translations\CategoryTranslation','category_id');
    }

    public function getElementByLang($locale,$element)
    {
        $item = $this->getLangItemByLocaleAndName($locale,$element);
        if($item)
            return $item->translate_text;
        return "";
    }

    public function getLangItemByLocaleAndName($locale,$col)
    {
        $item = $this->translates()->where('locale',$locale)->where('column_name',$col)->first();
        return $item;
    }
    public function deleteLang()
    {
        if(count($this->translates)){
            foreach($this->translates as $translate){
                $translate->delete();
            }
        }
    }

    public static function getItemBySlug($slug)
    {
        $locales = ['en','vi'];
        if (in_array(config('app.locale'), $locales)){
            $item = self::whereHas('translates', function ($q) use ($slug) {
                $q->whereColumnName('slug')->whereTranslateText($slug);
            })->whereStatus(1)->first();
        }if(!$item){
            $item = self::whereSlug($slug)->firstOrFail();
        }
        return $item;
    }
    //end translate

    public function getArrayInformations()
    {
        $array = [];
        $arr_id  = $this->informations->pluck('id')->toArray();
        if($this->informations()->count()){
            foreach($this->informations->unique('key') as $key => $info){
                $array[$key]['name'] = $info->name;
                $array[$key]['key'] = $info->key;
                $arr_key[] = $info->key;
                    foreach($info->getRecordByKeyAndId($arr_id) as $key1 => $row){
                        $array[$key]['params'][$key1] = ['id' => $row->id,'value' => $row->value];
                    }
            }
        }
        return $array;
    }
}
