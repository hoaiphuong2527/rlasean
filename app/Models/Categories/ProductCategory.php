<?php

namespace App\Models\Categories;

use App\Helpers\BeautyLink;
use App\Models\Categories\Category;
use App\Models\Products\Product;
use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Category
{
    protected $table = "categories";
    

    public static function boot()
    {
        parent::boot();

        static::addGlobalScope(function ($query) {
            $query->where('context_type',config('ecommerce.context_type.product'));
        });
    }

    /**
     * getTreeProduct: get category has products
     * getCategoryNotMaster: get categories except category has parent id = 0
     * isSecondParent: check category is a category has parent is category master
     */
    public static function getSecondParentOnline()
    {
        return self::whereStatus(1)->whereParentId(1)->get();
    }
    public function children()
    {   
        return $this->hasMany('App\Models\Categories\ProductCategory', 'parent_id', 'id');
    }

    public function parent() {
        return $this->belongsTo('App\Models\Categories\ProductCategory', 'parent_id', 'id');
    }

    public function parentNotMaster() {
        return $this->belongsTo('App\Models\Categories\ProductCategory', 'parent_id', 'id')->where('parent_id','!=',0);
    }

    public function products()
    {
        return $this->hasMany('App\Models\Products\Product','category_id');
    }

    public function productOnlines()
    {
        return $this->hasMany('App\Models\Products\Product','category_id')->where('status',1);
    }

    function informations(){
        return $this->belongsToMany('App\Models\Informations\Information', 'category_information','category_id', 'information_id');
    }
    
    public static function getTreeProduct($categories)
    {
        $array_categories = [];
        foreach($categories as $key => $category){
            $array_categories[$key] = $category;
            $category->children;
            foreach($category->children as $key1 => $sub){
                $sub->children;
                $sub->products;
                foreach($sub->products as $product){
                    if($product->product_variants)
                        $product->product_variants;
                }
                // $sub->products->product_variants;
                parent::getTree($sub->children);
            }
        }
        return $array_categories;
    }

    public function getIdProduct(){
        return $this->hasMany('App\Models\Products\Product','category_id')->where('status',1)->pluck('id');
    }

    public function product_has_variants()
    {
        return $this->products()->whereHas('product_variants',function($q){
            $q->whereStatus(1);
        })->get();
    }

    public function getProductsForGuest()
    {
        if($this->parent_id == 1 || $this->parent_id == 0){
            $arr_cate_id[] = $this->children()->pluck('id')->toArray();
            $products = Product::isOnline()->whereCategoryId($this->id)->orWhereIn('category_id',$arr_cate_id[0]);
        }else{
            $products = $this->productOnlines();
        }
        return $products;
    }

    public function getIdChildren()
    {
        $array = [];
        foreach($this->children as $key => $category){
            $array[$key]= $category->id;
            if($category->children()->count()){
                $category->children;
                $this->getIdChildren();
            }
        }
        return $array;
    }

    public static function getProductByCateId($id)
    {
        $category = self::findOrFail($id);
        $categories[] = $category;
        if($category->children){
            foreach ($category->children as $cate ) {
                if($cate->parent_id == $id){
                    $categories[] = $cate;
                    if($cate->product_has_variants){
                        foreach($cate->products as $product){
                            $product->product_variants;
                            foreach($product->product_variants as $variant){
                                $variant->informations;
                            }
                        }
                        self::getProductByCateId($cate->id);
                    }
                }
            }
        }else{
            if($category->product_has_variants){
                foreach($category->products as $product){
                    $product->product_variants;
                    foreach($product->product_variants as $variant){
                        $variant->informations;
                    }
                }
                self::getProductByCateId($cate->id);
            }
        }
        return $categories;
    }

    public function getRelatedCategory()
    {
        if($this->isSecondParent()){
            $categories = $this->children()->where('status',1)->get();
            return self::getTreeOnline($this);
        }else{
            return $this->findParent($this->parent);
        }
        
    }

    public function getInformationByCategory()
    {
        $array = $this->getArrayInformations();
        if($this->children()->count()){
            foreach($this->children as $category){
                $array = array_merge($array,$category->getArrayInformations());
                if($category->children()->count()){
                    $category->getInformationByCategory();
                }
            }
        }
        return $array;
    }

    public function findParent($parent)
    {
        if($parent->isSecondParent()){
            // $categories = $this->parent->children()->where('status',1)->get();
            return self::getTreeOnline($this->parent);
        }elseif($this->parent_id != 0){
            $this->findParent($this->parent);
        }
    }
}
