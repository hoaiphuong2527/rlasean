<?php

namespace App\Models\Categories;

use App\Models\Categories\Category;
use Illuminate\Database\Eloquent\Model;

class PageCategory extends Category
{
    protected $table = "categories";

    public static function boot()
    {
        parent::boot();

        static::addGlobalScope(function ($query) {
            $query->where('context_type',config('ecommerce.context_type.page'));
        });
    }

    public function children()
    {   
        return $this->hasMany('App\Models\Categories\PageCategory', 'parent_id', 'id');
    }

    public function parent() {
        return $this->belongsTo('App\Models\Categories\PageCategory', 'parent_id', 'id');
    }

    public static function getCategoryNotMaster()
    {
        return self::where('parent_id', '!=', 0)->get();
    }
    
    public function isSecondParent()
    {
        if ($this->parent->parent_id == 0) return true;
        return false;
    }

}
