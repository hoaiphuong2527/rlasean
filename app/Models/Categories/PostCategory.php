<?php

namespace App\Models\Categories;

use App\Models\Categories\Category;
use Illuminate\Database\Eloquent\Model;

class PostCategory extends Category
{
    protected $table = "categories";

    public static function boot()
    {
        parent::boot();

        static::addGlobalScope(function ($query) {
            $query->where('context_type',config('ecommerce.context_type.post'));
        });
    }

    public function children()
    {
        return $this->hasMany('App\Models\Categories\PostCategory', 'parent_id', 'id');
    }

    public function parent() {
        return $this->belongsTo('App\Models\Categories\PostCategory', 'parent_id', 'id');
    }

    public static function getCategoryNotMaster()
    {
        return self::where('parent_id', '!=', 0)->get();
    }

    public static function firstCateChild()
    {
        return self::whereParentId(2)->first();
    }

    public function isSecondParent()
    {
        if ($this->parent->parent_id == 0) return true;
        return false;
    }

    public function latestPosts()
    {
        return $this->posts()->whereStatus(1)->latest()->with(['user'])->paginate(5);
    }

    public function urlGuestShow()
    {
        return route('guest_posts.category.'.app()->getLocale(), ['id' => $this->id, 'slug' => $this->slug]);
    }

    public static function categoryHasPostOnline()
    {
        return self::whereParentId(2)->whereHas('posts',function($q){
            $q->whereStatus(1);
        })->whereStatus(1)->get();
    }
}
