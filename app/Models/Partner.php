<?php

namespace App\Models;

use App\Helpers\SearchTrait;
use App\Helpers\BeautyLink;
use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{
    use SearchTrait,BeautyLink;
    public static $imgPath = 'upload/images/partner';
    public static $urlAdminEdit = 'admin_partners.edit';
    public static $urlAdminShow = 'admin_partners.show';
    public static $urlAdminDestroy = 'admin_partners.destroy';
    public static $urlAdminUpdate = 'admin_partners.update';
    public static $namespace = 'App\Models\Partner';

    protected $fillable = [
        'image',
        'domain',
        'address',
        'phone'
    ];
}
