<?php

namespace App\Models;

use App\Helpers\BeautyLink;
use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    use BeautyLink;
    public static $imgPath = 'upload/images/banner/';
    
    protected $fillable = [
        'image',
        'key',
        'name'
    ];

    public function getArrImg()
    {
        $pic = Media::where('link',$this->image)->first();
        if($pic)
            return array($pic);
        return array();
    }

    public function getImg()
    {
        $pic = Media::where('link',$this->image)->first();
        return $pic;

    }
}
