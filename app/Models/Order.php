<?php

namespace App\Models;

use Carbon\Carbon;
use App\Helpers\SearchTrait;
use App\Helpers\BeautyLink;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use SearchTrait,BeautyLink;
    public static $urlAdminEdit = 'admin_order.edit';
    public static $urlAdminShow = 'admin_order.show';
    public static $urlAdminDestroy = 'admin_order.destroy';
    public static $urlAdminUpdate = 'admin_order.update';

    protected $fillable = [
        'order_number',
        'status_payment',
        // 'user_id',
        'note',
        'total',
        'address',
        'order_status',
        'customer_name',
        'customer_phone',
        'customer_email',
        'payment_type',
        'shipping_fee',
        'breakable_cost',
        'total_after'
    ];

    protected $hidden =[
        'updated_at',
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public static function isEmpty() {
        $isEmpty = false;
        if (self::count() == 0) $isEmpty = true;
        return $isEmpty;
    }
    
    public static function filter($request)
    {
        $query = self::query();
        $key = $request->key;
        $status_payment = $request->status_payment;
        $order_status = $request->order_status;
        if($status_payment && $status_payment != 'all'){
            $query->whereStatusPayment($status_payment);
        }
        if($order_status && $order_status != 'all'){
            $query->whereOrderStatus($order_status);
        }
        if($request->start_date && $request->end_date){
            $start_date = $request->start_date." 00:00:00";
            $end_date    = $request->end_date." 23:59:59";
            $query->whereBetween('created_at', array($start_date, $end_date));
        }
        if($request->product_id){
            $id = $request->product_id;
            $query->whereHas('product_variants',function($q) use ($id){
                $q->where('product_variant_id',$id);
            });
        }
        if($key)
            $query->where('order_number', 'like', '%'.$key.'%')
                ->orWhere('customer_name', 'like', '%'.$key.'%')
                ->orWhere('customer_phone', 'like', '%'.$key.'%')
                ->orWhere('customer_email', 'like', '%'.$key.'%');
        
        return $query;
    }
    
    public static function valueChart()
    {
        $values = [];
        for($i = 1; $i <= 12 ; $i++){
            $value = self::where('order_status',config('ecommerce.order_statuses.delivered'))->whereMonth('created_at',$i)->sum('total');
            $values[] = $value;
        }
        return $values;
    }

    public function product_variants()
    {
        return $this->belongsToMany('App\Models\Products\ProductVariant','order_product','order_id','product_variant_id')->withPivot('amount');
    }

    public function getStatus()
    {
        if($this->order_status == config('ecommerce.order_statuses.draff'))
            echo config('ecommerce.order_status_display.D');
        elseif($this->order_status == config('ecommerce.order_statuses.waiting'))
            echo config('ecommerce.order_status_display_be.W');
        elseif($this->order_status == config('ecommerce.order_statuses.delivered'))
            echo config('ecommerce.order_status_display_be.De');
        elseif($this->order_status == config('ecommerce.order_statuses.new'))
            echo config('ecommerce.order_status_display_be.N'); 
        elseif($this->order_status == config('ecommerce.order_statuses.processing'))
            echo config('ecommerce.order_status_display_be.P'); 
    }   

    public function getPaymentType()
    {
        if($this->payment_type == config('ecommerce.type_payments.Online'))
            echo config('ecommerce.type_payment_display.On');
        elseif($this->payment_type == config('ecommerce.type_payments.Offline'))
            echo config('ecommerce.type_payment_display.Off');
    }
    function code()
    {
        return $this->belongsToMany('App\Models\Codes\Code', 'code_order','order_id', 'code_id');
    }

    public function customerInfo()
    {
        echo "<ul>
            <li>". $this->customer_email ."</li>
            <li>". $this->customer_name ."</li>
            <li>". $this->customer_phone ."</li>
        </ul>";
    }

    public function updateAmountVariant()
    {
        $variants = $this->product_variants;
        // dd($variants[0]->history);
        $create_date = $this->created_at->todatestring();
        foreach($variants as $variant){
            $history = $variant->historyByDate($create_date);
            $variant->amount = $variant->amount - $variant->pivot->amount;
            $history->amount_export = $variant->pivot->amount;
            $history->pendding -= $variant->pivot->amount;
            $history->save();
            $variant->save();
        }
    }

    public function getPrice(){
        $symbol = ' VNÐ';
        $symbol_thousand = '.';
        $decimal_place = 0;
        $number = $this->total;
        $pledge_value = number_format($number, $decimal_place, '', $symbol_thousand);
        return $pledge_value . $symbol;
    }

    public function getTotalPay()
    {
        $symbol = ' VNÐ';
        $symbol_thousand = '.';
        $decimal_place = 0;
        $number = $this->total_after;
        $pledge_value = number_format($number, $decimal_place, '', $symbol_thousand);
        return $pledge_value . $symbol;
    }

    public function getShipFee()
    {
        $symbol = ' VNÐ';
        $symbol_thousand = '.';
        $decimal_place = 0;
        $number = $this->shipping_fee;
        $pledge_value = number_format($number, $decimal_place, '', $symbol_thousand);
        return $pledge_value . $symbol;
    }

    public function getBreakableCost()
    {
        $symbol = ' VNÐ';
        $symbol_thousand = '.';
        $decimal_place = 0;
        $number = $this->breakable_cost;
        $pledge_value = number_format($number, $decimal_place, '', $symbol_thousand);
        return $pledge_value . $symbol;
    }
    public function getTotalAfter()
    {
        $symbol = ' VNÐ';
        $symbol_thousand = '.';
        $decimal_place = 0;
        $number = $this->total_after;
        $pledge_value = number_format($number, $decimal_place, '', $symbol_thousand);
        return $pledge_value . $symbol;
    }

    public function orrder_penddings()
    {
        return self::whereOrderStatus(config('ecommerce.order_statuses.waiting'))->get();
    }

    public function is_new()
    {
        if($this->order_status == config('ecommerce.order_statuses.new'))
            return 1;
        return 0;
    }

    public function getOrderNumberFormatAttribute()
    {
        if($this->is_new()) return "<p style='color:red; float: right; font-size: 2em;' >*</p><p style='padding-top: 4px;'>".$this->order_number."</p>";
        else return $this->order_number;
    }

    public function discountPromotionOrder()
    {
        $code = $this->code()->first();
        if($code->reduction_type == config('ecommerce.reduction_type.Percent')) { return $this->total * $code->promotion_price * 0.01; }
        else return  $code->promotion_price;
    }

    public function confirmOrder()
    {
        if($this->order_status != (config('ecommerce.order_statuses.new'))){
            return 1;
        }return 0;
    }

    public function getCreateDateAttribute()
    {
        return $this->created_at->format('d-m-Y h:m A' );
    }
    public function getPaymentStatus()
    {
        if($this->status_payment == config('ecommerce.status_payments.paid'))
            echo config('ecommerce.status_payment_display.paid');
        elseif($this->status_payment == config('ecommerce.status_payments.unpaid'))
            echo config('ecommerce.status_payment_display.unpaid');
    }

    public function getPaymentStatusGuest()
    {
        if($this->status_payment == config('ecommerce.status_payments.paid'))
            echo trans(config('ecommerce.status_payment_display_guest.paid'));
        elseif($this->status_payment == config('ecommerce.status_payments.unpaid'))
            echo trans(config('ecommerce.status_payment_display_guest.unpaid'));
    }

    public function getTotal()
    {
        $symbol = ' VNÐ';
        $symbol_thousand = '.';
        $decimal_place = 0;
        $number = $this->total + 0; //cong tien van chuyen
        $pledge_value = number_format($number, $decimal_place, '', $symbol_thousand);
        return $pledge_value . $symbol;
    }

    public function checkProductHasPromotion()
    {
        $variants = $this->product_variants;
        foreach($variants as $variant){
            if($variant->hasPromotion() || $variant->product->checkCode()){
                return ['result' => 1, 'text' => 'Giảm SP'];
                break;
            }else{
                return ['result' => 0, 'text' => 'N\A'];
                break;
            }
        }
    }
    public function getFinalTotal()
    {
        $symbol = ' VNÐ';
        $symbol_thousand = '.';
        $decimal_place = 0;
        $number = $this->total_after + $this->shipping_fee + $this->breakable_cost;
        $pledge_value = number_format($number, $decimal_place, '', $symbol_thousand);
        return $pledge_value . $symbol;
    }
    
} 
