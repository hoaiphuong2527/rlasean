<?php

namespace App\Models;

use App\Models\Media;
use App\Helpers\BeautyLink;
use App\Helpers\SearchTrait;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use SearchTrait,BeautyLink;
    public static $imgPath = 'upload/images/post/';
    public static $urlAdminEdit = 'admin_post.edit';
    public static $urlAdminShow = 'admin_post.show';
    public static $urlAdminDestroy = 'admin_post.destroy';
    public static $urlAdminUpdate = 'admin_post.update';
    public static $urlGuestShow = 'guest_posts.show';
    public static $namespace = 'App\Models\Post';

    protected $fillable = [
        'name',
        'slug',
        'image',
        'description',
        'sort_feature',
        'status',
        'is_featured',
        'category_id',
        'read',
        'user_id'
    ];

    public static $translate_column = [
        'name',
        'slug',
        'description'
    ];
    
    public static $translate_relation = "post_translations";
    /**
     *
     */

    public function page_category() {
        return $this->belongsTo('App\Models\Categories\PageCategory', 'category_id');
    }
    public function post_category(){
        return $this->belongsTo('App\Models\Categories\PostCategory', 'category_id');
    }
    public function media()
    {
        return $this->morphToMany('App\Models\Media', 'mediable'); 
    }

    public function getStatus()
    {
        if($this->status == 0)
            echo "Draff";
        else echo "Publish";
    }

    public static function getPosts(){
        $posts = self::has('post_category')->paginate(20);
        return $posts;
    }
    //translate
    public function post_translations()
    {
        return $this->hasMany('App\Models\Translations\PostTranslation');
    }

    public function getElementByLang($locale,$element)
    {
        $item = $this->getLangItemByLocaleAndName($locale,$element);
        if($item)
            return $item->translate_text;
        return "";
    }

    public function getLangItemByLocaleAndName($locale,$col)
    {
        $item = $this->post_translations()->where('locale',$locale)->where('column_name',$col)->first();
        return $item;
    }

    public static function getItemBySlug($slug)
    {
        $locales = ['en','vi'];
        if (in_array(app()->getLocale(), $locales)){  
            $post = self::whereHas('post_translations', function ($q) use ($slug) {
                $q->whereColumnName('slug')->whereTranslateText($slug);
            })->whereStatus(1)->first();
        }if(!$post){
            $post = self::whereSlug($slug)->first();
        }
        return $post;
    }
    //end translate

    public function short_decs( $words_number )
    {
        $excerp = strip_tags($this->getAttributeLang('description') ) ;
        return substr($excerp, 0, $words_number) . '...';
    }
    
    public function short_name( $words_number )
    {
        $excerp = strip_tags($this->getAttributeLang('name') ) ;
        if(strlen($excerp) < $words_number)
            return $excerp;
        return substr($excerp, 0, $words_number) . '...';
    }
    public function user() {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function clone()
    {
        $new_post = $this->replicate();
        $new_post->status = 0;
        $new_post->slug = str_slug($this->name).rand(100000, 999999);
        $new_post->save();
        $this->clone_translates($new_post->id);
        return $new_post;
    }

    public function clone_translates($p_id)
    {
        foreach ($this->post_translations as  $item) {
            $new_item = $item->replicate();
            $new_item->post_id = $p_id;
            $new_item->save();
        }
    }

    public function comments()
    {
        return $this->morphMany('App\Models\Comment', 'reviewtable');
    }

    public function getCateName()
    {
        if($this->post_category)
            return $this->post_category->name;
        return "N/A";
    }

    public function getImg()
    {
        $pic = Media::where('link',$this->image)->first();
        return $pic;
    }

}
