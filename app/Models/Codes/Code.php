<?php

namespace App\Models\Codes;

use App\Helpers\SearchTrait;
use App\Helpers\BeautyLink;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Code extends Model
{
    use SearchTrait, BeautyLink;

    public static $urlAdminEdit = 'admin_code.edit';
    public static $urlAdminShow = 'admin_code.show';
    public static $urlAdminDestroy = 'admin_code.destroy';
    public static $urlAdminUpdate = 'admin_code.update';

    protected $fillable = [
        'id',
        'promotion',
        'promotion_price',
        'reduction_type',
        'promotion_type',
        'amount',
        'begin_day',
        'end_day',
        'status',
        'title',
        'apply_type',
        'is_over',
        'value_above',
        'amount_ordered'
    ];


    /**
     * order : relation with Order
     * productCode : relation with ProductCode
     * getPromotionType : show type of promotion in Code
     *
     * @return void
     */
    function order(){
        return $this->belongsToMany('App\Models\Order', 'code_order','code_id', 'order_id')->withPivot('user_id,email');
    }
    // function products(){
    //     return $this->belongsToMany('App\Models\Products\Product', 'product_code');
    // }
    function productCodes(){
        return $this->hasMany('app\Models\Codes\ProductCode');
    }

    function firstRecordByObjectId($id){
        return $this->productCodes()->where('objecttable_id',$id)->latest()->first();
    }

    function getPromotionType(){
        if ($this->reduction_type == config('ecommerce.reduction_type.Percent'))
            echo  config('ecommerce.reduction_type_display.percent');
        else echo config('ecommerce.reduction_type_display.absolute');
    }
    function getPromotionApply(){
        if($this->apply_type == config('ecommerce.apply_options.all_order'))
            echo config('ecommerce.apply_options_display.all_order');
        elseif($this->apply_type == config('ecommerce.apply_options.value_above'))
            echo config('ecommerce.apply_options_display.value_above');
        elseif($this->apply_type == config('ecommerce.apply_options.product'))
            echo config('ecommerce.apply_options_display.product');
        else echo config('ecommerce.apply_options_display.variant');
    }
    function getPromotionPrice(){
        if($this->reduction_type == config('ecommerce.reduction_type.Percent')){
            echo $this->promotion_price." %";
        }else{
            echo number_format($this->promotion_price)." Đ";
        }
    }

    public function invalidTime()
    {
        $now = Carbon::now()->format('Y-m-d h:m:s');
        if($this->is_over == 1){
            if(Carbon::parse($this->begin_day)->lt(Carbon::now())) return true;
            else return false;
        }else{
            if(Carbon::parse($this->begin_day)->lt(Carbon::now()) && !Carbon::parse($this->end_day)->lt(Carbon::now())) return true;
            else return false;
        }
    }

    function checkCodeValid(){
        if($this->invalidTime() && $this->promotion_type == config('ecommerce.promotion_type.order') && $this->status == config('ecommerce.code_statuses.running')){
            if($this->amount_ordered <= $this->amount)
                return true;
            return false;
        }
        return false;
    }

    function checkCodeValidProduct($amount_default,$amount_ordered)
    {
        if($this->invalidTime() && $this->promotion_type == config('ecommerce.promotion_type.product') && $this->status == config('ecommerce.code_statuses.running')){
            if($amount_default >= $amount_ordered)
                return true;
            return false;
        }
        return false;
    }
    function getBeginDay(){
        if($this->begin_day == null){
            echo "Code áp dụng số lượng";
        }
        else{
            echo date("d-m-Y h:m A", strtotime($this->begin_day));
        }

    }
    function getEndDay()
    {
        if ($this->end_day == null) {
            echo "Code áp dụng số lượng";
        } else {
            echo date("d-m-Y h:m A", strtotime($this->end_day));
        }
    }

    public function getCountCodeOrderUserdAttribute()
    {
        return $this->order->count();
    }

    public function getCountCodeProductUsedAttribute()
    {
        $total = 0;
        if($this->productCodes()->count()){
            foreach($this->productCodes as $item){
                $total += $item->amount_ordered;
            }
        }
        return $total;
    }
}
