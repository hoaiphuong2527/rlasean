<?php

namespace App\Models\Codes;

use Illuminate\Database\Eloquent\Model;

class CodeOrder extends Model
{
    protected $table = 'code_order';
    protected $fillable =[
        'id',
        'email',
        'user_id',
        'code_id',
        'order_id'
    ];


    function code()
    {
        return $this->belongsTo('App\Models\Codes\Code');
    }
    function oder()
    {
        return $this->belongsTo('App\Models\Order');
    }

}
