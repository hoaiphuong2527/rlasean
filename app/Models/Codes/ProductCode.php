<?php

namespace App\Models\Codes;

use Illuminate\Database\Eloquent\Model;

class ProductCode extends Model
{
    protected $table = 'product_code';
    protected $fillable = [
        'id',
        'objecttable_id',
        'objecttable_type',
        'amount_default',
        'amount_ordered',
        'code_id',
        'created_at',
        'updated_at'
    ];


    function code(){
        return $this->belongsTo('App\Models\Codes\Code','code_id');
    }


    function objecttable(){
        return $this->morphTo();
    }


}
