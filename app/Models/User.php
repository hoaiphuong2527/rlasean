<?php

namespace App\Models;

use Laravel\Passport\HasApiTokens;
use App\Helpers\SearchTrait;
use App\Helpers\BeautyLink;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens,Notifiable, SearchTrait,BeautyLink;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public static $imgPath = 'upload/images/user/';
    public static $urlAdminEdit = 'admin_user.edit';
    public static $urlAdminShow = 'admin_user.show';
    public static $urlAdminDestroy = 'admin_user.destroy';
    public static $urlAdminUpdate = 'admin_user.update';

    protected $fillable = [
        'name',
        'email',
        'password',
        'email_verified_at',
        'is_admin',
        'is_natural',
        'phone',
        'address',
        'dob',
        'middleware_token',
        'is_change_avt',
        'image',
        'flag',
        'provider',
        'provider_id',
        'role'

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public static function getAdmins()
    {
        $users = User::where('is_admin', 1)->get();
        $arrUserSelected = [];
        $arrAdminReceiveMail = AdminReceiveMail::where('id', '>', 0)->pluck('user_id')->toArray();
        foreach ($users as $user) {
            if (!in_array($user->id, $arrAdminReceiveMail)) {
                $arrUserSelected[] = $user;
            }
        }
        return $arrUserSelected;
    }

    public function getRole()
    {
        if($this->is_admin) return "Quản trị"; return "Người dùng";
    }
    public function isAdmin() {
        return $this->is_admin == 1;
    }

    public function getFlag()
    {
        if($this->flag) return "Đang hoạt động"; return "Đã khóa";
    }

    public function getRoleAdmin()
    {
        if($this->role === config('ecommerce.admin_role.contributor'))
            return config('ecommerce.admin_role_display.contributor');
        elseif($this->role === config('ecommerce.admin_role.editor'))
            return config('ecommerce.admin_role_display.editor');
        elseif($this->role === config('ecommerce.admin_role.admin'))
            return config('ecommerce.admin_role_display.admin');
        elseif($this->role === config('ecommerce.admin_role.controller'))
            return config('ecommerce.admin_role_display.controller');
        return "N\A"; 
    }

    public function posts()
    {
        return $this->hasMany('App\Models\Post','user_id');
    }

    public function getAvata()
    {
        if ($this->image == null ) {
            return asset('images/default_avata.png');
        } elseif ($this->is_login_social == 1 && $this->is_change_avt == 1 || $this->is_login_social
                    == 0 && $this->is_change_avt == 1) {
            return asset(self::$imgPath.'/'.$this->image);
        } 
        else {
            return $this->image;
        }
    }


    public function reviews()
    {
        return $this->hasMany('App\Models\Review');
    }
    public static function getUserbyToken($token)
    {
        $users = User::where('middleware_token',$token )->first();
        
        return $users;
    }

    public function orders()
    {
        return $this->hasMany('App\Models\Order');
    }

}
