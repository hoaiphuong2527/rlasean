<?php

namespace App\Models;

use App\Helpers\BeautyLink;
use Illuminate\Database\Eloquent\Model;

class Warranty extends Model
{
    use BeautyLink;
    public static $urlAdminEdit = 'admin_warranties.edit';
    public static $urlAdminShow = 'admin_warranties.show';
    public static $urlAdminDestroy = 'admin_warranties.destroy';
    public static $urlAdminUpdate = 'admin_warranties.update';
    
    protected $fillable = [
        'f_name', 
        'email',
        'phone',
        'amount_product',
        'purchase_date',
        'l_name',
        'location',
        'is_invite'
    ];

    public function getNameAttribute()
    {
        return  "{$this->f_name} {$this->l_name}";
    }

    public function getInvite()
    {
        if($this->is_invite)
            return "Yes";
        return "No";
    }
}
