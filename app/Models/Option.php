<?php

namespace App\Models;
use App\Models\Media;
use App\Helpers\BeautyLink;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    use BeautyLink;
    public static $imgPath = 'upload/images/option';
    public static $namespace = 'App\Models\Option';
    protected $fillable = [
            'name',
            'address',
            'email',
            'main_logo',
            'footer_logo',
            'phone',
            'iframe_gg',
            'facebook_acc',
            'intagram_acc',
            'youtube_acc',
            'type_order',
            'sologan',
            'favicon',
            'district_id',
            'percent_breakable',
            'district_stock',
            'province_stock',
            'address_stock',
            'ghtk_token',
            'ghn_token'
    ];

    public static $translate_column = [
        'name',
        'address',
        'sologan'
    ];

    public function linkImage($col)
    {
        if ($col == 'main_logo' && $this->main_logo ) {
            return $this->main_logo;
        }elseif($col == 'footer_logo' && $this->footer_logo){
            return $this->footer_logo;
        }elseif($col == 'favicon' && $this->favicon){
            return $this->favicon;
        }
        return asset('images/static/empty_image.jpg');
    }

    public function getLogoAtFooter()
    {
        if($this->footer_logo)
            return $this->footer_logo;
        else return $this->main_logo;
    }
    
    public function getLogo()
    {
        if($this->main_logo)
            return $this->main_logo;
        else return $this->footer_logo;
    }
    
    // translate
    public function translates()
    {
        return $this->morphMany('App\Models\Translations\GeneralTranslation', 'table');
    }

    public function getElementByLang($locale,$element)
    {
        $item = $this->getLangItemByLocaleAndName($locale,$element);
        if($item)
            return $item->translate_text;
        return "";
    }

    public function getLangItemByLocaleAndName($locale,$col)
    {
        $item = $this->translates()->where('locale',$locale)->where('column_name',$col)->first();
        return $item;
    }

    public function getImg($col)
    {
        $pic = Media::where('link',$this->$col)->first();
        return $pic;
    }
}
