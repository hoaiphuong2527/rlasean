<?php

namespace App\Models;

use App\Helpers\BeautyLink;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use BeautyLink;

    public static $urlAdminEdit = 'admin_comments.edit';
    public static $urlAdminShow = 'admin_comments.show';
    public static $urlAdminDestroy = 'admin_comments.destroy';
    public static $urlAdminUpdate = 'admin_comments.update';

    protected $fillable = [
        'user_id',
        'content',
        'reviewtable_id',
        'reviewtable_type',
        'status',
        'parent_id'
    ];

    /**
     * 
     */

    public static function getComments($search_query)
    {
        return $search_query->paginate(20)->load(['user']);
    }

    public function reviewtable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function parent() {
        return $this->belongsTo('App\Models\Comment', 'parent_id', 'id');
    }

    public function children()
    {   
        return $this->hasMany('App\Models\Comment', 'parent_id', 'id');
    }


    public function getStatus()
    {
        if($this->status === 0)
            return "Skip";
        return "Public";
    }

    public function getReviewType()
    {
        if($this->reviewtable_type === config('ecommerce.reviewtable_type.product'))
            return config('ecommerce.reviewtable_type_display.product');
        return config('ecommerce.reviewtable_type_display.post');
    }
}

