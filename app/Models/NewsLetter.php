<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Helpers\BeautyLink;
use App\Helpers\SearchTrait;

class NewsLetter extends Model
{
    use SearchTrait,BeautyLink;
    public static $urlAdminEdit = 'admin_news.edit';
    public static $urlAdminShow = 'admin_news.show';
    public static $urlAdminDestroy = 'admin_news.destroy';
    public static $urlAdminUpdate = 'admin_news.update';
    public static $namespace = 'App\Models\NewsLetter';

    protected $fillable = [
        'email',
        'status'
    ];
    
    protected $hidden =[
        'status'
    ];
}
