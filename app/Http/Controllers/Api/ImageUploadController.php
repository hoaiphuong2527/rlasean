<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Media;
use Auth;

class ImageUploadController extends Controller
{
    protected $inputName = 'file';
    public function store(Request $request) {
        
        $image = Input::file($this->inputName);
        if ($image) {

            $image_filename = time() . '-' . $image->getClientOriginalName();
            $image->move('upload/images', $image_filename);
            $publicImageLink = url('/upload/images/' . $image_filename);
            $meida = Media::create([
                'file_name' => $image_filename,
                'link' => $publicImageLink,
                // 'user_id' => Auth::user()->id
            ]);
            $data = $meida;
            return response()->json($data, 200);
        }
        return response()->json(['message' => 'No images found.']);
    }
}
