<?php

namespace App\Http\Controllers\Category;

use Illuminate\Http\Request;
use App\Models\Categories\Category;
use App\Http\Controllers\Controller;
use App\Models\Products\Product;
use App\Models\Categories\ProductCategory;

class ApiController extends Controller
{
    public function getProductForGuest($slug)
    {
        $categories = ProductCategory::where('parent_id',0)->get();
        $array = ProductCategory::getTreeProduct($categories);
        $category = ProductCategory::whereSlug($slug)->firstOrFail();
        $selected_cate = [$category];
        if(count($category->products)){
            $category->products;
            foreach($category->products as $product){
                if($product->product_variants)
                    $product->product_variants;
            }
        }
        $data = [
            'selected_category'  => $selected_cate,
            'categories'         => $array
        ];
        return response()->json($data,200);

    }

    public function filterProducts(Request $request)
    {
        $arr_category_ids = $request->dataFilter['arr_categories'];
        $arr_prices = $request->dataFilter['arr_prices'];
        $arr_informations = $request->dataFilter['arr_informations'];
        $arr_category_master_ids =$request->dataFilter['arr_categories_master'];
        if($arr_category_ids && $arr_prices && $arr_informations && $arr_category_master_ids){
            $products = Product::filterAll($arr_category_ids,$arr_category_master_ids,$arr_informations,$arr_prices);
        }elseif($arr_category_ids && $arr_prices && $arr_category_master_ids){
            $products = Product::catePriceMaster($arr_category_ids,$arr_category_master_ids,$arr_prices);
        }elseif($arr_category_ids && $arr_prices && $arr_informations){
            $products = Product::catePriceInformation($arr_category_ids,$arr_informations,$arr_prices);
        }elseif($arr_category_ids && $arr_category_master_ids && $arr_informations){
            $products = Product::cateMasterInfomation($arr_category_ids,$arr_category_master_ids,$arr_informations);
        }elseif($arr_prices && $arr_category_master_ids && $arr_informations){
            $products = Product::priceMasterInfo($arr_prices,$arr_category_master_ids,$arr_informations);
        }elseif($arr_category_ids && $arr_prices){
            $products = Product::catePrice($arr_category_ids,$arr_prices);
        }elseif($arr_category_ids && $arr_informations){
           $products = Product::cateInfomation($arr_category_ids,$arr_informations);
        }elseif($arr_prices && $arr_informations){
           $products = Product::priceInformation($arr_prices,$arr_informations);
        }elseif($arr_category_master_ids && $arr_prices){
            $products = Product::fillterCateMasterWithPrice($arr_category_master_ids,$arr_prices);
        }elseif($arr_category_master_ids && $arr_category_ids){
            $products = Product::masterCate($arr_category_master_ids,$arr_category_ids);
        }elseif($arr_category_master_ids && $arr_informations){
            $products = Product::masterInformation($arr_category_master_ids,$arr_informations);
        }elseif($arr_category_ids){
            $products = Product::getProductsByArrayCategory($arr_category_ids);
        }elseif($arr_category_master_ids){
            $products = Product::fillterCateMaster($arr_category_master_ids);
        }elseif($arr_prices){
            $products = Product::fillterPrice($arr_prices);
        }elseif($arr_informations){
            $products = Product::filterInformation($arr_informations);
        }
        $products->map(function ($product){
            $product['images']                  = $product->images;
            $product['default_price']           = $product->default_price;
            $product['discount_price']          = $product->discount_price;
            $product['discount_price_display']  = $product->discount_price_display;
            $product['discount_value']          = $product->discount_value;
            $product['url']                     = $product->url;
            $product['colors']                  = $product->colors;
            $product['sizes']                   = $product->sizes;
            return $product;
        });
        $data = [
            'products' => $products->toArray(),
        ];
        return response()->json($data,200);
    }

    public function getProductByCategoryId(Request $request)
    {
        $category_id = $request->category_id;
        $categories = [];
        foreach($category_id as $id){
            $categories[] = ProductCategory::getProductByCateId($id);
        }
        $data = [
            'categories'         => $categories
        ];
        return response()->json($data,200);
    }

    public function getCategoriesRelated($id)
    {
        $category = ProductCategory::find($id);
        $data = [
            'categories' => $category->getRelatedCategory()
        ];
        return response()->json($data,200);
    }

    public function getInformations($id)
    {
        $category = ProductCategory::find($id);
        $data = [
            'informations' => $category->getInformationByCategory()
        ];
        return response()->json($data,200);
    }
}
