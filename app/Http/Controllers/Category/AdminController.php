<?php

namespace App\Http\Controllers\Category;

use App\Models\Media;
use Illuminate\Http\Request;
use App\Helpers\UploadImage;
use App\Helpers\UpdateSomething;
use App\Helpers\CreateTranslation;
use App\Models\Categories\Category;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Models\Categories\PageCategory;
use App\Models\Categories\PostCategory;
use Illuminate\Support\Facades\Validator;
use App\Models\Categories\ProductCategory;
use App\Models\Translations\CategoryTranslation;

class AdminController extends Controller
{
    use UpdateSomething;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $view_name = 'index';
        if(isset($request->type)){
            $type_cate = $request->type;
            switch($type_cate){
                case('product'):
                    $model = "App\Models\Categories\ProductCategory";
                    $categories = ProductCategory::where('parent_id',1)->paginate(20);
                    break;
                case 'post':
                    $model = "App\Models\Categories\PostCategory";
                    $categories = PostCategory::where('parent_id',2)->paginate(20);
                    break;
                case 'page':
                    $model = "App\Models\Categories\PageCategory";
                    $categories = PageCategory::where('parent_id',3)->paginate(20);
                    break;
                default: 
                    return abort(404);
            }
            if(isset($_GET['name'])){
                $categories = $model::search($request->name)->whereNotIn('id',[1,2,3])->paginate( $request->get('show', 20));
                $view_name = 'search';
            }
            if(!isset($_GET['context_type'])) {
                $type = config('ecommerce.fillter_categories.all');
            }else{
                $type = $request->context_type;
            }
            return view('categories.admin.'.$view_name)->with([
                'categories' => $categories,
                'name'       => $request->name,
                'context_type' => $request->context_type,
                'type_cate'     => $request->type
                ]);
        }else{
            return abort(404);
        }
        
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $item = $this->storeItem($request);
        //translate
        foreach(Category::$translate_column as $col){
            CreateTranslation::createRecord(CategoryTranslation::$namespace,$item->id,CategoryTranslation::$foreign_key,$col,$request);
        }
        //end translate
        return redirect()->route('admin_category.edit',['id' => $item->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Category::findOrFail($id);
        $information_id = $item->informations->pluck('id')->toArray();
        $categories = Category::getCategoriesByKey($item->context_type,$id);
        return view('categories.admin.edit',['item' => $item,'categories' => $categories, 'information_id' => $information_id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = Category::findOrFail($id);
        $rule = [
            'name' => 'required',
            // 'parent_id' => 'required',
            'description' => 'sometimes|max:1000'

        ];
        $mess = [
            'name.required' => 'Vui lòng điền tên danh mục.',
            'description.max' => 'Mô tả có tối đa 1000 ký tự'
        ];
        $validator = Validator::make(Input::all(), $rule,$mess);
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $item->name = $request->name;
            //information 
            if($request->information_array)
                $item->informations()->sync(explode(",",$request->information_array));
            if($item->slug == null)
                $item->slug = str_slug($request->name).rand(100000, 999999);
            if($request->parent_id)
                $parent_id = $request->parent_id;
            else if($item->context_type == config('ecommerce.context_type.product') && $item->id != 1){
                $parent_id = 1;
            }else if($item->context_type == config('ecommerce.context_type.post') && $item->id != 2){
                $parent_id = 2;
            }else if($item->context_type == config('ecommerce.context_type.page') && $item->id != 3){
                $parent_id = 3;
            }
            $item->parent_id = $parent_id;
            $item->description = $request->description;
            if($request->image)
            $item->image = Media::find($request->image)->link;
            if($request->icon)
                $item->icon = Media::find($request->icon)->link;
            $item->save();
            //translate
            foreach(Category::$translate_column as $col){
                CreateTranslation::saveLang($col,$request,$item,CategoryTranslation::$namespace,CategoryTranslation::$foreign_key);
            }
            //end
            if($request->publish){
                $item->status = 1;
                $item->save();
                $item->updateStatusChildren();
            }
            elseif($request->continue){
                $item->status = 1;
                $item->save();
                $context_type = $item->context_type;
                $request['context_type'] = $context_type;
                $new = $this->storeItem($request);
                return redirect()->route('admin_category.edit',['id' => $new->id]);
            }else{
                $item->status = 0;
                $item->save();
                $item->updateStatusChildren();
            }
            return redirect()->route('admin_category.index',['type' => $item->context_type])->with(['status' => 'Cập nhật thành công']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Category::findOrFail($id);
        if($item->parent_id == 0 && $item->status != 2)
            return redirect()->back()->with(['status' => 'Không thể xóa danh mục '. $item->name]);
        $item->deleteChildren();
        $item->delete();
            return redirect()->back()->with(['destroy' => 'Dữ liệu được xóa thành công!']);
    }

    public function updateStatusCategory($id)
    {
        $item = Category::findOrFail($id);
        $this->updateStatus($item);
        if(!$item->status)
            $item->updateStatusChildren();
    }

    public function storeItem($request)
    {
        return Category::create([
            'context_type' => $request->context_type,
            'status'       => 2,
            'parent_id'    => 1
        ]);
    }
}
