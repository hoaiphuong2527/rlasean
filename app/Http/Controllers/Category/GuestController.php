<?php

namespace App\Http\Controllers\Category;

use Illuminate\Http\Request;
use App\Models\Products\Product;
use App\Http\Controllers\Controller;
use App\Models\Slideshow;
use App\Models\Post;
use App\Models\Option;
use App\Models\Partner;
use App\Models\Banner;
use Illuminate\Support\Facades\Session;

use App\Models\Categories\ProductCategory;

class GuestController extends Controller
{
    public function index()
    {
        if( Session::get('is_change')){
            Session::put('is_change',0);
            $id = Session::get('id');
            if($id){
                $slug = Session::get('model')::find($id)->getAttributeLang('slug');
                return redirect()->route(Session::get('route_name'),['id' => $id ,'slug' => $slug]);
            }
            return redirect()->route(Session::get('route_name'));
        }
        $slideshows = Slideshow::orderBy('sort_feature')->whereStatus(1)->take(3)->get();
        $posts = Post::whereHas('post_category')->whereStatus(1)->latest()->take(3)->with(['user'])->get();
        $categories = ProductCategory::has('productOnlines')->whereStatus(1)->get();
        // $new_arrivals = Product::has('product_variant_onlines')->whereStatus(1)->latest()->take(10)->get();
        // $specials = Product::has('product_variant_onlines')->whereStatus(1)->whereIsFeatured(1)->take(10)->get();
        // $best_sellers = Product::getBestSeller();
        $product_has_promotion = Product::productHasCode();
        $latest_product = Product::latestProduct();
        return view('guest.index')->with([
            'categories'            => $categories,
            'slideshows'            => $slideshows,
            'posts'                 => $posts,
            'option'                => Option::first(),
            'partners'              => Partner::all(),
            // 'new_arrivals'      => $new_arrivals,
            // 'specials'          => $specials,
            // 'best_sellers'      => $best_sellers
            'product_has_promotion' => $product_has_promotion,
            'latest_product'        => $latest_product
        ]);
    }

    public function getAllCategoryOnline()
    {
        $categories = ProductCategory::has('productOnlines')->whereStatus(1)->get();
        return view('categories.guest.product.index')->with([
            'item'         =>  ProductCategory::first(),
            'categories'    => $categories,
            'option'            => Option::first(),
            'banner' => Banner::find(3)
        ]);

    }
}
