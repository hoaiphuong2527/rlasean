<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function getRegister(){
        return view('register_login.register');
    }
    public function postRegister(Request $req){
        $check = $req->only('email', 'password');

        if (Auth::attempt($check)) {
            // Authentication passed...
            return redirect()->intended('regiseter');
        }
    }
}
