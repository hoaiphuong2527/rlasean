<?php

namespace App\Http\Controllers\Pages;

use Auth;
use App\Models\Post;
use Illuminate\Http\Request;
use App\Helpers\UpdateSomething;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Models\Categories\PageCategory;
use Illuminate\Support\Facades\Validator;
use App\Helpers\CreateTranslation;
use App\Models\Translations\PostTranslation;

class AdminController extends Controller
{
    use UpdateSomething;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = Post::whereHas('page_category')->get();
        return view('static_page.admin.index')->with([
            'pages' => $pages
        ]);
    }

    public function store(Request $request)
    {
        $post = Post::create([
            'user_id' => Auth::user()->id,
            'status' => 0,
            'category_id' => 3
        ]);
        return redirect()->route('admin_page.edit',['id' => $post->id]);

    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::findOrFail($id);
        return route('guest.view_static_page',['slug' => $post->slug]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::findOrFail($id);
        $category = PageCategory::first();
        return view('static_page.admin.edit',[
            'item' => $post,
            'category' => $category,
            'url'      => 'guest.view_static_page'
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $page = Post::findOrFail($id);
        $rule = [
            'name'        => 'required|max:191',
            'description' => 'required',
        ];
        $mess = [
            'name.required' => 'Vui lòng điền tên trang tĩnh.',
            'name.max'  => ' Tên có tối đa 191 ký tự',
            'description'   => 'Vui lòng điền nội dung cho trang tĩnh'
        ];
        $validator = Validator::make(Input::all(), $rule,$mess);        
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $page->name = $request->name;
            $page->slug = str_slug($request->name);
            // $page->category_id = $request->category_id;
            $page->description = $request->description;
            if($request->update_lang) {
                $page->save();
                return redirect()->route('admin_page.edit_lang',$page->id);
            }
            if($request->publish){
                $page->status = 1;
            }elseif($request->continue){
                $page->status = 1;
                $page->save();
                $new = Post::create([
                    'user_id' => Auth::user()->id,
                    'status' => 0,
                    'category_id' => 3
                ]);
                return redirect()->route('admin_page.edit',['id' => $new->id]);
            }else{
                $page->status = 0;
            }
            $page->save();
        }
        return redirect()->back()->with(['status' => 'Cập nhật thông tin thành công!']);
    }

    public function updateStatusByAjax($id)
    {
        $item = Post::findOrFail($id);
        $this->updateStatus($item);
    }

    
    public function editTextLang($id){
        $item = Post::findOrFail($id);
        return view('static_page.admin.lang')->with(['item'=>$item,
            'url' => 'guest.view_static_page'

        ]);
    }
    public  function updateLang($id,Request $request){
        // translate
        $item = Post::findOrFail($id);
        foreach(Post::$translate_column as $col){
            CreateTranslation::saveLang($col,$request,$page,PostTranslation::$namespace,PostTranslation::$foreign_key);
        }
        $item->save();
        return redirect()->back()->with(['status' => 'Cập nhật dữ liệu thành công!']);
    }
}
