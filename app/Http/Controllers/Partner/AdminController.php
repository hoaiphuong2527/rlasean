<?php

namespace App\Http\Controllers\Partner;

use App\Models\Partner;
use App\Helpers\UploadImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('partners.admin.index')->with(['partners' => Partner::all()]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $item = Partner::create();
        return redirect($item->urlAdminEdit());
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Partner::findOrFail($id);
        return view('partners.admin.edit')->with(['item' => $item]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = Partner::findOrFail($id);
        $rule = [
            'image' => 'required',
            'domain' => 'required|max:191',
            'address' => 'required|max:255',
            'phone'     => 'required|min:10|max:13'

        ];
        $validator = Validator::make(Input::all(), $rule);
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $item->domain = $request->domain;
            $item->address = $request->address;
            $item->phone = $request->phone;
            if (Input::hasfile('image'))
            {
                $path = Partner::$imgPath;
                $image = $request->file('image');
                $file_name = 'image';
                $slug = str_slug(explode('.',Input::file('image')->getClientOriginalName())[0]);
                $item->image = UploadImage::uploadImage($file_name,$image,$path,$slug,$item,500,350,'image');
            }
            $item->save();
            return redirect()->route('admin_partners.index')->with(['status' => 'Cập nhật thành công']);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Partner::findOrFail($id);
        $item->delete();
        return redirect()->route('admin_partners.index')->with(['destroy' => 'Dữ liệu xóa thành côngss']);
    }
}
