<?php

namespace App\Http\Controllers\Partner;

use App\Models\Option;
use App\Models\Banner;
use App\Models\Partner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GuestController extends Controller
{
    public function index()
    {
        $banner = Banner::find(3);
        return view('partners.guest.index')->with(['partners' => Partner::all(),'banner' => $banner,'option'   => Option::firstOrFail()
        ]);
    }
}
