<?php

namespace App\Http\Controllers\Auth;

use Auth;
use Carbon\Carbon;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Events\User\NoticeActiveSuccessEvent;

class ActivateAccountController extends Controller
{
    public function activate($user_id,$token)
    {
        $user = User::findOrFail((int) $user_id);
        $exp = Carbon::parse($user->expried_active);
        if($user->middleware_token != $token)
        {
            return abort(404);
        }
        $user->is_active = 1;
        $user->save();
        event(new NoticeActiveSuccessEvent($user));
        return redirect('/');
    }
}
