<?php

namespace App\Http\Controllers\Api;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CheckAuthController extends Controller
{
    public function checkAuth()
    {
        if(Auth::check()){
            $data = [
                'auth'  => 1
            ];
        }else{
            $data = [
                'auth'  => 0
            ];
        }
        return response()->json($data, 200);
    }
}
