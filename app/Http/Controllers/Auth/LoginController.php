<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Socialite;
use Auth;
use App\Models\User;
use Illuminate\Http\Request;
use PHPUnit\Framework\MockObject\Stub\ReturnArgument;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    protected function authenticated($request , $user){
        if($user->email_verified_at == null || $user->flag == 0){
            auth()->logout();
            return redirect()->back();
        }
        return redirect()->intended($this->redirectPath());
    }
    public function redirectPath()
    {
        if (session()->has('url.intended')) {
            $url = session()->get('url.intended');
            session()->forget('url.intended');
            return $url;
        } else {
            return $this->redirectTo;
        }
    }
    public function showLoginForm()
    {
        // if (!session()->has('url.intended')) {
        //     session(['url.intended' => 'hello']);
        // }
        // if(!Auth::check())
        //     return view('auth.login');
        return redirect($this->redirectTo);
    }
    public function setIntendedUrl(Request $request)
    {
        $url = $request->json()->get('intended_url');
        if (!session()->has('url.intended')) {
            session(['url.intended' => $url]);
        }
    }
}
