<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Socialite;
use Carbon\Carbon;
use Auth;

class SocialiteController extends Controller
{
    protected $redirectTo = '/';

    public function redirectToGoogle($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function handleGoogleCallback($provider)
    {
        $user = Socialite::driver($provider)->stateless()->user();
        $authUser = $this->findOrCreateUser($user,$provider);
        Auth::login($authUser,true);
        return redirect($this->redirectTo);
    }

    public function findOrCreateUser($user, $provider){
        $authUser = User::where('provider_id', $user->id)->first();
        if($authUser){
            return $authUser;
        }
        return User::Create([
            'name' => $user->name,
            'email' => $user->email,
            'email_verified_at' => Carbon::now(),
            'password' => md5(Carbon::now()),
            'middleware_token' => md5(Carbon::now().rand(000000,999999)),
            'provider' => strtoupper($provider),
            'provider_id' => $user->id
        ]);
    }
}
