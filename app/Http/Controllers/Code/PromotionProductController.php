<?php

namespace App\Http\Controllers\Code;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Products\Product;
use App\Models\Codes\Code;
use App\Models\Codes\ProductCode;

class PromotionProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $product = Product::all();
        $code = Code::where('promotion_type',config('ecommerce.promotion_type.order'))->get();
        $productcode = ProductCode::with('Product','Code')->paginate(10);
        // dd($productcode);
        // die();
        return view('admin.code.promotion_products.index')->with([
            'product' => $product,
            'code' => $code,
            'productcode' => $productcode
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pros = $request->input('product_id');
        dd($pros);
        foreach( $pros as $pro){
            $productcode = new ProductCode();
            $productcode->product_id = $pro;

            $productcode->code_id = $request->code_id;
            $productcode->save();
        }

        // $productcode = new ProductCode();
        // $productcode->product_id = $request->product_id;
        // $productcode->code_id = $request->code_id;
        // $productcode->save();
        return redirect()->back()->with(['status' => 'Bạn vừa thêm 1 sản phẩm khuyến mãi.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
