<?php

namespace App\Http\Controllers\Code;

use Carbon\Carbon;
use App\Models\Codes\Code;
use Illuminate\Http\Request;
use App\Models\Codes\CodeOrder;
use App\Models\Products\Product;
use App\Models\Codes\ProductCode;
use App\Http\Controllers\Controller;
use App\Models\Products\ProductVariant;

class ApiController extends Controller
{

    public function checkCode(Request $request)
    {
        $code = $this->isValidCode($request->receiveCode);
        if($code){
            if($code->apply_type == config('ecommerce.apply_options.value_above')){
                if($code->is_over == $request->total_order){
                    $data = [
                        'resultCode' => 1,
                        'code'=> $code,
                    ];
                }else{
                    $data = [
                        'resultCode' => -1,
                        'code' =>''
                    ];
                }
            }else{
                $data = [
                    'resultCode' => 1,
                    'code'=> $code,
                ];
            }
        }else{
            $data = [
                'resultCode' => -1,
                'code' =>''
            ];
        }
        return response()->json($data);
    }

    public function checkEmail(Request $request){
        $receiveEmail = $request->checkEmail;
        $receiveCode = $request->checkCode;
        if($receiveCode){
            $code = $this->isValidCode($receiveCode);
            if($code){
                $item = CodeOrder::whereEmail($receiveEmail)->whereCodeId($code->id)->first();
                if(!$item){
                    $data = [
                        'resultCode' => 1,
                        'code'=> $code,
                        'message' => " Bạn có thể sử dụng mã khuyến mãi!"
                    ];
                }else{
                    $data = [
                        'resultCode' => -1,
                        'code' =>'',
                        'message' => " Email này đã sử dụng mã khuyến mãi!"
                    ];
                }
            }else{
                $data = [
                    'resultCode' => -1,
                    'code'=> '',
                    'message' => "Mã này không tồn tại hoặc đã hết hạn!"
                ];
            }
        }else{
            $data = [
                'resultCode' => -1,
                'code'=> '',
                'message' => ""
            ];
        }
        return response()->json($data);
    }

    public function isValidCode($receiveCode)
    {
        $code = Code::where('promotion', 'LIKE',$receiveCode)->first();
        if($code){
            if($code->checkCodeValid() && $receiveCode === $receiveCode){
                return $code;
            }else 
                return 0;
        }else{
            return 0;
        }
    }

    public function validateCode(Request $request)
    {
        $promotion = $request->promotion;
        $promotion_type = $request->promotion_type;

        $code = Code::wherePromotionType($promotion_type)->wherePromotion($promotion)->first();
        if($code){
            $data = [
                'result' => 1,
                'message' => 'Mã giảm giá đã tồn tại.',
            ];
        }else{
            $data = [
                'result' => 0,
                'message' => 'Hợp lệ.',
            ];
        }
        return response()->json($data);
    }

    public function searchProductByNameInvalid(Request $request)
    {
        $search = $request->search;
        $apply_type = $request->apply_type;
        if($search){
            $products = Product::whereHas('product_variants')->search($search)->get();
            $arr_id = [];
            if($products->count() != 0){
                foreach($products as $product){
                    if($product->hasCode()){
                        foreach($product->product_codes as $item){
                            if(!$item->code->invalidTime()){
                                $arr_id[] = $product->id;
                            }
                        }
                    }else {
                        $arr_id[] = $product->id;
                    }
                }
                $products = Product::whereHas('product_variants')->whereIn('id',$arr_id)->get();
                if($apply_type == config('ecommerce.apply_options.product'))
                    $products->map(function ($product){
                        $product['image'] = $product->linkImage();
                        $product['price_round'] = $product->price;
                        $product['max']         = $product->getTotal();
                    });
                else{
                    $products = ProductVariant::whereIn('product_id',$arr_id)->get();
                    $products->map(function ($product){
                        $product['max']         = $product->amount;

                    });
                }
                $data = [
                    'result'   => 1,
                    'products' => $products->toArray(),
                ];
            }else{
                $data = [
                    'result'   => 0,
                    'products' => 0,
                ];
            }
        }else if($search == null && $apply_type == config('ecommerce.apply_options.product')){
            $products = Product::whereHas('product_variants')->get();
            $products->map(function ($product){
                $product['image'] = $product->linkImage();
                $product['price_round'] = $product->price;
                $product['max']         = $product->getTotal();
            });
            $data = [
                'result'   => 1,
                'products' => $products
            ];
        }else if($search == null && $apply_type == config('ecommerce.apply_options.variant')) {
            $arr_id = Product::whereHas('product_variants')->pluck('id');
            $products = ProductVariant::whereIn('product_id',$arr_id)->get();
            $products->map(function ($product){
                $product['max']         = $product->amount;

            });
            $data = [
                'result'   => 1,                
                'products' => $products->toArray()
            ];
        }else if($search == null) {
            $products = Product::whereHas('product_variants')->get();
            $products->map(function ($product){
                $product['image'] = $product->linkImage();
                $product['price_round'] = $product->price;
                $product['max']         = $product->getTotal();
            });
            $data = [
                'result'   => 1,
                'products' => $products
            ];
        }
        return response()->json($data,200);
    }
}
