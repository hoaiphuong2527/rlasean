<?php

namespace App\Http\Controllers\Code;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Codes\Code;
use Validator;
use Hash;
use Auth;
use Carbon\Carbon;
use App\Models\Products;
use App\Models\Products\Product;
use App\Models\Codes\ProductCode;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Code::query();

        if($request->promotion){
            $query->where('promotion', 'like', '%'.$request->promotion.'%');

        }if(!$request->reduction_type){
            $type = config('ecommerce.fillter_code.all');
        }else{
            $type = $request->reduction_type;
        }
        switch ($type) {
            case config('ecommerce.fillter_code.all'):
                break;
            default:
            $query->whereReductionType($request->reduction_type);
        }
        $code = $query->paginate(20);
        return view('code.admin.index')->with([
            'code' => $code,
            'promotion' => $request->promotion,
            'reduction_type' => $request->reduction_type
        ]);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $item = Code::Create();
        return redirect()->route('admin_code.edit', ['id' => $item->id]);


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $code = Code::findOrFail($id);
        if($code->reduction_type){
            return redirect()->back();
        }
        return view('code.admin.edit')->with([
            'code' => $code,
        ]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $code = Code::findOrFail($id);
        // $validator =
        //     Validator::make($request->all(), [
        //     'promotion'                    => 'required',
        //     'reduction_type'               => 'required',
        //     'promotion_price'              => 'required|numeric',
        //     // 'amount'                       => 'required|numeric',
        // ], [
        //     'promotion.unique'             => 'Mã đã tồn tại vui lòng nhập 1 mã khác',
        //     'promotion.renquired'          => 'Vui lòng nhập Mã Giảm Giá',
        //     'reduction_type.required'      => 'Vui lòng chọn loại mã giảm',
        //     'promotion_price.required'     => 'Vui lòng nhập số tiền muốn giảm',
        //     'promotion_price.numeric'      => 'Giá tiền phải là số',
        //     // 'amount.required'              => 'Vui lòng nhập số lượng',
        //     // 'amount.numeric'               => 'Số lượng phải là số',
        // ]);
        // if ($validator->fails()) {
        //     return redirect()->back()->withErrors($validator)->withInput();
        // } else {
            $code->promotion = $request->promotion;
            $code->reduction_type = $request->reduction_type;
            $code->promotion_price = $request->promotion_price;
            $code->promotion_type = $request->promotion_type;
            $code->value_above = $request->value_above;
            if($code->promotion_type == config('ecommerce.promotion_type.product')){
                if($request->apply_type == config('ecommerce.apply_options.product'))
                    $objecttable_type = config('ecommerce.objects.product');
                else  $objecttable_type = config('ecommerce.objects.variant');
                $total_amount = 0;
                $arr_object = json_decode($request->arr_object,true);
                foreach($arr_object as $key => $obj){
                    if($obj['value'] != null)
                        $total_amount += (int) $obj['value'];
                    ProductCode::create([
                        'code_id'           => $id,
                        'objecttable_id'    => $obj['id'],
                        'objecttable_type'  => $objecttable_type,
                        'amount_default'    => (int) $obj['value']
                    ]);
                }
                $code->apply_type = $request->apply_type;
                $code->amount = $total_amount;
            }else{
                $code->apply_type = $request->apply_type;
                $code->amount = $request->amount;
            }
            if($request->is_over){
                $code->is_over = 1;
                $code->begin_day = Carbon::createFromFormat('d/m/Y h:i A',$request->begin_day)->format('Y-m-d h:m:s');
                $code->end_day = null;
            }else{
                $code->begin_day = Carbon::createFromFormat('d/m/Y h:i A',$request->begin_day)->format('Y-m-d h:m:s');
                $code->end_day = Carbon::createFromFormat('d/m/Y h:i A',$request->end_day)->format('Y-m-d h:m:s');
                $code->is_over = 0;
            }
            if($request->is_running)
                $code->status = config('ecommerce.code_statuses.running');
            else $code->status = config('ecommerce.code_statuses.pausing');
            $code->save();
            return redirect()->route('admin_code.index')->with([
                'success' => 'Cập nhật dữ liệu thành công!',
            ]);
        // }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Code::findOrFail($id);
        $item->delete();
        return redirect()->route('admin_code.index')->with([
            'status' => 'Xóa dữ liệu thành công!',
        ]);
    }

    public function updateStatus($id)
    {
        $item = Code::findOrFail($id);
        if ($item == null){
            $data = [
                'status' => -1,
            ];
        }else{
            if($item->status == config('ecommerce.code_statuses.running')){
                $item->status =  config('ecommerce.code_statuses.pausing');   
            }
            else {
                $item->status = config('ecommerce.code_statuses.running');
            }
            $data = [
                'status' => $item->status,
            ];
        }
        $item->save();
        return response()->json($data, 200);
    }
}
