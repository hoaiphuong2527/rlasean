<?php

namespace App\Http\Controllers;

use App\Models\Banner;
use App\Models\Post;
use Illuminate\Http\Request;

class StaticPageController extends Controller
{
    public function index($slug)
    {
        $post = Post::getItemBySlug($slug);
        if($post){
            if($post->id == 1){
                $view_name = 'about_us';
            }
            else{
                $view_name = 'static_page';
            }
            return view('guest.'.$view_name)->with([
                'item'  => $post,
                'banner' => Banner::where('key','static_page')->first()
            ]);
        }
        return abort(404);
    
    }
}
