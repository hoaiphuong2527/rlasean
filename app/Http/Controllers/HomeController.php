<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\SessionManager;
use App\Models\Products\Product;
use Cookie;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    public function setLocale(Request $request)
    {
        $locale = $request->locale;
        $url = $request->url;
        $model = $id = '';
        if($request->model) $model = $request->model;
        if($request->id) $id = $request->id;
        $name_route = app('router')->getRoutes()->match(app('request')->create($url))->getName();
        $locales = ['en','vi'];
        $segments = explode('.', $name_route);
        if(in_array($locale, $locales) ){
            if(isset($segments[2])) {
                if(in_array($segments[2],$locales)){
                    Session::put("model",$model);
                    Session::put("id",$id);    
                    Session::put("is_change",1);
                    Session::put("route_name",str_replace($segments[2],$locale, $name_route ));
                    Cookie::queue("lang", $locale, 60);
                    app()->setLocale($locale);    
                    return redirect('/');
                }
            }else{
                return redirect()->back();
            }
            
        }
        return redirect()->back();
    }

    public function search(Request $request)
    {
        $name = $request->product_name;
        $products = Product::has('category')->search($name)->whereStatus(1)->get();
        return view('guest.search')->with([
            'products'      => $products,
            'product_name'  => $name,
        ]);
    }

    public function getPromotionProduct()
    {
        $product_has_promotion = Product::productHasCode();
        return view('guest.products',[
            'title'    => trans('component_text.labels.product_promotion'),
            'products' => $product_has_promotion
            ]);
    }

    public function getNewProduct()
    {
        $latest_product = Product::latestProduct();
        return view('guest.products',[
            'title'    => trans('component_text.labels.new_product'),
            'products' => $latest_product
        ]);
    }

}
