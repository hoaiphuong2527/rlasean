<?php

namespace App\Http\Controllers\Contacts;

use App\Models\Banner;
use App\Models\Option;
use App\Models\Contact;
use Illuminate\Http\Request;
use App\Models\AdminReceiveMail;
use App\Http\Controllers\Controller;
use App\Events\GuestSendContactEvent;
use Illuminate\Support\Facades\Validator;

class GuestController extends Controller
{
    public function index()
    {
        $banner = Banner::where('key','contact')->first();  
        $option = Option::firstOrFail();
        return view('guest.contact')->with([
            'banner'   => $banner,
            'option'   => $option
        ]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'f_name'        => 'sometimes|max:191',
                'l_name'        => 'sometimes|max:191',
                'email'         => 'sometimes|min:6|max:191',
                'phone'         => 'sometimes|numeric',
                'message'       => 'sometimes',
                'company'       => 'sometimes|max:255',
                'website'       => 'sometimes|max:255',
                'country'       => 'sometimes|max:255',
            ],[]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $contact = Contact::create([
                'f_name'    => $request->f_name,
                'l_name'    => $request->l_name,
                'email'     => $request->email,
                'phone'     => $request->phone,
                'message'   => $request->message,
                'company'   => $request->company,
                'website'   => $request->website,
                'country'   => $request->country,
                'type'      => $request->type
            ]);
            event(new GuestSendContactEvent($contact));
            return redirect()->back()->with(['status' => trans('component_text.alerts.contact')]);
        }
    }

    public function corporateGift(Request $request)
    {
        $banner = Banner::where('key','contact')->first();  
        $option = Option::firstOrFail();
        return view('guest.corporate_gift')->with([
            'banner'   => $banner,
            'option'   => $option
        ]);
    }
}
