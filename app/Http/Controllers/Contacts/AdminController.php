<?php

namespace App\Http\Controllers\Contacts;

use App\Models\Contact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Events\Admin\SendMailContact;
use Illuminate\Support\Facades\Validator;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gift_contacts = Contact::whereType(config('ecommerce.contact_types.gift'))->paginate(20);
        $contacts = Contact::whereType(config('ecommerce.contact_types.contact'))->paginate(20);
        return view('contacts.admin.index',['contacts' => $contacts ,'gift_contacts' => $gift_contacts]);
}

    public function edit($id)
    {
        $item = Contact::findOrFail($id);
        $item->status = 1;
        $item->save();
        return view('contacts.admin.reply',['contact' => $item]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $contact = Contact::findOrFail($id);
        $validator = Validator::make(
            $request->all(),
            [
                'reply_content'         => 'required'
            ],[
                'reply_content.required'          => 'Vui lòng nhập nội dung phản hồi'
            ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $content = $request->input('reply_content');
            $contact->status = 2; $contact->save();
            event(new SendMailContact($contact,$content));
            return redirect()->route('admin_contacts.index')->with(['status' => 'Đã gửi dữ liệu đến '.$contact->name]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $contact = Contact::findOrFail($id);
        $contact->delete();
        return redirect()->route('admin_contacts.index')->with(['destroy' => 'Xóa dữ liệu thành công']);
    }

    public function deleteMutile()
    {
        Contact::deleteMulti();
        return redirect()->route('admin_contacts.index')->with(['destroy' => 'Xóa dữ liệu thành công']);
    }
}
