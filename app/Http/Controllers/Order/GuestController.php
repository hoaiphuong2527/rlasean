<?php

namespace App\Http\Controllers\Order;

use Cart;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Order;
use App\Models\Option;
use App\Models\Codes\Code;
use Illuminate\Http\Request;
use App\Helpers\MakeOrderNumber;
use App\Models\Products\Product;
use App\Models\Codes\ProductCode;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use App\Events\OrderSuccessfullyEvent;
use App\Models\Products\ProductVariant;
use Illuminate\Support\Facades\Session;
use App\Events\User\CreateNewUserNatural;
use Illuminate\Support\Facades\Validator;
use App\Models\Products\HistoryImportProduct;

class GuestController extends Controller
{
    public function index()
    {
        $option = Option::first();
        $cartInfor = Cart::content();
        if(count($cartInfor) == 0){
            return redirect()->back();
        }
        return view('order.guest.checkout')->with(['cartInfor' => $cartInfor,'total' => Cart::subtotal(),'option' => $option]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'customer_name'               => 'required',
                'customer_email'              => 'required|max:191',
                'customer_phone'              => 'required|max:11',
                'note'                        => 'sometimes|max:255',
                // 'address'                     => 'required|max:255'
            ],[
            ]
        );
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $address = $request->address.",".$request->ward.",".$request->district.",".$request->province;
            $user = User::where('email',$request->customer_email)->first();
            if(!$user){
                $str_pass = $this->randomPassword(6);
                if($this->checkUser($request,$str_pass)){
                    $user = $this->checkUser($request,$str_pass);
                    if($user->is_natural){
                        event(new CreateNewUserNatural($user,$str_pass));
                    }
                }
            }
            $order = Order::create([
                'order_number'      => MakeOrderNumber::makeOrderNumber(),
                'total'             => str_replace(',', '', $request->total),
                // 'user_id'           => 1,
                'payment_type'      => $request->payment_type,
                'customer_email'    => $request->customer_email,
                'customer_phone'    => $request->customer_phone,
                'customer_name'     => $request->customer_name,
                'note'              => $request->note,
                'address'           => $address,
                'order_status'      => config('ecommerce.order_statuses.new'),
                'shipping_fee'      => $request->shipping_fee,
                'total_after'       => str_replace(',', '', $request->total_after),
                'breakable_cost'    => $request->breakable_cost
            ]);

            $cartInfor = Cart::content();
            if (count($cartInfor) > 0) {
                $arr_id = [];
                foreach ($cartInfor as $key => $item) {
                    $qty = $this->checkAmount($item->id,$item->qty);
                    $array[$key] = [
                        'order_id'              => $order->id,
                        'product_variant_id'    => $item->id,
                        'amount'                => $qty
                    ];
                    HistoryImportProduct::updateOrCreateExportInDate($item->id,$qty);
                    // $this->updateAmount($item->id,$item->qty);
                    $order->product_variants()->sync($array);
                }
            }
            $code_name = $request->input('codeName');
            $this->updateCodeOrderAmount($order, $code_name);
            $this->updateCodeProductAmount($cartInfor);
            Cart::destroy();
            event( new OrderSuccessfullyEvent($order,$request->customer_email,$request->customer_name));
            // if($order->status_payment == config('ecommerce.type_payments.Online')){
            //     return "tích hợp PTTT";
            // }
            return redirect()->route('guest_order.success',['order_number' => $order->order_number]);
        }
    }
    public function updateCodeProductAmount($cartInfor) {

        foreach($cartInfor as $item){
            $variant = ProductVariant::find($item->id)->first();
            $product = $variant->product;
            $code = $variant->checkTypeApply()['code'];
            if($code) {
                $pivot_code_item = $this->getPivotItemAccept($code,$variant,$variant->checkTypeApply()['type']);
                $amount_accept = $pivot_code_item->amount_ordered;
                $amount_default = $pivot_code_item->amount_default;    
                Session::put("begin_amount_". $code->id,$amount_default);
                $session = Session::get("amount_".$code->id);
                if($session < 0) $session = 0;
                    $pivot_code_item->amount_accept = $session;
                $code->save();
                // $amount = Session::forget("amount_".$code->id);
            }
        }

    }
     public function updateCodeOrderAmount($order , $code){
            if($code){
                $code_request = Code::where('promotion',$code)->firstOrFail();
                $code_amount = $code_request->amount;
                $code_request->amount = $code_amount - 1;
                $code_id = $code_request->id;
                $email = $order->customer_email;
                if($code->amount_ordered <= $code->amount)
                    $code->amount_ordered += 1; 
                $code->save();
                $arr = [
                    'code_id' => $code_id,
                    'email'  => $email,
                    'user_id' => null
                ];
                $order->code()->sync(array($arr),true);
                $code_request->save();
            }
    }
    public function checkUser($request,$str_pass)
    {
        if($request->customer_email == null){
            return null;
        }else{
            $user = User::firstOrCreate([
                'email' => $request->customer_email
            ], [
                'name'              => $request->customer_name,
                'email'             => $request->customer_email,
                'phone'             => $request->customer_phone,
                'password'          => Hash::make($str_pass),
                'is_natural'        => 1,
                'middleware_token' => md5(Carbon::now().rand(00000,99999)),
                'email_verified_at' => Carbon::now()
            ]);
        }
        return $user;
    }

    public function randomPassword($length)
    {
        $str = "";
        $characters = array_merge(range('A','Z'), range('a','z'), range('0','9'));
        $max = count($characters) - 1;
        for ($i = 0; $i < $length; $i++) {
            $rand = mt_rand(0, $max);
            $str .= $characters[$rand];
        }
        return $str;
    }

    public function thanksBooking($order_number)
    {
        $order = Order::whereOrderNumber($order_number)->with('product_variants')->first();
        return view('order.guest.thank_for_booking')->with([
            'order' => $order,
            'option'    => Option::first()
        ]);
    }

    public function checkAmount($id,$qty)
    {
        $variant = ProductVariant::find($id);
        $order = $variant->countAmountOrderWaiting();
        $stock = $variant->amount - $order;
        if($qty <= $stock)
            return $qty;
        return $stock;
    }

    public function getPivotItemAccept($code,$variant,$type_apply)
    {
        if($type_apply == 'product'){
            $item = $code->firstRecordByObjectId($variant->product->id);
        }else{
            $item = $code->firstRecordByObjectId($variant->id);
        }
        return $item;
    }
 
}
