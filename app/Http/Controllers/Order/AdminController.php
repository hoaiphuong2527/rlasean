<?php

namespace App\Http\Controllers\Order;

use App\Models\Order;
use App\Models\Option;
use App\Models\Codes\Code;
use Illuminate\Http\Request;
use App\Exports\OrdersExport;
use App\Helpers\MakeOrderNumber;
use App\Http\Controllers\Controller;
use App\Events\OrderSuccessfullyEvent;
use App\Models\Products\ProductVariant;
use Illuminate\Support\Facades\Session;
use App\Models\Products\HistoryImportProduct;

class AdminController extends Controller
{
    public function index(Request $request)
    {
        if($request->export){
            return (new OrdersExport($request))->download('DS_Don_Hang.xlsx');
        }
        if (Order::isEmpty()) {
            return view('order.admin.empty');
        }
        $orders = Order::filter($request)->latest()->paginate(20);
        return view('order.admin.index')->with([
            'orders' => $orders,
            'key'    => $request->key,
            'status_payment' => $request->status_payment,
            'order_status'   => $request->order_status
        ]);    
    }

    public function create()
    {
        return view('order.admin.create',['option' => Option::first()]);
    }

    public function edit($id)
    { 
        $order = Order::findOrFail($id)->load('product_variants');

        if($order->order_status == config('ecommerce.order_statuses.new')){
            $order->order_status = config('ecommerce.order_statuses.processing');
            $order->save();
        }
        $statuses = config('ecommerce.order_status_display_be');
        $payment_types = config('ecommerce.type_payment_display');
        $payment_statuses = config('ecommerce.status_payment_display');
        return view('order.admin.edit')->with(['item' => $order,'statuses' => $statuses,'payment_types' => $payment_types,'payment_statuses' => $payment_statuses]);
    }

    public function update(Request $request, $id)
    {
        $order = Order::findOrFail($id);
        if($request->backup){
            $this->backup($order,$request);
        }elseif($request->destroy){
            $this->backup($order,$request);
            $this->destroy($id);
            return redirect()->route('admin_order.index')->with(['destroy' => 'Xóa dữ liệu thành công!']);
        }else{
            // $total = 0;
            // foreach($order->product_variants as $key => $item ){
            //     if($item->amount < $item->pivot->amount){
            //         return redirect()->back()->with(['status' => 'Số lượng trong kho không đủ để cung cấp!']);
            //     }else{
            //         $array[$key] = [
            //             'order_id'              => $order->id,
            //             'product_variant_id'    => $item->id,
            //             'amount'                => $request->amount[$key]
            //         ];
            //         $total += $item->getDiscountPriceUseIntoOrderBackEnd() * $request->amount[$key];
            //         $order->product_variants()->sync($array);
            //     }
            // }
            $order->order_status = $request->status;
            $order->status_payment = $request->payment_status;
            // $order->total = $total;
            $order->payment_type = $request->type_payment;
            if($order->order_status == config('ecommerce.order_statuses.delivered')){
                $order->status_payment = config('ecommerce.status_payments.paid');
            }
            // if($order->code()->first()){
            //     $new_total =  $total - $order->discountPromotionOrder();
            // }else $new_total = $order->total;
            // $order->total = $new_total;
            $order->save();
            if($order->order_status == config('ecommerce.order_statuses.delivered')){
                $order->updateAmountVariant();
            }
        }
        return redirect()->back()->with(['status' => 'Cập nhật thông tin thành công!']);
    }

    public function destroy($id)
    {
        $order = Order::findOrFail($id);
        $order->product_variants()->detach();
        $order->delete();
        return redirect()->route('admin_order.index')->with(['destroy' => 'Xóa dữ liệu thành công!']);
    }

    public function store(Request $request)
    {
        $products = json_decode($request->selected_product,true);
        $address = $request->stress.",".$request->ward.",".$request->district.",".$request->province;
        $order = Order::create([
            'order_number'      => MakeOrderNumber::makeOrderNumber(),
            'total'             => (int) $request->total,
            // 'user_id'           => 1,
            'payment_type'      => config('ecommerce.type_payments.Offline'),
            'customer_email'    => $request->customer_email,
            'customer_phone'    => $request->customer_phone,
            'customer_name'     => $request->customer_name,
            'address'           => $address,
            'order_status'      => config('ecommerce.order_statuses.new'),
            'shipping_fee'      => $request->shipping_fee,
            'total_after'       => (int) $request->total_after,
            'breakable_cost'    => $request->breakable_cost
        ]);
        if (count($products) > 0) {
            $arr_id = [];
            foreach ($products as $key => $item) {
                $qty = $this->checkAmount($item['id'],$item['order_amount']);
                $array[$key] = [
                    'order_id'              => $order->id,
                    'product_variant_id'    => $item['id'],
                    'amount'                => $qty
                ];
                HistoryImportProduct::updateOrCreateExportInDate($item['id'],$qty);
                $order->product_variants()->sync($array);
            }
        }
        $code_name = $request->input('promotion');
        $this->updateCodeOrderAmount($order, $code_name);
        $this->updateCodeProductAmount($products);
        event( new OrderSuccessfullyEvent($order,$request->customer_email,$request->customer_name));
        return redirect()->route('admin_order.edit',['id' => $order->id])->with(['status' => 'Cập nhật thông tin thành công!']);
    }

    public function backup($order,$request)
    {
        $order->order_status = config('ecommerce.order_statuses.processing');
        $order->save();
        foreach($order->product_variants as $key => $item){
            $item->amount += $request->amount[$key];
            $item->save();
            $item->history->pendding += $request->amount[$key];
            $item->history->amount_export -= $request->amount[$key];
            $item->history->save();
        }
    }

    public function updateCodeOrderAmount($order , $code){
        if($code){
            $code_request = Code::where('promotion',$code)->firstOrFail();
            $code_amount = $code_request->amount;
            // $code_request->amount = $code_amount - 1;
            $code_id = $code_request->id;
            $email = $order->customer_email;
            if($code_request->amount_ordered <= $code_request->amount)
                $code_request->amount_ordered += 1; 
            $code_request->save();
            $arr = [
                'code_id' => $code_id,
                'email'  => $email,
                'user_id' => null
            ];
            $order->code()->sync(array($arr),true);
            $code_request->save();
        }
    }

    public function updateCodeProductAmount($cartInfor) {

        foreach($cartInfor as $item){
            $variant = ProductVariant::find($item['id'])->first();
            $product = $variant->product;
            $code = $variant->checkTypeApply()['code'];
            if($code) {
                $pivot_code_item = $this->getPivotItemAccept($code,$variant,$variant->checkTypeApply()['type']);
                $amount_accept = $pivot_code_item->amount_ordered;
                $amount_default = $pivot_code_item->amount_default;    
                Session::put("begin_amount_". $code->id,$amount_default);
                $session = Session::get("amount_".$code->id);
                if($session < 0) $session = 0;
                    $pivot_code_item->amount_accept = $session;
                $code->save();
                // $amount = Session::forget("amount_".$code->id);
            }
        }

    }

    public function getPivotItemAccept($code,$variant,$type_apply)
    {
        if($type_apply == 'product'){
            $item = $code->firstRecordByObjectId($variant->product->id);
        }else{
            $item = $code->firstRecordByObjectId($variant->id);
        }
        return $item;
    }

    public function checkAmount($id,$qty)
    {
        $variant = ProductVariant::find($id);
        $order = $variant->countAmountOrderWaiting();
        $stock = $variant->amount - $order;
        if($qty <= $stock)
            return $qty;
        return $stock;
    }
}
