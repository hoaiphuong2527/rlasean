<?php

namespace App\Http\Controllers\Order;

use App\Models\Option;
use App\Models\Order;
use App\Models\Codes\Code;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Products\ProductVariant;
use Illuminate\Support\Facades\Session;

class ApiController extends Controller
{
    public function checkAmountVariant($id)
    {
        $order = Order::find($id);
        $result = 1;
        foreach($order->product_variants as $variant){
            if($variant->amount < $variant->pivot->amount){
                $result = 0;
                break;
            }
            break;
        }
        $data = [
            'result' => $result
        ];
        return response()->json($data);
    }

    public function addProduct(Request $request)
    {
        $id = $request->id;
        $variant = ProductVariant::findOrFail($id);
        $product = $variant->product;
        $code = $variant->checkTypeApply()['code'];
        $session_amount = 0;
        if($code){
            $pivot_code_item = $this->getPivotItemAccept($code,$variant,$variant->checkTypeApply()['type']);
            $amount_accept = $pivot_code_item->amount_ordered;
            $amount_default = $pivot_code_item->amount_default;
            if($amount_default != null){
                if(Session::has("amount_". $code->id)){
                    $session_amount = Session::get("amount_". $code->id);
                    if($session_amount >= 1){
                        $data = [
                            'has_code'  => 1,
                            'product'   => $variant,
                            'amount_accept' => Session::get("amount_". $code->id),
                            'code_id'       => $code->id,
                            'breakable' => $product->is_breakable
                        ];
                        Session::put("amount_". $code->id,$session_amount - 1);
                    }else{
                        $data = [
                            'has_code'  => 0,
                            'product'   => $variant,
                            'amount_accept' => 0,
                            'code_id'       => $code->id,
                            'breakable' => $product->is_breakable
                        ];
                    }
                }else{
                    if($amount_default >= 1 && $amount_accept >= 1){
                        $data = [
                            'has_code'  => 1,
                            'product'   => $variant,
                            'amount_accept' => $amount_accept,
                            'code_id'       => $code->id,
                            'breakable' => $product->is_breakable
                        ];
                        Session::put("amount_". $code->id,$amount_accept - 1);
                    }elseif($amount_default >= 1 && $amount_accept == null){
                        $data = [
                            'has_code'  => 1,
                            'product'   => $variant,
                            'amount_accept' => $amount_default,
                            'code_id'       => $code->id,
                            'breakable' => $product->is_breakable
                        ];
                        Session::put("amount_". $code->id,$amount_default - 1);
                    }else{
                        $data = [
                            'has_code'  => 0,
                            'product'   => $variant,
                            'amount_accept' => 0,
                            'code_id'       => $code->id,
                            'breakable' => $product->is_breakable
                        ];
                    }
                }
            }else{
                $data = [
                    'has_code'  => 0,
                    'product'   => $variant,    
                    'amount_accept' => 0,
                    'code_id'       => 0,
                    'breakable' => $product->is_breakable
                ];
            }
        }else{
            $data = [
                'has_code'  => 0,
                'product' => $variant,
                'amount_accept' => 0,
                'code_id'       => 0,
                'breakable' => $product->is_breakable
            ];
        }
        return response()->json($data);
    }

    public function updateAmountProduct(Request $request)
    {
        $id = $request->id;
        $variant = ProductVariant::find($id);
        $amount = $request->amount;
        $code = $variant->checkTypeApply()['code'];
        $productArray = $request->productArray;
        if($code){
            $session = Session::get("amount_". $code->id);
            $pivot_code_item = $this->getPivotItemAccept($code,$variant,$variant->checkTypeApply()['type']);
            if($pivot_code_item->amount_ordered != null)
                $amount_accept = $pivot_code_item->amount_ordered;
            $amount_accept = $pivot_code_item->amount_default;
            if($productArray == 1){
                if($amount <= $amount_accept){
                    $data = [
                        'has_promotion' => 1,
                        'new_amount'    => $amount,
                        'more'          => 0,
                        'code_id'       => $code->id
                    ];
                    Session::put("amount_". $code->id,$amount_accept - $amount);
                }else{
                    $data = [
                        'has_promotion' => 1,
                        'new_amount'    => $amount,
                        'more'          => 1,
                        'has_code'      => $amount_accept,
                        'normal'        => $amount - $amount_accept,
                        'code_id'       => $code->id
                    ];
                    Session::put("amount_".$code->id,0);
                }
            }else{
                if($session == 0){
                    $data = [
                        'has_promotion' => 1,
                        'new_amount'    => $amount,
                        'more'          => 0,
                        'code_id'       => $code->id
                    ];
                }elseif($amount <= $session){
                    $data = [
                        'has_promotion' => 1,
                        'new_amount'    => $amount,
                        'more'          => 0,
                        'code_id'       => $code->id
                    ];
                    Session::put("amount_". $code->id,$session - $amount);
                }else{
                    $data = [
                        'has_promotion' => 1,
                        'new_amount'    => $amount,
                        'more'          => 1,
                        'has_code'      => $session,
                        'normal'        => $amount - $session,
                        'code_id'       => $code->id,
                    ];
                    Session::put("amount_". $code->id,0);
                }
            }
        }else{
            $data = [
                'has_promotion' => 0,
                'new_amount'    => $amount
            ];
        }
        return response()->json($data);
    }


    public function updateSession(Request $request)
    {
        $code_id = $request->code_id;
        $amount_accept = $request->amount_accept; //2
        $operator = $request->operator;
        $variant = ProductVariant::find($request->variant_id);
        if($operator == 'add'){
            $code = Code::find($code_id);
            $pivot_code_item = $this->getPivotItemAccept($code,$variant,$variant->checkTypeApply()['type']);
            $amount_accept_code = $pivot_code_item->amount_ordered;

            $session_amount = Session::get("amount_". $code_id);
            if($session_amount + $amount_accept > $amount_accept_code) $session_amount = $amount_accept_code;else $session_amount = $session_amount + $amount_accept;
                Session::put("amount_". $code_id,$session_amount);
        }else{
            if($amount_accept - 1 < 0) $amount_accept = 0; else $amount_accept = $amount_accept - 1;
            Session::put("amount_". $code_id,$amount_accept);
        }
        
        $data = [
            'session' => Session::get("amount_". $code_id)
        ];
        return response()->json($data);
    }

    public function findPivotItemCode($id)
    {
        $variant = ProductVariant::findOrFail($id);
        $pivot_code_item = $variant->checkTypeApply();
        return $pivot_code_item;
    }

    public function getPivotItemAccept($code,$variant,$type_apply)
    {
        if($type_apply == 'product'){
            $item = $code->firstRecordByObjectId($variant->product->id);
        }else{
            $item = $code->firstRecordByObjectId($variant->id);
        }
        return $item;
    }

    public function backupSession()
    {
        $variants = ProductVariant::all();
        foreach ($variants as $variant) {
            $code = $variant->checkTypeApply()['code'];
            if($code){
                $pivot_code_item = $this->getPivotItemAccept($code,$variant,$variant->checkTypeApply()['type']);
                $amount_accept = $pivot_code_item->amount_ordered;
                Session::put("amount_". $code->id,$amount_accept);
            }
        }
    }

    public function calculatorGhtkShippingCost(Request $request)
    {
        $option = Option::first();
        $data_request = array(
            "pick_province" => $option->province_stock,
            "pick_district" => $option->district_stock,
            "province" => $request->province,
            "district" => $request->district,
            "address" =>  $request->address,
            "weight" => 0,
            "value" => $request->value,
            "transport" => "fly"
        );
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://services.giaohangtietkiem.vn/services/shipment/fee?" . http_build_query($data_request),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_HTTPHEADER => array(
                "Token: $option->ghtk_token",
            ),
        ));
        
        $response = curl_exec($curl);
        curl_close($curl);
        $data = [
            json_decode($response)
        ];
        return response()->json($data);
    }
}
