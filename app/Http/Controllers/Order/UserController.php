<?php

namespace App\Http\Controllers\Order;

use App\Models\User;
use App\Models\Order;
use App\Models\Option;
use App\Models\Codes\Code;
use Illuminate\Http\Request;
use App\Exports\OrdersExport;
use App\Helpers\MakeOrderNumber;
use App\Http\Controllers\Controller;
use App\Events\OrderSuccessfullyEvent;
use App\Models\Products\ProductVariant;
use Illuminate\Support\Facades\Session;
use App\Models\Products\HistoryImportProduct;

class UserController extends Controller
{
    public function index($token)
    {
        $user = User::whereMiddlewareToken($token)->first();   
        $orders = Order::where('customer_email',$user->email)->get();
        return view('order.user.index')->with([
            'orders' => $orders
        ]);    
    }
    public function show($token,$order_number)
    {
        //findORFail & find chỉ dùng cho id thôi nha em. còn các trường khác thì mình dùng where 
        //whereOrderNumber = where('order_number',$order_number)
        $order = Order::whereOrderNumber($order_number)->with('product_variants')->first();
        return view('order.user.show')->with([
            'order' => $order
        ]);
        
    
    }

    
}
