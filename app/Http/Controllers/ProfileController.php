<?php

namespace App\Http\Controllers;

use App\Helpers\UploadImage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Auth;
use App\Models\User;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        return view('user.profile',['user' => $user]);
    }

    public function update(Request $request,$token)
    {
        $user = Auth::user();
        if($request->get('password') == ""){
            $rule = [
                'name' => 'required',
                'phone' => 'sometimes|min:10|max:12',
                'address' => 'sometimes|max:1000'
            ];
        }else{
            $rule = [
                'name' => 'required',
                'phone' => 'sometimes|min:10|max:12',
                'address' => 'sometimes|max:1000',
                'password'   => 'required|min:6|confirmed',
            ];
        }
        $validator = Validator::make(Input::all(), $rule);
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }else{
            if (Input::hasfile('image'))
            {
                $user->is_change_avt = 1;
                $path = User::$imgPath;
                $image = $request->file('image');
                $file_name = "image";
                $slug = explode('.',Input::file('image')->getClientOriginalName())[0];
                $user->image = UploadImage::uploadImage($file_name,$image,$path,$slug,$user,150,150,'image');
            }
            $user->name = $request->name;
            $user->phone = $request->phone;
            $user->address = $request->address;
            if($request->password)
                $user->password = Hash::make($request->get('password'));
            $user->save();
            return redirect()->back();
        }
    }

    public function changePassword($token,$active)
    {
        $user = User::where('middleware_token',$token)->first();
        if($user){
            Auth::login($user);
            return redirect()->route('user_profiles.index',['token' =>$token]);
        }else{
            return redirect()->route('guest_home.index.'.app()->getLocale());
        }
    }
}
