<?php

namespace App\Http\Controllers\Warranty;

use App\Models\Warranty;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $warranties = Warranty::paginate(20);
        return view('warranty.admin.index')->with(['warranties' => $warranties]);
    }

    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Warranty::findOrFail($id);
        $item->delete();
        return redirect()->back()->with(['status' => 'Cập nhật thành công!']);
    }
}
