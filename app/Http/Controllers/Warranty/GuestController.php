<?php

namespace App\Http\Controllers\Warranty;

use App\Models\Option;
use App\Models\Banner;
use App\Models\Warranty;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Events\GuestSendWarrantyEvent;
use Illuminate\Support\Facades\Validator;

class GuestController extends Controller
{
    public function index()
    {
        $banner = Banner::where('key','contact')->first();  
        $option = Option::firstOrFail();
        return view('warranty.guest.index')->with([
            'banner'   => $banner,
            'option'   => $option
        ]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'f_name'                => 'sometimes|max:191',
                'l_name'                => 'sometimes|max:191',
                'email'                 => 'sometimes|min:6|max:191',
                'phone'                 => 'sometimes|numeric',
                'amount_product'        => 'sometimes',
                'location'              => 'sometimes|max:255',
                'purchase_date'         => 'date',
                'is_invite'             => 'sometimes'
            ],[]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $warranty = Warranty::create($request->all());
            event(new GuestSendWarrantyEvent($warranty));
            return redirect()->back()->with(['status' => 'Send request success!']);
        }
    }
}
