<?php

namespace App\Http\Controllers\Information;

use Illuminate\Http\Request;
use App\Helpers\BeautyLink;
use App\Http\Controllers\Controller;
use App\Models\Informations\Information;
use Validator;
use DB;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $info = Information::all()->unique('key');
        $infos = Information::paginate(20);
        if (isset($_GET['name']))
            $infos = Information::search($request->name)->paginate($request->get('show', 20));
        // if (!isset($_GET['key'])) {
        //     $type = config('ecommerce.fillter_information.all');
        // } else {
        //     $type = $request->key;
        // }
        // switch ($type) {
        //     case config('ecommerce.fillter_information.all'):
        //         $infos = Information::paginate(20);
        //         break;
        //     default:
        //         $infos = Information::whereKey($_GET['key'])->paginate($request->get('show', 20));
        // }
        return view('information.admin.index')->with([
            'infos' => $infos,
            'name' => $request->name,
            'info' => $info,
            'text_key' => $request->key
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->input('value')){
            Information::create([
                'key'   => $request->key,
                'name'  => $request->name,
                'value' => $request->value
            ]);
            return redirect()->back()->with(['status' => 'Cập nhật dữ liệu thành công!']);    
        }else{
            $info = Information::Create();
            return redirect($info->urlAdminEdit());    
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $info = Information::findOrFail($id);

        return view('information.admin.edit')->with([
            'info' => $info,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $info = Information::findOrFail($id);

        $validator =
            Validator::make($request->all(), [
            'name' => 'required',
            'key' => 'required',
            'value' => 'required'
        ], [
            'name.required' => 'Vui lòng nhập tên',
            'key.required' => 'Vui lòng nhập thông số',
            'value.required' => 'Vui lòng nhập dữ liệu'

        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        } else {

            $info->name = $request->name;
            $info->key = $request->key;
            $info->value = $request->value;
            $info->save();

            return redirect()->back()->with([
                'success' => 'Cập nhật dữ liệu thành công!',
                'item' => $info,
            ]);


        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Information::findOrFail($id);
        $item->delete();
        return redirect()->route('admin_information.index')->with([
            'destroy' => 'Xóa dữ liệu thành công!',
        ]);
    }
}
