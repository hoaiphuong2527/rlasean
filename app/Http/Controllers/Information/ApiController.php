<?php

namespace App\Http\Controllers\Information;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Informations\Information;
use App\Models\Categories\ProductCategory;
use App\Models\Informations\CategoyInformation;

class ApiController extends Controller
{
    public function store($category_id, Request $request)
    {
        $category = ProductCategory::find($category_id);
        if($category){
            $key = $request->key;
            $value = $request->value;
            $name = $request->name;
            $info = Information::create([
                'key'   => $key,
                'value' => $value,
                'name'  => $name
            ]);
            $data = [
                'result' => 1,
                'informations' => $category->informations,
                'new_id'    => $info->id
            ];
        }else{
            $data = [
                'result' => 0
            ];
        }
        return response()->json($data,200);
    }

    public function createNewKey($category_id,Request $request)
    {
        $category = ProductCategory::find($category_id);
        if($category){
            $key = $request->key;
            $info = Information::create([
                'key'   => $key,
                'value' => 0,
                'name'  => $key
            ]);
            $data = [
                'result' => 1,
                'informations' => $category->informations
            ];
        }else{
            $data = [
                'result' => 0
            ];
        }
        return response()->json($data,200);
    }

    public function update($category_id,$id,Request $request)
    {
        $info = Information::find($id);
        $info->value = $request->value;
        $info->save();
        return response()->json([
            'result' => 1
        ],200);
    }

    public function destroy($category_id,$id)
    {
        $category = ProductCategory::find($category_id);
        $info = Information::find($id);
        $info->delete();
        $data = [
            'result' => 1,
            'informations' => $category->informations
        ];
        return response()->json($data,200);
    }

    public function save($category_id,Request $request)
    {
        $item = ProductCategory::findOrFail($category_id);
        $item->informations()->sync($request->information_array);
        return response()->json(200);
    }

    public function destroyGroup($category_id,Request $request)
    {
        $key = $request->key;
        $category = ProductCategory::find($category_id);
        if($key){
            foreach(Information::where('key',$key)->get() as $item){
                $item->delete();
            }
            $data = [
                'result' => 1,
                'informations' => $category->informations
            ];
        }else {         
            $data = [
                'result' => 0,
                'informations' => $category->informations
            ];
        }

        return response()->json($data,200);
    }
}
