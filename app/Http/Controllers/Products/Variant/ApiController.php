<?php

namespace App\Http\Controllers\Products\Variant;

use File;
use Image;
use Illuminate\Http\Request;
use App\Models\Products\Gallery;
use App\Models\Products\Product;
use App\Http\Controllers\Controller;
use App\Models\Products\ProductVariant;

class ApiController extends Controller
{
    public function uploadImage($id,Request $request)
    {
        $path = Gallery::$imgPath;
        $gallery = $request->file('file');
        $item = ProductVariant::findOrFail($id);
        if($gallery)
        {
            $image_name = time() . '.' . $gallery->getClientOriginalExtension();
            if (! File::exists(public_path().'/'.$path)) {
                File::makeDirectory(public_path().'/'.$path,0777,true);
            }
            Image::make($gallery)->fit(600,600)->save(public_path($path . $image_name));
            Gallery::create([
                'image' => $image_name,
                'product_variant_id' => $id
            ]);
        }
        return response()->json(['success' => 'Ok'], 200);
    }

    public function deletePhoto(Request $request, $id)
    {
        $photo = Gallery::find($id);
        if($photo){
            $oldImage = $photo->name;
            if($oldImage != '')
            {
                if(File::exists(public_path(Gallery::$imgPath) . $oldImage))
                {
                    unlink(public_path(Gallery::$imgPath) . $oldImage);   
                } 
            }
            $photo->delete();
            $data = [
            'success' => true
            ];
        }else{
            $data = [
                'success' => false
                ];
        }
        return response()->json($data,200);  
    }

    public function search(Request $request)
    {
        $search = $request->search;
        if($search){
            $variants = ProductVariant::whereHas('product', function($q) use ($search){
                $q->where('name','LIKE', '%'.$search.'%');
            })->get();
            if($variants->count() != 0){
                $data = [
                    'result'   => 1,
                    'variants' => $variants->toArray(),
                ];
            }else{
                $data = [
                    'result'   => 0,
                    'variants' => 0,
                ];
            }
        }else {
            $data = [
                'result'   => 1,
                'variants' => ProductVariant::all()->toArray()
            ];
        }
        return response()->json($data,200);
    }
}
