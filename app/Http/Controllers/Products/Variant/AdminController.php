<?php

namespace App\Http\Controllers\Products\Variant;

use Illuminate\Http\Request;
use App\Helpers\UpdateSomething;
use App\Models\Products\Product;
use App\Helpers\CreateTranslation;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Models\Informations\Information;
use App\Models\Products\ProductVariant;
use Illuminate\Support\Facades\Validator;
use App\Models\Categories\ProductCategory;
use App\Models\Products\VariantTranslation;

class AdminController extends Controller
{
    use UpdateSomething;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($slug)
    {
        $product = Product::whereSlug($slug)->firstOrFail();
        $variants = $product->product_variants;
        return view('products.variant.admin.index')->with([
            'variants'  => $variants,
            'product'   => $product,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($slug)
    {
        $variant = $this->storeItem($slug);
        return redirect($variant->urlAdminEdit($slug));
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug,$id)
    {
        $variant = ProductVariant::findOrFail($id);
        $category = $variant->product->category;
        $arr_info_unique = $category->informations->unique('key');
        $infos = Information::all();
        $product = Product::whereSlug($slug)->firstOrFail();
        return view('products.variant.admin.edit')->with([
            'item'              => $variant,
            'product'           => $product,
            'arr_info_unique'   => $arr_info_unique
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$slug, $id)
    {
        $product = Product::whereSlug($slug)->firstOrFail();
        $item = ProductVariant::findOrFail($id);
        $rule = [
            'price'         => 'required|max:20|regex:/^-?[0-9]+(?:\.[0-9]{1,2})?$/',
            'amount'        => 'required|integer|between:1,10000',

        ];
        $mess = [
            'price.required'    => 'Vui lòng nhập giá tiền cho sản phẩm.',
            'amount.required'   => 'Vui lòng điền số lượng sản phẩm.',
            'amount.integer'    => 'Số lượng phải là kiểu số nguyên.'
        ];
        $validator = Validator::make(Input::all(), $rule,$mess);
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $item->media()->sync($request->image);

            $item->color = '#'.str_replace('#','',$request->color);
            $item->size  = $request->size;
            $item->title = $request->title;
            $item->weight  = $request->weight;
            $item->price = $request->price;
            $item->amount = $request->amount;
            // $item->position = $request->position;

            if($request->publish) $item->status = 1;
            if($request->draff) $item->status = 0;
            if($request->continue){
                $item->status = 1;
                $item->save();
                $variant = $this->storeItem($product->slug);
                return redirect($variant->urlAdminEdit($product->slug));
            }
            $arr_info_unique = $item->product->category->informations->unique('key');
            $arr_id = [];
            foreach ($arr_info_unique as $key => $row) {
                $arr_id[] = $request->get('infomation_'.$row->key);
                // $item->informations()->sync($request->get('infomation_'.$row->key));
            }
            $item->informations()->sync(array_diff($arr_id,array("null")));
            $item->save();
            return redirect()->back()->with(['status' => 'Cập nhật thành công!']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug,$id)
    {
        $item = ProductVariant::findOrFail($id);
        $item->delete();
        return redirect()->back()->with(['destroy' => 'Dữ liệu được xóa thành công!']);
    }

    public function sortPosition(Request $request,$id)
    {
        $item = ProductVariant::findOrFail($id);
        $item->position = $request->position;
        $item->title = $request->title;
        $item->price = $request->price;
        $item->amount = $request->amount;
        $item->save();
        return redirect()->back();
    }

    public function updateStatusVariant($id)
    {
        $item = ProductVariant::findOrFail($id);
        $this->updateStatus($item);
    }

    public function storeItem($slug)
    {
        $product = Product::whereSlug($slug)->firstOrFail();
        if(count($product->product_variants) > 0){
            $latest_variant = $product->product_variants()->latest()->first();
            $variant = ProductVariant::create([
                'product_id' => $product->id,
                'position'   => $latest_variant->position+1
            ]);
        }else{
            $variant = ProductVariant::create([
                'product_id' => $product->id
            ]);
        }
        return $variant;
    }
}
