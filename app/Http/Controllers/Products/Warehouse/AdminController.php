<?php

namespace App\Http\Controllers\Products\Warehouse;

use Illuminate\Http\Request;
use App\Models\Products\Product;
use App\Http\Controllers\Controller;
use App\Models\Products\ProductVariant;
use App\Models\Products\HistoryImportProduct;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $name = $request->name;
        $list = HistoryImportProduct::latest()->with(['product_variant'])->paginate(20);
        if($name){
            $list = HistoryImportProduct::search($name)->with(['product_variant'])->paginate(20);
        }
        return view('products.warehouse.summary')->with([
            'products'  => $list,
            'name'      => $name
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $product = Product::whereSlug($slug)->firstOrFail();
        return view('products.warehouse.edit')->with([
            'item'  => $product
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $variant = ProductVariant::findOrFail($id);
        $import = $request->import;
        $variant->amount = $import + $variant->amount;
        $variant->save();
        HistoryImportProduct::updateOrCreateInDate($id,$import);
        // foreach( $product->product_variants as $key => $variant){
        //     $variant->amount = $import[$key] + $variant->amount;
        //     $variant->save();
        //     HistoryImportProduct::updateOrCreateInDate($variant->id,$import[$key]);
        // }
        // if($request->create){
        //     return redirect()->route('admin_product.edit',['id' => $product->id]);
        // }
        return redirect()->back()->with(['status' => 'Cập nhật dữ liệu thành công!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
