<?php

namespace App\Http\Controllers\Products\Product;

use Illuminate\Http\Request;
use App\Helpers\UpdateSomething;
use App\Models\Products\Product;
use App\Helpers\CreateTranslation;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Models\Products\ProductVariant;
use App\Models\Informations\Information;
use Illuminate\Support\Facades\Validator;
use App\Models\Categories\ProductCategory;
use App\Models\Translations\ProductTranslation;

class AdminController extends Controller
{
    use UpdateSomething;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $name = $request->name;
        $category_slug = $request->category_slug;
        $query = Product::query();
        $categories = ProductCategory::whereParentId(1)->get();
        if(!$category_slug) $category_slug = 'all';
        else $category_slug = $category_slug;
        if($name){
            $query->where('name', 'like', '%'.$name.'%');
        }
        switch($category_slug){
            case 'all':
            break;
            default:
                $cate = ProductCategory::whereSlug($category_slug)->firstOrFail();
                $query->whereCategory($cate->id);
        }

        $products = $query->with(['category'])->paginate(20);
        return view('products.product.admin.index')->with([
            'name'              => $name,
            'category_slug'     => $category_slug,
            'products'          => $products,
            'categories'        => $categories,
            'search_name'       => $request->product_name
        ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product = Product::create();
        //translate
        foreach(Product::$translate_column as $col){
            CreateTranslation::createRecord(ProductTranslation::$namespace,$product->id,ProductTranslation::$foreign_key,$col,$request);
        }
        //end translate
        return redirect($product->urlAdminEdit());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::findOrFail($id);
        $categories = ProductCategory::whereParentId(1)->get();
        return view('products.product.admin.edit')->with([
            'item'          => $product,
            'categories'    => $categories,
            'url'           => 'guest_products.show.'.app()->getLocale(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = Product::findOrFail($id);
        // if($request->position){
        //     $this->isFeature($request,$id);
        //     $item->is_featured = 1;
        //     $item->save();
        // }
        $categories = ProductCategory::getCategoryNotMaster();
        $rule = [
            'name'              => 'required|max:191',
            'description'       => 'required',
            'short_description' => 'required|max:500',
            'category_id'       => 'required',
            'description'       => 'required'

        ];
        $mess = [
            'name.required'                 => 'Vui lòng điền tên sản phẩm',
            'name.max'                      => 'Tên sản phẩm có tối đa 191 ký tự.',
            'description.required'          => 'Vui lòng điền nội dung mô tả sản phẩm.',
            'short_description.required'    => 'Vui lòng điền nội dung mô tả ngắn của sản phẩm.',
            'short_description.max'         => 'Nội dung mô tả ngắn có tối đa 500 ký tự',

        ];
        $validator = Validator::make(Input::all(), $rule,$mess);
        if ($validator->fails()) {
            // dd($validator->fails());
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $item->name = $request->name;
            if($item->slug == null)
                $item->slug = str_slug($request->name);
            else
                $item->slug = str_slug($request->slug);
            $item->short_description = $request->short_description;
            $item->category_id = $request->category_id;
            $item->description = $request->description;
            $item->specification = $request->specification;
            $item->overview = $request->overview;
            $item->compatibility = $request->compatibility;
            if($request->is_breakable == 'on')
                $item->is_breakable = 1;
            else $item->is_breakable = 0;
            // $infos = $request->input('information_id');
            // $item->informations()->sync($infos);
            if($request->update_lang) {
                $item->save();
                return redirect()->route('admin_product.edit_lang',$item->id);
            }elseif($request->publish){
                $item->status = 1;
                $item->updateStatusVariants();
            }elseif($request->draff){
                $item->status = 0;
                $item->updateStatusVariants();
            }elseif($request->continue){
                $item->status = 1;
                $item->save();
                $new = Product::create();
                return redirect($new->urlAdminEdit());
            }
            $item->save();
            if($request->create_variant){
                if(count($item->product_variants) > 0){
                    $latest_variant = $item->product_variants()->latest()->first();
                    $variant = ProductVariant::create([
                        'product_id' => $item->id,
                        'position'   => $latest_variant->position+1
                    ]);
                }else{
                    $variant = ProductVariant::create([
                        'product_id' => $item->id
                    ]);
                }
                return redirect($variant->urlAdminEdit($item->slug));
            }
            return redirect()->back()->with([
                'status' => 'Cập nhật dữ liệu thành công!',
                'categories'    => $categories
                ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Product::findOrFail($id);
        $categories = ProductCategory::getCategoryNotMaster();
        foreach($item->reviews as $revi){
            $revi->delete();
        }
        $item->delete();
        return redirect()->route('admin_product.index')->with([
            'destroy' => 'Xóa dữ liệu thành công!',
            'categories'    => $categories
            ]);
    }

    public function sortFeature(Request $request,$id)
    {
        $item = Product::findOrFail($id);
        $this->sorting($request,$item);
        $categories = ProductCategory::getCategoryNotMaster();
        return redirect()->back()->with([
            'status' => 'Dữ liệu đã được cập nhật',
            'categories'    => $categories
        ]);
    }

    public function updateStatusProduct($id)
    {
        $item = Product::findOrFail($id);
        $this->updateStatus($item);
        $item->updateStatusVariants();
        return redirect()->back();
    }

    public function clone($slug)
    {
        $product = Product::whereSlug($slug)->firstOrFail();
        $new_product = $product->clone();
        return redirect($new_product->urlAdminEdit());
    }

    public function isFeature(Request $request,$id)
    {
        $item = Product::findOrFail($id);
        $variants = $item->product_variants;
        $position = $request->input("value_position");
        foreach( $variants as $key => $variant){
            $variant->position = $position[$key];
            $variant->save();
        }
        return redirect()->back()->with(['status' => 'Cập nhật thông tin thành công!']);
    }
    public function editTextLang($id){
        $item = Product::findOrFail($id);
        return view('products.product.admin.lang')->with(['item'=>$item,
            'url' => 'guest_products.show.' . app()->getLocale()

        ]);
    }
    public  function updateLang($id,Request $request){
        // translate
        $item = Product::findOrFail($id);
            foreach (Product::$translate_column as $col) {
            CreateTranslation::saveLang($col, $request, $item, ProductTranslation::$namespace, ProductTranslation::$foreign_key);
        }
        $item->save();
        return redirect()->back()->with(['status' => 'Cập nhật dữ liệu thành công!']);
    }
}
