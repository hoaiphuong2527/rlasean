<?php

namespace App\Http\Controllers\Products\Product;

use App\Models\Categories\ProductCategory;
use App\Models\Products\Product;
use App\Models\ProductCode;
use Illuminate\Http\Request;
use App\Models\Products\ProductVariant;
use App\Http\Controllers\Controller;
use App\Models\Code\Codes;

class ApiController extends Controller
{
    public function show($id)
    {
        $product = Product::find($id);
        if($product){
            if(count($product->product_variant_onlines)){
                $product['name'] = $product->getAttributeLang('name');
                $product['colors'] = $product->getParam('color');
                $product['sizes'] = $product->getParam('size');
                $variants = $product->product_variant_onlines;
                $variant['discount_price_display']  = $product->discount_price_display;
                $product->product_variant_onlines->map(function ($variant) {
                    $variant['inventory'] = $variant->countAmountOrderWaiting();
                    $variant['beauty_price'] = $variant->price_format;
                    $variant['discount_price'] = $variant->getDiscountPrice();
                    $variant['discount_value'] = $variant->getDiscountValue();

                    });
                // foreach($variants as $key => $variant){
                //     if(count($variant->media)){
                //         $variant->media;
                //     }
                // }
            }
            $data = [
                'product'  => $product,
                'productcode' => 0
            ];
        }else{
            $data = [
                'result' => -1
            ];
        }
        return response()->json($data,200);
    }

    public function getId($slug)
    {
        $category = ProductCategory::whereSlug($slug)->firstOrFail();
        $products_id = $category->getIdProduct();
        $data = [
            'arr_id' => $products_id
        ];
        return response()->json($data,200);
    }

    public function getVariant($variant_id)
    {
        $variant = ProductVariant::findOrFail($variant_id);
        $data = [
            'variant' => $variant->toArray(),
            'intoventory' => $variant->amount - $variant->countAmountOrderWaiting()
        ];
        return response()->json($data,200);
    }

    public function checkAmountProduct(Request $request)
    {
        $apply_type = $request->apply_type;
        $product_id = $request->id;
        $amount_pomotion = $request->amount;
        if($apply_type == config('ecommerce.apply_options.variant')){
            $product = ProductVariant::find($product_id);
            if($amount_pomotion != null && $amount_pomotion > $product->amount){
                $data = [
                    'message' => 'Vượt quá số lượng cho phép',
                    'stock'   => $product->amount
                ];
            }else{
                $data = [
                    'message' => 'Hợp lệ',
                    'stock'   => $product->amount
                ];
            }
        }elseif($apply_type == config('ecommerce.apply_options.product')){
            $product = Product::find($product_id);
            if($amount_pomotion != null && $amount_pomotion > $product->getTotal()){
                $data = [
                    'message' => 'Vượt quá số lượng cho phép',
                    'stock'   => $product->getTotal()
                ];
            }else{
                $data = [
                    'message' => 'Hợp lệ',
                    'stock'   => $product->getTotal()
                ];
            }
        }
        return response()->json($data,200);
    }
}
