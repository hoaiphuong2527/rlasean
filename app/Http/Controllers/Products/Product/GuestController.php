<?php

namespace App\Http\Controllers\Products\Product;

use Auth;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Banner;
use App\Models\Review;
use Illuminate\Http\Request;
use App\Helpers\SessionManager;
use App\Models\Products\Product;
use App\Models\Categories\Category;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Models\Products\ProductVariant;
use Illuminate\Support\Facades\Session;
use App\Models\Informations\Information;
use App\Events\User\CreateNewUserNatural;
use App\Models\Categories\ProductCategory;
use App\Models\Informations\ProductInformation;

class GuestController extends Controller
{
    
    public function getProductByCategory(Request $request,$id,$slug)
    {
        $category = ProductCategory::findOrFail($id);
        if($category->id == 1){
            return redirect()->route('guest_all_categories.index.'.app()->getLocale());
        }
        $categories = ProductCategory::getCategoryNotMaster();
        $category_children = $category->children;
        $secondCategories = ProductCategory::getSecondParentOnline();

        if($request->sort_name){
            $val = $request->sort_name;
            $products = Product::sortProductByAtt($id,'name',$val)->get();
            $count = Product::sortProductByAtt($id,'name',$val)->count();
        }elseif($request->sort_type){
            $val = $request->sort_type;
            $products = Product::sortProductPrice($id,$val);
            $count = count($products);

        }else{
            $products = $category->getProductsForGuest()->get();
            $count = $category->getProductsForGuest()->count();
        }
	    $category_id = $request->category_id;
	    $info_key = $request->key;
	    if($category_id && $info_key){
            $products = Product::where('category_id', $category_id)->whereHas('informations',function ($q) use($info_key){
                $q->where('information_id' , $info_key);
            })->paginate(20);
        }elseif($category_id){
            $products = Product::getProductsByArrayCategory($category_id);
        }elseif($info_key){
            $products = Product::whereHas('informations', function ($q) use ($info_key) {
                $q->where('information_id', $info_key);
            })->paginate(20);
            $count = count($products);
        }
        $info = Information::all()->unique('key');
        $arr_product_id = $products->pluck('id');
        if($category->id == 1){
            $view_name = 'cate_master_with_product';
        }else{
            $view_name = 'list_product';
        }
        return view('products.guest.'. $view_name)->with([
            'item'              => $category,
            'category_children'     => $category_children,
            'arr_product_id'        => $arr_product_id,
            'secondCategories'      => $secondCategories,
            'products'              => $products,
            'count'                 => $count,
	        'info'		            => $info,
	        'info_key'	            => $info_key,
	        'category_id' 	    => $category_id,
            'categories'	    => $categories,
            'image'             => Category::find($id)->linkImage()
            ]);
    }
    
    public function getKeyProduct(Request $request,$id){
        $info = Information::all()->unique('key');
        // $infos = Information::findOrFails($id);
        $name = $info->name;
        if($request->name == $name){
            $product = $info->product->where('infomation_id',$id)->pagiante(20);
        }
        return view('products.guest.list_product')->with([
            'info' => $info
        ]);
    }

    public function show($id, $slug)
    {
        $has_session = SessionManager::validSession('product_seen');
        if($has_session){
            $arr_product_id = Session::get("product_seen");
            if(!in_array($id,$arr_product_id)){
                array_unshift($arr_product_id,$id);
            }
        }else{
            $arr_product_id = array($id);
        }
        Session::put("product_seen",$arr_product_id);
        $product = Product::findOrFail($id);
        $product->getAttributeLang('slug');
        $related_products = $product->getRelatedProduct();
        return view('products.guest.detail')->with(['item' => $product,'related_products' => $related_products]);
    }

}
