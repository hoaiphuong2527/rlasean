<?php

namespace App\Http\Controllers\Products\History;

use Illuminate\Http\Request;
use App\Models\Products\Product;
use App\Http\Controllers\Controller;
use App\Models\Products\ProductVariant;
use App\Models\Products\HistoryImportProduct;

class AdminController extends Controller
{
    public function index(Request $request)
    {
        $name = $request->name;
        $products = ProductVariant::paginate(20)->load(['product']);
        if($name){
            $products = ProductVariant::whereHas('product', function($q) use ($name){
                $q->where('name','LIKE', '%'.$name.'%');
            })->paginate(20);
        }
        return view('products.warehouse.index')->with([
            'name'  => $name,
            'products'  => $products
        ]);
    }

    public function update(Request $request,$id)
    {
        $item = HistoryImportProduct::findOrFail($id);
        $variant = $item->product_variant;
        $curent_import = $request->curent_import;
        $new_import = $request->new_import;
        if($new_import == null) $new_import = 0;
        $old_variant = $variant->returnOldAmount($curent_import);
        $old_variant->amount = $new_import + $old_variant->amount;
        $old_variant->save();
        $item->amount_import = $new_import; 
        $item->save();
        return redirect()->back();
    }
}
