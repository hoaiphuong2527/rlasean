<?php

namespace App\Http\Controllers\Cart;

use Cart;
use App\Models\Products\ProductVariant;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Products\Product;
use Illuminate\Support\Facades\Session;

class ApiController extends Controller
{
    public function addProduct(Request $request)
    {
        $id = $request->id;
        $variant = ProductVariant::findOrFail($id);
        $product = $variant->product;
        $code = $variant->checkTypeApply()['code'];
        $qty = $request->qty;
        $session_amount = 0;
        if($code){
            $pivot_code_item = $this->getPivotItemAccept($code,$variant,$variant->checkTypeApply()['type']);
            $amount_accept = $pivot_code_item->amount_ordered;
            $amount_default = $pivot_code_item->amount_default;
            if($amount_accept == null) $amount_accept = $amount_default;
            if($amount_default != null){
                $rows = Cart::search(function ($cartItem, $rowId) use($id) {
                    return $cartItem->id == (int)$id;
                });
                $item = $rows->last();
                if($item){
                    Cart::update($item->rowId, $item->qty + $qty);
                }else {
                    if( $amount_accept - $qty < 0 ){
                        Cart::add([
                            'id' => (int)$request->id,
                            'name' => $request->name,
                            'price' => $variant->getDiscountPriceUseIntoOrderBackEnd(),
                            'options'=>  ['size' => $request->size,
                                    'image' => $variant->linkImage(),
                                    'note' => 'no code',
                                    'session' => $amount_accept,
                                    'breakable' => $product->is_breakable,
                                    'weight'   => $variant->weight
                                ],
                            'qty' => $amount_accept ,
                        ]);
                        Cart::add( [
                            'id' => (int)$request->id,
                            'name' => $request->name,
                            'price' => $variant->price_format,
                            'options'=>  ['size' => $request->size,
                                    'image' => $variant->linkImage(),
                                    'session' => 0,
                                    'breakable' => $product->is_breakable,
                                    'weight'   => $variant->weight
                                ],
                            'qty' => $qty - $amount_accept ,
                        ]);
                    }else{
                        $cartInfo = [
                            'id' => (int)$request->id,
                            'name' => $request->name,
                            'price' => $variant->getDiscountPriceUseIntoOrderBackEnd(),
                            'options'=>  ['size' => $request->size,
                                    'image' => $variant->linkImage(),
                                    'session' => $qty,
                                    'breakable' => $product->is_breakable,
                                    'weight'   => $variant->weight
                                ],
                            'qty' => $qty ,
                        ];
                        Cart::add($cartInfo);
                    }                }
                if(Session::has("amount_". $code->id)){
                    $session_amount = Session::get("amount_". $code->id);
                    if($session_amount >= $amount_accept){
                        Session::put("amount_". $code->id,$amount_accept - $qty);
                    }else Session::put("amount_". $code->id,$session_amount - $qty);
                }else{
                    Session::put("amount_". $code->id,$amount_accept - $qty);
                }
            }else{
                $cartInfo = [
                    'id' => (int)$request->id,
                    'name' => $request->name,
                    'price' => $variant->getDiscountPriceUseIntoOrderBackEnd(),
                    'options'=>  ['size' => $request->size,
                            'image' => $variant->linkImage(),
                            'session' => $qty,
                            'breakable' => $product->is_breakable,
                            'weight'   => $variant->weight
                        ],
                    'qty' => $qty ,
                ];
                Cart::add($cartInfo);
            }
        }else {
            $price = $variant->price_format;
            $cartInfo = [
                'id' => (int)$request->id,
                'name' => $request->name,
                'price' => $variant->price,
                'options'=>  ['size' => $request->size,
                        'image' => $variant->linkImage(),
                        'session' => 0,
                        'breakable' => $product->is_breakable,
                        'weight'   => $variant->weight
                    ],
                'qty' => $qty,
            ];
            Cart::add($cartInfo);
        }
        $cart = Cart::content();
        $data = [
            'cart' => $cart,
            'total' => Cart::total(),
            'total_item' => Cart::count(),
            'session_amount' => $session_amount
        ];
        return response()->json($data);
    }

    public function increment(Request $request)
    {
        $rows = Cart::search(function ($cartItem, $rowId) use($request) {
            return $cartItem->id == (int)$request->product_id;
        });
        $pivot_code_item = $this->findPivotItemCode($request->product_id);
        $code = $pivot_code_item['code'];
        $variant = ProductVariant::find($request->product_id);
        $session_amount = 0;
        if($code){
            $amount_accept = $this->getPivotItemAccept($code,$variant,$pivot_code_item['type'])->amount_ordered;
            $amount_default = $this->getPivotItemAccept($code,$variant,$pivot_code_item['type'])->amount_default;    
            if($amount_default == null){
                $item = $rows->first();
                Cart::update($item->rowId, $item->qty + 1);    
            }else{
                $session_amount = Session::get("amount_".$code->id);
                if ($session_amount >= 1 ){
                    Session::put("amount_". $code->id,$session_amount - 1);
                    $item = $rows->first();
                    Cart::update($item->rowId, $item->qty + 1);
                }else{
                    if(count($rows) === 1 && $session_amount == 0){
                        if($code->amount == 0){
                            $item = $rows->last();
                            $new_qty = $item->qty + 1;
                            Cart::update($item->rowId, $new_qty);
                        }else{
                            $variant = ProductVariant::findOrFail($request->product_id);
                            $first_image =  $variant->galleries[0];
                            $cartInfo = [
                                'id' => (int)$request->product_id,
                                'name' => $variant->product->name,
                                'price' => $variant->price_format,
                                'options'=>  [
                                    'size' => $variant->color,
                                    'image' => $variant->linkImage(),
                                    'breakable' => $product->is_breakable,
                                    'weight'   => $variant->weight
                                ],
                                'qty' => 1,
                            ];
                            Cart::add($cartInfo);
                        }
                    }else{
                        $item = $rows->last();
                        $new_qty = $item->qty + 1;
                        Cart::update($item->rowId, $new_qty);
                    }  
                }
            }
        }else{
            $item = $rows->first();
            Cart::update($item->rowId, $item->qty + 1);
        }
        $cart = Cart::content();
        $data = [
            'cart' => $cart,
            'total' => Cart::total(),
            'total_item' => Cart::count(),
            'session_amount' => $session_amount
        ];
        return response()->json($data);
    }

    public function decrease(Request $request)
    {
        $rows = Cart::search(function ($cartItem, $rowId) use($request) {
            return $cartItem->id == (int)$request->product_id;
        });
        if(count($rows) > 1){
            $variant = ProductVariant::find($request->product_id);
            $item = $rows->last();
            $session_amount = 0;
            $pivot_code_item = $this->findPivotItemCode($request->product_id);
            $code = $pivot_code_item['code'];
            if($code && Session::has("amount_". $code->id)){
                $amount_accept = $this->getPivotItemAccept($code,$variant,$pivot_code_item['type'])->amount_ordered;
                $amount_default = $this->getPivotItemAccept($code,$variant,$pivot_code_item['type'])->amount_default;        
                $session_amount = Session::get("amount_".$code->id);
                Session::put("amount_". $code->id,$session_amount + 1);
                $session_amount = Session::get("amount_". $code->id);
                if($session_amount > $amount_accept){
                    Session::put("amount_". $code->id,$amount_accept); 
                }
            }
        }else $item = $rows->first();
        
        Cart::update($item->rowId, $item->qty - 1);
        $cart = Cart::content();
        $data = [
        'cart' => $cart,
        'total' => Cart::total(),
        'total_item' => Cart::count(),
        ];
        return response()->json($data);
    }

    public function remove(Request $request)
    {
        $rows = Cart::search(function ($cartItem, $rowId) use($request) {
            return $cartItem->id == (int)$request->product_id;
        });
        $item = $rows->last();
        $pivot_code_item = $this->findPivotItemCode($request->product_id);
        $code = $pivot_code_item['code'];
        $variant = ProductVariant::find($request->product_id);
        if($code && Session::has("amount_". $code->id)){
            $amount_accept = $this->getPivotItemAccept($code,$variant,$pivot_code_item['type'])->amount_ordered;
            $amount_default = $this->getPivotItemAccept($code,$variant,$pivot_code_item['type'])->amount_default;        
            if(count($rows) > 1)
                Session::put("amount_". $code->id,0);
            else Session::put("amount_". $code->id,$amount_accept);
        }
        Cart::remove( $item->rowId);
        $cart = Cart::content();
        $data = [
            'cart' => $cart,
            'total' => Cart::total(),
            'total_item' => Cart::count()
        ];
        return response()->json($data);
    }

    public function destroy(){
        foreach(Cart::content() as $key => $item){
            $pivot_code_item = $this->findPivotItemCode($request->product_id);
            $code = $pivot_code_item['code'];
            if($code){
                Session::forget('amount_'.($code)->id);
            }
            // $this->backupAmountCode($item->id,$item->qty);
        }
        Cart::destroy();
        return response()->json(['status' => 'deleted']);
    }

    public function getCart()
    {
        $cart = Cart::content();
        foreach($cart as $key => $item){
            $variant = ProductVariant::findOrFail($item->id);
            $product = $variant->product;
            $code = $variant->checkTypeApply()['code'];
            if($code){
                $pivot_code_item = $this->findPivotItemCode($item->id);
                $amount_accept = $this->getPivotItemAccept($code,$variant,$pivot_code_item['type'])->amount_ordered;
                $amount_default = $this->getPivotItemAccept($code,$variant,$pivot_code_item['type'])->amount_default;            
                if($amount_accept >= $item->qty){
                    $pivot_code_item = $this->getPivotItemAccept($code,$variant,$variant->checkTypeApply()['type']);
                    $session_amount = Session::get("amount_".$code->id);
                    if($session_amount >= $item->qty )
                        $price = $variant->getDiscountPriceUseIntoOrderBackEnd();
                    else $price = $item->price;
                }else{
                    $price = $variant->getDiscountPriceUseIntoOrderBackEnd();
                }
            }else{
                $price = $variant->price_format;
            }
            Cart::update($item->rowId, ['price' => $price]);
        }
        $data = [
            'cart' => $cart,
            'total' => Cart::total(),
            'total_item' => Cart::count(),
        ];
        return response()->json($data);
    }

    public function updateAmount(Request $request)
    {
        $rows = Cart::search(function ($cartItem, $rowId) use($request) {
            return $cartItem->id == (int)$request->product_id;
        });
        $item = $rows->first();
        $variant = ProductVariant::findOrFail($request->product_id);
        $code = $variant->checkTypeApply()['code'];
        if($code){
            if( $request->amount <= ($amount_accept || Session::get("amount_". $code->id))){
                Session::put("amount_". $code->id,0);
                Cart::update($item->rowId, $request->amount);
            }else{
                $new_qty = $request->amount - $amount_accept;
                Cart::update($item->rowId, $amount_accept);
                $cartInfo = [
                    'id' => (int)$request->product_id,
                    'name' => $variant->product->name,
                    'price' => $variant->price_format,
                    'options'=>  [
                        'size' => $variant->color,
                        'image' => $variant->linkImage(),
                        'weight'   => $variant->weight
                    ],
                    'qty' => $new_qty,
                ];
                Cart::add($cartInfo);
            }
        }else{
            Cart::update($item->rowId, $request->amount);
        }
        $cart = Cart::content();
        $data = [
            'cart' => $cart,
            'total' => Cart::total(),
            'total_item' => Cart::count()
        ];
        return response()->json($data);
    }

    // public function backupAmountCode($id,$qty)
    // {
    //     $code = $this->findPivotItemCode($id);
    //     if($code){
    //         $amount = null;
    //         if (Session::has("amount")) {
    //             $amount = Session::get("amount");
    //             $code->amount = $amount ;
    //             $code->save();
    //         }
    //         // $code->amount += $qty;
    //         //     $code->save();
    //     }
    // }

    public function findPivotItemCode($id)
    {
        $variant = ProductVariant::findOrFail($id);
        $pivot_code_item = $variant->checkTypeApply();
        return $pivot_code_item;
    }

    public function getPivotItemAccept($code,$variant,$type_apply)
    {
        if($type_apply == 'product'){
            $item = $code->firstRecordByObjectId($variant->product->id);
        }else{
            $item = $code->firstRecordByObjectId($variant->id);
        }
        return $item;
    }

}
