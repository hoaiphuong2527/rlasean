<?php

namespace App\Http\Controllers\Review;

use App\Models\Review;
use Illuminate\Http\Request;
use App\Helpers\UpdateSomething;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    use UpdateSomething;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {        
        $status = $request->status;
        $review_type = $request->review_type;
        $search_query = Review::query();
        $search_query->whereType(config('ecommerce.review_types.review'));
        if($status)
        {
            if($status === "all")
                $search_query->get();
            if($status === 'skip')
                $search_query->whereStatus(0);
            if($status === 'public')
                $search_query->whereStatus(1);
        }
        if($review_type)
        {
            if($review_type === "all")
                $search_query->get();
            if($review_type === config('ecommerce.context_type.product'))
                $search_query->whereReviewtableType(config('ecommerce.reviewtable_type.product'));
            if($review_type === config('ecommerce.context_type.post'))
                $search_query->whereReviewtableType(config('ecommerce.reviewtable_type.post'));
        }
        
        $reviews = $search_query->paginate(20);
        return view('reviews.admin.index')->with([
            'reviews'       => $reviews,
            'status'        => $status,
            'review_type'   => $review_type
        ]);
    }

    public function updateStatusByAjax($id)
    {
        $item = Review::findOrFail($id);
        $this->updateStatus($item);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $review = Review::findOrFail($id);
        $review->delete();
        return redirect()->back()->with(['status' => 'Cập nhật dữ liệu thành công']);
    }
}
