<?php

namespace App\Http\Controllers\Review;

use Auth;
use Carbon\Carbon;
use App\Models\Post;
use App\Models\User;
use App\Models\Review;
use Illuminate\Http\Request;
use App\Models\Products\Product;
use App\Http\Controllers\Controller;
use App\Events\Admin\NewReviewProduct;
use App\Events\User\CreateNewUserNatural;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;


class UserController extends Controller
{
    public function review($id,$type,Request $request)
    {
        $product = Product::findOrFail($id);
        $pass = $this->randomPassword(6);
        $user = $this->checkUser($request,$pass);
        $review = Review::create([
            'content'           => $request->content,
            'reviewtable_type'  => $request->reviewtable_type,
            'reviewtable_id'    => $id,
            'parent_id'         => $request->parent_id,
            'level'             => $request->level,
            'type'              => $type,
            'user_id'           => $user->id
        ]);
        if($review->type == config('ecommerce.review_types.comment')){
            $review->status = 1;
            $review->save();
            $content_mail = "review";
        }else {
            $content_mail = "bình luận";
        }
        if(!Auth::check())  Auth::login($user);
        if($user->is_natural){
            event(new CreateNewUserNatural($user,$pass));
        }
        event(new NewReviewProduct($review,$content_mail));
        return redirect()->back()->with(['status' => 'Thành công!']);
    }

    public function randomPassword($length)
    {
        $str = "";
        $characters = array_merge(range('A','Z'), range('a','z'), range('0','9'));
        $max = count($characters) - 1;
        for ($i = 0; $i < $length; $i++) {
            $rand = mt_rand(0, $max);
            $str .= $characters[$rand];
        }
        return $str;       
    }

    public function checkUser($request,$str_pass)
    {
        if($request->email == null){
            return null;
        }else{
            $user = User::firstOrCreate([
                'email' => $request->email
            ], [
                'email_verified_at'    => Carbon::now(),
                'name'                  =>  $request->review_name,
                'email'                 =>  $request->email,
                'password'              =>  Hash::make($request->password),
                'phone'                 =>  $request->phone,
                'flag'                  =>  1,
                'middleware_token'      =>  md5(Carbon::now() . rand(0000,9999)),
                'is_admin'              =>  0,
                'is_natural'            =>  1
            ]);
        }
        return $user;
    }
}
