<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Media;


class MediaController extends Controller
{
    public function index(Request $request) {
        $upload_dir = base_path( config('ecommerce.media.upload_dir') ) ;

        return view('media.admin.index');
    }
    public function getAll(Request $request) {
        $data = Media::latest()->get()->toArray();
        return response()->json($data, 200);
    }

    public function update($id, Request $request)
    {
        $item = Media::find($id);
        if($item){
            $item->alt = $request->alt;
            $item->description = $request->description;
            $item->save();
            $data = [
                'result' => 1,
                'item'  => $item
            ];
        }else{
            $data = [
                'result' => 0
            ];
        }
        return response()->json($data, 200);
    }

    public function destroy($id){
        $item = Media::find($id);
        if($item){
            $item->delete();
            $data = [
                'result' => 1
            ];
        }else{
            $data = [
                'result' => 0
            ];
        }
        return response()->json($data, 200);
    }
}
