<?php

namespace App\Http\Controllers\Admin;

use App\Models\Media;
use App\Models\Banner;
use Illuminate\Http\Request;
use App\Helpers\UploadImage;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class BannerController extends Controller
{

    public function index()
    {
       $banners = Banner::all();
       return view('banners.admin.index',['banners' => $banners]);
    }

    public function update(Request $request)
    {
        $banners = Banner::all();
        foreach($banners as $banner){
            if($request->get($banner->key)){
                $banner->image = Media::find($request->get($banner->key))->link;
                $banner->save();    
            }
        }
        return redirect()->back()->with(['status' => 'Cập nhật thông tin thành công!']); 
    }
}
