<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Validator;
use Hash;
use Carbon\Carbon;
use App\Models\AdminReceiveMail;
use App\Events\User\SendMailActiveEvent;

class UserController extends Controller
{
    function index(Request $request){
        $query = $request->query('search');
        $users = User::all();
        if(isset($_GET['search'])) 
            $users = User::search($query)->paginate( $request->get('show', 10) );
        if(!isset($_GET['filterRole'])) $filterRole = 'All';
        else $filterRole = $_GET['filterRole'];
        switch($filterRole){
            case 'All';
                $users;
                break;
            default:
                $users = User::whereIsAdmin($filterRole)->paginate( $request->get('show', 10) );
        }
        if(!isset($_GET['filterFlag'])) $filterFlag = config('ecommerce.user_flag.all');
        else $filterFlag = $_GET['filterFlag'];
        switch($filterFlag){
            case config('ecommerce.user_flag.all');
                $users;
                break;
            default:
                $users = User::whereFlag($filterFlag)->paginate( $request->get('show', 10) );
        }
        if(!isset($_GET['filterRoleAdmin'])) $filterRoleAdmin = "All";
        else $filterRoleAdmin = $_GET['filterRoleAdmin'];
        switch($filterRoleAdmin){
            case "All";
                $users;
                break;
            default:
                $users = User::whereRole($filterRoleAdmin)->paginate( $request->get('show', 10) );
        }
        return view('admin.user.index')->with([
            'users' => $users,
            'search' => $query, 
            'filterRole' => $filterRole,
            'filterFlag' => $filterFlag
        ]);
    }
    function create(){
        return view('admin.user.create');
    }
    function store(Request $request){
        $validator =
        Validator::make(
            $request->all(),
            [
                'name'      =>'required',
                'email'     =>'required|email|unique:users,email',
                'password'  =>'required|min:6|confirmed',
            ],
            [
                'name.required'         => 'Vui lòng nhập tên cho người dùng.',
                'email.required'        => 'Vui lòng nhập email của người dùng.',
                'email.email'           => 'Email vừa nhập không hợp lệ.',
                'email.unique'          => 'Email đã tồn tại.',
                'password'              => 'Vui lòng nhập mật khẩu.',
                'password.confirmed'    => 'Nhập lại mật khẩu không đúng',
                'password.min'          => 'Mật khẩu có ít nhất 6 ký tự',
                'phone.required'        => 'Vui lòng nhập số điện thoại cho người dùng',
            ]
        );
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }else{
            $user = User::create([
                'name'                  =>  $request->name,
                'email'                 =>  $request->email,
                'password'              =>  Hash::make($request->password),
                'phone'                 =>  $request->phone,
                'flag'                  =>  1,
                'middleware_token'      =>  md5(Carbon::now() . rand(0000,9999)),
                'is_admin'              =>  $request->role,
            ]);
            if($user->is_admin){
                $user->email_verified_at = Carbon::now();
                $user->role = $request->role_admin;
                $user->save();
            }else {event(new SendMailActiveEvent($user));}
            return redirect()->route('admin_user.index');
        }
    }
    function edit($id){
        $user = User::findOrFail($id);
        return view('admin.user.edit',compact('user'));
    }
    function update(Request $request , $id){
        $user = User::findOrFail($id);

        $validator =
        Validator::make($request->all(),
            [
                'name'              => 'required',
                // 'phone' => 'required|min:10|max:12'
            ],
            [
                'name.required'     => 'Vui lòng nhập tên cho thành viên',
                // 'phone.required' => 'Vui lòng nhập số điện thoại cho người dùng',
                // 'phone.min' => 'Số điện thoại ít nhất 10 số',
                // 'phone.max' => 'Số điện thoại tối đa là 10 số',
            ]
        );
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }else{
            $user->name = $request->name;
            $user->is_admin = $request->role;
            if($user->is_admin){
                $user->role = $request->role_admin;
            }else{
                $user->role = "";
            }
            $user->save();
            $admin_receive_mail = AdminReceiveMail::whereUserId($id)->first();
            if($admin_receive_mail && !$user->is_admin)
            {
                $admin_receive_mail->delete();
            }
            return redirect()->back()->with('success','Lưu dữ liệu thành công');
        }
    }
    function destroy($id){
        $user = User::findOrFail($id);
        $user->flag = 0;
        $user->save();
        return redirect()->back();
    }

    public function updateFlag($id)
    {
        $user = User::findOrFail($id);
        $user->flag = 1;
        $user->save();
        return redirect()->back();
    }
}
