<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use App\Http\Controllers\Controller;

class LangController extends Controller
{
    public function index($file_name,$locale)
    {
        $filePath = base_path('resources/lang/'.$locale.'/'.$file_name.'.php');
        $file = file_get_contents($filePath);
        $locales = [
            'vi'    => 'Tiếng Việt',
            'en'    => 'Tiếng Anh'
        ];
        if(File::exists($filePath)){
            return view('admin.lang')->with(['file'=> $file,'locale' => $locale,'locales' => $locales,'file_name' => $file_name]);    
        }
        return abort(404);
    }

    public function update(Request $request,$file_name,$locale)
    {
        $filePath = base_path('resources/lang/'. $locale .'/'.$file_name.'.php');
        file_put_contents($filePath, $request->lang_file);
        return redirect()->back()->with(['success' => 'Cập nhật dữ liệu thành công!']);
    }
}
