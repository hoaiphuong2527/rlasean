<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\UploadImage;
use App\Helpers\UpdateSomething;
use App\Models\Option;
use App\Models\AdminReceiveMail;
use App\Models\User;
use Illuminate\Support\Facades\Input;
use Validator;
use App\Models\Media;
use App\Helpers\CreateTranslation;
use App\Models\Translations\GeneralTranslation;

class OptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $option = Option::firstOrFail();
        $admins = AdminReceiveMail::all();
        $user_is_admins = User::getAdmins();
        // $user = User::orderBy('id', 'desc')->get();

        return view('admin.to_option.index', ['option' => $option, 'admins' => $admins, 'user_is_admins' => $user_is_admins]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = User::find($request->input('admins'));
        AdminReceiveMail::create([
            'user_id' => $user->id
        ]);
        return redirect()->back();
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $option = Option::firstOrFail();
        $validator =
            Validator::make(
            $request->all(),
            [
                'name' => 'required',
                'address' => 'required',
                'email' => 'required|email',
                'phone' => 'required|min:10|max:14',
                'iframe_gg' => 'required',
                'facebook_acc' => 'sometimes|max:255',
                'intagram_acc' => 'sometimes|max:255',
                'youtube_acc' => 'sometimes|max:255',
                'sologan'     => 'sometimes|max:255'
                // 'type_order' => 'required'
            ],
            [
                'name.required' => 'Vui lòng nhập tên .',
                'address.required' => 'Vui lòng nhập địa chỉ',
                'email.required' => 'Vui lòng nhập email của người dùng.',
                'email.email' => 'Email vừa nhập không hợp lệ.',
                'phone.required' => 'Vui lòng nhập số điện thoại cho người dùng',
                'phone.min' => 'Số điện thoại ít nhất 10 số',
                'phone.max' => 'Số điện thoại tối đa là 14 số',
                'iframe_gg.required' => 'Vui lòng nhấp nút chọn vị trí gg để lấy vị trí công ty',
                'facebook_acc.max' => 'Độ dài vượt quá mức cho phép 255 ký tự',
                'intagram_acc.max' => 'Độ dài vượt quá mức cho phép 255 ký tự',
                'youtube_acc.max' => 'Độ dài vượt quá mức cho phép 255 ký tự',
                // 'type_order.max' => 'Vui lòng nhập loại thanh toán'
            ]
        );
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        } else {

            $option->name = $request->name;
            if($request->main_logo)
                $option->main_logo =  Media::find($request->main_logo)->link;
            if($request->footer_logo)
            $option->footer_logo = Media::find($request->footer_logo)->link;
            if($request->favicon)
                $option->favicon = Media::find($request->favicon)->link;
            $option->address = $request->address;
            $option->email = $request->email;
            $option->phone = $request->phone;
            $option->iframe_gg = $request->iframe_gg;
            $option->facebook_acc = $request->facebook_acc;
            $option->intagram_acc = $request->intagram_acc;
            $option->youtube_acc = $request->youtube_acc;
            $option->sologan = $request->sologan;
            $option->percent_breakable = $request->percent_breakable;
            $option->ghtk_token = $request->ghtk_token;
            $option->ghn_token = $request->ghn_token;
            // $options->type_order = $request->type_order;
            $option->save();
            foreach(Option::$translate_column as $col){
                CreateTranslation::updatePolymorphism($col,$request,$option,Option::$namespace);
            }
            return redirect()->route('admin_option.index')->with(['status' => 'Cập nhật thông tin thành công!']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $admins = AdminReceiveMail::find($id);
        $admins->delete();
        return redirect()->route('admin_option.index');
    }

    public function statusPayment()
    {
        $item = Option::first();
        if($item->type_order == 'NP'){
            $item->type_order = 'HP';
        }else{
            $item->type_order = 'NP';
        }
        $item->save();
        return response()->json(200);
    }

    public function saveDistrictID($id,Request $request)
    {
        $option = Option::first();
        $option->district_id = $id;
        $option->district_stock = $request->district_stock;
        $option->province_stock = $request->province_stock;
        $option->address_stock = $request->address_stock;
        $option->save();
        return response()->json(200);
    }
}
