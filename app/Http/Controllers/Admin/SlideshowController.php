<?php

namespace App\Http\Controllers\Admin;

use DB;
use App\Models\Media;
use App\Models\Slideshow;
use App\Helpers\UploadImage;
use Illuminate\Http\Request;
use App\Helpers\UpdateSomething;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class SlideshowController extends Controller
{
    use UpdateSomething;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $slideshows = Slideshow::orderBy('sort_feature')->get();
        return view('slideshow.admin.index',['slideshows' => $slideshows]);

    }

    public function create()
    {
        return view('slideshow.admin.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $slideshow = Slideshow::create();
        $slideshow->url = $request->input('url');
        if($request->image)
            $slideshow->image = Media::find($request->image)->link;
        $slideshow->save();
        return redirect()->route('admin_slideshow.index')->with(['status' => 'Lưu dữ liệu thành công!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slideshow = Slideshow::findOrFail($id);
        return view('slideshow.admin.edit',['item' => $slideshow]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $slideshow = Slideshow::findOrFail($id);
        $validator = Validator::make(
            $request->all(),
            [
                'url'      => 'sometimes|max:191'
            ],[
                'url.max'     => 'Vui lòng điền đường dẫn'
            ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $slideshow->url = $request->input('url');
            if($request->image)
                $slideshow->image = Media::find($request->image)->link;
            $slideshow->save();
            return redirect()->back()->with(['status' => 'Dữ liệu đã được cập nhật']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slideshow = Slideshow::findOrFail($id);
        $slideshow->delete();
        return redirect()->route('admin_slideshow.index')->with(['destroy' => 'Dữ liệu đã được xóa']);
    }

    public function updateStatusByAjax($id)
    {
        $item = Slideshow::findOrFail($id);
        $this->updateStatus($item);
    }

    public function sorting(Request $request)
    {
        $arr_id = $request->array_id;
        $arr_pos = $request->arr_pos;
        $ids_ordered = implode(',', $arr_id);
        $slideshows = Slideshow::whereIn('id', $arr_id)
         ->orderByRaw(DB::raw("FIELD(id, $ids_ordered)"))
         ->get();
        foreach($slideshows as $key => $item){
            $item->sort_feature = $arr_pos[$key];
            $item->save();
        }
        $data = [
            'result' => 1,
        ];
        return response()->json($data, 200);
    }
}
