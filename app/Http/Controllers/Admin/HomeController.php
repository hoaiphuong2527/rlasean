<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\Order;
use App\Charts\OrderChart;
use Illuminate\Http\Request;
use App\Models\Products\Product;
use App\Http\Controllers\Controller;
use App\Models\Products\ProductVariant;
use App\Models\Categories\ProductCategory;

class HomeController extends Controller
{
    public function index()
    {
        $chart = new OrderChart();
        $chart->labels(['Tháng 1', 'Tháng 2','Tháng 3','Tháng 4','Tháng 5','Tháng 6','Tháng 7','Tháng 8','Tháng 9','Tháng 10','Tháng 11','Tháng 12']);
        $chart->dataset('Doanh thu theo tháng', 'bar', Order::valueChart())->backgroundColor('#ea5126');
        $users = User::count();
        $total_products = ProductVariant::count();
        $waiting_order = Order::whereIn('order_status',[config('ecommerce.order_statuses.waiting'),config('ecommerce.order_statuses.processing')])->count();
        $total_orders = Order::where('order_status','!=',config('ecommerce.order_statuses.draff'))->count();
        $total_order_new = Order::where('order_status',config('ecommerce.order_statuses.new'))->count();
        $latest_orders = Order::latest()->take(5)->get();
        return view('admin.home')->with([
            'users' => $users,
            'total_products' => $total_products,
            'waiting_order' => $waiting_order,
            'total_orders'  => $total_orders,
            'latest_orders' => $latest_orders,
            'total_order_new' => $total_order_new,
            'chart'         => $chart
        ]);
    }

    public function search(Request $request)
    {
        $categories = ProductCategory::getCategoryNotMaster();
        $products = null;
        $search_name = $request->product_name;
        if($search_name){
            $products = Product::search($search_name)->whereNotNull('category_id')->paginate( $request->get('show', 20))->load(['category']);
        }
        return view('products.product.admin.index')->with([
            'search_name'       => $search_name,
            'name'              => $request->name,
            'category_slug'     => $request->category_slug,
            'products'          => $products,
            'categories'        => $categories,
        ]);
    }

    public function getValueChart()
    {
        $data = Order::valueChart();
        return response()->json($data, 200);
    }
}
