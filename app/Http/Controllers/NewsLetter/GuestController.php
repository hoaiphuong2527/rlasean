<?php

namespace App\Http\Controllers\NewsLetter;

use App\Models\NewsLetter;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Events\RegisterNewsLetterEvent;
use Illuminate\Support\Facades\Validator;

class GuestController extends Controller
{
    public function store(Request $request)
    {
        $rule = [
            'news_email' => 'required|email|unique:news_letters,email|max:255',
        ];
        $validator = Validator::make(Input::all(), $rule);
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $news_email = $request->news_email;
            NewsLetter::create([
                'email' => $news_email
            ]);
            event( new RegisterNewsLetterEvent($news_email));
            return redirect()->back()->with(['status' => 'Dữ liệu đã được tiếp nhận và xử lý!']);
        }
    }
}
