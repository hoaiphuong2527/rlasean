<?php

namespace App\Http\Controllers\NewsLetter;

use App\Models\NewsLetter;
use Illuminate\Http\Request;
use App\Exports\NewsLetterExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->export){
            return Excel::download(new NewsLetterExport, 'newsletter.xlsx');
        }
        $email = $request->email;
        if($email){
            $news = NewsLetter::search($email)->paginate( $request->get('show', 20));
        }
        $news = NewsLetter::paginate(20);
        return view('newsletter.admin.index')->with([
            'news' => $news,
            'email' => $email
            ]);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\NewsLetter  $newsLetter
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = NewsLetter::findOrFail($id);
        $item->delete();
        return redirect()->back()->with(['status' => 'Xóa dữ liệu thành công!']);
    }
}
