<?php

namespace App\Http\Controllers\DynamicMenu;

use DB;
use App\Models\DynamicMenu;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApiController extends Controller
{
    public function sorting(Request $request)
    {
        $arr_id = $request->array_id;
        $arr_pos = $request->arr_pos;
        $ids_ordered = implode(',', $arr_id);
        $menu = DynamicMenu::whereIn('id', $arr_id)
         ->orderByRaw(DB::raw("FIELD(id, $ids_ordered)"))
         ->get();
        foreach($menu as $key => $item){
            $item->sort = $arr_pos[$key];
            $item->save();
        }
        $data = [
            'result' => 1,
        ];
        return response()->json($data, 200);
    }
}
