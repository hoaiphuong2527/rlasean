<?php

namespace App\Http\Controllers\DynamicMenu;

use App\Models\DynamicMenu;
use Illuminate\Http\Request;
use App\Helpers\CreateTranslation;
use App\Http\Controllers\Controller;
use App\Models\Translations\MenuTranslation;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = DynamicMenu::whereParent(0)->orderBy('sort')->get();
        return view('menu.admin.index',[
            'list'  => $list
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $item = DynamicMenu::create([
            'url'               => $request->url,
            'display_name'      => $request->display_name,
            'parent'            => $request->parent,
            'sort'              => DynamicMenu::count() + 1
        ]);
        foreach(DynamicMenu::$translate_column as $col){
            CreateTranslation::createRecord(MenuTranslation::$namespace,$item->id,MenuTranslation::$foreign_key,$col,$request);
        }
        $list = DynamicMenu::all();
        return redirect()->back()->with(['status' => 'Cập nhật thông tin thành công!']);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = DynamicMenu::findOrFail($id);
        $item->url          = $request->url;
        $item->display_name = $request->display_name;
        $item->save();
        foreach(DynamicMenu::$translate_column as $col){
            CreateTranslation::saveLang($col,$request,$item,MenuTranslation::$namespace,MenuTranslation::$foreign_key);
        }
        return redirect()->back()->with(['status' => 'Cập nhật thông tin thành công!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = DynamicMenu::findOrFail($id);
        $item->deleteChilden();
        return redirect()->back()->with(['status' => 'Xóa dữ liệu thành công!']);
    }
}
