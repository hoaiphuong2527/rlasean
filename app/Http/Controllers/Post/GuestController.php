<?php

namespace App\Http\Controllers\Post;

use App\Models\Option;
use App\Models\Post;
use App\Models\Banner;
use App\Models\Categories\PostCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GuestController extends Controller
{
    public function index(Request $request)
    {
        $category = PostCategory::first();
        if($category){
            $child = PostCategory::firstCateChild();
            $latest_post = $child->posts()->whereStatus(1)->latest()->with(['user'])->first();
            $latest_posts = $child->posts()->whereStatus(1)->where('id','!=',$latest_post->id)->latest()->with(['user'])->take(4)->get();
            $categories = PostCategory::categoryHasPostOnline();
            $collection = collect([$latest_post->id]);
            $merged = $collection->merge($child->posts()->whereStatus(1)->where('id','!=',$latest_post->id)->take(4)->pluck('id'));    
            $related_posts = Post::whereNotIn('id',$merged)->whereCategoryId($child->id)->whereStatus(1)->latest()->with(['user'])->paginate(6);
            return view('post.guest.index')->with([
                'item'          => $category,
                'categories'    => $categories,
                'latest_post'   => $latest_post,
                'latest_posts'  => $latest_posts,
                'related_posts' => $related_posts,
                'option'        => Option::first(),
                'is_first'      => 1
            ]);
        }else return abort(404);
        
    }


    public function show($id)
    {
        $item = Post::findOrFail($id);
        $item->read += 1;
        $item->save();
        $related_post = Post::whereCategoryId($item->category_id)->where('id','!=',$item->id)->whereStatus(1)->take(5)->get();
        $latest_post = Post::whereCategoryId($item->category_id)->whereStatus(1)->where('id','!=',$item->id)->latest()->first();
        return view('post.guest.single')->with([
            'item'  => $item,
            'related_post' => $related_post,
            'latest_post'   => $latest_post
        ]);
    }

    public function category(Request $request,$id,$slug)
    {
        $category = PostCategory::findOrFail($id);
        $latest_post = $category->posts()->whereStatus(1)->latest()->with(['user'])->first();
        if($latest_post){
            $latest_posts = $category->posts()->whereStatus(1)->where('id','!=',$latest_post->id)->latest()->with(['user'])->take(4)->get();
            $collection = collect([$latest_post->id]);
            $merged = $collection->merge($category->posts()->whereStatus(1)->where('id','!=',$latest_post->id)->take(4)->pluck('id'));
            $categories = PostCategory::categoryHasPostOnline();
            $related_posts = Post::whereNotIn('id',$merged)->whereCategoryId($id)->whereStatus(1)->latest()->with(['user'])->paginate(6);
            return view('post.guest.category')->with([
                'item'          => $category,
                'categories'    => $categories,
                'latest_post'   => $latest_post,
                'latest_posts'  => $latest_posts,
                'related_posts' => $related_posts,
                'option'        => Option::first()
            ]);
        }else return abort(404);
        
    }
}
