<?php

namespace App\Http\Controllers\Post;

use Auth;
use Validator;
use App\Models\Post;
use App\Models\User;
use App\Models\Media;
use Illuminate\Http\Request;
use App\Helpers\UploadImage;
use App\Helpers\UpdateSomething;
use App\Helpers\CreateTranslation;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Models\Categories\PostCategory;
use App\Models\Translations\PostTranslation;

class AdminController extends Controller
{
    use UpdateSomething;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->search;
        $category_id = $request->category_id;
        $post = Post::getPosts();
        $categories = PostCategory::all();
        if($search){
            $post = Post::whereHas('post_category')->search($search)->with(['post_category'])->paginate(20);
        }
        if($category_id){
            $post = Post::whereCategoryId($category_id)->with(['post_category'])->paginate(20);
        }
        return view('post.admin.index',compact('post','search','categories','category_id'));
    }




    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post = Post::create([
            'user_id' => (Auth::user()->id),
            'category_id' => 2,
            'status' => 0
        ]);
        //translate
        foreach(Post::$translate_column as $col){
            CreateTranslation::createRecord(PostTranslation::$namespace,$post->id,PostTranslation::$foreign_key,$col,$request);
        }      
        //end translate  
        return redirect($post->urlAdminEdit());

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::findOrFail($id);
        $categories = PostCategory::whereParentId(2)->get();
        return view('post.admin.edit')->with([
            'item' => $post,
            'categories' => $categories,
            'url'        => 'guest_posts.show.'.app()->getLocale()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Post::findOrFail($id);
        $categories = PostCategory::getCategoryNotMaster();
        $validator =
            Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'required',
        ], [
            'name.required' => 'Vui lòng nhập tiêu đề',
            'description.required' => 'Vui lòng nhập nội dung bài viết',

        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $post->name = $request->name;
            if($post->slug == null)
                $post->slug = str_slug($request->name);
            else
                $post->slug = str_slug($request->slug);
            if($request->image)
                $post->image = Media::find($request->image)->link;            
            $post->description = $request->description;
            if($request->update_lang) {
                $post->save();
                return redirect()->route('admin_post.edit_lang',$post->id);
            }
            if ($request->publish) {
                $post->status = 1;
            }
            if ($request->draff) {
                $post->status = 0;
            }elseif($request->continue){
                $post->status = 1;
                $post->save();
                $new = Post::create([
                    'user_id' => (Auth::user()->id),
                    'category_id' => 2,
                    'status' => 0
                ]);
                return redirect($new->urlAdminEdit());
            }
            if($request->category_id == null)
                $category_id = 2;
            else $category_id =  $request->category_id;
            $post->category_id = $category_id;
            $post->save();
            return redirect()->back()->with([
                'success' => 'Cập nhật dữ liệu thành công!',
                'item' => $post,
                'categories' => $categories
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Post::findOrFail($id);
        $categories = PostCategory::getCategoryNotMaster();
        $item->delete();
        return redirect()->back()->with([
            'destroy' => 'Xóa dữ liệu thành công!',
            'categories' => $categories
        ]);

    }

    public function updateStatusPost($id)
    {
        $item = Post::findOrFail($id);
        $this->updateStatus($item);
    }

    public function clone($slug)
    {
        $post = Post::whereSlug($slug)->firstOrFail();
        $new_post = $post->clone();
        return redirect($new_post->urlAdminEdit());
    }

    public function editTextLang($id){
        $item = Post::findOrFail($id);
        return view('post.admin.lang')->with(['item'=>$item,
            'url' => 'guest_posts.show.' . app()->getLocale()

        ]);
    }
    public  function updateLang($id,Request $request){
        // translate
        $item = Post::findOrFail($id);
        foreach(Post::$translate_column as $col){
            CreateTranslation::saveLang($col,$request,$item,PostTranslation::$namespace,PostTranslation::$foreign_key);
        }
        $item->save();
        return redirect()->back()->with(['status' => 'Cập nhật dữ liệu thành công!']);
    }
}
