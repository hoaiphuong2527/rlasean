<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AdminAuthenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        if(empty($user) || $user->is_admin == 0){
            if ($request->ajax()) {
                return response()->json(['error' => 'not authenticated']);
            } else {
                return redirect()->back();
            }
        }
        return $next($request);
    }
}
