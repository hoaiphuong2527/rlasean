<?php

namespace App\Http\Middleware;

use Closure;
use App\Helpers\SessionManager;
use Cookie;
class Locale
{
    public function handle($request, Closure $next)
    {
        if(!Cookie::get('lang'))
            app()->setLocale(config('app.locale')); 
        else  app()->setLocale(Cookie::get('lang')); 
        return $next($request);
    }
}