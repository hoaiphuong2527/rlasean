<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class GuestSendWarrantyEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $warranty;
    public function __construct($warranty)
    {
        $this->warranty = $warranty;
    }
}
