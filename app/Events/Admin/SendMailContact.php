<?php

namespace App\Events\Admin;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class SendMailContact
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $contact;
    public $content;
    public function __construct($contact,$content)
    {
        $this->contact = $contact;
        $this->content = $content;
    }
}
